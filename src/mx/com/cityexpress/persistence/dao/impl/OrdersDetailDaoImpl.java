package mx.com.cityexpress.persistence.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.persistence.dao.IOrdersDetailDao;

public class OrdersDetailDaoImpl extends DAOBase implements IOrdersDetailDao {
	
	private Log log = LogFactory.getLog(OrdersDetailDaoImpl.class);

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> selectOrderDetail(Long idOrder)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectOrderDetail --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select P.idProduct, P.name, OD.quantity, P.unidad, ");
	        qry.append("        P.price, OD.comments ");
	        qry.append(" From OrdersDetail OD, Product P ");
	        qry.append(" Where OD.product.idProduct = P.idProduct ");
	        qry.append(" and   OD.orders.idOrder = " + idOrder);
	        qry.append(" order by P.idProduct ");
	        
	        log.info(":::...[DAO] (selectOrderDetail) query: " + qry.toString());
	        final List detPedidoList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectOrderDetail OK");
	        return detPedidoList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectOrderDetail : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}

}
