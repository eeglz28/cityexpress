/**
 * 
 */
package mx.com.cityexpress.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.mapping.pojo.Users;
import mx.com.cityexpress.persistence.dao.IUsersDao;

/**
 * @author franktor
 *
 */
public class UsersDaoImpl extends DAOBase implements IUsersDao {
	
	private Log log = LogFactory.getLog(UsersDaoImpl.class);

	/* (non-Javadoc)
	 * @see mx.com.cityexpress.persistence.dao.IUsersDao#getActiveUser(java.lang.String, java.lang.String)
	 */
	public Users getActiveUser(String user, String pass)
			throws DatabaseException {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see mx.com.cityexpress.persistence.dao.IUsersDao#getActiveUsers(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<Users> getActiveUsers(String user) throws DatabaseException {
		
		log.info(":::...[DAO]: begin getActiveUsers OK");
		
		List<Users> listaUsers = new ArrayList<Users>();
        StringBuffer qry = new StringBuffer();
        try{
            qry.append(" Select US ");
            qry.append(" From Users US ");
            qry.append(" Where US.nick = '");
            qry.append( user );
            qry.append("' " );
            listaUsers = getHibernateTemplate().find(qry.toString());
            
            log.info(":::...[DAO]: end getActiveUsers OK");
            return listaUsers;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getActiveUsers : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Users> selectUsersByClient(Long idClient) 
			throws DatabaseException {
		
		log.info(":::...[DAO]: end selectUsersByClient OK");
		
		List<Users> listaUsers = new ArrayList<Users>();
        StringBuffer qry = new StringBuffer();
        try{
            qry.append(" Select US ");
            qry.append(" From Users US ");
            qry.append(" Where US.client.idClient = ");
            qry.append( idClient );
            qry.append(" order by US.nick");
            
            
            listaUsers = getHibernateTemplate().find(qry.toString());
            if (listaUsers != null && listaUsers.size() > 0) {
            	for (Users us : listaUsers ) {
            		log.info(us.getProfile().getDescription());
            	}
            }
            
            log.info(":::...[DAO]: end selectUsersByClient OK");
            return listaUsers;
        } catch(Exception e) {
            log.error(":::...[DAO] end selectUsersByClient " + e.getMessage());
            throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
        }
	}

	
	@SuppressWarnings("rawtypes")
	public Users getUserbyNick(String nick) throws DatabaseException {
		
		log.info(":::...[DAO]: begin getUserbyNick OK");
		
		StringBuffer qry = new StringBuffer();
		Users users = null;
        try{
            qry.append(" Select US ");
            qry.append(" From Users US, Client C ");
            qry.append(" Where US.client.idClient = C.idClient ");
            qry.append(" and US.nick = '");
            qry.append( nick );
            qry.append("' " );
            
            List usersList = getHibernateTemplate().find(qry.toString());;
            if ( usersList != null && usersList.size() > 0) {
            	users = (Users)usersList.get(0);
            	log.debug(":::...[VIEW]: user - client: " + 
            			users.getClient().getClientName() + 
            			"  profile: " + users.getProfile().getIdProfile());
            }
            
            log.info(":::...[DAO]: end getUserbyNick OK");
            return users;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getUserbyNick : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public Users selectUserById(Long idUser) throws DatabaseException {
		log.debug(":::...[DAO] begin selectUserById OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Users.class);
			
			criteria.add(Expression.eq("idUser", idUser));
			
			final Users user = (Users)criteria.uniqueResult();
			
			log.debug(":::...[DAO] end selectUserById OK");
			return user;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectUserById : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public void saveUsuario(Users users) throws DatabaseException {
		log.info(":::...[DAO]: begin saveUsuario OK");
		try {
			getHibernateTemplate().save(users);
			log.info(":::...[DAO]: end saveUsuario OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end saveUsuario : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public void updateUsuario(Users users) throws DatabaseException {
		log.info(":::...[DAO]: begin updateUsuario OK");
		try {
			getHibernateTemplate().update(users);
			log.info(":::...[DAO]: end updateUsuario OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end updateUsuario : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	@SuppressWarnings("unchecked")
	public String selectUserEmail(String idClient, String idProfile) 
			throws DatabaseException {
		String email = "";
		
		log.info(":::...[DAO]: begin selectUserEmail OK");
		
		List<Users> listaUsers = new ArrayList<Users>();
        StringBuffer qry = new StringBuffer();
        try{
            qry.append(" Select US ");
            qry.append(" From Users US ");
            qry.append(" Where US.client.idClient = '");
            qry.append( idClient );
            qry.append("' " );
            qry.append(" And US.profile.idProfile = '");
            qry.append( idProfile );
            qry.append("' " );
            listaUsers = getHibernateTemplate().find(qry.toString());
            
            if (listaUsers != null && listaUsers.size() > 0) {
            	Users user = (Users)listaUsers.get(0);
            	email = user.getEmail();
            }
            
            log.info(":::...[DAO]: end selectUserEmail OK");
            return email;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectUserEmail : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	@SuppressWarnings("rawtypes")
	public String selectIdClientbyUser(String idUser) throws DatabaseException {
		
		log.info(":::...[DAO]: begin selectIdClientbyUser OK");
		
		StringBuffer qry = new StringBuffer();
		String idClient = null;
        try{
            qry.append(" Select US ");
            qry.append(" From Users US, Client C ");
            qry.append(" Where US.client.idClient = C.idClient ");
            qry.append(" and US.idUser = '");
            qry.append( idUser );
            qry.append("' " );
            
            List usersList = getHibernateTemplate().find(qry.toString());;
            if ( usersList != null && usersList.size() > 0) {
            	Users users = (Users)usersList.get(0);
            	log.debug(":::...[VIEW]: user - client: " + 
            			users.getClient().getIdClient());
            	idClient = users.getClient().getIdClient().toString();
            	
            }
            
            log.info(":::...[DAO]: end selectIdClientbyUser OK");
            return idClient;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectIdClientbyUser : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	/**
	 * select email from users where status = 1 and email is not null 
	 * and id_client in (7,3,2,1) and id_profile in (1,2,3,4,5,6,7);
	 */

	@SuppressWarnings("unchecked")
	public List<String> selectUserEmail(String[] clientIdList, 
			String[] profileIdList) throws DatabaseException {
		log.info(":::...[DAO]: begin selectUserEmail OK");
		
		StringBuilder qry = new StringBuilder();
        try{
            qry.append(" Select US.email ");
            qry.append(" From Users US ");
            qry.append(" Where US.status = 1 ");
            qry.append(" and US.email is not null ");
            if (clientIdList != null && clientIdList.length > 0) {
            	qry.append(" and US.client.idClient in (");
            	for (String clientId : clientIdList) {
            		qry.append( clientId + "," );
            	}
            	qry.deleteCharAt(qry.length()-1);
            	qry.append(") ");
            }
            
            if (profileIdList != null && profileIdList.length > 0) {
            	qry.append(" and US.profile.idProfile in (");
            	for (String profileId : profileIdList) {
            		qry.append( profileId + "," );
            	}
            	qry.deleteCharAt(qry.length()-1);
            	qry.append(") ");
            }
            
            List<String> emailList = getHibernateTemplate().find(qry.toString());
            
            log.info(":::...[DAO]: end selectUserEmail OK");
            return emailList;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectIdClientbyUser : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
			
	
	
	

}
