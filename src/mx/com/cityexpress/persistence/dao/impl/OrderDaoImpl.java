package mx.com.cityexpress.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.mapping.pojo.Orders;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;
import mx.com.cityexpress.persistence.dao.IOrderDao;

public class OrderDaoImpl extends DAOBase implements IOrderDao {
	
	private Log log = LogFactory.getLog(OrderDaoImpl.class);

	public void getOrdersByNumber(long number) throws DatabaseException {
				
	}

	public void insertOrder(Orders orders) throws DatabaseException {
		getHibernateTemplate().save(orders);
	}

	
	@SuppressWarnings("unchecked")
	public Long getMaxOrder() throws DatabaseException {
		
		log.info(":::...[DAO]: IN getMaxOrder OK");
		
		Long max = new Long(0);
	    StringBuffer qry = new StringBuffer();
	    try{
	        qry.append(" Select max(ORD.idOrder) From Orders ORD " );
	        
	        List<Long> res = getHibernateTemplate().find(qry.toString());
	        if (res != null && res.size() > 0) {
	        	max = (Long)res.get(0);
	        } 
	        
		    log.info(":::...[DAO]: end getMaxOrder OK");
		    return max;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getMaxOrder : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	
	@SuppressWarnings("rawtypes")
	public List getOrderDetail(String norder) throws DatabaseException {
		
		log.info(":::...[DAO]: IN getOrderDetail OK");
		
		List detailList = new ArrayList();
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.numOrder, P.name, ");
	        qry.append("        OD.quantity, P.price, OD.comments, P.unidad ");
	        qry.append(" From Orders O, OrdersDetail OD, Product P ");
	        qry.append(" Where O.idOrder = OD.orders.idOrder ");
	        qry.append(" and   OD.product.idProduct = P.idProduct ");
	        qry.append(" and   O.numOrder = '" + norder +"' " );
	        qry.append(" order by O.idOrder " );		
	        detailList = getHibernateTemplate().find(qry.toString());
	        
		    log.info(":::...[DAO]: end getOrderDetail OK");
		    return detailList ;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getOrderDetail : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Object[]> selectPedidos(BusquedaPedidoVo busquedaVo)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectPedidos --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.idOrder, O.numOrder, O.creationDate,  ");
	        qry.append("        O.total, U.nick, C.clientName, C.idClient,  ");
	        qry.append("        O.status, O.guia, O.comments, U.name ");
	        qry.append(" From Orders O, Users U, Client C ");
	        qry.append(" Where O.users.idUser = U.idUser ");
	        qry.append(" and   U.client.idClient = C.idClient ");
	        
	        if (busquedaVo.getEstatus()!= null && !busquedaVo.getEstatus().equals("-1")) {
	        	qry.append(" and O.status = '" + busquedaVo.getEstatus()+"' ");
	        }
	        
	        if (busquedaVo.getNumPedido() != null && busquedaVo.getNumPedido().trim().length() > 0) {
	        	qry.append(" and O.numOrder = '" + busquedaVo.getNumPedido()+"' ");
	        }
	        
	        if (busquedaVo.getCliente() != null && !busquedaVo.getCliente().equals("-1")) {
	        	qry.append(" and C.idClient = '" + busquedaVo.getCliente()+"' ");
	        }
	        
	        if(busquedaVo.getFechaInicial() != null && busquedaVo.getFechaFinal() != null) {
	        	qry.append(" and O.creationDate >= '" + busquedaVo.getFechaInicial()+"' ");
	        	qry.append(" and O.creationDate <= '" + busquedaVo.getFechaFinal()+"' ");
	        }
	        
	        
	        
	        
	        qry.append(" order by O.idOrder ");
	        
	        
	        log.info(":::...[DAO] (selectPedidos) query: " + qry.toString());
	        		
	        final List pedidosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectPedidos OK");
	        return pedidosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidos : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	    
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> selectPedidosUsuario(BusquedaPedidoVo busquedaVo)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectPedidosUsuario --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.idOrder, O.numOrder, O.creationDate,  ");
	        qry.append("        O.total, O.status, O.guia ");
	        qry.append(" From Orders O, Users U ");
	        qry.append(" Where O.users.idUser = U.idUser ");
	        qry.append(" and O.status != '3' ");
	        qry.append(" and U.idUser = '" + busquedaVo.getIdUsuario()+"' ");
	        qry.append(" order by O.idOrder ");
	        
	        log.info(":::...[DAO] (selectPedidosUsuario) query: " + qry.toString());
	        		
	        final List pedidosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectPedidosUsuario OK");
	        return pedidosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidosUsuario : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	    
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> selectPedidosCliente(BusquedaPedidoVo busquedaVo)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectPedidosCliente --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.idOrder, O.numOrder, O.creationDate,  ");
	        qry.append("        O.total, O.status, O.guia ");
	        qry.append(" From Orders O, Users U ");
	        qry.append(" Where O.users.idUser = U.idUser ");
	        qry.append(" and O.status != '3' ");
	        qry.append(" and U.client.idClient = '" + busquedaVo.getCliente()+"' ");
	        qry.append(" order by O.idOrder ");
	        
	        
	        log.info(":::...[DAO] (selectPedidosCliente) query: " + qry.toString());
	        		
	        final List pedidosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectPedidosCliente OK");
	        return pedidosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidosCliente : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	    
	}
	
	
	
	public void updateEstatusPedido(InputPedidoVO inputVO) 
			throws DatabaseException {
		log.debug(":::...[DAO] Begin updatePedido --> OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Orders.class);
			
			criteria.add(Expression.eq("idOrder", inputVO.getIdPedido()));
			final Orders ord = (Orders)criteria.uniqueResult();
			
			if (ord != null) {
				ord.setStatus(inputVO.getEstatus());
				ord.setGuia(inputVO.getGuia());
				ord.setComments(inputVO.getComments());
				getHibernateTemplate().saveOrUpdate(ord);
			} else {
				throw new DatabaseException("El pedido ya no esta registrado.");
			}
			
			log.debug(":::...[DAO] end updatePedido --> OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end updateEstatusPedido : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> selectPedidoPorId(Long idOrder)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectPedidoPorId --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.idOrder, O.numOrder, O.creationDate,  ");
	        qry.append("        O.total, U.nick, C.clientName, C.idClient, ");
	        qry.append("        O.status, O.guia, O.comments, U.name ");
	        qry.append(" From Orders O, Users U, Client C ");
	        qry.append(" Where O.users.idUser = U.idUser ");
	        qry.append(" and   U.client.idClient = C.idClient ");
	        qry.append(" and   O.idOrder = " + idOrder.toString() +" ");
	        
	        log.info(":::...[DAO] (selectPedidoPorId) query: " + qry.toString());
	        		
	        final List pedidosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectPedidoPorId OK");
	        return pedidosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidoPorId : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	    
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Object[]> selectFechaCreacionPedidoPorNumPed(String numOrder)
			throws DatabaseException {
		log.info(":::...[DAO]: Begin selectFechaCreacionPedidoPorNumPed --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	        qry.append(" Select O.creationDate, O.numOrder  ");
	        qry.append(" From Orders O ");
	        qry.append(" Where O.numOrder = '" + numOrder +"' ");
	        
	        log.info(":::...[DAO] (selectFechaCreacionPedidoPorNumPed) query: " + qry.toString());
	        		
	        final List pedidosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectFechaCreacionPedidoPorNumPed OK");
	        return pedidosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectFechaCreacionPedidoPorNumPed : " + 
    				e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	    
	}
	
}
