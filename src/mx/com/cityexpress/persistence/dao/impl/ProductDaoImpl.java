package mx.com.cityexpress.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Product;
import mx.com.cityexpress.persistence.dao.IProductDao;

public class ProductDaoImpl extends DAOBase implements IProductDao {

	Log log = LogFactory.getLog(ProductDaoImpl.class);
	
	@SuppressWarnings("unchecked")
	public List<Product> selectAllProducts() throws DatabaseException {
		
		log.info(":::...[DAO]: begin getAllProducts OK");
		
		List<Product> productsList = new ArrayList<Product>();
		StringBuffer query = new StringBuffer();
		try {
			query.append(" Select P ");
			query.append(" From Product P ");
			query.append(" Order by  P.name ");
			
			productsList = getHibernateTemplate().find(query.toString());
			
			log.info(":::...[DAO]: end getAllProducts OK");
			return productsList;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getAllProducts : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Product> selectActiveProducts() throws DatabaseException {
		
		log.info(":::...[DAO]: begin getActiveProducts OK");
		
		List<Product> productsList = new ArrayList<Product>();
		StringBuffer query = new StringBuffer();
		try {
			query.append(" Select P ");
			query.append(" From Product P ");
			query.append(" Where status = 0 ");
			query.append(" Order by  P.name ");
			
			productsList = getHibernateTemplate().find(query.toString());
			
			log.info(":::...[DAO]: end getActiveProducts OK");
			return productsList;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end getActiveProducts : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	public Product selectProductByCode(int code) throws DatabaseException {
		log.info(":::...[DAO] begin selectProductByCode OK");
		try {
			final Criteria c = getSession().createCriteria(Product.class);
			c.add(Expression.eq("codeProduct", code));
			
			final Product p =  (Product)c.uniqueResult();
			
			log.info(":::...[DAO] end selectProductByCode OK");
			return p;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectProductByCode : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public void saveProduct(Product product) throws DatabaseException {
		log.info(":::...[DAO]: begin saveProduct OK");
		try {
			getHibernateTemplate().save(product);
			log.info(":::...[DAO]: end saveProduct OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end saveProduct : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public void updateProduct(Product product) throws DatabaseException {
		log.info(":::...[DAO]: begin updateProduct OK");
		try {
			getHibernateTemplate().update(product);
			log.info(":::...[DAO]: end updateProduct OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end updateProduct : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> selectRepProductos(BusquedaFiltroVO filtroVo) 
			throws DatabaseException {
		
		log.info(":::...[DAO]: Begin selectRepProductos --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	    	qry.append(" SELECT P.name, C.clientName, O.numOrder, P.unidad, sum(OD.quantity)  ");
	    	qry.append(" FROM Orders O, OrdersDetail OD, Product P, Users U, Client C  ");
	    	qry.append(" WHERE O.idOrder = OD.orders.idOrder  ");
	    	qry.append(" AND   OD.product.idProduct = P.idProduct  ");
	    	qry.append(" AND   O.users.idUser = U.idUser  ");
	    	qry.append(" AND   U.client.idClient = C.idClient  ");
	    	
	    	if (filtroVo.getIdCliente()!= null && !filtroVo.getIdCliente().equals("-1")) {
	        	qry.append(" AND U.client.idClient = '" + filtroVo.getIdCliente()+"' ");
	        }
	    	
	        if (filtroVo.getEstatus()!= null && !filtroVo.getEstatus().equals("-1")) {
	        	qry.append(" AND O.status = '" + filtroVo.getEstatus()+"' ");
	        }
	          
	        if(filtroVo.getFechaInicial() != null && filtroVo.getFechaFinal() != null) {
	        	qry.append(" AND O.creationDate >= '" + filtroVo.getFechaInicial()+"' ");
	        	qry.append(" AND O.creationDate <= '" + filtroVo.getFechaFinal()+"' ");
	        }
	    	
	    	
	    	qry.append(" group by C.idClient, P.name, C.clientName, O.numOrder, P.unidad  ");
	    	qry.append(" order by P.name, C.idClient, O.numOrder ");
	        
	        log.info(":::...[DAO] (selectRepProductos) query: " + qry.toString());
	        		
	        final List<Object[]> repProductosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectRepProductos OK");
	        return repProductosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidos : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> selectRepProductosOld(BusquedaFiltroVO filtroVo) 
			throws DatabaseException {
		
		log.info(":::...[DAO]: Begin selectRepProductos --> OK");
		
	    StringBuffer qry = new StringBuffer();
	    try{

	    	qry.append(" SELECT P.name, C.clientName, P.unidad, sum(OD.quantity)  ");
	    	qry.append(" FROM Orders O, OrdersDetail OD, Product P, Users U, Client C  ");
	    	qry.append(" WHERE O.idOrder = OD.orders.idOrder  ");
	    	qry.append(" AND   OD.product.idProduct = P.idProduct  ");
	    	qry.append(" AND   O.users.idUser = U.idUser  ");
	    	qry.append(" AND   U.client.idClient = C.idClient  ");
	    	
	    	if (filtroVo.getIdCliente()!= null && !filtroVo.getIdCliente().equals("-1")) {
	        	qry.append(" AND U.client.idClient = '" + filtroVo.getIdCliente()+"' ");
	        }
	    	
	        if (filtroVo.getEstatus()!= null && !filtroVo.getEstatus().equals("-1")) {
	        	qry.append(" AND O.status = '" + filtroVo.getEstatus()+"' ");
	        }
	          
	        if(filtroVo.getFechaInicial() != null && filtroVo.getFechaFinal() != null) {
	        	qry.append(" AND O.creationDate >= '" + filtroVo.getFechaInicial()+"' ");
	        	qry.append(" AND O.creationDate <= '" + filtroVo.getFechaFinal()+"' ");
	        }
	    	
	    	
	    	qry.append(" group by C.idClient, P.name, C.clientName, P.unidad  ");
	    	qry.append(" order by P.name, C.idClient  ");
	        
	        log.info(":::...[DAO] (selectRepProductos) query: " + qry.toString());
	        		
	        final List<Object[]> repProductosList = 
	        		getHibernateTemplate().find(qry.toString());
	        
	        log.info(":::...[DAO]: end selectRepProductos OK");
	        return repProductosList ;     
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectPedidos : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
		
	}

}
