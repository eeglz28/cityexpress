package mx.com.cityexpress.persistence.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.persistence.dao.IClientDao;

public class ClientDaoImpl extends DAOBase implements IClientDao {
	
	private Log log = LogFactory.getLog(ClientDaoImpl.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Client> selectClients() throws DatabaseException {
		log.debug(":::...[DAO] begin selectClients OK");
		
		try {
			final Criteria 
					criteria = getSession().createCriteria(Client.class);
			criteria.addOrder(Order.asc("clientName"));
			
			List resList = criteria.list();
			
			log.debug(":::...[DAO] end selectClients OK");
			return resList;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectClients : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	public Client selectClient(Long idClient) throws DatabaseException {
		log.debug(":::...[DAO] begin selectClient OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Client.class);
			
			criteria.add(Expression.eq("idClient", idClient));
			
			final Client cli = (Client)criteria.uniqueResult();
			
			log.debug(":::...[DAO] end selectClient OK");
			return cli;
		} catch (Exception e) {
    		log.error(":::...[DAO] end selectClient " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
		
	}
	
	public Client selectClientByName(String nombre) throws DatabaseException {
		log.debug(":::...[DAO] begin selectClientByName OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Client.class);
			
			criteria.add(Expression.eq("clientName", nombre.trim()));
			
			final Client cli = (Client)criteria.uniqueResult();
			
			log.debug(":::...[DAO] end selectClientByName OK");
			return cli;
		} catch (Exception e) {
    		log.error(":::...[DAO] end selectClientByName " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
		
	}
	
	public void saveClient(Client client) throws DatabaseException {
		log.info(":::...[DAO]: begin saveClient OK");
		try {
			getHibernateTemplate().save(client);
			log.info(":::...[DAO]: end saveClient OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end saveClient : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	
	public void updateClient(Client client) throws DatabaseException {
		log.info(":::...[DAO]: begin updateClient OK");
		try {
			getHibernateTemplate().update(client);
			log.info(":::...[DAO]: end updateClient OK");
    	} catch (Exception e) {
    		log.error(":::...[DAO] end updateClient : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
		
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Client> selectOnlyClients() throws DatabaseException {
		log.debug(":::...[DAO] begin selectClients OK");
		
		try {
			final Criteria 
					criteria = getSession().createCriteria(Client.class);
			criteria.add(Expression.ne("idClient", new Long(9999)));
			criteria.addOrder(Order.asc("clientName"));
			
			List resList = criteria.list();
			
			log.debug(":::...[DAO] end selectClients OK");
			return resList;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectClients : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}

}
