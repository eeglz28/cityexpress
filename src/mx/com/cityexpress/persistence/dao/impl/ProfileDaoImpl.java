package mx.com.cityexpress.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.persistence.DAOBase;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.persistence.dao.IProfileDao;

public class ProfileDaoImpl extends DAOBase implements IProfileDao {
	
	private Log log = LogFactory.getLog(ProfileDaoImpl.class);
	

	/*
	 * Si val es nulo , la consulta a la tabla de perfiles no debe de tomar 
	 * en cuenta al perfil administrador y coordinador  
	 * (non-Javadoc)
	 * @see mx.com.cityexpress.persistence.dao.IProfileDao#selectAllProfiles(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<Profile> selectAllProfiles(String val) throws DatabaseException {
		log.info(":::...[DAO]: begin selectAllProfiles OK");
		
		List<Profile> listaProfiles = new ArrayList<Profile>();
        StringBuffer qry = new StringBuffer();
        try{
            qry.append(" Select PF ");
            qry.append(" From Profile PF ");
            if (val == null) {
            	qry.append(" Where PF.type = 'E'");	
            } else {
            	qry.append(" Where PF.type = 'I'");
            }
            
            listaProfiles = getHibernateTemplate().find(qry.toString());
            
            log.info(":::...[DAO]: end selectAllProfiles OK");
            return listaProfiles;
    	} catch (Exception e) {
    		log.error(":::...[DAO] end selectAllProfiles : " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	public Profile selectProfileById(String idProfile) throws DatabaseException {
		log.debug(":::...[DAO] begin selectProfileById OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Profile.class);
			
			criteria.add(Expression.eq("idProfile", Long.parseLong(idProfile)));
			
			final Profile profile = (Profile)criteria.uniqueResult();
			
			log.debug(":::...[DAO] end selectProfileById OK");
			return profile;
		} catch (Exception e) {
    		log.error(":::...[DAO] end selectProfileById " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}
	
	@SuppressWarnings("unchecked")
	public List<Profile> selectProfileByType(String type) 
			throws DatabaseException {
		log.debug(":::...[DAO] begin selectProfileByType OK");
		try {
			final Criteria 
					criteria = getSession().createCriteria(Profile.class);
			
			criteria.add(Expression.eq("type", type));
			criteria.addOrder(Order.asc("description"));
			
			final List<Profile> profiles = criteria.list();
			
			log.debug(":::...[DAO] end selectProfileByType OK");
			return profiles;
		} catch (Exception e) {
    		log.error(":::...[DAO] end selectProfileByType " + e.getMessage());
    		throw new DatabaseException(ConstantsCExpress.ERROR_DATABASE);
    	}
	}

}

