package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.mapping.pojo.Profile;

public interface IProfileDao {
	
	public List<Profile> selectAllProfiles(String val) throws DatabaseException;
	
	public Profile selectProfileById(String idProfile) throws DatabaseException;
	
	public List<Profile> selectProfileByType(String type) throws DatabaseException;
	

}
