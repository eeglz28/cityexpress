package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;

public interface IOrdersDetailDao {
	
	public List<Object[]> selectOrderDetail(Long idOrder) throws DatabaseException;

}
