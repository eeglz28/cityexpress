/**
 * 
 */
package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.mapping.pojo.Users;

/**
 * @author franktor
 *
 */
public interface IUsersDao {
	
	public List<Users> getActiveUsers(String user) throws DatabaseException;
	
	public Users getActiveUser(String user, String pass) 
			throws DatabaseException;
	
	public Users getUserbyNick(String nick) throws DatabaseException;
	
	public List<Users> selectUsersByClient(Long idClient) 
			throws DatabaseException;
	
	public Users selectUserById(Long idUser) throws DatabaseException;
	
	public void saveUsuario(Users users) throws DatabaseException;
	
	public void updateUsuario(Users users) throws DatabaseException;
	
	public String selectUserEmail(String idClient, String idProfile) 
			throws DatabaseException;

	public String selectIdClientbyUser(String idUser) throws DatabaseException;
	
	public List<String> selectUserEmail(String[] clientIdList, 
			String[] profileIdList) throws DatabaseException;
}
