package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Product;

public interface IProductDao {
	
	public List<Product> selectAllProducts() throws DatabaseException;
	
	public List<Product> selectActiveProducts() throws DatabaseException;
	
	public Product selectProductByCode(int code) throws DatabaseException;
	
	public void saveProduct(Product product) throws DatabaseException;
	
	public void updateProduct(Product product) throws DatabaseException;

	public List<Object[]> selectRepProductos(BusquedaFiltroVO filtroVo) 
			throws DatabaseException;
}
