package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.mapping.pojo.Orders;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;


public interface IOrderDao {
	
	public void insertOrder(Orders orders) throws DatabaseException;
	
	public void getOrdersByNumber(long number) throws DatabaseException;
	
	public Long getMaxOrder() throws DatabaseException;

	
	@SuppressWarnings("rawtypes")
	public List getOrderDetail(String norder) throws DatabaseException;
	
	
	public List<Object[]> selectPedidos(BusquedaPedidoVo busquedaVo) 
			throws DatabaseException;
	
	public List<Object[]> selectPedidosUsuario(BusquedaPedidoVo busquedaVo)
			throws DatabaseException;
	
	public List<Object[]> selectPedidosCliente(BusquedaPedidoVo busquedaVo)
			throws DatabaseException;
	
	
	public void updateEstatusPedido(InputPedidoVO inputVO) 
			throws DatabaseException;
	
	public List<Object[]> selectPedidoPorId(Long idOrder)
			throws DatabaseException;
	
	public List<Object[]> selectFechaCreacionPedidoPorNumPed(String numOrder)
			throws DatabaseException;
	
}
