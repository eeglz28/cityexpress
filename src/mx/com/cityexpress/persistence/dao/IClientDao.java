package mx.com.cityexpress.persistence.dao;

import java.util.List;

import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.mapping.pojo.Client;

public interface IClientDao {
	
	public List<Client> selectClients() throws DatabaseException;
	
	public Client selectClient(Long idClient) throws DatabaseException;
	
	public Client selectClientByName(String nombre) throws DatabaseException;
	
	
	public void saveClient(Client client) throws DatabaseException;
	
	public void updateClient(Client client) throws DatabaseException;
	
	public List<Client> selectOnlyClients() throws DatabaseException;

}
