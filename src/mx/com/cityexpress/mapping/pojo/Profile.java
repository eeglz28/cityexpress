package mx.com.cityexpress.mapping.pojo;

public class Profile {
	
	private Long    idProfile;
	private String  description;
	private String  type;


	/**
	 * 
	 */
	public Profile() {
		super();
	}


	/**
	 * @param idProfile
	 * @param description
	 */
	public Profile(Long idProfile, String description, String type) {
		super();
		this.idProfile = idProfile;
		this.description = description;
	}


	/**
	 * @return the idProfile
	 */
	public Long getIdProfile() {
		return idProfile;
	}


	/**
	 * @param idProfile the idProfile to set
	 */
	public void setIdProfile(Long idProfile) {
		this.idProfile = idProfile;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return "ID: ".concat(getIdProfile().toString())
				.concat("  DESCRIPTION: ").concat(getDescription());
	}


	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
