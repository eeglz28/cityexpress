package mx.com.cityexpress.mapping.pojo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Users {
	
	private Long    idUser;
	private Client  client;
	private String  nick;
	private String  pass;
	private Date    creationDate;
	private Date    updateDate;
	private int     invalid;
	private int     status;
	private String  email;
	private String  name;
	private Profile profile;
	
	@SuppressWarnings("rawtypes")
	private Set     orderses = new HashSet();
	
	/**
	 * 
	 */
	public Users() {
		super();
	}

	/**
	 * @param idUser
	 * @param client
	 * @param nick
	 * @param pass
	 * @param creationDate
	 * @param updateDate
	 * @param invalid
	 * @param status
	 * @param email
	 * @param orderses
	 */
	@SuppressWarnings("rawtypes")
	public Users(Long idUser, Client client, String nick, String pass,
			Date creationDate, Date updateDate, int invalid, int status,
			String email, String name, Profile profile, Set orderses) {
		super();
		this.idUser = idUser;
		this.client = client;
		this.nick = nick;
		this.pass = pass;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.invalid = invalid;
		this.status = status;
		this.email = email;
		this.orderses = orderses;
		this.profile = profile;
		this.name = name;
	}

	/**
	 * @return the idUser
	 */
	public Long getIdUser() {
		return idUser;
	}

	/**
	 * @param idUser the idUser to set
	 */
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * @return the nick
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * @param nick the nick to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the updateDate
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the invalid
	 */
	public int getInvalid() {
		return invalid;
	}

	/**
	 * @param invalid the invalid to set
	 */
	public void setInvalid(int invalid) {
		this.invalid = invalid;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the orderses
	 */
	@SuppressWarnings("rawtypes")
	public Set getOrderses() {
		return orderses;
	}

	/**
	 * @param orderses the orderses to set
	 */
	@SuppressWarnings("rawtypes")
	public void setOrderses(Set orderses) {
		this.orderses = orderses;
	}
	
	public String toString() {
		return "ID: " + idUser +
				"  CLIENT: " + client +
				"  NICK: " + nick +
				"  PASS: " + pass +
				"  CREATIONDATE: " + creationDate +
 				"  UPDATEDATE: " + updateDate +
				"  INVALID: " + invalid +
				"  STATUS: " + status +
				"  EMAIL: " + email +
				"  NAME: " + name + 
				"  PROFILE: " + profile;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}
