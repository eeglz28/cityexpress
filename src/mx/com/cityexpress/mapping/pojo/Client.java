package mx.com.cityexpress.mapping.pojo;

import java.util.HashSet;
import java.util.Set;

public class Client {
	
	private Long    idClient;
	private String  clientName;
	private String  agent;
	private String  address;
	private String  phone;
	private String  email;
	@SuppressWarnings("rawtypes")
	private Set     userses = new HashSet();
	
	/**
	 * 
	 */
	public Client() {
		super();
	}

	/**
	 * @param idClient
	 * @param clientName
	 * @param agent
	 * @param address
	 * @param phone
	 * @param email
	 * @param userses
	 */
	@SuppressWarnings("rawtypes")
	public Client(Long idClient, String clientName, String agent,
			String address, String phone, String email, Set userses) {
		super();
		this.idClient = idClient;
		this.clientName = clientName;
		this.agent = agent;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.userses = userses;
	}

	/**
	 * @return the idClient
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * @param idClient the idClient to set
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientName the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the agent
	 */
	public String getAgent() {
		return agent;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(String agent) {
		this.agent = agent;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the userses
	 */
	@SuppressWarnings("rawtypes")
	public Set getUserses() {
		return userses;
	}

	/**
	 * @param userses the userses to set
	 */
	@SuppressWarnings("rawtypes")
	public void setUserses(Set userses) {
		this.userses = userses;
	}
	
	public String toString() {
		return "ID: ".concat(getIdClient().toString())
				.concat("  NAME: ").concat(getClientName())
				.concat("  AGENT: ").concat(getAgent())
				.concat("  ADDRESS: ").concat(getAddress())
				.concat("  PHONE: ").concat(getPhone())
				.concat("  EMAIL: ").concat(getEmail());
	}
	
	

}
