package mx.com.cityexpress.mapping.pojo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Orders {
	
	private Long        idOrder;
	private Users       users;
	private String		numOrder;
	private Date        creationDate;
	private BigDecimal  total;
	private String 		status;
	private String  	guia;
	private String 		comments;

	@SuppressWarnings("rawtypes")
	private Set         ordersDetails = new HashSet(0);
	
	/**
	 * 
	 */
	public Orders() {
		super();
	}

	/**
	 * @param idOrder
	 * @param users
	 * @param creationDate
	 * @param total
	 * @param ordersDetails
	 */
	
	@SuppressWarnings("rawtypes")
	public Orders(Long idOrder, Users users, Date creationDate,
			BigDecimal total, Set ordersDetails, String guia, 
			String status, String comments) {
		super();
		this.idOrder = idOrder;
		this.users = users;
		this.creationDate = creationDate;
		this.total = total;
		this.ordersDetails = ordersDetails;
		this.guia = guia;
		this.status = status;
		this.comments = comments;
	}

	/**
	 * @return the idOrder
	 */
	public Long getIdOrder() {
		return idOrder;
	}

	/**
	 * @param idOrder the idOrder to set
	 */
	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}

	/**
	 * @return the users
	 */
	public Users getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Users users) {
		this.users = users;
	}
	
	/**
	 * @return the numOrder
	 */
	public String getNumOrder() {
		return numOrder;
	}

	/**
	 * @param numOrder the numOrder to set
	 */
	public void setNumOrder(String numOrder) {
		this.numOrder = numOrder;
	}


	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the ordersDetails
	 */
	@SuppressWarnings("rawtypes")
	public Set getOrdersDetails() {
		return ordersDetails;
	}

	/**
	 * @param ordersDetails the ordersDetails to set
	 */
	@SuppressWarnings("rawtypes")
	public void setOrdersDetails(Set ordersDetails) {
		this.ordersDetails = ordersDetails;
	}
	
	public String toString() {
		return "ID: ".concat(getIdOrder().toString())
				.concat("  USER: ").concat(getUsers().toString())
				.concat("  CREATIONDATE: ").concat(getCreationDate().toString())
				.concat("  TOTAL: ").concat(getTotal().toString()
				.concat("  NUMORDER: ").concat(String.valueOf(getNumOrder()))
				.concat("  STATUS: ").concat(getStatus()));
	}


        /**
	 * @return the guia
	 */
        public String getGuia() {
                return guia;
        }

        /**
 	 * @param status the guia to set
         */
        public void setGuia(String guia) {
                this.guia = guia;
        }

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	
}
