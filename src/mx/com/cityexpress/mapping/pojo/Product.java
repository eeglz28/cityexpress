package mx.com.cityexpress.mapping.pojo;

import java.math.BigDecimal;

public class Product {
	
	private Long        idProduct;
	private int      	codeProduct;
	private String      name;
	private BigDecimal  price;
	private int         status;
	private String		description;
	private String		unidad;
	private String 		entrega;
	private boolean		folioStatus;
	
	/**
	 * 
	 */
	public Product() {
		super();
	}
	
	/**
	 * @param idProduct
	 * @param codeProduct
	 * @param name
	 * @param price
	 * @param status
	 */
	public Product(Long idProduct, int codeProduct, String name,
			BigDecimal price, int status, String description,
			boolean folioStatus) {
		super();
		this.idProduct = idProduct;
		this.codeProduct = codeProduct;
		this.name = name;
		this.price = price;
		this.status = status;
		this.description = description;
		this.folioStatus = folioStatus;
	}
	
	/**
	 * @return the idProduct
	 */
	public Long getIdProduct() {
		return idProduct;
	}
	
	/**
	 * @param idProduct the idProduct to set
	 */
	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}
	
	/**
	 * @return the codeProduct
	 */
	public int getCodeProduct() {
		return codeProduct;
	}
	
	/**
	 * @param codeProduct the codeProduct to set
	 */
	public void setCodeProduct(int codeProduct) {
		this.codeProduct = codeProduct;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
	
	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return the entrega
	 */
	public String getEntrega() {
		return entrega;
	}

	/**
	 * @param entrega the entrega to set
	 */
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	
	/**
	 * @return the folioStatus
	 */
	public boolean isFolioStatus() {
		return folioStatus;
	}

	/**
	 * @param folioStatus the folioStatus to set
	 */
	public void setFolioStatus(boolean folioStatus) {
		this.folioStatus = folioStatus;
	}
	
	public String toString() {
		return "ID: ".concat(getIdProduct().toString())
				.concat("  CODE: ").concat(String.valueOf(getCodeProduct()))
				.concat("  NAME: ").concat(getName())
				.concat("  PRICE: ").concat(getPrice().toString())
				.concat("  STATUS: ").concat(String.valueOf(getStatus()))
				.concat("  DESCRIPTION: ").concat(String.valueOf(getDescription()))
				.concat("  UNIDAD: ").concat(String.valueOf(getUnidad()))
				.concat("  ENTREGA: ").concat(String.valueOf(getEntrega()))
				.concat("  FOLIOSTATUS: ").concat(String.valueOf(folioStatus));
	}


 
}
