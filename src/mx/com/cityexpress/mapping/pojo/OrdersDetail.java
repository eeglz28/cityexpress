package mx.com.cityexpress.mapping.pojo;

import java.math.BigDecimal;

public class OrdersDetail {

	private Long     idOrderDetail;
	private Orders   orders;
	private Product  product;
	private BigDecimal quantity;
	private String   comments;
	
	/**
	 * 
	 */
	public OrdersDetail() {
		super();
	}

	/**
	 * @param idOrderDetail
	 * @param orders
	 * @param product
	 * @param quantity
	 */
	public OrdersDetail(Long idOrderDetail, Orders orders, Product product,
			BigDecimal quantity, String comments) {
		super();
		this.idOrderDetail = idOrderDetail;
		this.orders = orders;
		this.product = product;
		this.quantity = quantity;
		this.comments = comments;
	}

	/**
	 * @return the idOrderDetail
	 */
	public Long getIdOrderDetail() {
		return idOrderDetail;
	}

	/**
	 * @param idOrderDetail the idOrderDetail to set
	 */
	public void setIdOrderDetail(Long idOrderDetail) {
		this.idOrderDetail = idOrderDetail;
	}

	/**
	 * @return the orders
	 */
	public Orders getOrders() {
		return orders;
	}

	/**
	 * @param orders the orders to set
	 */
	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	public String toString() {
		return "ID: ".concat(getIdOrderDetail().toString())
				.concat("  QUANTITY: ").concat(String.valueOf(getQuantity()));
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
}
