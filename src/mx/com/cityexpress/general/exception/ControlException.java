package mx.com.cityexpress.general.exception;

public class ControlException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ControlException(String message){
		super(message);
	}

}
