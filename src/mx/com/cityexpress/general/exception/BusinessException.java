/*
 * BusinessException.java
 *
 * Created on September 23, 2007, 11:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */


package mx.com.cityexpress.general.exception;

public class BusinessException extends ControlException {

	private static final long serialVersionUID = 1L;

	public BusinessException(String message)
	{
	    super(message);
	}
}
