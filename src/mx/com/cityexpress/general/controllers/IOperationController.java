package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;

public interface IOperationController {
	
	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo)
			throws ControlException;
	
	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder)
			throws ControlException;

}
