package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.ClientTableVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.mail.sender.MailerVo;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;

public interface IConsultaController {
	
	public List<Client> obtenerClientes() throws ControlException;
	
	
	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo) 
			throws ControlException;
	
	
	public List<OrdersTableVo> obtenerPedidosUsuarios(BusquedaPedidoVo 
			busquedaVo) throws ControlException;
	
	
	public List<ClientTableVo> obtenerClientePorId(long idCliente) 
			throws ControlException;
	
	
	public void actualizarEstatusPedido(InputPedidoVO inputVO) 
			throws ControlException;
	
	
	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder)
			throws ControlException;
	
	
	public List<DetailOrderTableVo> getOrderDetail(String norder) 
			throws ControlException;
	
	
	public boolean sendMail(MailerVo mailerVo) throws ControlException;
	
	
	public String getClientName(String idClient) throws ControlException;
	
	public UsuarioEnvioCorreoVO obtenerUsuarioPorNick(String nick) 
			throws ControlException;
	
	public OrdersTableVo obtenerPedidoPorId(Long idOrder)
			throws ControlException;
	
	public String obtenerEmailGerente(String idClient, String idProfile) 
			throws ControlException;
}
