package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

public interface ILoginController {
	
	public UsuarioSesionVO validateLogin(String user, String pass) 
			throws ControlException;
	
	public List<ProductTableVo> getProductsToCatalog() throws ControlException;

}
