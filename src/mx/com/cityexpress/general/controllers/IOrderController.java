package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.FechasVo;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mail.sender.MailerVo;

public interface IOrderController {
	
	public List<ProductTableVo> getProductsToCatalog() throws ControlException; 
	
	public String insertOrder(List<ProductTableVo> productList, 
			UsuarioSesionVO usuarioSesion) throws ControlException;
	
	public List<DetailOrderTableVo> getOrderDetail(String norder) throws 
			ControlException;
	
	public boolean sendMail(MailerVo mailerVo) throws ControlException;
	
	public String getClientName(String idClient) throws ControlException;
	
	public FechasVo obtenerFechasPedidoPorNumPed(String numOrder)
			throws ControlException;
	
	public String obtenerEmailGerente(String idClient, String idProfile) 
			throws ControlException;

}
