package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

public interface IReporteController {
	
	public List<RepProductoVo> obtenerReporteProductos(BusquedaFiltroVO filtroVo)
			throws ControlException;
	
	public List<Client> obtenerClientes() throws ControlException;

}
