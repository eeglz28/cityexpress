package mx.com.cityexpress.general.controllers;

import java.util.List;

import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;
import mx.com.cityexpress.modules.admon.vo.ProductosVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuariosTblVO;

public interface IAdministraController {
	
	public List<ProductosVO> obtieneTodosProductos() throws ControlException;
	
	public String creaProducto(ProductoInputVO productoVO) 
			throws ControlException;
	
	public String actualizaProducto(ProductoInputVO productoVO) 
			throws ControlException;
	
	public List<ClientesTblVO> obtieneTodosClientes() throws ControlException;
	
	public String creaCliente(ClienteInputVO clienteVO) 
			throws ControlException;
	
	public String actualizaCliente(ClienteInputVO clienteVO) 
			throws ControlException;
	
	public List<UsuariosTblVO> obtieneTodosUsuarios(Long idClient) 
			throws ControlException;
	
	public String creaUsuario(UsuarioInputVO usuarioVO) 
			throws ControlException;
	
	public String actualizaUsuario(UsuarioInputVO usuarioVO) 
			throws ControlException;
	
	public String obtieneNombreClientePorId(String idCliente) 
			throws ControlException;
	
	public List<Profile> obtienePerfiles(String val) throws ControlException;
	
	public List<ProfileVo> retrieveProfileByType(String type) 
			throws ControlException;
	
	public List<ClientVo> getOnlyClients() throws ControlException;
	
	public String sendMasiveMail(String[] clientIdList, String[] profileIdList, 
			String body, String subject) throws ControlException;

}
