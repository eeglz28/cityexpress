package mx.com.cityexpress.general.controllers.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.IOperationController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.modules.consultas.services.IConsultaService;

public class OperationControllerImpl implements IOperationController {

	private Log log = LogFactory.getLog(OperationControllerImpl.class);

	private IConsultaService consultaService;

	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo)
			throws ControlException {
		log.info(":::...[C] obtenerPedidos OK");
		
		try {
			return consultaService.obtenerPedidos(busquedaVo);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder)
			throws ControlException {
		log.info(":::...[C] obtenerDetallePedido OK");
		
		try {
			return consultaService.obtenerDetallePedido(idOrder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	

	/**
	 * @return the consultaService
	 */
	public IConsultaService getConsultaService() {
		return consultaService;
	}

	/**
	 * @param consultaService the consultaService to set
	 */
	public void setConsultaService(IConsultaService consultaService) {
		this.consultaService = consultaService;
	}
}
