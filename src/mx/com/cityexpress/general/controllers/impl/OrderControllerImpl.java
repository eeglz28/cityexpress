package mx.com.cityexpress.general.controllers.impl;

import java.util.List;


//import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.IOrderController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.FechasVo;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mail.sender.MailerSender;
import mx.com.cityexpress.mail.sender.MailerVo;
import mx.com.cityexpress.modules.order.services.IOrderService;

public class OrderControllerImpl implements IOrderController {
	
	private Log log = LogFactory.getLog(OrderControllerImpl.class);
	
	private IOrderService orderService;
	
	private MailerSender mailer;

	public List<ProductTableVo> getProductsToCatalog() throws ControlException {
		log.info(":::...[C]: getProductsToCatalog OK");
		 
		try {
			return orderService.getProductsToCatalog();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public String insertOrder(List<ProductTableVo> productList, 
			UsuarioSesionVO usuarioSesion) throws ControlException {
		log.info(":::...[C]: insertOrder OK");
		
		try {
			return orderService.insertOrder(productList, usuarioSesion);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public List<DetailOrderTableVo> getOrderDetail(String norder) throws 
			ControlException {
		log.info(":::...[C]: getOrderDetail OK");
		
		try {
			return orderService.getOrderDetail(norder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public boolean sendMail(MailerVo mailerVo) throws ControlException {
		log.info(":::...[C]: sendMail OK");
		
		try {	
			mailer.send(mailerVo);
			
			return true;
		} catch (ControlException e) {
			log.error(":::...[C] sendMail: " + e.getMessage());
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public String getClientName(String idClient) throws ControlException {
		log.info(":::...[C] getClientName OK");
		
		try {
			return orderService.getClientName(idClient);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public FechasVo obtenerFechasPedidoPorNumPed(String numOrder)
			throws ControlException {
		log.info(":::...[C] obtenerFechasPedidoPorNumPed OK");
		
		try {
			return orderService.obtenerFechasPedidoPorNumPed(numOrder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public String obtenerEmailGerente(String idClient, String idProfile) 
			throws ControlException {
		log.info(":::...[C] obtenerEmailGerente OK");
		
		try {
			return orderService.obtenerEmailGerente(idClient, idProfile);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
//	private String createBody(String order, String user, 
//			List<DetailOrderTableVo> list, String solicitante) {
//		
//		log.info(":::...[CONTROLLER]: IN createBody OK");
//		
//		BigDecimal tot = new BigDecimal(0);
//		for(DetailOrderTableVo vo : list) {
//			tot = tot.add(vo.getSubtotal());
//		}
//		
//		String s = "Se ha generado un nuevo pedido para Publione. \n";
//		s = s.concat("El detalle del pedido es el siguiente: \n");
//		s = s.concat("\n");
//		s = s.concat("  Número de pedido: " + order + "\n");
//		s = s.concat("  Monto Total: " + "$ "+ tot.toString() + "\n");
//		s = s.concat("  Usuario: " + user + "\n");
//		s = s.concat("  Solicitante: " + solicitante + "\n");
//		s = s.concat("\n");
//		
//		s = s.concat("  "+StringUtils.rightPad("CANTIDAD", 10, " ") + 
//				StringUtils.rightPad("PRODUCTO", 30, " ") +
//				StringUtils.rightPad("PRECIO", 8, " ") +
//				StringUtils.rightPad("MONTO", 8, " ") + "\n");
//		
//		for(DetailOrderTableVo detail : list) {
//			s = s.concat("  "+ StringUtils.rightPad(String.valueOf(detail.getQuantity()), 10, " ") +
//					StringUtils.rightPad(detail.getProduct(), 30, " ") +
//					StringUtils.rightPad(String.valueOf(detail.getPrice()), 8, " ") +
//					StringUtils.rightPad(String.valueOf(detail.getSubtotal()), 8, " ") +"\n");
//		}
//		
//		s = s.concat("\n");
//		s = s.concat("\n");
//		s = s.concat("Si necesita un tiempo de entrega mas corto por favor solicitelo y de ser posible se les informará la disponibilidad. \n");
//		s = s.concat("\n");
//		s = s.concat("Por su atención, gracias.\n");
//		s = s.concat("\n");
//		s = s.concat("\n");
//		s = s.concat(" Impulsora Publione Tecnologia e Información\n");
//		s = s.concat("Lázaro Cárdenas #05 Int. 102 SN Lucas Tepetlacalco\n");
//		s = s.concat("Tlalnepantla, Estado de México C.P. 54055\n");
//		s = s.concat("Tel. 01 (55)5236 4002\n");
//		
//		log.info(":::...[CONTROLLER]: OUT createBody OK");
//		
//		return s;
//	}
//	
//	
//	
//	public static void main(String args[]) {
//		
//		List<DetailOrderTableVo> list = new ArrayList<DetailOrderTableVo>();
//		DetailOrderTableVo vo = new DetailOrderTableVo();
//		vo.setProduct("Carretas");
//		vo.setQuantity(new BigDecimal("100"));
//		vo.setPrice(new BigDecimal(2));
//		vo.setSubtotal(new BigDecimal(200));
//		list.add(vo);
//		
//		vo = new DetailOrderTableVo();
//		vo.setProduct("Ruedas");
//		vo.setQuantity(new BigDecimal("200"));
//		vo.setPrice(new BigDecimal(1));
//		vo.setSubtotal(new BigDecimal(200));
//		list.add(vo);
//		
//		vo = new DetailOrderTableVo();
//		vo.setProduct("Motores");
//		vo.setQuantity(new BigDecimal("100"));
//		vo.setPrice(new BigDecimal(4));
//		vo.setSubtotal(new BigDecimal(400));
//		list.add(vo);
//		
//		String s = "Se ha generado un nuevo pedido para Publione. \n";
//		s = s.concat("El detalle del pedido es el siguiente: \n");
//		s = s.concat("\n");
//		s = s.concat("  Número de pedido: " + "4" + "\n");
//		s = s.concat("  Usuario: " + "Pacocop" + "\n");
//		s = s.concat("  Monto Total: " + "$ 1500" + "\n");
//		s = s.concat("\n");
//		s = s.concat("  "+StringUtils.rightPad("CANTIDAD", 10, " ") + 
//				StringUtils.rightPad("PRODUCTO", 30, " ") +
//				StringUtils.rightPad("PRECIO", 8, " ") +
//				StringUtils.rightPad("MONTO", 8, " ") + "\n");
//		for(DetailOrderTableVo detail : list) {
//			s = s.concat("  "+ StringUtils.rightPad(String.valueOf(detail.getQuantity()), 10, " ") +
//					StringUtils.rightPad(detail.getProduct(), 30, " ") +
//					StringUtils.rightPad(String.valueOf(detail.getPrice()), 8, " ") +
//					StringUtils.rightPad(String.valueOf(detail.getSubtotal()), 8, " ") +"\n");
//		}
//		
//		s = s.concat("\n");
//		s = s.concat("\n");
//		s = s.concat("Si necesita un tiempo de entrega mas corto por favor solicitelo y de ser posible se les informará la disponibilidad. \n");
//		s = s.concat("\n");
//		s = s.concat("Por su atención, gracias.\n");
//		s = s.concat("\n");
//		s = s.concat("\n");
//		s = s.concat(" Impulsora Publione Tecnologia e Información\n");
//		s = s.concat("Lázaro Cárdenas #05 Int. 102 SN Lucas Tepetlacalco\n");
//		s = s.concat("Tlalnepantla, Estado de México C.P. 54055\n");
//		s = s.concat("Tel. 01 (55)5236 4002\n");
//		 	
//		System.out.println(s);
//		
//	}
	

	/**
	 * @return the orderService
	 */
	public IOrderService getOrderService() {
		return orderService;
	}

	/**
	 * @param orderService the orderService to set
	 */
	public void setOrderService(IOrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * @return the mailer
	 */
	public MailerSender getMailer() {
		return mailer;
	}

	/**
	 * @param mailer the mailer to set
	 */
	public void setMailer(MailerSender mailer) {
		this.mailer = mailer;
	}



}
