package mx.com.cityexpress.general.controllers.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.ILoginController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.login.services.ILoginService;

public class LoginControllerImpl implements ILoginController {
	
	private ILoginService loginService;
	
	private Log log = LogFactory.getLog(LoginControllerImpl.class);

	public UsuarioSesionVO validateLogin(String user, String pass)
			throws ControlException {
		log.info(":::...[C] validateLogin OK");
		
		try {
			return loginService.validateLogin(user, pass);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<ProductTableVo> getProductsToCatalog() throws ControlException {
		log.info(":::...[C] getProductsToCatalog OK");
		
		try {
			return loginService.getProductsToCatalog();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	

	/**
	 * @return the loginService
	 */
	public ILoginService getLoginService() {
		return loginService;
	}

	/**
	 * @param loginService the loginService to set
	 */
	public void setLoginService(ILoginService loginService) {
		this.loginService = loginService;
	}



}
