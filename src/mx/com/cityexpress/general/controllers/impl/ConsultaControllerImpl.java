package mx.com.cityexpress.general.controllers.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.ClientTableVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.mail.sender.MailerSender;
import mx.com.cityexpress.mail.sender.MailerVo;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.consultas.services.IConsultaService;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;
import mx.com.cityexpress.modules.order.services.IOrderService;

public class ConsultaControllerImpl implements IConsultaController {
	
	private Log log = LogFactory.getLog(ConsultaControllerImpl.class);
	
	private IConsultaService consultaService;
	private IOrderService orderService;
	private MailerSender mailer;

	public List<Client> obtenerClientes() throws ControlException {
		log.info(":::...[C] obtenerClientes OK");
		
		try {
			return consultaService.obtenerClientes();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}


	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo)
			throws ControlException {
		log.info(":::...[C] obtenerPedidos OK");
		
		try {
			return consultaService.obtenerPedidos(busquedaVo);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public List<OrdersTableVo> obtenerPedidosUsuarios(BusquedaPedidoVo 
			busquedaVo) throws ControlException {
		log.info(":::...[C] obtenerPedidosUsuarios OK");
		
		try {
			return consultaService.obtenerPedidosUsuarios(busquedaVo);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	

	public List<ClientTableVo> obtenerClientePorId(long idCliente)
			throws ControlException {
		log.info(":::...[C] obtenerClientePorId OK");
		
		try {
			return consultaService.obtenerClientePorId(idCliente);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}

	public void actualizarEstatusPedido(InputPedidoVO inputVO) 
			throws ControlException {
		log.info(":::...[C] actualizarEstatusPedido OK");
		
		try {
			consultaService.actualizarEstatusPedido(inputVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}

	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder)
			throws ControlException {
		log.info(":::...[C] obtenerDetallePedido OK");
		try {
			return consultaService.obtenerDetallePedido(idOrder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public List<DetailOrderTableVo> getOrderDetail(String norder) throws 
			ControlException {
		log.info(":::...[C]: getOrderDetail OK");
		
		try {
			return orderService.getOrderDetail(norder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public boolean sendMail(MailerVo mailerVo) throws ControlException {
		log.info(":::...[C]: sendMail OK");
		
		try {	
			mailer.send(mailerVo);
			
			return true;
		} catch (ControlException e) {
			log.error(":::...[C] sendMail: " + e.getMessage());
			throw new ControlException(e.getMessage());
		}
	}
	
	public String getClientName(String idClient) throws ControlException {
		log.info(":::...[C] getClientName OK");
		
		try {
			return orderService.getClientName(idClient);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public UsuarioEnvioCorreoVO obtenerUsuarioPorNick(String nick) 
			throws ControlException {
			log.info(":::...[C] obtenerUsuarioPorNick OK");
		
		try {
			return consultaService.obtenerUsuarioPorNick(nick);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public OrdersTableVo obtenerPedidoPorId(Long idOrder)
			throws ControlException {
		log.info(":::...[C] obtenerPedidoPorId OK");
		
		try {
			return consultaService.obtenerPedidoPorId(idOrder);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public String obtenerEmailGerente(String idClient, String idProfile) 
			throws ControlException {
		log.info(":::...[C] obtenerEmailGerente OK");
		
		try {
			return orderService.obtenerEmailGerente(idClient, idProfile);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public IConsultaService getConsultaService() {
		return consultaService;
	}

	public void setConsultaService(IConsultaService consultaService) {
		this.consultaService = consultaService;
	}


	/**
	 * @return the orderService
	 */
	public IOrderService getOrderService() {
		return orderService;
	}


	/**
	 * @param orderService the orderService to set
	 */
	public void setOrderService(IOrderService orderService) {
		this.orderService = orderService;
	}


	/**
	 * @return the mailer
	 */
	public MailerSender getMailer() {
		return mailer;
	}


	/**
	 * @param mailer the mailer to set
	 */
	public void setMailer(MailerSender mailer) {
		this.mailer = mailer;
	}

}
