package mx.com.cityexpress.general.controllers.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.modules.admon.services.IAdministraService;
import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;
import mx.com.cityexpress.modules.admon.vo.ProductosVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuariosTblVO;

public class AdministraControllerImpl implements IAdministraController {
	
	private Log log = LogFactory.getLog(getClass());
	
	private IAdministraService administraService;
	
	
	public List<ProductosVO> obtieneTodosProductos() throws ControlException { 
		log.info(":::...[C] obtieneTodosProductos OK ");
		try {
			return administraService.obtieneTodosProductos();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public String creaProducto(ProductoInputVO productoVO) 
			throws ControlException {
		log.info(":::...[C] creaProductoNuevo OK ");
		try {
			return administraService.creaProducto(productoVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public String actualizaProducto(ProductoInputVO productoVO) 
			throws ControlException {
		log.info(":::...[C] actualizaProductoNuevo OK ");
		try {
			return administraService.actualizaProducto(productoVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<ClientesTblVO> obtieneTodosClientes() throws ControlException {
		log.info(":::...[C] obtieneTodosClientes OK ");
		try {
			return administraService.obtieneTodosClientes();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public String creaCliente(ClienteInputVO clienteVO) 
			throws ControlException {
		log.info(":::...[C] creaCliente OK ");
		try {
			return administraService.creaCliente(clienteVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
			
			
	public String actualizaCliente(ClienteInputVO clienteVO) 
			throws ControlException {
		log.info(":::...[C] actualizaCliente OK ");
		try {
			return administraService.actualizaCliente(clienteVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public List<UsuariosTblVO> obtieneTodosUsuarios(Long idClient) 
			throws ControlException {
		log.info(":::...[C] obtieneTodosUsuarios OK ");
		try {
			return administraService.obtieneTodosUsuarios(idClient);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public String creaUsuario(UsuarioInputVO usuarioVO) 
			throws ControlException {
		log.info(":::...[C] creaUsuario OK ");
		try {
			return administraService.creaUsuario(usuarioVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}

	
	public String actualizaUsuario(UsuarioInputVO usuarioVO) 
			throws ControlException {
		log.info(":::...[C] actualizaUsuario OK ");
		try {
			return administraService.actualizaUsuario(usuarioVO);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public String obtieneNombreClientePorId(String idCliente) 
			throws ControlException {
		log.info(":::...[C] obtieneNombreClientePorId OK ");
		try {
			return administraService.obtieneNombreClientePorId(idCliente);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<Profile> obtienePerfiles(String val) throws ControlException{
		log.info(":::...[C] obtieneNombreClientePorId OK ");
		try {
			return administraService.obtienePerfiles(val);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<ProfileVo> retrieveProfileByType(String type) 
			throws ControlException {
		log.info(":::...[C] retrieveProfileByType OK ");
		try {
			return administraService.retrieveProfileByType(type);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<ClientVo> getOnlyClients() throws ControlException {
		log.info(":::...[C] getOnlyClients OK ");
		try {
			return administraService.getOnlyClients();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	
	public String sendMasiveMail(String[] clientIdList, String[] profileIdList, 
			String body, String subject) throws ControlException {
		log.info(":::...[C] sendMasiveMail OK ");
		try {
			return administraService.sendMasiveMail(clientIdList, 
					profileIdList, body, subject);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	/**
	 * @return the administraService
	 */
	public IAdministraService getAdministraService() {
		return administraService;
	}

	/**
	 * @param administraService the administraService to set
	 */
	public void setAdministraService(IAdministraService administraService) {
		this.administraService = administraService;
	}

}
