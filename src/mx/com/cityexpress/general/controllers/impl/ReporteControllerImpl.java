package mx.com.cityexpress.general.controllers.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.controllers.IReporteController;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.reportes.services.IReporteService;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

public class ReporteControllerImpl implements IReporteController {
	
	private Log log = LogFactory.getLog(ReporteControllerImpl.class);
	
	private IReporteService reporteService;
	

	public List<RepProductoVo> obtenerReporteProductos(BusquedaFiltroVO filtroVo)
			throws ControlException {
		log.info(":::...[C]: obtenerReporteProductos OK");
		 
		try {
			return reporteService.obtenerReporteProductos(filtroVo);
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	public List<Client> obtenerClientes() throws ControlException {
		log.info(":::...[C]: obtenerClientes OK");
		 
		try {
			return reporteService.obtenerClientes();
		} catch(BusinessException e) {
			throw new ControlException(e.getMessage());
		}
	}
	
	


	/**
	 * @return the reporteService
	 */
	public IReporteService getReporteService() {
		return reporteService;
	}


	/**
	 * @param reporteService the reporteService to set
	 */
	public void setReporteService(IReporteService reporteService) {
		this.reporteService = reporteService;
	}

}
