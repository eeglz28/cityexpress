package mx.com.cityexpress.general.vo;

public class ClientVo {
	
	private int idClient;
	private String name;
	private String contact;
	private String address;
	private String phone;
	
	/**
	 * @param idClient
	 */
	public ClientVo(int idClient) {
		super();
		this.idClient = idClient;
	}

	/**
	 * @param idClient
	 * @param name
	 */
	public ClientVo(int idClient, String name) {
		super();
		this.idClient = idClient;
		this.name = name;
	}

	/**
	 * @return the idClient
	 */
	public int getIdClient() {
		return idClient;
	}

	/**
	 * @param idClient the idClient to set
	 */
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClientVo [idClient=" + idClient + ", name=" + name
				+ ", contact=" + contact + ", address=" + address + ", phone="
				+ phone + "]";
	}
	
	

}
