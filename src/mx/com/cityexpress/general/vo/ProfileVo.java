package mx.com.cityexpress.general.vo;

public class ProfileVo {
	
	// Identifier for the profile
	private String idProfile;
	
	// General profile description
	private String description;
	
	// Type of the profile
	private String typeId;
	
	// Description of the profile type
	private String typeDescription;
	
	
	/**
	 * 
	 */
	public ProfileVo() {
		super();
	}


	/**
	 * @param description
	 * @param typeId
	 * @param typeDescription
	 */
	public ProfileVo(String idProfile, String description, String typeId,
			String typeDescription) {
		super();
		this.idProfile = idProfile;
		this.description = description;
		this.typeId = typeId;
		this.typeDescription = typeDescription;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}


	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}


	/**
	 * @return the typeDescription
	 */
	public String getTypeDescription() {
		if (typeId != null) {
			if (typeId.equals("I")) {
				typeDescription = "Interno";
			} else if (typeId.equals("E")) {
				typeDescription = "Externo";
			}
		}
		
		return typeDescription;
	}


	/**
	 * @param typeDescription the typeDescription to set
	 */
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProfileVo [description=" + description + ", typeId=" + typeId
				+ ", typeDescription=" + typeDescription + "]";
	}


	/**
	 * @return the idProfile
	 */
	public String getIdProfile() {
		return idProfile;
	}


	/**
	 * @param idProfile the idProfile to set
	 */
	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}
	
	

}
