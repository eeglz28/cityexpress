package mx.com.cityexpress.general.vo;

public class UsuarioEnvioCorreoVO {

	private String nombre;
	private String email;
	private String idCliente;
	private String idPerfil;

	
	
	/**
	 * 
	 */
	public UsuarioEnvioCorreoVO() {
		super();
	}


	/**
	 * @param nombre
	 * @param email
	 * @param fecha
	 * @param entrega
	 */
	public UsuarioEnvioCorreoVO(String nombre, String email, String idCliente, 
			String idPerfil) {
		super();
		this.nombre = nombre;
		this.email = email;
		this.idCliente = idCliente;
		this.idPerfil = idPerfil;
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the idPerfil
	 */
	public String getIdPerfil() {
		return idPerfil;
	}


	/**
	 * @param idPerfil the idPerfil to set
	 */
	public void setIdPerfil(String idPerfil) {
		this.idPerfil = idPerfil;
	}


	
}
