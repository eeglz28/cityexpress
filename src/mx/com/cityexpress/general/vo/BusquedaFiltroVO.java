package mx.com.cityexpress.general.vo;

public class BusquedaFiltroVO {
	
	private String fechaInicial;
	private String fechaFinal;
	private String estatus;
	private String idCliente;
	
	/**
	 * 
	 */
	public BusquedaFiltroVO() {
		super();
	}
	/**
	 * @param fechaInicial
	 * @param fechaFinal
	 * @param estatus
	 */
	public BusquedaFiltroVO(String fechaInicial, String fechaFinal,
			String estatus, String idCliente) {
		super();
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.estatus = estatus;
		this.idCliente = idCliente;
	}
	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}
	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}
	
	

}
