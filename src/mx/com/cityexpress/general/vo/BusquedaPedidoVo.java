package mx.com.cityexpress.general.vo;

public class BusquedaPedidoVo {
	
	private String cliente;
	private String numPedido;
	private String fechaInicial;
	private String fechaFinal;
	private String estatus;
	private String idUsuario;
	private String idPerfil;
	
	
	public BusquedaPedidoVo() {
		super();
	}


	public BusquedaPedidoVo(String cliente, String numPedido,
			String fechaInicial, String fechaFinal, String estatus,
			String idUsuario) {
		super();
		this.cliente = cliente;
		this.numPedido = numPedido;
		this.fechaInicial = fechaInicial;
		this.fechaFinal = fechaFinal;
		this.estatus = estatus;
		this.idUsuario = idUsuario;
	}


	public String getCliente() {
		return cliente;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	public String getNumPedido() {
		return numPedido;
	}


	public void setNumPedido(String numPedido) {
		this.numPedido = numPedido;
	}


	public String getFechaInicial() {
		return fechaInicial;
	}


	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}


	public String getFechaFinal() {
		return fechaFinal;
	}


	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}


	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the idPerfil
	 */
	public String getIdPerfil() {
		return idPerfil;
	}


	/**
	 * @param idPerfil the idPerfil to set
	 */
	public void setIdPerfil(String idPerfil) {
		this.idPerfil = idPerfil;
	}
	
	
	
	
}
