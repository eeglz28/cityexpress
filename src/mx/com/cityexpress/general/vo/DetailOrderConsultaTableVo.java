package mx.com.cityexpress.general.vo;

import java.math.BigDecimal;

public class DetailOrderConsultaTableVo {

	private Long idProduct;
	private String producto;
	private BigDecimal cantidad;
	private String unidad;
	private BigDecimal precio;
	private BigDecimal subtotal;
	private String folio;
	
	public DetailOrderConsultaTableVo() {
		super();
	}

	public DetailOrderConsultaTableVo(Long idProduct, String producto, BigDecimal cantidad,
			String unidad, BigDecimal precio, BigDecimal subtotal, String folio) {
		super();
		this.idProduct = idProduct;
		this.producto = producto;
		this.cantidad = cantidad;
		this.unidad = unidad;
		this.precio = precio;
		this.subtotal = subtotal;
		this.folio = folio;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}
	
	
}
