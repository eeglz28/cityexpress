package mx.com.cityexpress.general.vo;

import java.math.BigDecimal;

public class DetailOrderTableVo {
	
	private String product;
	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal subtotal; 
	private String folios;
	private String unidad;
	
	
	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}
	
	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}
	
	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}
	
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * @return the folios
	 */
	public String getFolios() {
		return folios;
	}

	/**
	 * @param folios the folios to set
	 */
	public void setFolios(String folios) {
		this.folios = folios;
	}

	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	
	
	
	

}
