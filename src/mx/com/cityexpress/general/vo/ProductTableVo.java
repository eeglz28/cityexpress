package mx.com.cityexpress.general.vo;

import java.math.BigDecimal;

import mx.com.cityexpress.mapping.pojo.Product;

public class ProductTableVo {

	private Product product;
	private BigDecimal subtotal;
	private String quantity;
	private String comment;
	
	/**
	 * 
	 */
	public ProductTableVo() {
		super();
	}

	/**
	 * @param product
	 * @param enabled
	 * @param quantity
	 */
	public ProductTableVo(Product product, BigDecimal subtotal, String quantity,
			String comment) {
		super();
		this.product = product;
		this.subtotal = subtotal;
		this.quantity = quantity;
		this.comment = comment;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}


	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	

	/**
	 * @return the subtotal
	 */
	public BigDecimal getSubtotal() {
		return subtotal;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	
	public String toString() {
		return "PRODUCT: ".concat(product.toString()).
				concat("   SUBTOTAL: ").concat(String.valueOf(subtotal)).
				concat("   QUANTITY:  ").concat(String.valueOf(quantity));
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
