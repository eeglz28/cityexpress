package mx.com.cityexpress.general.vo;

import java.math.BigDecimal;

public class OrdersTableVo {
	
	private Long idOrder;
	private String numOrder;
	private String creationDate;
	private BigDecimal total;
	private String nick;
	private String clientName;
	private Long idClient;
	private String estatus;
	private String descEstatus;
	private String guia;
	private boolean estatusRendered;
	private String comments;
	private String entrega;
	private String nombreUsuario;
	
	
	public OrdersTableVo() {
		super();
	}


	public OrdersTableVo(Long idOrder, String numOrder, String creationDate,
			BigDecimal total, String nick, String clientName, Long idClient,
			String estatus, boolean estatusRendered, String descEstatus, 
			String guia, String comments) {
		super();
		this.idOrder = idOrder;
		this.numOrder = numOrder;
		this.creationDate = creationDate;
		this.total = total;
		this.nick = nick;
		this.clientName = clientName;
		this.idClient = idClient;
		this.estatus = estatus;
		this.estatusRendered = estatusRendered;
		this.descEstatus = descEstatus;
		this.guia = guia;
		this.comments = comments;
	}



	public Long getIdOrder() {
		return idOrder;
	}


	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}


	public String getNumOrder() {
		return numOrder;
	}


	public void setNumOrder(String numOrder) {
		this.numOrder = numOrder;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}


	public String getNick() {
		return nick;
	}


	public void setNick(String nick) {
		this.nick = nick;
	}


	public String getClientName() {
		return clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Long getIdClient() {
		return idClient;
	}


	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public boolean isEstatusRendered() {
		return estatusRendered;
	}


	public void setEstatusRendered(boolean estatusRendered) {
		this.estatusRendered = estatusRendered;
	}


	public String getDescEstatus() {
		return descEstatus;
	}


	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}


	/**
	 * @return the guia
	 */
	public String getGuia() {
		return guia;
	}


	/**
	 * @param guia the guia to set
	 */
	public void setGuia(String guia) {
		this.guia = guia;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}


	/**
	 * @return the entrega
	 */
	public String getEntrega() {
		return entrega;
	}


	/**
	 * @param entrega the entrega to set
	 */
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}


	/**
	 * @return the nombreUsuario
	 */
	public String getNombreUsuario() {
		return nombreUsuario;
	}


	/**
	 * @param nombreUsuario the nombreUsuario to set
	 */
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}







	
	

}
