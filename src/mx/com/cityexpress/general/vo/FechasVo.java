package mx.com.cityexpress.general.vo;

public class FechasVo {
	
	private String creacion;
	private String entrega;
	
	
	/**
	 * 
	 */
	public FechasVo() {
		super();
	}


	/**
	 * @param creacion
	 * @param entrega
	 */
	public FechasVo(String creacion, String entrega) {
		super();
		this.creacion = creacion;
		this.entrega = entrega;
	}


	/**
	 * @return the creacion
	 */
	public String getCreacion() {
		return creacion;
	}


	/**
	 * @param creacion the creacion to set
	 */
	public void setCreacion(String creacion) {
		this.creacion = creacion;
	}


	/**
	 * @return the entrega
	 */
	public String getEntrega() {
		return entrega;
	}


	/**
	 * @param entrega the entrega to set
	 */
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	
	
	

}
