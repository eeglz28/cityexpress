package mx.com.cityexpress.general.vo;

import java.util.List;

public class MailBodyOrderVo {
	
	public final static String FROM = "pedidos.aldakar@gmail.com";
	
	private String order; 
	private String user;
	private List<DetailOrderTableVo> listDetail; 
	private String solicitante;
	private String fecha_alta;
	private String fecha_entrega;
	private String atencion;
	private String info1;
	private String comentario;
	
	
	/**
	 * 
	 */
	public MailBodyOrderVo() {
		super();
	}


	/**
	 * @param order
	 * @param user
	 * @param destination
	 * @param list
	 * @param solicitante
	 */
	public MailBodyOrderVo(String order, String user,
			List<DetailOrderTableVo> listDetail, String solicitante,
			String info1, String comentario) {
		super();
		this.order = order;
		this.user = user;
		this.listDetail = listDetail;
		this.solicitante = solicitante;
		this.info1 = info1;
		this.comentario = comentario;
	}


	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}


	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}


	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}


	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}


	/**
	 * @return the solicitante
	 */
	public String getSolicitante() {
		return solicitante;
	}


	/**
	 * @param solicitante the solicitante to set
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}


	/**
	 * @return the listDetail
	 */
	public List<DetailOrderTableVo> getListDetail() {
		return listDetail;
	}


	/**
	 * @param listDetail the listDetail to set
	 */
	public void setListDetail(List<DetailOrderTableVo> listDetail) {
		this.listDetail = listDetail;
	}


	/**
	 * @return the fecha_alta
	 */
	public String getFecha_alta() {
		return fecha_alta;
	}


	/**
	 * @param fecha_alta the fecha_alta to set
	 */
	public void setFecha_alta(String fecha_alta) {
		this.fecha_alta = fecha_alta;
	}


	/**
	 * @return the fecha_entrega
	 */
	public String getFecha_entrega() {
		return fecha_entrega;
	}


	/**
	 * @param fecha_entrega the fecha_entrega to set
	 */
	public void setFecha_entrega(String fecha_entrega) {
		this.fecha_entrega = fecha_entrega;
	}


	/**
	 * @return the atencion
	 */
	public String getAtencion() {
		return atencion;
	}


	/**
	 * @param atencion the atencion to set
	 */
	public void setAtencion(String atencion) {
		this.atencion = atencion;
	}


	/**
	 * @return the info1
	 */
	public String getInfo1() {
		return info1;
	}


	/**
	 * @param info1 the info1 to set
	 */
	public void setInfo1(String info1) {
		this.info1 = info1;
	}


	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}


	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

}
