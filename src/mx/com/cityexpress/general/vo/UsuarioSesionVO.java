package mx.com.cityexpress.general.vo;

public class UsuarioSesionVO {
	
	private String idUsuario;
	private String idCliente;
	private String usuario;
	private String estatus;
	private String email;
	private String name;
	private String perfil;
	
	
	/**
	 * @param idCliente
	 * @param usuario
	 * @param estatus
	 * @param email
	 */
	public UsuarioSesionVO(String idUsuario, String idCliente, String usuario, 
			String estatus, String email, String name, String perfil) {
		super();
		this.idUsuario = idUsuario;
		this.idCliente = idCliente;
		this.usuario = usuario;
		this.estatus = estatus;
		this.email = email;
		this.name = name;
		this.perfil = perfil;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}


	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}


	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the perfil
	 */
	public String getPerfil() {
		return perfil;
	}


	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	
	

}
