package mx.com.cityexpress.general.vo;

public class ClientTableVo {
	
	private String clientName;
	private String telephone;
	private String email;
	
	
	public ClientTableVo() {
		super();
	}


	public ClientTableVo(String clientName, String telephone, String email) {
		super();
		this.clientName = clientName;
		this.telephone = telephone;
		this.email = email;
	}


	public String getClientName() {
		return clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	
	
}
