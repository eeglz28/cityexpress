package mx.com.cityexpress.general.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class DetOrdToDetailTableTransformer {
	
	private static Log log = LogFactory.getLog(
			DetOrdToDetailTableTransformer.class);

	
	@SuppressWarnings("rawtypes")
	public static List<DetailOrderTableVo> transform(List inputList) 
			throws BusinessException {
		
		List<DetailOrderTableVo> detailList = new 
				ArrayList<DetailOrderTableVo>();
		
		try {
			
			for(Iterator it = inputList.iterator(); it.hasNext();) {
				Object[] obj = (Object[])it.next();
				DetailOrderTableVo vo = new DetailOrderTableVo();
				BigDecimal qt = new BigDecimal( obj[2].toString() );
				vo.setProduct( String.valueOf(obj[1]) );
				vo.setQuantity( new BigDecimal(obj[2].toString()) );
				vo.setPrice( (BigDecimal)obj[3] );
				vo.setSubtotal( qt.multiply(vo.getPrice()) );
				String folios = (String)obj[4] == null ? "": (String)obj[4];
				vo.setFolios(folios);
				vo.setUnidad((String)obj[5]);
				detailList.add(vo);
			}
			
		} catch(Exception e) {
			log.error("Ocurrio un error al obtener " +
					"el detalle del pedido, Contacte al administrador.");
			throw new BusinessException("Ocurrio un error al obtener " +
					"el detalle del pedido, Contacte al administrador.");
		}
		return detailList;
		
	}

}
