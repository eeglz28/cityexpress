package mx.com.cityexpress.general.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.mapping.pojo.Product;

public class ProductToProductTableTransformer {
	
	private static Log log = LogFactory.getLog(
			ProductToProductTableTransformer.class);
	
	public static List<ProductTableVo> transform(List<Product> inputList) 
			throws BusinessException {
		List<ProductTableVo> productsList = new ArrayList<ProductTableVo>(); 
		try {
			for(Product product : inputList) {
				ProductTableVo prodVo = new ProductTableVo();
				prodVo.setProduct(product);
				prodVo.setSubtotal(new BigDecimal(0));
				prodVo.setQuantity("");
				
				productsList.add(prodVo);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
		}
		
		log.info(":::...[TRANSFORMER]: Se obtuvo el catalogo de productos " +
				"para la tabla de pedidos.");
		return productsList;
	}

}
