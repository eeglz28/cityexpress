package mx.com.cityexpress.general.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.modules.reportes.vo.ClienteCantidadVo;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ProductToProductReportTransformer {
	
	private static Log log = LogFactory.getLog(
			ProductToProductReportTransformer.class);
	
	public static List<RepProductoVo> transform(List<Object[]> inputList) 
			throws BusinessException {
		List<RepProductoVo> repProductList = new ArrayList<RepProductoVo>(); 
		try {
			boolean isProdPrimeraVez = true;
			String prodNameAnt = "";
			RepProductoVo repProdVo = null;
			List<ClienteCantidadVo> listCliente = null;
			ClienteCantidadVo clienteCantVo = null;
			BigDecimal total = new BigDecimal("0");
			
			for(Object[] object : inputList) {
				
				if ( isProdPrimeraVez ) {
					repProdVo = new RepProductoVo();
					listCliente = new ArrayList<ClienteCantidadVo>();
					
					repProdVo.setConcepto((String)object[0]);
					
					clienteCantVo = new ClienteCantidadVo(
							(String)object[1],
							(String)object[3],
							((BigDecimal)object[4]).toString(),
							(String)object[2]
							);
					listCliente.add(clienteCantVo);
					
					total = total.add((BigDecimal)object[4]);
					//log.info("...................Total: " + total);
					isProdPrimeraVez = false;
				} else {
					if ( !((String)object[0]).equals(prodNameAnt) ) {
						repProdVo.setClientes(listCliente);
						repProdVo.setTotalProducto(total.toString());
						//log.info("...................Total: " + total);
						total = new BigDecimal("0");
						
						repProductList.add(repProdVo);
						
						repProdVo = new RepProductoVo();
						listCliente = new ArrayList<ClienteCantidadVo>();
						
						repProdVo.setConcepto((String)object[0]);
						
						clienteCantVo = new ClienteCantidadVo(
								(String)object[1],
								(String)object[3],
								((BigDecimal)object[4]).toString(),
								(String)object[2]
								);
						listCliente.add(clienteCantVo);
						total = total.add((BigDecimal)object[4]);
						
					} else {
						
						total = total.add((BigDecimal)object[4]);
						//log.info("...................Total: " + total);
						
						clienteCantVo = new ClienteCantidadVo(
								(String)object[1],
								(String)object[3],
								((BigDecimal)object[4]).toString(),
								(String)object[2]
								);
						listCliente.add(clienteCantVo);
					}	
				}		
				prodNameAnt = (String)object[0];
				
			}
			
			repProdVo.setClientes(listCliente);
			repProdVo.setTotalProducto(total.toString());
			repProductList.add(repProdVo);
			
			
			log.info(":::...[TRANSFORMER]: Se obtuvo el reporte por producto. ");
			return repProductList;
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
		}
	}

}
