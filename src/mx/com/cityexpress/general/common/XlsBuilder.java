package mx.com.cityexpress.general.common;

//import java.util.Iterator;
//import java.util.List;

import java.util.List;

import mx.com.cityexpress.modules.reportes.vo.ClienteCantidadVo;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

public class XlsBuilder {
	
	/**
	 * Método que se encarga de construir el archivo en formato .xls, en base a
	 * la información que se envía como argumento
	 * 
	 * @param redInternaVO
	 * @return
	 */
	public HSSFWorkbook crearXlsReportePedidos(List<RepProductoVo> listPedidos){
		HSSFWorkbook wb = new HSSFWorkbook();
		long r = -1;
		short fontSize1 = 300;
		short fontSize2 = 200;
		try {
			HSSFFont font1 = wb.createFont();
			font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font1.setFontHeight(fontSize1);
			
			HSSFFont font2 = wb.createFont();
			font2.setFontHeight(fontSize2);
			
//			HSSFFont font3 = wb.createFont();
//			font3.setItalic(true);
			
			HSSFFont font4 = wb.createFont();
			font4.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			font4.setFontHeight(fontSize2);
			
			HSSFSheet sheet1 = wb.createSheet();
			sheet1.setColumnWidth((short)0, (short)13000);
			sheet1.setColumnWidth((short)1, (short)12000);
			sheet1.setColumnWidth((short)2, (short)4000);
			sheet1.setColumnWidth((short)3, (short)4000);
			
			
			HSSFCellStyle style1 = wb.createCellStyle();
			style1.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
			style1.setFont(font1);
			style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style1.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
			style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style1.setTopBorderColor(new HSSFColor.BLACK().getIndex());
			style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style1.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style1.setRightBorderColor(new HSSFColor.BLACK().getIndex());
			//style1.setFillForegroundColor(new HSSFColor.WHITE().getIndex());
			//style1.setFillBackgroundColor(new HSSFColor.BLACK().getIndex());
			style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			style1.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT()
					.getIndex());
			
//			HSSFCellStyle style1 = wb.createCellStyle();
//			HSSFFont font1 = wb.createFont();
//			font1.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
//			style1.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
//			style1.setFont(font1);
			
			HSSFCellStyle style2 = wb.createCellStyle();
			style2.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style2.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
			style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style2.setTopBorderColor(new HSSFColor.BLACK().getIndex());
			style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style2.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style2.setRightBorderColor(new HSSFColor.BLACK().getIndex());
			
			HSSFCellStyle style3 = wb.createCellStyle();
			style3.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			style3.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style3.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
			style3.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style3.setTopBorderColor(new HSSFColor.BLACK().getIndex());
			style3.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style3.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style3.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style3.setRightBorderColor(new HSSFColor.BLACK().getIndex());
			style3.setFont(font2);
			
			HSSFCellStyle style4 = wb.createCellStyle();
			style4.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			style4.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style4.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
			style4.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style4.setTopBorderColor(new HSSFColor.BLACK().getIndex());
			style4.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style4.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style4.setFont(font4);
			
			HSSFCellStyle style5 = wb.createCellStyle();
			style5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
			style5.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			style5.setBottomBorderColor(new HSSFColor.BLACK().getIndex());
			style5.setBorderTop(HSSFCellStyle.BORDER_THIN);
			style5.setTopBorderColor(new HSSFColor.BLACK().getIndex());
			style5.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style5.setRightBorderColor(new HSSFColor.BLACK().getIndex());
			style5.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			style5.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style5.setFont(font4);
			
			HSSFCellStyle style6 = wb.createCellStyle();
			style6.setLeftBorderColor(new HSSFColor.BLACK().getIndex());
			style6.setBorderRight(HSSFCellStyle.BORDER_THIN);
			style6.setAlignment(HSSFCellStyle.ALIGN_LEFT);

			
			
			
			/* Datos Generales de la consulta */
			/*
			HSSFRow rowGral_1 = sheet1.createRow((short)++r);
			HSSFCell cG1_0_0 = rowGral_1.createCell((short)0);
			cG1_0_0.setCellValue("NRP: ");
			cG1_0_0.setCellStyle(style2);
			HSSFCell cG1_0_1 = rowGral_1.createCell((short)1);
			cG1_0_1.setCellValue(empresa);
			cG1_0_1.setCellStyle(style1);
			*/
			
			HSSFRow rowHeader = sheet1.createRow((short)++r);
			rowHeader.setHeight((short)500);
			
			HSSFCell c_A1 = rowHeader.createCell((short)0);
			c_A1.setCellValue("CONCEPTO");
			c_A1.setCellStyle(style1);
			
			HSSFCell c_B1 = rowHeader.createCell((short)1);
			c_B1.setCellValue("CLIENTE");
			c_B1.setCellStyle(style1);
			
			HSSFCell c_C1 = rowHeader.createCell((short)2);
			c_C1.setCellValue("UNIDAD");
			c_C1.setCellStyle(style1);
			
			HSSFCell c_D1 = rowHeader.createCell((short)3);
			c_D1.setCellValue("PEDIDO");
			c_D1.setCellStyle(style1);

			HSSFCell c_E1 = rowHeader.createCell((short)4);
			c_E1.setCellValue("TOTAL");
			c_E1.setCellStyle(style1);
			
			
			HSSFRow rowProd = sheet1.createRow((short)++r);
			rowProd.setHeight((short)400);
			
			for (RepProductoVo repProdVo : listPedidos) {
				
				HSSFCell c_Ay = rowProd.createCell((short)0);
				c_Ay.setCellValue(repProdVo.getConcepto());
				c_Ay.setCellStyle(style6);
				
				for (ClienteCantidadVo clientesVo : repProdVo.getClientes()) {
					HSSFCell c_By = rowProd.createCell((short)1);
					c_By.setCellValue(clientesVo.getCliente());
					c_By.setCellStyle(style2);
					
					HSSFCell c_Cy = rowProd.createCell((short)2);
					c_Cy.setCellValue(clientesVo.getUnidad());
					c_Cy.setCellStyle(style2);
					
					HSSFCell c_Dy = rowProd.createCell((short)3);
					c_Dy.setCellValue(clientesVo.getPedido());
					c_Dy.setCellStyle(style3);
					
					HSSFCell c_Ey = rowProd.createCell((short)4);
					c_Ey.setCellValue(clientesVo.getTotal());
					c_Ey.setCellStyle(style3);
					
					rowProd = sheet1.createRow((short)++r);
					rowProd.setHeight((short)400);
				}
				
				HSSFCell c_ABCy = rowProd.createCell((short)0);
				c_ABCy.setCellValue("Total " + repProdVo.getConcepto());
				c_ABCy.setCellStyle(style4);
				sheet1.addMergedRegion(new Region((int)r,(short)0,(int)r,(short)3));
				
				HSSFCell c_Dy = rowProd.createCell((short)4);
				c_Dy.setCellValue(repProdVo.getTotalProducto());
				c_Dy.setCellStyle(style5);
				
				rowProd = sheet1.createRow((short)++r);
				rowProd.setHeight((short)400);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return wb;

	}

}
