/*
 * CatalogoItem.java
 *
 * Created on 12 de julio de 2007, 11:39 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mx.com.cityexpress.general.common;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.model.SelectItem;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.mapping.pojo.Profile;

/**
 *
 * @author ffuentes
 */
public class CatalogoItem {
    

    /**
     * De una lista de los tipos de movimientos,
     * los conjunta en un catalogo
     * @param lista
     * @return
     * @throws BusinessException
     */
    
	public Set<SelectItem[]> getSelectClientes(List<Client> lista) 
			throws BusinessException {
        Set<SelectItem[]> cat = new HashSet<SelectItem[]>();
        SelectItem[] selectItem;
        try{
            if(lista != null && lista.size() > 0){
                int cant = lista.size();
                SelectItem[] selectI = new SelectItem[cant+1];
                SelectItem item = new SelectItem();
                item.setLabel("Seleccione una Opción");
                item.setValue("-1");
                selectI[0] = item;
                int cont = 1;
                
                for (Client obj : lista) {
                	SelectItem item1 = new SelectItem();
                    item1.setLabel(obj.getClientName().trim());
                    item1.setValue(String.valueOf(obj.getIdClient()));
                    selectI[cont] = item1;
                    cont++;
                }
                selectItem = selectI;
                
            } else {
                SelectItem[] selectI = new SelectItem[1];
                SelectItem item = new SelectItem();
                item.setLabel("Seleccione una Opción");
                item.setValue("-1");
                selectI[0] = item;
                selectItem = selectI;
            }
            cat.add(selectItem);
        }catch(Exception e){
            e.printStackTrace();
            throw new BusinessException("Ocurrió un error al crear el catálogo " +
            		"de clientes.");
        }
        return cat;
    }
	
	
	/**
	 * Convierte la lista de perfiles al catalogo de perfiles que se muestra
	 * en la pantalla.
	 * @param lista
	 * @return
	 * @throws BusinessException
	 */
	public Set<SelectItem[]> getSelectPerfiles(List<Profile> lista) 
			throws BusinessException {
        Set<SelectItem[]> cat = new HashSet<SelectItem[]>();
        SelectItem[] selectItem;
        try{
            if(lista != null && lista.size() > 0){
                int cant = lista.size();
                SelectItem[] selectI = new SelectItem[cant+1];
                SelectItem item = new SelectItem();
                item.setLabel("Seleccione una Opción");
                item.setValue("-1");
                selectI[0] = item;
                int cont = 1;
                
                for (Profile obj : lista) {
                	SelectItem item1 = new SelectItem();
                    item1.setLabel(obj.getDescription().trim());
                    item1.setValue(String.valueOf(obj.getIdProfile()));
                    selectI[cont] = item1;
                    cont++;
                }
                selectItem = selectI;
                
            } else {
                SelectItem[] selectI = new SelectItem[1];
                SelectItem item = new SelectItem();
                item.setLabel("Seleccione una Opción");
                item.setValue("-1");
                selectI[0] = item;
                selectItem = selectI;
            }
            cat.add(selectItem);
        }catch(Exception e){
            e.printStackTrace();
            throw new BusinessException("Ocurrió un error al crear el catálogo " +
            		"de perfiles.");
        }
        return cat;
    }
	
	
	public Set<SelectItem[]> getSelectClientesReporte(List<Client> lista) 
			throws BusinessException {
        Set<SelectItem[]> cat = new HashSet<SelectItem[]>();
        SelectItem[] selectItem;
        try{
            if(lista != null && lista.size() > 0){
                int cant = lista.size();
                SelectItem[] selectI = new SelectItem[cant+1];
                SelectItem item = new SelectItem();
                item.setLabel("Todos los Clientes");
                item.setValue("-1");
                selectI[0] = item;
                int cont = 1;
                
                for (Client obj : lista) {
                	SelectItem item1 = new SelectItem();
                    item1.setLabel(obj.getClientName().trim());
                    item1.setValue(String.valueOf(obj.getIdClient()));
                    selectI[cont] = item1;
                    cont++;
                }
                selectItem = selectI;
                
            } else {
                SelectItem[] selectI = new SelectItem[1];
                SelectItem item = new SelectItem();
                item.setLabel("Todos los Clientes");
                item.setValue("-1");
                selectI[0] = item;
                selectItem = selectI;
            }
            cat.add(selectItem);
        }catch(Exception e){
            e.printStackTrace();
            throw new BusinessException("Ocurrió un error al crear el catálogo " +
            		"de clientes.");
        }
        return cat;
    }
	
	
	public Set<SelectItem[]> getSelectClientsMenuList(
			List<ClientVo> clientList) throws BusinessException {
        Set<SelectItem[]> catalogo = new HashSet<SelectItem[]>();
        SelectItem[] selectItem;
        try{
            if(clientList != null && clientList.size() > 0){
                int cant = clientList.size();
                int cont = 0;
                
                SelectItem[] selectI = new SelectItem[cant];
                
                for (ClientVo obj : clientList) {
                	SelectItem item1 = new SelectItem();
                    item1.setLabel(obj.getName());
                    item1.setValue(String.valueOf(obj.getIdClient()));
                    selectI[cont] = item1;
                    cont++;
                }
                selectItem = selectI;
                catalogo.add(selectItem);
                
            }  else {
            	SelectItem[] selectI = new SelectItem[1];
            	SelectItem item1 = new SelectItem();
            	item1.setLabel("");
                item1.setValue("-1");
                selectI[0] = item1;
                selectItem = selectI;
                catalogo.add(selectItem);
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new BusinessException("Ocurrió un error al crear el catálogo " +
            		"de clientes.");
        }
        return catalogo;
    }
	

	public Set<SelectItem[]> getSelectProfilesManyList(
			List<ProfileVo> profileList) throws BusinessException {
        Set<SelectItem[]> catalogo = new HashSet<SelectItem[]>();
        SelectItem[] selectItem;
        try{
            if(profileList != null && profileList.size() > 0){
                int cant = profileList.size();
                int cont = 0;
                
                SelectItem[] selectI = new SelectItem[cant];
                
                for (ProfileVo obj : profileList) {
                	SelectItem item1 = new SelectItem();
                    item1.setLabel(obj.getDescription());
                    item1.setValue(String.valueOf(obj.getIdProfile()));
                    selectI[cont] = item1;
                    cont++;
                }
                selectItem = selectI;
                catalogo.add(selectItem);
                
            }  else {
            	SelectItem[] selectI = new SelectItem[1];
            	SelectItem item1 = new SelectItem();
            	item1.setLabel("");
                item1.setValue("-1");
                selectI[0] = item1;
                selectItem = selectI;
                catalogo.add(selectItem);
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new BusinessException("Ocurrió un error al crear el catálogo " +
            		"de Perfiles.");
        }
        return catalogo;
    }
    
}
