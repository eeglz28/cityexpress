package mx.com.cityexpress.general.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;

public class DataValidator {
	
	public boolean isEmptyText(HtmlInputText input) {
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {
			return true;
		} 
		return false;
	}
	
	public boolean isEmptyText(HtmlInputSecret input) {
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {
			return true;
		} 
		return false;
	}
	
	
	public boolean isNumeric(HtmlInputText input) {
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {	
			return false;
		} else {
			String s = input.getValue().toString().trim();
			Pattern p = Pattern.compile("^[0-9]+$");
			Matcher m = p.matcher(s);
			if (!m .find()) {
				return false;
			}
		}
		return true;
	}
	
	
	public boolean isNumericAndDecimal(HtmlInputText input) {
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {	
			return false;
		} else {
			String s = input.getValue().toString().trim();
			Pattern p = Pattern.compile("^\\d+\\.?\\d+$");
			Matcher m = p.matcher(s);
			if (!m .find()) {
				return false;
			}
		}
		return true;
	}
	
	
	public boolean haveLength(HtmlInputText input, int length) {
		if (isEmptyText(input)) 
			return false;
		
		if (input.getValue().toString().trim().length() < length) 
			return false;
		
		return true;
	}
	
	public boolean haveLength(HtmlInputSecret input, int length) {
		if (isEmptyText(input)) 
			return false;
		
		if (input.getValue().toString().trim().length() < length) 
			return false;
		
		return true;
	}
	
	
	public boolean isValidOption(HtmlSelectOneMenu input) {
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {	
			return false;
		} else {
			if ( input.getValue().toString().equals("-1") ) {
				return false;
			}
		}
		return true;
	} 	
	
	
	public boolean isEmptyText(HtmlInputTextarea input) {
		
		if (input == null || input.getValue() == null || 
				input.getValue().toString().trim().length() < 1) {
			return true;
		} 
		return false;
	}
	
	
	public boolean isEmptyNull(Object obj) {
		if (obj == null || obj.toString().trim().length() < 1 ) {
			return true;
		}
		return false;
	}
	
	
	public boolean equals(HtmlInputSecret input1, HtmlInputSecret input2) {
		return input1.getValue().toString().equals(
				input2.getValue().toString());
	}
	
	
	public static String setEmptyNull(Object obj) {
		return obj == null ? "" : obj.toString().trim();
	}
	

}
