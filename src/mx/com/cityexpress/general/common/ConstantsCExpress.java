package mx.com.cityexpress.general.common;

public class ConstantsCExpress {
	
	// Mensajes de Errores Generales Por Exception
	public static final String ERROR_DATABASE 						= "No. 100100(D): Por favor contacte al Administrador.";
	public static final String ERROR_BUSINESS 						= "No. 200100(S): Por favor contacte al Administrador.";
	public static final String ERROR_BUSINESS_GENERAL 				= "No. 200200(S): Por favor contacte al Administrador.";
	public static final String ERROR_CONTROL 						= "No. 300100(C): Por favor contacte al Administrador.";
	public static final String ERROR_PAGE	 						= "No. 400400(P): Por favor contacte al Administrador.";

	// Login
	public static final String GO_LOGIN_TO_INICIO 					= "goLoginToInicio";
	
	// Creacion de un pedido
	public static final String GO_ORDER_TO_MENU 					= "goOrderToMenu";
	public static final String GO_ORDER_TO_NOSESSION 				= "goOrderToNoSession";
	public static final String GO_ORDER_TO_DETAILORDER 				= "goOrderToDetailOrder";
	public static final String GO_ORDER_TO_FAIL 					= "goOrderToFail";
	public static final String GO_ORDER_TO_FOTO						= "goOrderToFoto";
	public static final String GO_DETAILORDER_TO_ORDER 				= "goDetailOrderToOrder";
	public static final String GO_DETAILORDER_TO_NOSESSION 			= "goDetailOrderToNoSession";
	public static final String GO_DETAILORDER_TO_CONFIRM 			= "goDetailOrderToConfirm";
	public static final String GO_DETAILORDER_TO_FAIL		 		= "goDetailOrderToFail";
	public static final String GO_CONFIRM_TO_MENU 					= "goConfirmToMenu";
	public static final String GO_CONFIRM_TO_NOSESSION 				= "goConfirmToNoSession";
	public static final String GO_CONFIRM_TO_FAIL	 				= "goConfirmToFail";
	
	
	// Consulta de Pedidos del administrador
	public static final String GO_BUSQUEDA_PED_TO_PEDIDOS 			= "goBusquedaPedToPedidos";
	public static final String GO_BUSQUEDA_PED_TO_NOSESSION 		= "goBusquedaPedToNoSession";
	public static final String GO_BUSQUEDA_PED_TO_MENU	 			= "goBusquedaPedToMenu";
	public static final String GO_BUSQUEDA_PED_TO_LOGIN 			= "goBusquedaPedToLogin";
	public static final String GO_BUSQUEDA_PED_TO_FAIL 				= "goBusquedaPedToFail";
	public static final String GO_PEDIDOS_TO_DETALLE_PEDIDO 		= "goPedidosToDetallePedido";
	public static final String GO_PEDIDOS_TO_NOSESSION 				= "goPedidosToNoSession";
	public static final String GO_PEDIDOS_TO_BUSQUEDA_PED 			= "goPedidosToBusquedaPed";
	public static final String GO_PEDIDOS_TO_CAMBIA_ESTATUS			= "goPedidosToCambiaEstatus";
	public static final String GO_PEDIDOS_TO_FAIL		 			= "goPedidosToFail";
	public static final String GO_PEDIDOS_TO_SUCCESS		 		= "goPedidosToSuccess";
	public static final String GO_DETALLE_PEDIDO_TO_PEDIDOS 		= "goDetallePedidoToPedidos";
	public static final String GO_DETALLE_PEDIDO_TO_NOSESSION 		= "goDetallePedidoToNoSession";
	public static final String GO_CAMBIA_ESTATUS_TO_PEDIDOS			= "goCambiaEstatusToPedidos";
	public static final String GO_CAMBIA_ESTATUS_TO_NOSESSION		= "goCambiaEstatusToNoSession";
	public static final String GO_CAMBIA_ESTATUS_TO_SUCCESS			= "goCambiaEstatusToSuccess";
	public static final String GO_CAMBIA_ESTATUS_TO_FAIL			= "goCambiaEstatusToFail";
	
	// Consulta de Pedidos del usuario
	public static final String GO_PEDIDOS_USU_TO_DET_PEDIDOS_USU	= "goPedidosUsuToDetPedidosUsu";
	public static final String GO_PEDIDOS_USU_TO_NOSESSION 			= "goPedidosUsuToNoSession";
	public static final String GO_PEDIDOS_USU_TO_MENU				= "goPedidosUsuToMenu";
	public static final String GO_PEDIDOS_USU_TO_FAIL	 			= "goPedidosUsuToFail";
	public static final String GO_DET_PEDIDOS_USU_TO_PEDIDOS_USU	= "goDetPedidosUsuToPedidosUsu";
	public static final String GO_DET_PEDIDOS_USU_TO_NOSESSION 		= "goDetPedidosUsuToNoSession";
	
	// Administracion de productos
	public static final String GO_PRODUCTOS_TO_CREA_PROD 			= "goProductosToCreaProd";
	public static final String GO_PRODUCTOS_TO_MENU 				= "goProductosToMenu";
	public static final String GO_PRODUCTOS_TO_ACT_PROD 			= "goProductosToActProd";
	public static final String GO_PRODUCTOS_TO_NOSESSION 			= "goProductosToNoSession";
	public static final String GO_PRODUCTOS_TO_FAIL		 			= "goProductosToFail";
	public static final String GO_CREA_PROD_TO_SUCCESS	 			= "goCreaProdToSuccess";
	public static final String GO_CREA_PROD_TO_PRODUCTOS 			= "goCreaProdToProductos";
	public static final String GO_CREA_PROD_TO_NOSESSION 			= "goCreaProdToNoSession";
	public static final String GO_CREA_PROD_TO_FAIL		 			= "goCreaProdToFail";
	public static final String GO_ACT_PROD_TO_SUCCESS	 			= "goActProdToSuccess";
	public static final String GO_ACT_PROD_TO_PRODUCTOS	 			= "goActProdToProductos";
	public static final String GO_ACT_PROD_TO_NOSESSION	 			= "goActProdToNoSession";
	public static final String GO_ACT_PROD_TO_FAIL		 			= "goActProdToFail";
	
	// Administracion de Clientes
	public static final String GO_CLIENTES_TO_CREA_CLIE	 			= "goClientesToCreaClie";
	public static final String GO_CLIENTES_TO_ACT_CLIE	 			= "goClientesToActClie";
	public static final String GO_CLIENTES_TO_MENU		 			= "goClientesToMenu";
	public static final String GO_CLIENTES_TO_USUARIOS	 			= "goClientesToUsuarios";
	public static final String GO_CLIENTES_TO_NOSESSION	 			= "goClientesToNoSession";
	public static final String GO_CLIENTES_TO_FAIL	 				= "goClientesToFail";
	public static final String GO_CREA_CLIE_TO_SUCCESS	 			= "goCreaClieToSuccess";
	public static final String GO_CREA_CLIE_TO_CLIENTES	 			= "goCreaClieToClientes";
	public static final String GO_CREA_CLIE_TO_NOSESSION	 		= "goCreaClieToNoSession";
	public static final String GO_CREA_CLIE_TO_FAIL	 				= "goCreaClieToFail";
	public static final String GO_ACT_CLIE_TO_SUCCESS	 			= "goActClieToSuccess";
	public static final String GO_ACT_CLIE_TO_CLIENTES	 			= "goActClieToClientes";
	public static final String GO_ACT_CLIE_TO_NOSESSION	 			= "goActClieToNoSession";
	public static final String GO_ACT_CLIE_TO_FAIL	 				= "goActClieToFail";
	
	// Administracion de Usuarios
	public static final String GO_USUARIOS_TO_CREA_USUA	 			= "goUsuariosToCreaUsua";
	public static final String GO_USUARIOS_TO_ACT_USUA	 			= "goUsuariosToActUsua";
	public static final String GO_USUARIOS_TO_MENU		 			= "goUsuariosToMenu";
	public static final String GO_USUARIOS_TO_CLIENTES	 			= "goUsuariosToClientes";
	public static final String GO_USUARIOS_TO_NOSESSION	 			= "goUsuariosToNoSession";
	public static final String GO_USUARIOS_TO_FAIL		 			= "goUsuariosToFail";
	public static final String GO_CREA_USUA_TO_SUCCESS	 			= "goCreaUsuaToSuccess";
	public static final String GO_CREA_USUA_TO_USUARIOS	 			= "goCreaUsuaToUsuarios";
	public static final String GO_CREA_USUA_TO_NOSESSION	 		= "goCreaUsuaToNoSession";
	public static final String GO_CREA_USUA_TO_FAIL			 		= "goCreaUsuaToFail";
	public static final String GO_ACT_USUA_TO_SUCCESS	 			= "goActUsuaToSuccess";
	public static final String GO_ACT_USUA_TO_USUARIOS	 			= "goActUsuaToUsuarios";
	public static final String GO_ACT_USUA_TO_NOSESSION	 			= "goActUsuaToNoSession";
	public static final String GO_ACT_USUA_TO_FAIL	 				= "goActUsuaToFail";
	
	public static final String GO_ENVIO_CORREO_TO_MENU		 		= "goEnvioCorreoToMenu";
	public static final String GO_ENVIO_CORREO_TO_NOSESSION	 		= "goEnvioCorreoToNoSession";
	public static final String GO_ENVIO_CORREO_TO_SUCCESS	 		= "goEnvioCorreoToSuccess";
	public static final String GO_ENVIO_CORREO_TO_FAIL	 			= "goEnvioCorreoToFail";
	
	// Reportes
	public static final String GO_REP_PRO_BUS_TO_REP_PRO_RES	 	= "goRepProBusToRepProRes";
	public static final String GO_REP_PRO_BUS_TO_MENU		 		= "goRepProBusToMenu";
	public static final String GO_REP_PRO_BUS_TO_NOSESSION	 		= "goRepProBusToNoSession";
	public static final String GO_REP_PRO_BUS_TO_FAIL		 		= "goRepProBusToFail";
	public static final String GO_REP_PRO_RES_TO_REP_PRO_BUS	 	= "goRepProResToRepProBus";
	public static final String GO_REP_PRO_RES_TO_MENU		 		= "goRepProResToMenu";
	public static final String GO_REP_PRO_RES_TO_NOSESSION	 		= "goRepProResToNoSession";
	public static final String GO_REP_PRO_RES_TO_FAIL		 		= "goRepProResToFail";
	
	
	// Operation
	public static final String GO_PED_EN_PRO_TO_HOJA_SUR	 		= "goPedEnProToHojaSur";
	public static final String GO_PED_EN_PRO_TO_MENU		 		= "goPedEnProToMenu";
	public static final String GO_PED_EN_PRO_TO_NOSESSION	 		= "goPedEnProToNoSession";
	public static final String GO_PED_EN_PRO_TO_FAIL		 		= "goPedEnProToFail";
	
	
	//Basics
	public static final String GO_SUCCESS_TO_MENU 					= "goSuccessToMenu";
	public static final String GO_FAIL_TO_MENU	 					= "goFailToMenu";
	public static final String GO_NOSESSION_TO_LOGIN 				= "goNoSessionToLogin";
	public static final String GO_MENU_TO_LOGIN 					= "goMenuToLogin";
	public static final String GO_REFRESH_PAGE 						= "";
	
	
	// Variables en sesion
	public static final String SESSION_USERS_LOGIN					= "sessionUsersLogin";
	public static final String SESSION_VO							= "sessionVo";
	public static final String SESSION_TBL_VO 						= "sessionTblVo";
	public static final String SESSION_TBL_VO_2 					= "sessionTblVo2";
	public static final String SESSION_ID	 						= "sessionId";
	public static final String SESSION_PRODUCTS	 					= "sessionProducts";
	
	// Variables en request
	public static final String REQUEST_ID_ORDER 					= "requestIdOrder";
	public static final String REQUEST_ID		 					= "requestId";
	public static final String REQUEST_MESSAGE						= "requestMessage";
	public static final String REQUEST_EXCEPTION 					= "requestException";
	
	
	
	public static final String MAIL_FROM 							= "Publione";
	public static final String MAIL_SUBJECT 						= " Solicitud de pedido Publione ";
	public static final String MAIL_SUBJECT_GUIA 					= " Envío de Guía de pedido Publione ";
	public static final String MAIL_ADMIN 							= "inocruz@publione.com.mx";
	public static final String MAIL_CC_1 							= "alberto.navarro@publione.com.mx";
	public static final String MAIL_CC_2 							= "noemi.ortiz@publione.com.mx";
	public static final String MAIL_CC_3 							= "adriana.fuentes@publione.com.mx";
	public static final String MAIL_CC_4 							= "rosalba.mejia@publione.com.mx";
	public static final String MAIL_CC_5 							= "sarai.alonzo@publione.com.mx";
	public static final String MAIL_CC_6 							= "serafin.carreno@publione.com.mx";
	
	
	//Pruebas
//	public static final String MAIL_FROM 							= "Publione";
//	public static final String MAIL_SUBJECT 						= " Solicitud de pedido Publione de Pruebas ";
//	public static final String MAIL_SUBJECT_GUIA 					= " Envío de Guía de pedido Publione de Pruebas ";
//	public static final String MAIL_ADMIN 							= "eeglz28@gmail.com";
//	public static final String MAIL_CC_1 							= "eeglz28@hotmail.com";
//	public static final String MAIL_CC_2 							= "eeglz28@yahoo.com.mx";
//	public static final String MAIL_CC_3 							= "eeglz28@gmail.com";
//	public static final String MAIL_CC_4 							= "eeglz28@gmail.com";
//	public static final String MAIL_CC_5 							= "eeglz28@gmail.com";
//	public static final String MAIL_CC_6 							= "eeglz28@gmail.com";
	
	
	public static final String ESTATUS_EN_PROCESO 					= "En proceso";
	public static final String ESTATUS_ENTREGADO 					= "Entregado";
	public static final String ESTATUS_CERRADO 						= "Cerrado";
	public static final String ESTATUS_PARCIAL 						= "Parcial";
	public static final String ESTATUS_EN_PROCESO_NUM 				= "1";
	public static final String ESTATUS_ENTREGADO_NUM 				= "2";
	public static final String ESTATUS_CERRADO_NUM 					= "3";
	public static final String ESTATUS_PARCIAL_NUM 					= "4";
	
	//Estatus PRODUCTO
	public static final String ESTATUS_ACTIVADO 					= "0";
	public static final String ESTATUS_DESACTIVADO 					= "1";
	
	
	public static final String URL_REDIRECT_NOSESSION				= "http://aldakar.com/cityexpress/faces/production/login/login.jsp";
	
	
	// Mensajes
	public static final String MSG_ORDER_OK 						= "El detalle del pedido se ha enviado a su dirección de correo. Le confirmaremos su pedido a la brevedad. Le agradecemos de antemano.";
	public static final String MSG_ORDER_MAIL_FAILED 				= "El detalle del pedido no se pudo enviar por correo. Contacte al administrador del sistema para que le envíe el detalle a su correo. Le agradecemos de antemano.";
	public static final String MSG_RESEND_MAIL_OK 					= "El detalle del pedido se ha enviado a la dirección de correo registrada.";
	public static final String MSG_RESEND_MAIL_FAILED 				= "El detalle del pedido no se pudo enviar a la dirección de correo registrada. Vuelva a intentarlo o contacte al administrador del sistema.";
	
	//Valores
	public static final int    C_DIAS_ENTREGA_PEDIDO				= 14;
	
	//Perfiles
	public static final String PERFIL_GERENTE						= "3";
	
}

