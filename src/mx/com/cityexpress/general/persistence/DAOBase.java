/*
 * DAOBase.java
 *
 * Created on 9 de julio de 2007, 06:40 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mx.com.cityexpress.general.persistence;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author ffuentes
 */
public class DAOBase extends HibernateDaoSupport{
    
	
	@SuppressWarnings("rawtypes")
	protected List resultados;

    public void saveObject(Object obj){
    	getHibernateTemplate().save(obj);
    } 
        
    public void updateObject(Object obj){
    	getHibernateTemplate().update(obj);
    }
    
    public void saveOrUpdateObject(Object obj) {
    	getHibernateTemplate().saveOrUpdate(obj);
    }
   
    
    @SuppressWarnings("rawtypes")
	public void saveOrUpdateObjectAll(List lst) {
    	getHibernateTemplate().saveOrUpdateAll(lst);
    }
    
    
    @SuppressWarnings("rawtypes")
	public List findObject(Object obj){
            resultados=getHibernateTemplate().findByExample(obj);
            return resultados;
    }
    
}