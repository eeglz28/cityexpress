package mx.com.cityexpress.mail.sender;

import javax.mail.MessagingException;
//import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import mx.com.cityexpress.general.exception.ControlException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MailerSender {
	
	
    private JavaMailSenderImpl sender;
    private final Log log = LogFactory.getLog(getClass());
    
    /**
     * Se encarga de generar el correo electronico y envialos a los destinatarios 
     * @param clazz El scheduler que esta llamando a este metodo.
     * @param subject El subject del mensaje
     * @param body El cuerpo del mensaje
     * @param destination Los destinatarios
     * @param attach El attachment, o null si no debe haber alguno
     */
    
    public void send(MailerVo mailVo)
    //String subject, String body, String[] destination, String user, File attach
    		throws ControlException {
    	
    	if(sender == null) {
    		if(log.isTraceEnabled()) {
    			log.trace("Envio de correo no configurado");
    		}
			return;    		
    	}
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper;
        
        if(log.isDebugEnabled()) {
            for(int i = 0 ; i < mailVo.getDestination().length ; i++) {
                log.debug("Enviando notificacion de reporte a " + mailVo.getDestination()[i]);
            }
        }
        
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(mailVo.getDestination());
            if (mailVo.getBccDestination() != null && 
            		mailVo.getBccDestination().length > 0) {
            	helper.setBcc(mailVo.getBccDestination());
            }
            	
            helper.setFrom(mailVo.getFrom(), mailVo.getFrom());
            helper.setText(mailVo.getBody(), true); 
            
            if(mailVo.getAttach() != null) {
                helper.addAttachment(mailVo.getAttach().getName(), mailVo.getAttach());
            }
            
            //Para simular una falla
            //int a = 4/0;
            
            
            helper.setSubject(mailVo.getSubject());
            sender.send(message);
            
            if(mailVo.getAttach() != null)
            	mailVo.getAttach().delete();
            log.info("Enviado reporte");
            
            
            
        } catch (MessagingException e) {
            log.fatal("No puedo generar el correo : " + e);
            throw new ControlException("No puedo generar el correo");
        } catch (Exception e) {
    	    log.fatal("Ocurrio un error al enviar el correo: " + e.getMessage());
    	    throw new ControlException("Ocurrio un error al enviar el correo: " + e.getMessage());
        } 
    }

    /**
     * @param sender The sender to set.
     */
    public void setSender(JavaMailSenderImpl sender) {
        this.sender = sender;
    }

	/**
	 * @return the sender
	 */
	public JavaMailSenderImpl getSender() {
		return sender;
	}

}
