package mx.com.cityexpress.mail.sender;

import java.io.File;

public class MailerVo {

	private String subject;
	private String from;
	private String[] destination;
	private String[] bccDestination;
	private String body;
	private File attach;
	/**
	 * 
	 */
	public MailerVo() {
		super();
	}
	/**
	 * @param subject
	 * @param from
	 * @param destination
	 * @param body
	 * @param attach
	 */
	public MailerVo(String subject, String from, String[] destination,
			String[] bccDestination, String body, File attach) {
		super();
		this.subject = subject;
		this.from = from;
		this.destination = destination;
		this.bccDestination = bccDestination;
		this.body = body;
		this.attach = attach;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the destination
	 */
	public String[] getDestination() {
		return destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String[] destination) {
		this.destination = destination;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * @return the attach
	 */
	public File getAttach() {
		return attach;
	}
	/**
	 * @param attach the attach to set
	 */
	public void setAttach(File attach) {
		this.attach = attach;
	}
	/**
	 * @return the cc_destination
	 */
	public String[] getBccDestination() {
		return bccDestination;
	}
	/**
	 * @param cc_destination the cc_destination to set
	 */
	public void setBccDestination(String[] bccDestination) {
		this.bccDestination = bccDestination;
	}
	
	
}
