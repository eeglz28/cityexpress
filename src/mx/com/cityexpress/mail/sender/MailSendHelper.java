package mx.com.cityexpress.mail.sender;

import java.math.BigDecimal;

import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.MailBodyOrderVo;

public class MailSendHelper {
	
	public String createBodyNewOrder(MailBodyOrderVo mailVo) {
		
		BigDecimal tot = new BigDecimal(0);
		
		String body = 
        	"<html><LINK href='http://www.aldakar.com/cityexpress/resources/css/cityexpress-mail.css' rel='stylesheet type='text/css'>" +
        	//"<html><LINK href='http://localhost:8080/cityexpress/resources/css/cityexpress-mail.css' rel='stylesheet type='text/css'>" +
			"<body><br>" +
        	"<b><h1>AVISO DE REGISTRO DE PEDIDO</h1></b> <br><br>" +
			"<table>" +
        	"<tr><td align='right'><b>No.:</b></td><td align='left'>" + mailVo.getOrder() + "</td></tr>" +
        	"<tr><td align='right'><b>cliente:</b></td><td align='left'>" + mailVo.getSolicitante() + "</td></tr>" +
        	//"<tr><td align='right'><b>atención:</b></td><td align='left'>" + "</td></tr>" +
        	"<tr><td align='right'><b>registrado por:</b></td><td align='left'>" + mailVo.getUser() + "</td></tr>" +
        	"<tr><td align='right'><b>fecha:</b></td><td align='left'>" + mailVo.getFecha_alta() + "</td></tr>" +
        	"<tr><td align='right'><b>envío:</b></td><td align='left'>" + mailVo.getFecha_entrega() +"</td></tr>" +
        	"</table>" +
        	"<br><br><br> " +
        	"Estimado(a) " + mailVo.getAtencion() +": <br><br>" +
			"Le confirmamos que ha generado el pedido <b>" + mailVo.getOrder() + "</b> en nuestro sistema, el tiempo de entrega es de 10 días hábiles." +
			"<br><br><br> A continuaci&oacute;n le mostramos el detalle del pedido: <br><br>" +
			"<table><tr bgcolor='#00FFFF'><td align='center'>CANTIDAD</td><td align='center'>UNIDAD</td><td align='center'>PRODUCTO</td>" +
			"<td align='center' width='80'>PRECIO</td><td align='center' width='80'>TOTAL</td><td align='center'>FOLIO INICIAL</td></tr>";
        
        if (mailVo.getListDetail() != null ) {
            for (DetailOrderTableVo tableVo : mailVo.getListDetail()) {
            	body = body + "<tr><td align='center'>" + tableVo.getQuantity().setScale(1) + "</td>" +
            	"<td align='center'>" + tableVo.getUnidad() + "</td>" +
            	"<td align='left'>" + tableVo.getProduct() + "</td>" +
            	"<td align='right'>" + tableVo.getPrice().setScale(2, BigDecimal.ROUND_DOWN) + "</td>" + 
            	"<td align='right'>" + tableVo.getSubtotal().setScale(2, BigDecimal.ROUND_DOWN) + "</td>" +
            	"<td align='right'>" + tableVo.getFolios() + "</td>";
            	
            	tot = tot.add(tableVo.getSubtotal().setScale(2, BigDecimal.ROUND_DOWN));
            }
        }
        
        body = body + "</table><br><br>" +
        		"<b>El monto total es: $ " + tot + "</b>" +
        		"<br><br>Si necesita un tiempo de entrega mas corto por favor " +
        		"solicitelo y de ser posible se les informar&aacute; la disponibilidad." +
        		"<br><br><br>Por su atenci&oacute;n, gracias.<br><br><br>" +
        		"</body></html>";
        		//"<img src='http://localhost:8080/cityexpress/faces/resources/img/logo_mail_aldakar.jpg'/></body></html>";
        
        return body;
	}
	
	
	
	public String createBodyInfoGuia(MailBodyOrderVo mailVo) {
		
		String body = 
        	"<html><LINK href='http://www.aldakar.com/cityexpress/resources/css/cityexpress-mail.css' rel='stylesheet type='text/css'>" +
        	//"<html><LINK href='http://localhost:8080/cityexpress/resources/css/cityexpress-mail.css' rel='stylesheet type='text/css'>" +
			"<body><br>" +
        	"<b><h1>AVISO DE ENVIO DE PEDIDO</h1></b> <br><br>" +
			"<table>" +
        	"<tr><td align='right'><b>No.:</b></td><td align='left'>" + mailVo.getOrder() + "</td></tr>" +
        	"<tr><td align='right'><b>cliente:</b></td><td align='left'>" + mailVo.getSolicitante() + "</td></tr>" +
        	"<tr><td align='right'><b>registrado por:</b></td><td align='left'>" + mailVo.getUser() + "</td></tr>";
        	if (mailVo.getComentario() != null && mailVo.getComentario().length() > 0) {
        		body = body + "<tr><td align='right'><b>Notas Adicionales:</b></td><td align='left'>" + mailVo.getComentario() + "</td></tr>" ;
        	}
        	body = body + 
	        	"</table>" +
	        	"<br><br><br> " +
	        	"Estimado(a) " + mailVo.getAtencion() +": <br><br>" +
				"Su pedido ha sido enviado hoy con la guía de mensajería No. " + mailVo.getInfo1() + ", puede consultar su estatus de entrega en " +
	        	"http://www.fedex.com/mx/ o http://www.redpack.com.mx, en las notas adicionales indicamos la mensajería por la que ha sido enviado, " +
				"puede ser incluso diferente a las mencionada;  por favor tome en cuenta que el envío estandar tarda de 24 a 72 horas habiles y " +
	        	"en algunos destinos hasta 96 horas habiles; si esta en la Ciudad de México, su area metropolitana, Toluca ó Tula su entrega se " +
				"realiza generalmente en su domicilio o instalaciones. ";
			
        
        
        
        body = body + 
        		"<br><br><br>Por su atenci&oacute;n, gracias.<br><br><br>" +
        		"</body></html>";
        
        return body;
	}

}
