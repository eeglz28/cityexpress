package mx.com.cityexpress.modules.admon.vo;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;

public class ProductoValidateVO {

	private String idProducto;
	private HtmlInputText codigoText;
	private HtmlInputText productoText;
	private HtmlInputText precioText;
	private HtmlInputText descripcionText;
	private HtmlInputText unidadText;
	private HtmlInputText entregaText;
	private HtmlSelectBooleanCheckbox estatusCheckbox;
	private HtmlSelectBooleanCheckbox folioCheckbox;
	private String codigoAnterior;
	
	private String codigoErr;
	private String productoErr;
	private String precioErr;
	private String unidadErr;
	private String entregaErr;
	private String message;
	
	
	/**
	 * 
	 */
	public ProductoValidateVO() {
		super();
	}
	
	/**
	 * @param codigoText
	 * @param productoText
	 * @param precioText
	 * @param descripcionText
	 * @param unidadText
	 * @param entregaText
	 * @param estatusCheckbox
	 * @param folioCheckbox
	 */
	public ProductoValidateVO(HtmlInputText codigoText,
			HtmlInputText productoText, HtmlInputText precioText,
			HtmlInputText descripcionText, HtmlInputText unidadText,
			HtmlInputText entregaText,
			HtmlSelectBooleanCheckbox estatusCheckbox,
			HtmlSelectBooleanCheckbox folioCheckbox) {
		super();
		this.codigoText = codigoText;
		this.productoText = productoText;
		this.precioText = precioText;
		this.descripcionText = descripcionText;
		this.unidadText = unidadText;
		this.entregaText = entregaText;
		this.estatusCheckbox = estatusCheckbox;
		this.folioCheckbox = folioCheckbox;
	}
	
	
	/**
	 * @param idProducto
	 * @param codigoText
	 * @param productoText
	 * @param precioText
	 * @param descripcionText
	 * @param unidadText
	 * @param entregaText
	 * @param estatusCheckbox
	 * @param folioCheckbox
	 */
	public ProductoValidateVO(String idProducto, HtmlInputText codigoText,
			HtmlInputText productoText, HtmlInputText precioText,
			HtmlInputText descripcionText, HtmlInputText unidadText,
			HtmlInputText entregaText,
			HtmlSelectBooleanCheckbox estatusCheckbox,
			HtmlSelectBooleanCheckbox folioCheckbox,
			String codigoAnterior) {
		super();
		this.idProducto = idProducto;
		this.codigoText = codigoText;
		this.productoText = productoText;
		this.precioText = precioText;
		this.descripcionText = descripcionText;
		this.unidadText = unidadText;
		this.entregaText = entregaText;
		this.estatusCheckbox = estatusCheckbox;
		this.folioCheckbox = folioCheckbox;
		this.codigoAnterior = codigoAnterior;
	}

	
	
	/**
	 * @return the codigoText
	 */
	public HtmlInputText getCodigoText() {
		return codigoText;
	}

	/**
	 * @param codigoText the codigoText to set
	 */
	public void setCodigoText(HtmlInputText codigoText) {
		this.codigoText = codigoText;
	}

	/**
	 * @return the productoText
	 */
	public HtmlInputText getProductoText() {
		return productoText;
	}

	/**
	 * @param productoText the productoText to set
	 */
	public void setProductoText(HtmlInputText productoText) {
		this.productoText = productoText;
	}

	/**
	 * @return the precioText
	 */
	public HtmlInputText getPrecioText() {
		return precioText;
	}

	/**
	 * @param precioText the precioText to set
	 */
	public void setPrecioText(HtmlInputText precioText) {
		this.precioText = precioText;
	}

	/**
	 * @return the descripcionText
	 */
	public HtmlInputText getDescripcionText() {
		return descripcionText;
	}

	/**
	 * @param descripcionText the descripcionText to set
	 */
	public void setDescripcionText(HtmlInputText descripcionText) {
		this.descripcionText = descripcionText;
	}

	/**
	 * @return the unidadText
	 */
	public HtmlInputText getUnidadText() {
		return unidadText;
	}

	/**
	 * @param unidadText the unidadText to set
	 */
	public void setUnidadText(HtmlInputText unidadText) {
		this.unidadText = unidadText;
	}

	/**
	 * @return the entregaText
	 */
	public HtmlInputText getEntregaText() {
		return entregaText;
	}

	/**
	 * @param entregaText the entregaText to set
	 */
	public void setEntregaText(HtmlInputText entregaText) {
		this.entregaText = entregaText;
	}

	/**
	 * @return the estatusCheckbox
	 */
	public HtmlSelectBooleanCheckbox getEstatusCheckbox() {
		return estatusCheckbox;
	}

	/**
	 * @param estatusCheckbox the estatusCheckbox to set
	 */
	public void setEstatusCheckbox(HtmlSelectBooleanCheckbox estatusCheckbox) {
		this.estatusCheckbox = estatusCheckbox;
	}

	/**
	 * @return the folioCheckbox
	 */
	public HtmlSelectBooleanCheckbox getFolioCheckbox() {
		return folioCheckbox;
	}

	/**
	 * @param folioCheckbox the folioCheckbox to set
	 */
	public void setFolioCheckbox(HtmlSelectBooleanCheckbox folioCheckbox) {
		this.folioCheckbox = folioCheckbox;
	}

	/**
	 * @return the codigoErr
	 */
	public String getCodigoErr() {
		return codigoErr;
	}

	/**
	 * @param codigoErr the codigoErr to set
	 */
	public void setCodigoErr(String codigoErr) {
		this.codigoErr = codigoErr;
	}

	/**
	 * @return the productoErr
	 */
	public String getProductoErr() {
		return productoErr;
	}

	/**
	 * @param productoErr the productoErr to set
	 */
	public void setProductoErr(String productoErr) {
		this.productoErr = productoErr;
	}

	/**
	 * @return the precioErr
	 */
	public String getPrecioErr() {
		return precioErr;
	}

	/**
	 * @param precioErr the precioErr to set
	 */
	public void setPrecioErr(String precioErr) {
		this.precioErr = precioErr;
	}

	/**
	 * @return the unidadErr
	 */
	public String getUnidadErr() {
		return unidadErr;
	}

	/**
	 * @param unidadErr the unidadErr to set
	 */
	public void setUnidadErr(String unidadErr) {
		this.unidadErr = unidadErr;
	}

	/**
	 * @return the entregaErr
	 */
	public String getEntregaErr() {
		return entregaErr;
	}

	/**
	 * @param entregaErr the entregaErr to set
	 */
	public void setEntregaErr(String entregaErr) {
		this.entregaErr = entregaErr;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the codigoAnterior
	 */
	public String getCodigoAnterior() {
		return codigoAnterior;
	}

	/**
	 * @param codigoAnterior the codigoAnterior to set
	 */
	public void setCodigoAnterior(String codigoAnterior) {
		this.codigoAnterior = codigoAnterior;
	}
	
	
	
	
	

}
