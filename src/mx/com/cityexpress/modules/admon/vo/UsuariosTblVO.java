package mx.com.cityexpress.modules.admon.vo;

import java.util.Date;

public class UsuariosTblVO {
	
	private String idUsuario;
	private String idCliente;
	private String name;
	private String nickName;
	private String password;
	private String estatus;
	private String estatusDesc;
	private String email;
	private Date creationDate;
	private String updateDate;
	private String invalid;
	private String profile;
	private String profileDesc;
	
	
	
	
	/**
	 * 
	 */
	public UsuariosTblVO() {
		super();
	}
	
	
	/**
	 * @param idCliente
	 * @param nickName
	 * @param password
	 * @param estatus
	 * @param estatusDesc
	 * @param email
	 */
	public UsuariosTblVO(String idCliente, String nickName, String password,
			String estatus, String estatusDesc, String email, String name,
			String profile, String profileDesc) {
		super();
		this.idCliente = idCliente;
		this.nickName = nickName;
		this.password = password;
		this.estatus = estatus;
		this.estatusDesc = estatusDesc;
		this.email = email;
		this.name = name;
		this.profile = profile;
		this.profileDesc = profileDesc;
	}


	/**
	 * @param idUsuario
	 * @param idCliente
	 * @param nickName
	 * @param password
	 * @param estatus
	 * @param estatusDesc
	 * @param email
	 */
	public UsuariosTblVO(String idUsuario, String idCliente, String nickName,
			String password, String estatus, String estatusDesc, String email, Date creationDate) {
		super();
		this.idUsuario = idUsuario;
		this.idCliente = idCliente;
		this.nickName = nickName;
		this.password = password;
		this.estatus = estatus;
		this.estatusDesc = estatusDesc;
		this.email = email;
		this.creationDate = creationDate;
	}


	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}


	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}


	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the estatusDesc
	 */
	public String getEstatusDesc() {
		return estatusDesc;
	}


	/**
	 * @param estatusDesc the estatusDesc to set
	 */
	public void setEstatusDesc(String estatusDesc) {
		this.estatusDesc = estatusDesc;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * @return the updateDate
	 */
	public String getUpdateDate() {
		return updateDate;
	}


	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	/**
	 * @return the invalid
	 */
	public String getInvalid() {
		return invalid;
	}


	/**
	 * @param invalid the invalid to set
	 */
	public void setInvalid(String invalid) {
		this.invalid = invalid;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the profile
	 */
	public String getProfile() {
		return profile;
	}


	/**
	 * @param profile the profile to set
	 */
	public void setProfile(String profile) {
		this.profile = profile;
	}


	/**
	 * @return the profileDesc
	 */
	public String getProfileDesc() {
		return profileDesc;
	}


	/**
	 * @param profileDesc the profileDesc to set
	 */
	public void setProfileDesc(String profileDesc) {
		this.profileDesc = profileDesc;
	}
	
	
	

}
