package mx.com.cityexpress.modules.admon.vo;

import javax.faces.component.html.HtmlInputText;

public class ClienteValidateVO {
	
	private String idCliente;
	private HtmlInputText nombreText;
	private HtmlInputText contactoText;
	private HtmlInputText estadoText;
	private HtmlInputText telefonoText;
	private String nombreAnterior;
	
	private String nombreErr;
	private String contactoErr;
	private String estadoErr;
	private String telefonoErr;
	private String message;
	
	
	/**
	 * @param nombreText
	 * @param contactoText
	 * @param estadoText
	 * @param telefonoText
	 */
	public ClienteValidateVO(HtmlInputText nombreText,
			HtmlInputText contactoText, HtmlInputText estadoText,
			HtmlInputText telefonoText) {
		super();
		this.nombreText = nombreText;
		this.contactoText = contactoText;
		this.estadoText = estadoText;
		this.telefonoText = telefonoText;
	}


	/**
	 * @param idCliente
	 * @param nombreText
	 * @param contactoText
	 * @param estadoText
	 * @param telefonoText
	 */
	public ClienteValidateVO(String idCliente, HtmlInputText nombreText,
			HtmlInputText contactoText, HtmlInputText estadoText,
			HtmlInputText telefonoText, String nombreAnterior) {
		super();
		this.idCliente = idCliente;
		this.nombreText = nombreText;
		this.contactoText = contactoText;
		this.estadoText = estadoText;
		this.telefonoText = telefonoText;
		this.nombreAnterior = nombreAnterior;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the nombreText
	 */
	public HtmlInputText getNombreText() {
		return nombreText;
	}


	/**
	 * @param nombreText the nombreText to set
	 */
	public void setNombreText(HtmlInputText nombreText) {
		this.nombreText = nombreText;
	}


	/**
	 * @return the contactoText
	 */
	public HtmlInputText getContactoText() {
		return contactoText;
	}


	/**
	 * @param contactoText the contactoText to set
	 */
	public void setContactoText(HtmlInputText contactoText) {
		this.contactoText = contactoText;
	}


	/**
	 * @return the estadoText
	 */
	public HtmlInputText getEstadoText() {
		return estadoText;
	}


	/**
	 * @param estadoText the estadoText to set
	 */
	public void setEstadoText(HtmlInputText estadoText) {
		this.estadoText = estadoText;
	}


	/**
	 * @return the telefonoText
	 */
	public HtmlInputText getTelefonoText() {
		return telefonoText;
	}


	/**
	 * @param telefonoText the telefonoText to set
	 */
	public void setTelefonoText(HtmlInputText telefonoText) {
		this.telefonoText = telefonoText;
	}


	/**
	 * @return the nombreErr
	 */
	public String getNombreErr() {
		return nombreErr;
	}


	/**
	 * @param nombreErr the nombreErr to set
	 */
	public void setNombreErr(String nombreErr) {
		this.nombreErr = nombreErr;
	}


	/**
	 * @return the contactoErr
	 */
	public String getContactoErr() {
		return contactoErr;
	}


	/**
	 * @param contactoErr the contactoErr to set
	 */
	public void setContactoErr(String contactoErr) {
		this.contactoErr = contactoErr;
	}


	/**
	 * @return the estadoErr
	 */
	public String getEstadoErr() {
		return estadoErr;
	}


	/**
	 * @param estadoErr the estadoErr to set
	 */
	public void setEstadoErr(String estadoErr) {
		this.estadoErr = estadoErr;
	}


	/**
	 * @return the telefonoErr
	 */
	public String getTelefonoErr() {
		return telefonoErr;
	}


	/**
	 * @param telefonoErr the telefonoErr to set
	 */
	public void setTelefonoErr(String telefonoErr) {
		this.telefonoErr = telefonoErr;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the nombreAnterior
	 */
	public String getNombreAnterior() {
		return nombreAnterior;
	}


	/**
	 * @param nombreAnterior the nombreAnterior to set
	 */
	public void setNombreAnterior(String nombreAnterior) {
		this.nombreAnterior = nombreAnterior;
	}
	
	
	
	
	

}
