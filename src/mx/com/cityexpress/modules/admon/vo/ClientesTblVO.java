package mx.com.cityexpress.modules.admon.vo;

public class ClientesTblVO {
	
	private String idCliente;
	private String nombre;
	private String contacto;
	private String estado;
	private String telefono;
	
	/**
	 * @param idCliente
	 * @param nombre
	 * @param contacto
	 * @param estado
	 * @param telefono
	 */
	public ClientesTblVO(String idCliente, String nombre, String contacto,
			String estado, String telefono) {
		super();
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.contacto = contacto;
		this.estado = estado;
		this.telefono = telefono;
	}
	
	/**
	 * 
	 */
	public ClientesTblVO() {
		super();
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the contacto
	 */
	public String getContacto() {
		return contacto;
	}

	/**
	 * @param contacto the contacto to set
	 */
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}

	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
