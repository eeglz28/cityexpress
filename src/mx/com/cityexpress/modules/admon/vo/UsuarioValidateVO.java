package mx.com.cityexpress.modules.admon.vo;

import java.util.Date;

import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;

public class UsuarioValidateVO {
	
	private String idUsuario;
	private String idCliente;
	private HtmlSelectOneMenu clienteSelect;
	private HtmlInputText nickText;
	private HtmlInputSecret passText;
	private HtmlInputSecret confText;
	private HtmlSelectBooleanCheckbox estatusCheckbox;
	private HtmlInputText nameText;
	private HtmlInputText emailText;
	private HtmlSelectOneMenu perfilSelectOneMenu;
	private String nickAnterior;
	private String passAnterior;
	private Date creationDate;
	
	private String clienteErr;
	private String nickErr;
	private String passErr;
	private String confErr;
	private String nameErr;
	private String emailErr;
	private String perfilErr;
	private String message;
	
	
	/**
	 * 
	 * @param idCliente
	 * @param nickText
	 * @param passText
	 * @param confText
	 * @param estatusCheckbox
	 * @param emailText
	 */
	public UsuarioValidateVO(String idCliente, HtmlInputText nickText,  
			HtmlInputSecret passText, HtmlInputSecret confText,
			HtmlSelectBooleanCheckbox estatusCheckbox, HtmlInputText emailText,
			HtmlInputText nameText, HtmlSelectOneMenu perfilSelectOneMenu) {
		super();
		this.idCliente = idCliente;
		this.nickText = nickText;
		this.passText = passText;
		this.confText = confText;
		this.estatusCheckbox = estatusCheckbox;
		this.emailText = emailText;
		this.nameText = nameText;
		this.perfilSelectOneMenu = perfilSelectOneMenu;
	}


	/**
	 * 
	 * @param idUsuario
	 * @param idCliente
	 * @param nickText
	 * @param passText
	 * @param confText
	 * @param estatusCheckbox
	 * @param emailText
	 * @param nickAnterior
	 * @param creationDate
	 */
	public UsuarioValidateVO(String idUsuario, String idCliente,
			HtmlInputText nickText, HtmlInputSecret passText,  HtmlInputSecret confText,
			HtmlSelectBooleanCheckbox estatusCheckbox, HtmlInputText emailText,
			String nickAnterior, Date creationDate, String passAnterior, 
			HtmlInputText nameText, HtmlSelectOneMenu perfilSelectOneMenu) {
		super();
		this.idUsuario = idUsuario;
		this.idCliente = idCliente;
		this.nickText = nickText;
		this.passText = passText;
		this.confText = confText;
		this.estatusCheckbox = estatusCheckbox;
		this.emailText = emailText;
		this.nickAnterior = nickAnterior;
		this.creationDate = creationDate;
		this.passAnterior = passAnterior;
		this.nameText = nameText;
		this.perfilSelectOneMenu = perfilSelectOneMenu;
	}


	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}


	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the clienteSelect
	 */
	public HtmlSelectOneMenu getClienteSelect() {
		return clienteSelect;
	}


	/**
	 * @param clienteSelect the clienteSelect to set
	 */
	public void setClienteSelect(HtmlSelectOneMenu clienteSelect) {
		this.clienteSelect = clienteSelect;
	}


	/**
	 * @return the nickText
	 */
	public HtmlInputText getNickText() {
		return nickText;
	}


	/**
	 * @param nickText the nickText to set
	 */
	public void setNickText(HtmlInputText nickText) {
		this.nickText = nickText;
	}


	/**
	 * @return the passText
	 */
	public HtmlInputSecret getPassText() {
		return passText;
	}


	/**
	 * @param passText the passText to set
	 */
	public void setPassText(HtmlInputSecret passText) {
		this.passText = passText;
	}


	/**
	 * @return the estatusCheckbox
	 */
	public HtmlSelectBooleanCheckbox getEstatusCheckbox() {
		return estatusCheckbox;
	}


	/**
	 * @param estatusCheckbox the estatusCheckbox to set
	 */
	public void setEstatusCheckbox(HtmlSelectBooleanCheckbox estatusCheckbox) {
		this.estatusCheckbox = estatusCheckbox;
	}


	/**
	 * @return the emailText
	 */
	public HtmlInputText getEmailText() {
		return emailText;
	}


	/**
	 * @param emailText the emailText to set
	 */
	public void setEmailText(HtmlInputText emailText) {
		this.emailText = emailText;
	}


	/**
	 * @return the clienteErr
	 */
	public String getClienteErr() {
		return clienteErr;
	}


	/**
	 * @param clienteErr the clienteErr to set
	 */
	public void setClienteErr(String clienteErr) {
		this.clienteErr = clienteErr;
	}


	/**
	 * @return the nickErr
	 */
	public String getNickErr() {
		return nickErr;
	}


	/**
	 * @param nickErr the nickErr to set
	 */
	public void setNickErr(String nickErr) {
		this.nickErr = nickErr;
	}


	/**
	 * @return the passErr
	 */
	public String getPassErr() {
		return passErr;
	}


	/**
	 * @param passErr the passErr to set
	 */
	public void setPassErr(String passErr) {
		this.passErr = passErr;
	}


	/**
	 * @return the emailErr
	 */
	public String getEmailErr() {
		return emailErr;
	}


	/**
	 * @param emailErr the emailErr to set
	 */
	public void setEmailErr(String emailErr) {
		this.emailErr = emailErr;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the nickAnterior
	 */
	public String getNickAnterior() {
		return nickAnterior;
	}


	/**
	 * @param nickAnterior the nickAnterior to set
	 */
	public void setNickAnterior(String nickAnterior) {
		this.nickAnterior = nickAnterior;
	}


	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * @return the confText
	 */
	public HtmlInputSecret getConfText() {
		return confText;
	}


	/**
	 * @param confText the confText to set
	 */
	public void setConfText(HtmlInputSecret confText) {
		this.confText = confText;
	}


	/**
	 * @return the confErr
	 */
	public String getConfErr() {
		return confErr;
	}


	/**
	 * @param confErr the confErr to set
	 */
	public void setConfErr(String confErr) {
		this.confErr = confErr;
	}


	/**
	 * @return the passAnterior
	 */
	public String getPassAnterior() {
		return passAnterior;
	}


	/**
	 * @param passAnterior the passAnterior to set
	 */
	public void setPassAnterior(String passAnterior) {
		this.passAnterior = passAnterior;
	}


	/**
	 * @return the nameText
	 */
	public HtmlInputText getNameText() {
		return nameText;
	}


	/**
	 * @param nameText the nameText to set
	 */
	public void setNameText(HtmlInputText nameText) {
		this.nameText = nameText;
	}


	/**
	 * @return the nameErr
	 */
	public String getNameErr() {
		return nameErr;
	}


	/**
	 * @param nameErr the nameErr to set
	 */
	public void setNameErr(String nameErr) {
		this.nameErr = nameErr;
	}


	/**
	 * @return the perfilSelectOneMenu
	 */
	public HtmlSelectOneMenu getPerfilSelectOneMenu() {
		return perfilSelectOneMenu;
	}


	/**
	 * @param perfilSelectOneMenu the perfilSelectOneMenu to set
	 */
	public void setPerfilSelectOneMenu(HtmlSelectOneMenu perfilSelectOneMenu) {
		this.perfilSelectOneMenu = perfilSelectOneMenu;
	}


	/**
	 * @return the perfilErr
	 */
	public String getPerfilErr() {
		return perfilErr;
	}


	/**
	 * @param perfilErr the perfilErr to set
	 */
	public void setPerfilErr(String perfilErr) {
		this.perfilErr = perfilErr;
	}

}
