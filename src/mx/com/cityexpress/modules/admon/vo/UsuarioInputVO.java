package mx.com.cityexpress.modules.admon.vo;

import java.util.Date;

public class UsuarioInputVO {
	
	private String idUsuario;
	private String idCliente;
	private String nickName;
	private String password;
	private String estatus;
	private String name;
	private String email;
	private String idPerfil;
	private String nickNameAnterior;
	private Date creationDate;
	
	
	/**
	 * @param idCliente
	 * @param nickName
	 * @param password
	 * @param estatus
	 * @param email
	 */
	public UsuarioInputVO(String idCliente, String nickName, String password,
			String estatus, String email, String name, String idPerfil) {
		super();
		this.idCliente = idCliente;
		this.nickName = nickName;
		this.password = password;
		this.estatus = estatus;
		this.email = email;
		this.name = name;
		this.idPerfil = idPerfil;
	}


	/**
	 * @param idUsuario
	 * @param idCliente
	 * @param nickName
	 * @param password
	 * @param estatus
	 * @param email
	 */
	public UsuarioInputVO(String idUsuario, String idCliente, String nickName,
			String password, String estatus, String email, 
			String nickNameAnterior, Date creationDate, String name, 
			String idPerfil) {
		super();
		this.idUsuario = idUsuario;
		this.idCliente = idCliente;
		this.nickName = nickName;
		this.password = password;
		this.estatus = estatus;
		this.email = email;
		this.nickNameAnterior = nickNameAnterior;
		this.creationDate = creationDate;
		this.name = name;
		this.idPerfil = idPerfil;
	}


	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}


	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}


	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the nickNameAnterior
	 */
	public String getNickNameAnterior() {
		return nickNameAnterior;
	}


	/**
	 * @param nickNameAnterior the nickNameAnterior to set
	 */
	public void setNickNameAnterior(String nickNameAnterior) {
		this.nickNameAnterior = nickNameAnterior;
	}


	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the idPerfil
	 */
	public String getIdPerfil() {
		return idPerfil;
	}


	/**
	 * @param idPerfil the idPerfil to set
	 */
	public void setIdPerfil(String idPerfil) {
		this.idPerfil = idPerfil;
	}
	
	

}
