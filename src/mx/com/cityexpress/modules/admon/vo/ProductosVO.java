package mx.com.cityexpress.modules.admon.vo;

public class ProductosVO {
	
	private String idProducto;
	private String codigo;
	private String nombre;
	private String precio;
	private String estatus;
	private String descEstatus;
	private String descripcion;
	private String unidad;
	private String entrega;
	private String conFolio;
	
	
	/**
	 * 
	 */
	public ProductosVO() {
		super();
	}


	/**
	 * @param idProducto
	 * @param codigo
	 * @param nombre
	 * @param precio
	 * @param estatus
	 * @param descripcion
	 * @param unidad
	 * @param entrega
	 * @param conFolio
	 */
	public ProductosVO(String idProducto, String codigo, String nombre,
			String precio, String estatus, String descEstatus, 
			String descripcion, String unidad,String entrega, String conFolio) {
		super();
		this.idProducto = idProducto;
		this.codigo = codigo;
		this.nombre = nombre;
		this.precio = precio;
		this.estatus = estatus;
		this.descEstatus = descEstatus;
		this.descripcion = descripcion;
		this.unidad = unidad;
		this.entrega = entrega;
		this.conFolio = conFolio;
	}


	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}


	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}


	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}


	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the precio
	 */
	public String getPrecio() {
		return precio;
	}


	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(String precio) {
		this.precio = precio;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}


	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}


	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}


	/**
	 * @return the entrega
	 */
	public String getEntrega() {
		return entrega;
	}


	/**
	 * @param entrega the entrega to set
	 */
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}


	/**
	 * @return the conFolio
	 */
	public String getConFolio() {
		return conFolio;
	}


	/**
	 * @param conFolio the conFolio to set
	 */
	public void setConFolio(String conFolio) {
		this.conFolio = conFolio;
	}


	/**
	 * @return the descEstatus
	 */
	public String getDescEstatus() {
		return descEstatus;
	}


	/**
	 * @param descEstatus the descEstatus to set
	 */
	public void setDescEstatus(String descEstatus) {
		this.descEstatus = descEstatus;
	}
	
	
	

}
