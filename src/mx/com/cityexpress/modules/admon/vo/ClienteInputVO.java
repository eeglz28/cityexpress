package mx.com.cityexpress.modules.admon.vo;

public class ClienteInputVO {
	
	private String idCliente;
	private String nombre;
	private String nombreAnterior;
	private String contacto;
	private String estado;
	private String telefono;
	private String email;
	
	
	/**
	 * @param nombre
	 * @param contacto
	 * @param estado
	 * @param telefono
	 * @param email
	 */
	public ClienteInputVO(String nombre, String contacto, String estado,
			String telefono, String email) {
		super();
		this.nombre = nombre;
		this.contacto = contacto;
		this.estado = estado;
		this.telefono = telefono;
		this.email = email;
	}


	/**
	 * @param idCliente
	 * @param nombre
	 * @param contacto
	 * @param estado
	 * @param telefono
	 * @param email
	 */
	public ClienteInputVO(String idCliente, String nombre, String contacto,
			String estado, String telefono, String email, 
			String nombreAnterior) {
		super();
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.contacto = contacto;
		this.estado = estado;
		this.telefono = telefono;
		this.email = email;
		this.nombreAnterior = nombreAnterior;
	}


	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}


	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the contacto
	 */
	public String getContacto() {
		return contacto;
	}


	/**
	 * @param contacto the contacto to set
	 */
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}


	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}


	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}


	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}


	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the nombreAnterior
	 */
	public String getNombreAnterior() {
		return nombreAnterior;
	}


	/**
	 * @param nombreAnterior the nombreAnterior to set
	 */
	public void setNombreAnterior(String nombreAnterior) {
		this.nombreAnterior = nombreAnterior;
	}

}
