package mx.com.cityexpress.modules.admon.builder;

import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClienteValidateVO;

public class AdministraClienteBuilder {
	
	public ClienteInputVO buildCreaCliente(ClienteValidateVO validateVO) {
		ClienteInputVO inputVO = new ClienteInputVO(
				validateVO.getNombreText().getValue().toString().trim(),
				validateVO.getContactoText().getValue().toString().trim(), 
				validateVO.getEstadoText().getValue().toString().trim(),
				validateVO.getTelefonoText().getValue().toString().trim(),
				"");
		return inputVO;
	}
	
	
	public ClienteInputVO buildActualizaCliente(ClienteValidateVO validateVO) {
		ClienteInputVO inputVO = new ClienteInputVO(
				validateVO.getIdCliente(),
				validateVO.getNombreText().getValue().toString().trim(),
				validateVO.getContactoText().getValue().toString().trim(), 
				validateVO.getEstadoText().getValue().toString().trim(),
				validateVO.getTelefonoText().getValue().toString().trim(),
				"",
				validateVO.getNombreAnterior());
		return inputVO;
	}

}
