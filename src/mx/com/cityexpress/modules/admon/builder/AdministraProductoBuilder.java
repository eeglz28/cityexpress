package mx.com.cityexpress.modules.admon.builder;

import mx.com.cityexpress.modules.admon.vo.ProductoValidateVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;

public class AdministraProductoBuilder {
	
	public ProductoInputVO buildCreaProducto(ProductoValidateVO validateVO) {
		ProductoInputVO inputVO = new ProductoInputVO(
				validateVO.getCodigoText().getValue().toString().trim(),
				validateVO.getProductoText().getValue().toString().trim(),
				validateVO.getPrecioText().getValue().toString().trim(),
				validateVO.getEstatusCheckbox().getValue().toString().trim(),
				validateVO.getDescripcionText().getValue().toString().trim(),
				validateVO.getUnidadText().getValue().toString().trim(),
				validateVO.getEntregaText().getValue().toString().trim(),
				validateVO.getFolioCheckbox().getValue().toString().trim()
				);
		return inputVO;
	}
	
	
	public ProductoInputVO buildActualizaProducto(
			ProductoValidateVO validateVO) {
		ProductoInputVO inputVO = new ProductoInputVO(
				validateVO.getIdProducto(),
				validateVO.getCodigoText().getValue().toString().trim(),
				validateVO.getCodigoAnterior(),
				validateVO.getProductoText().getValue().toString().trim(),
				validateVO.getPrecioText().getValue().toString().trim(),
				validateVO.getEstatusCheckbox().getValue().toString().trim(),
				validateVO.getDescripcionText().getValue().toString().trim(),
				validateVO.getUnidadText().getValue().toString().trim(),
				validateVO.getEntregaText().getValue().toString().trim(),
				validateVO.getFolioCheckbox().getValue().toString().trim()
				);
		return inputVO;
	}

}
