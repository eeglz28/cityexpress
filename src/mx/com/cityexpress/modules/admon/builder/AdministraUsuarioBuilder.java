package mx.com.cityexpress.modules.admon.builder;

import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioValidateVO;

public class AdministraUsuarioBuilder {
	
	public UsuarioInputVO buildCreaUsuario(UsuarioValidateVO validateVO) {
		UsuarioInputVO inputVO = new UsuarioInputVO (
				validateVO.getIdCliente(),
				validateVO.getNickText().getValue().toString().trim(),
				validateVO.getPassText().getValue().toString().trim(),
				validateVO.getEstatusCheckbox().getValue().toString().trim(),
				validateVO.getEmailText().getValue().toString().trim(),
				validateVO.getNameText().getValue().toString().trim(),
				validateVO.getPerfilSelectOneMenu().getValue().toString().trim()
				);
		return inputVO;
	}
	
	
	public UsuarioInputVO buildActualizaUsuario(UsuarioValidateVO validateVO) {
		UsuarioInputVO inputVO = new UsuarioInputVO (
				validateVO.getIdUsuario(),
				validateVO.getIdCliente(),
				validateVO.getNickText().getValue().toString().trim(),
				validateVO.getPassText().getValue().toString().trim(),
				validateVO.getEstatusCheckbox().getValue().toString().trim(),
				validateVO.getEmailText().getValue().toString().trim(),
				validateVO.getNickAnterior(),
				validateVO.getCreationDate(),
				validateVO.getNameText().getValue().toString().trim(),
				validateVO.getPerfilSelectOneMenu().getValue().toString().trim()
				);
		if (validateVO.getPassText().getValue().toString().trim().equals("")) {
			inputVO.setPassword(validateVO.getPassAnterior());
		}
		return inputVO;
	}

	
	
}
