package mx.com.cityexpress.modules.admon.validator;

import mx.com.cityexpress.general.common.DataValidator;
import mx.com.cityexpress.modules.admon.vo.UsuarioValidateVO;

public class AdministraUsuarioValidator {
	
	public static boolean validateCreaUsuario(UsuarioValidateVO validateVO) {
		
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
        if (validateVO.getIdCliente() == null) {
        	validateVO.setMessage("El id de Cliente ha perdido la sesion.");
			valid = false;
        }
		
		if ( validator.isEmptyText(validateVO.getNickText())) {
			validateVO.setNickErr("El nick del usuario es obligatorio.");
		    valid = false;
		} else {
			if (!validator.haveLength(validateVO.getNickText(), 8)) { 
				validateVO.setNickErr("El nick debe ser de 8 posiciones.");
		        valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getPassText())) {
			validateVO.setPassErr("La contraseña es obligatoria");
		    valid = false;
		} else {
			if (!validator.haveLength(validateVO.getPassText(), 8)) { 
				validateVO.setPassErr("La contraseña debe ser de 8-16 posiciones.");
		        valid = false;
			} else {
				if ( validator.isEmptyText(validateVO.getConfText())) {
					validateVO.setConfErr("Debe confirmar la contraseña.");
				    valid = false;
				} else {
					if (!validator.equals(validateVO.getPassText(), validateVO.getConfText())) {
						validateVO.setConfErr("La confirmación es incorrecta.");
					    valid = false;
					}
				}	
			}
			
			
		}
		
		if ( validator.isEmptyText(validateVO.getNameText())) {
			validateVO.setNameErr("El nombre del usuario es obligatorio");
		    valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getEmailText())) {
			validateVO.setEmailErr("El correo electrónico es obligatorio");
		    valid = false;
		}

		if ( !validator.isValidOption(validateVO.getPerfilSelectOneMenu())) {
			validateVO.setPerfilErr("Es obligatorio proporcionar un perfil.");
		    valid = false;
		}
		
		return valid;
	}
	
	
	public static boolean validateActualizaUsuario(UsuarioValidateVO validateVO) {
		
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
        if (validateVO.getIdCliente() == null) {
        	validateVO.setMessage("El id de Cliente ha perdido la sesion.");
			valid = false;
        }
        
        if (validateVO.getIdUsuario() == null) {
        	validateVO.setMessage("El id de Usuario ha perdido la sesion.");
			valid = false;
        }
		
		if ( validator.isEmptyText(validateVO.getNickText())) {
			validateVO.setNickErr("El nick del usuario es obligatorio.");
		    valid = false;
		} else {
			if (!validator.haveLength(validateVO.getNickText(), 8)) { 
				validateVO.setNickErr("El nick debe ser de 8 posiciones.");
		        valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getPassText())) {
			
		} else {
			if (!validator.haveLength(validateVO.getPassText(), 8)) { 
				validateVO.setPassErr("La contraseña debe ser de 8-16 posiciones.");
		        valid = false;
			} else {
				if ( validator.isEmptyText(validateVO.getConfText())) {
					validateVO.setConfErr("Debe confirmar la contraseña.");
				    valid = false;
				} else {
					if (!validator.equals(validateVO.getPassText(), validateVO.getConfText())) {
						validateVO.setConfErr("La confirmación es incorrecta.");
					    valid = false;
					}
				}	
			}
			
			
		}
		
		if ( validator.isEmptyText(validateVO.getNameText())) {
			validateVO.setNameErr("El nombre del usuario es obligatorio");
		    valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getEmailText())) {
			validateVO.setEmailErr("El correo electrónico es obligatorio");
		    valid = false;
		}
		
		if ( !validator.isValidOption(validateVO.getPerfilSelectOneMenu())) {
			validateVO.setPerfilErr("Es obligatorio proporcionar un perfil.");
		    valid = false;
		}

		return valid;
	}
	
	
	
	


}
