package mx.com.cityexpress.modules.admon.validator;

import mx.com.cityexpress.general.common.DataValidator;
import mx.com.cityexpress.modules.admon.vo.ProductoValidateVO;

public class AdministraProductoValidator {
	
	public static boolean validateCreaProducto(
			ProductoValidateVO validateVO) {
		
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
		if ( validator.isEmptyText(validateVO.getCodigoText())) {
			validateVO.setCodigoErr("El codigo del producto es obligatorio.");
		        valid = false;
		} else {
			if ( !validator.isNumeric(validateVO.getCodigoText()) ) {
				validateVO.setCodigoErr("El codigo del producto debe ser numérico.");
				valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getProductoText())) {
			validateVO.setProductoErr("El nombre del producto es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getPrecioText())) {
			validateVO.setPrecioErr("El precio del producto es obligatorio.");
		        valid = false;
		} else {
			if ( !validator.isNumericAndDecimal(validateVO.getPrecioText()) ) {
				validateVO.setPrecioErr("El precio del producto debe ser numérico.");
				valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getUnidadText())) {
			validateVO.setUnidadErr("La unidad es obligatoria.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getEntregaText())) {
			validateVO.setEntregaErr("La entrega es obligatoria.");
		        valid = false;
		}
		
		return valid;
		
	}
	
	
	public static boolean validateActualizaProducto(
			ProductoValidateVO validateVO) {
		
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
        if (validateVO.getIdProducto() == null || 
        		validateVO.getIdProducto().equals("")) {
        	validateVO.setMessage("El id del producto ha perdido la sesión.");
        	valid = false;
        }
        
        
		if ( validator.isEmptyText(validateVO.getCodigoText())) {
			validateVO.setCodigoErr("El codigo del producto es obligatorio.");
		        valid = false;
		} else {
			if ( !validator.isNumeric(validateVO.getCodigoText()) ) {
				validateVO.setCodigoErr("El codigo del producto debe ser numérico.");
				valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getProductoText())) {
			validateVO.setProductoErr("El nombre del producto es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getPrecioText())) {
			validateVO.setPrecioErr("El precio del producto es obligatorio.");
		        valid = false;
		} else {
			if ( !validator.isNumericAndDecimal(validateVO.getPrecioText()) ) {
				validateVO.setPrecioErr("El precio del producto debe ser numérico.");
				valid = false;
			}
		}
		
		if ( validator.isEmptyText(validateVO.getUnidadText())) {
			validateVO.setUnidadErr("La unidad es obligatoria.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getEntregaText())) {
			validateVO.setEntregaErr("La entrega es obligatoria.");
		        valid = false;
		}
		
		return valid;
		
	}

}
