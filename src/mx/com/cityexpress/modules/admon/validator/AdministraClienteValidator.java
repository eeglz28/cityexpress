package mx.com.cityexpress.modules.admon.validator;

import mx.com.cityexpress.general.common.DataValidator;
import mx.com.cityexpress.modules.admon.vo.ClienteValidateVO;

public class AdministraClienteValidator {
	
	public static boolean validateCreaCliente(ClienteValidateVO validateVO) {
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
		if ( validator.isEmptyText(validateVO.getNombreText())) {
			validateVO.setNombreErr("El nombre del cliente es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getContactoText())) {
			validateVO.setContactoErr("El contacto del cliente es obligatorio.");
		        valid = false;
		}
        
		if ( validator.isEmptyText(validateVO.getEstadoText())) {
			validateVO.setEstadoErr("El estado donde se ubica el cliente es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getTelefonoText())) {
			validateVO.setTelefonoErr("El telefono del contacto es obligatorio.");
		        valid = false;
		}
		
        return valid;
        
	}
	
	
	public static boolean validateActualizaCliente(ClienteValidateVO validateVO) {
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
        if (validateVO.getIdCliente() == null || 
        		validateVO.getIdCliente().equals("")) {
        	validateVO.setMessage("El id del cliente ha perdido la sesión.");
        	valid = false;
        }
        
		if ( validator.isEmptyText(validateVO.getNombreText())) {
			validateVO.setNombreErr("El nombre del cliente es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getContactoText())) {
			validateVO.setContactoErr("El contacto del cliente es obligatorio.");
		        valid = false;
		}
        
		if ( validator.isEmptyText(validateVO.getEstadoText())) {
			validateVO.setEstadoErr("El estado donde se ubica el cliente es obligatorio.");
		        valid = false;
		}
		
		if ( validator.isEmptyText(validateVO.getTelefonoText())) {
			validateVO.setTelefonoErr("El telefono del contacto es obligatorio.");
		        valid = false;
		}
		
        return valid;
        
	}

}
