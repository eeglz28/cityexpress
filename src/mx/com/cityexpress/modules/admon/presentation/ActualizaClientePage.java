package mx.com.cityexpress.modules.admon.presentation;

import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.admon.builder.AdministraClienteBuilder;
import mx.com.cityexpress.modules.admon.validator.AdministraClienteValidator;
import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClienteValidateVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ActualizaClientePage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	ClientesTblVO clienteVO = (ClientesTblVO) FacesContext.getCurrentInstance().
			getExternalContext().getSessionMap().get(
					ConstantsCExpress.SESSION_TBL_VO);
	
	private HtmlInputText nombreText;
	private HtmlInputText contactoText;
	private HtmlInputText estadoText;
	private HtmlInputText telefonoText;
	
	private String nombre;
	private String contacto;
	private String estado;
	private String telefono;
	
	private String nombreErr;
	private String contactoErr;
	private String estadoErr;
	private String telefonoErr;
	
	private String message;
	
	private String initPage;
	
	private IAdministraController administraController; 
	
	
	public void loadPage() {
		log.info(":::...[PAGE] loadPage OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return;		
		}
		
		setNombre(clienteVO.getNombre());
		setContacto(clienteVO.getContacto());
		setEstado(clienteVO.getEstado());
		setTelefono(clienteVO.getTelefono());
		
		log.info("id cliente: " + clienteVO.getIdCliente());
	}
	
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info("nombre: " + getNombreText().getValue());
		log.info("contacto: " + getContactoText().getValue());
		log.info("estado: " + getEstadoText().getValue());
		log.info("telefono: " + getTelefonoText().getValue());
			
		try {
			
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_ACT_CLIE_TO_NOSESSION;		
			}
			
			ClienteValidateVO validateVO = new ClienteValidateVO(
					clienteVO.getIdCliente(), getNombreText(), 
					getContactoText(), getEstadoText(), getTelefonoText(),
					clienteVO.getNombre());
			
			if (!AdministraClienteValidator.validateActualizaCliente(validateVO)) {
				setNombreErr(validateVO.getNombreErr());
				setContactoErr(validateVO.getContactoErr());
				setEstadoErr(validateVO.getEstadoErr());
				setTelefonoErr(validateVO.getTelefonoErr());
				setMessage(validateVO.getMessage());
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			AdministraClienteBuilder builder = new AdministraClienteBuilder();
			ClienteInputVO inputVO = builder.buildActualizaCliente(validateVO);
			
			String msg = administraController.actualizaCliente(inputVO);
			if (msg != null) {
				setMessage(msg);
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			log.info(":::...[PAGE] doAceptarAction fin --> OK");
			return ConstantsCExpress.GO_ACT_CLIE_TO_SUCCESS;
    	} catch(ControlException e) {    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_ACT_CLIE_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_ACT_CLIE_TO_FAIL;
    	}
	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] doRegresarAction --> OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_ACT_CLIE_TO_NOSESSION;
		}
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO);
		
		return ConstantsCExpress.GO_ACT_CLIE_TO_CLIENTES;
	}


	/**
	 * @return the nombreText
	 */
	public HtmlInputText getNombreText() {
		if (nombreText == null) {
			nombreText = (HtmlInputText) findComponentInRoot("nombreText");
        }
		return nombreText;
	}


	/**
	 * @param nombreText the nombreText to set
	 */
	public void setNombreText(HtmlInputText nombreText) {
		this.nombreText = nombreText;
	}


	/**
	 * @return the contactoText
	 */
	public HtmlInputText getContactoText() {
		if (contactoText == null) {
			contactoText = (HtmlInputText) findComponentInRoot("contactoText");
        }
		return contactoText;
	}


	/**
	 * @param contactoText the contactoText to set
	 */
	public void setContactoText(HtmlInputText contactoText) {
		this.contactoText = contactoText;
	}


	/**
	 * @return the estadoText
	 */
	public HtmlInputText getEstadoText() {
		if (estadoText == null) {
			estadoText = (HtmlInputText) findComponentInRoot("estadoText");
        }
		return estadoText;
	}


	/**
	 * @param estadoText the estadoText to set
	 */
	public void setEstadoText(HtmlInputText estadoText) {
		this.estadoText = estadoText;
	}


	/**
	 * @return the telefonoText
	 */
	public HtmlInputText getTelefonoText() {
		if (telefonoText == null) {
			telefonoText = (HtmlInputText) findComponentInRoot("telefonoText");
        }
		return telefonoText;
	}


	/**
	 * @param telefonoText the telefonoText to set
	 */
	public void setTelefonoText(HtmlInputText telefonoText) {
		this.telefonoText = telefonoText;
	}


	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the contacto
	 */
	public String getContacto() {
		return contacto;
	}


	/**
	 * @param contacto the contacto to set
	 */
	public void setContacto(String contacto) {
		this.contacto = contacto;
	}


	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}


	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}


	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}


	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	/**
	 * @return the nombreErr
	 */
	public String getNombreErr() {
		return nombreErr;
	}


	/**
	 * @param nombreErr the nombreErr to set
	 */
	public void setNombreErr(String nombreErr) {
		this.nombreErr = nombreErr;
	}


	/**
	 * @return the contactoErr
	 */
	public String getContactoErr() {
		return contactoErr;
	}


	/**
	 * @param contactoErr the contactoErr to set
	 */
	public void setContactoErr(String contactoErr) {
		this.contactoErr = contactoErr;
	}


	/**
	 * @return the estadoErr
	 */
	public String getEstadoErr() {
		return estadoErr;
	}


	/**
	 * @param estadoErr the estadoErr to set
	 */
	public void setEstadoErr(String estadoErr) {
		this.estadoErr = estadoErr;
	}


	/**
	 * @return the telefonoErr
	 */
	public String getTelefonoErr() {
		return telefonoErr;
	}


	/**
	 * @param telefonoErr the telefonoErr to set
	 */
	public void setTelefonoErr(String telefonoErr) {
		this.telefonoErr = telefonoErr;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}


	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}


	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}
}
