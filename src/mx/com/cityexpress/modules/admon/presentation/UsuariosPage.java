package mx.com.cityexpress.modules.admon.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.admon.vo.UsuariosTblVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UsuariosPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
		
	private HtmlDataTable dataTableUsuarios;
	
	private String initPage;
    private List<UsuariosTblVO> usuariosList;
    private Long rowCount = new Long(25);
    
    private IAdministraController administraController;
    
    
    private void loadPage() {
    	log.info(":::...[PAGE] begin loadPage OK");
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return;		
		}
    	
    	final String idCliente = (String) FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().get(ConstantsCExpress.SESSION_ID);
    	
    	log.info("<<<< Id Cliente: " + idCliente);
    	
    	try {
			setUsuariosList( administraController.obtieneTodosUsuarios(new Long(idCliente)) );
			log.info(":::...[PAGE] end loadPage OK");
    	} catch (Exception e) {
    		log.error(":::...[PAGE] end loadPage " + e.getMessage());
    		return;
    	}
    }
    
    
    public String doCrearAction() { 
    	log.info(":::...[PAGE] doCrearAction --> Crea Usua");
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_USUARIOS_TO_NOSESSION;	
		}
		
    	return ConstantsCExpress.GO_USUARIOS_TO_CREA_USUA;
    }
    
    @SuppressWarnings("unchecked")
	public String doActualizarAction() {
    	log.info(":::...[PAGE] begin doActualizarAction OK");
    	try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_USUARIOS_TO_NOSESSION;	
			}
			
			UsuariosTblVO usuariosVO = (UsuariosTblVO)
					getDataTableUsuarios().getRowData();
			
			log.info(":::...[PAGE] Id: " + usuariosVO.getIdUsuario() + 
					"  Nombre: " + usuariosVO.getNickName() + 
					"  Fecha Creacion: " + usuariosVO.getCreationDate());
			
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().put(
							ConstantsCExpress.SESSION_TBL_VO_2, usuariosVO);
			
			log.info(":::...[PAGE] end doActualizarAction OK");
			return ConstantsCExpress.GO_USUARIOS_TO_ACT_USUA;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doActualizarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_USUARIOS_TO_FAIL;
    	}
    }
    
    public String doRegresarAction() {
    	log.info(":::...[PAGE] doRegresarAction --> Crea Usua");
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_USUARIOS_TO_NOSESSION;		
		}

    	FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_ID);
    	
    	return ConstantsCExpress.GO_USUARIOS_TO_CLIENTES;
    }
    
    public String doMenuAction() { 
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_USUARIOS_TO_NOSESSION;
		}
		
    	log.info(":::...[PAGE] doMenuAction --> Menu");
    	
    	FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_ID);
    	
    	FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO);
    	
    	return ConstantsCExpress.GO_USUARIOS_TO_MENU;
    }


	/**
	 * @return the dataTableUsuarios
	 */
	public HtmlDataTable getDataTableUsuarios() {
		if (dataTableUsuarios == null) {
			dataTableUsuarios = (HtmlDataTable) 
					findComponentInRoot("dataTableUsuarios");
        }
		return dataTableUsuarios;
	}


	/**
	 * @param dataTableUsuarios the dataTableUsuarios to set
	 */
	public void setDataTableUsuarios(HtmlDataTable dataTableUsuarios) {
		this.dataTableUsuarios = dataTableUsuarios;
	}


	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}


	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	/**
	 * @return the usuariosList
	 */
	public List<UsuariosTblVO> getUsuariosList() {
		return usuariosList;
	}


	/**
	 * @param usuariosList the usuariosList to set
	 */
	public void setUsuariosList(List<UsuariosTblVO> usuariosList) {
		this.usuariosList = usuariosList;
	}


	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}


	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}


	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}


	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}

}
