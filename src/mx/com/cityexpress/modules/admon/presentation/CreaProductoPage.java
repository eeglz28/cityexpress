package mx.com.cityexpress.modules.admon.presentation;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.admon.builder.AdministraProductoBuilder;
import mx.com.cityexpress.modules.admon.validator.AdministraProductoValidator;
import mx.com.cityexpress.modules.admon.vo.ProductoValidateVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;

public class CreaProductoPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	private HtmlInputText codigoText;
	private HtmlInputText productoText;
	private HtmlInputText precioText;
	private HtmlInputText descripcionText;
	private HtmlInputText unidadText;
	private HtmlInputText entregaText;
	private HtmlSelectBooleanCheckbox estatusCheckbox;
	private HtmlSelectBooleanCheckbox folioCheckbox;
	
	private String codigoErr;
	private String productoErr;
	private String precioErr;
	private String unidadErr;
	private String entregaErr;
	
	
	private String message;
	
	private String initPage;
	
	private IAdministraController administraController; 
	
	
	public void loadPage() {
		log.info(":::...[PAGE] loadPage --> OK");
	}
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info("codigo: " + getCodigoText().getValue());
		log.info("producto: " + getProductoText().getValue());
		log.info("precio: " + getPrecioText().getValue());
		log.info("desc: " + getDescripcionText().getValue());
		log.info("unidad: " + getUnidadText().getValue());
		log.info("entrega: " + getEntregaText().getValue());
		log.info("estatus: " + getEstatusCheckbox().getValue());
		log.info("folio: " + getFolioCheckbox().getValue());
		
		try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_CREA_PROD_TO_NOSESSION;
			}
			
			ProductoValidateVO validateVO = new ProductoValidateVO(
					getCodigoText(), getProductoText(), getPrecioText(), 
					getDescripcionText(), getUnidadText(), getEntregaText(),
					getEstatusCheckbox(), getFolioCheckbox());
			
			if (!AdministraProductoValidator.validateCreaProducto(validateVO)) {
				setCodigoErr(validateVO.getCodigoErr());
				setProductoErr(validateVO.getProductoErr());
				setPrecioErr(validateVO.getPrecioErr());
				setUnidadErr(validateVO.getUnidadErr());
				setEntregaErr(validateVO.getEntregaErr());
				log.info(getCodigoErr() + " " + getProductoErr() + " " + getPrecioErr() + " " + getUnidadErr() + " " + getEntregaErr() );
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			AdministraProductoBuilder builder = new AdministraProductoBuilder();
			ProductoInputVO inputVO = builder.buildCreaProducto(validateVO);
			
			String msg = administraController.creaProducto(inputVO);
			if (msg != null) {
				setMessage(msg);
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			log.info(":::...[PAGE] doAceptarAction fin --> OK");
			return ConstantsCExpress.GO_CREA_PROD_TO_SUCCESS;
    	} catch(ControlException e) {    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_CREA_PROD_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_CREA_PROD_TO_FAIL;
    	}
	}
	
	
	public String doRegresarAction() {
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_CREA_PROD_TO_NOSESSION;
		}
		
		log.info(":::...[PAGE] doRegresarAction --> OK");
		return ConstantsCExpress.GO_CREA_PROD_TO_PRODUCTOS;
	}

	/**
	 * @return the codigoText
	 */
	public HtmlInputText getCodigoText() {
		if (codigoText == null) {
			codigoText = (HtmlInputText) findComponentInRoot("codigoText");
        }
		return codigoText;
	}

	/**
	 * @param codigoText the codigoText to set
	 */
	public void setCodigoText(HtmlInputText codigoText) {
		this.codigoText = codigoText;
	}

	/**
	 * @return the productoText
	 */
	public HtmlInputText getProductoText() {
		if (productoText == null) {
			productoText = (HtmlInputText) findComponentInRoot("productoText");
        }
		return productoText;
	}

	/**
	 * @param productoText the productoText to set
	 */
	public void setProductoText(HtmlInputText productoText) {
		this.productoText = productoText;
	}

	/**
	 * @return the precioText
	 */
	public HtmlInputText getPrecioText() {
		if (precioText == null) {
			precioText = (HtmlInputText) findComponentInRoot("precioText");
        }
		return precioText;
	}

	/**
	 * @param precioText the precioText to set
	 */
	public void setPrecioText(HtmlInputText precioText) {
		this.precioText = precioText;
	}

	/**
	 * @return the descripcionText
	 */
	public HtmlInputText getDescripcionText() {
		if (descripcionText == null) {
			descripcionText = (HtmlInputText) findComponentInRoot("descripcionText");
        }
		return descripcionText;
	}

	/**
	 * @param descripcionText the descripcionText to set
	 */
	public void setDescripcionText(HtmlInputText descripcionText) {
		this.descripcionText = descripcionText;
	}

	/**
	 * @return the unidadText
	 */
	public HtmlInputText getUnidadText() {
		if (unidadText == null) {
			unidadText = (HtmlInputText) findComponentInRoot("unidadText");
        }
		return unidadText;
	}

	/**
	 * @param unidadText the unidadText to set
	 */
	public void setUnidadText(HtmlInputText unidadText) {
		this.unidadText = unidadText;
	}

	/**
	 * @return the entregaText
	 */
	public HtmlInputText getEntregaText() {
		if (entregaText == null) {
			entregaText = (HtmlInputText) findComponentInRoot("entregaText");
        }
		return entregaText;
	}

	/**
	 * @param entregaText the entregaText to set
	 */
	public void setEntregaText(HtmlInputText entregaText) {
		this.entregaText = entregaText;
	}

	/**
	 * @return the estatusCheckbox
	 */
	public HtmlSelectBooleanCheckbox getEstatusCheckbox() {
		if (estatusCheckbox == null) {
			estatusCheckbox = (HtmlSelectBooleanCheckbox) findComponentInRoot("estatusCheckbox");
        }
		return estatusCheckbox;
	}

	/**
	 * @param estatusCheckbox the estatusCheckbox to set
	 */
	public void setEstatusCheckbox(HtmlSelectBooleanCheckbox estatusCheckbox) {
		this.estatusCheckbox = estatusCheckbox;
	}

	/**
	 * @return the folioCheckbox
	 */
	public HtmlSelectBooleanCheckbox getFolioCheckbox() {
		if (folioCheckbox == null) {
			folioCheckbox = (HtmlSelectBooleanCheckbox) findComponentInRoot("folioCheckbox");
        }
		return folioCheckbox;
	}

	/**
	 * @param folioCheckbox the folioCheckbox to set
	 */
	public void setFolioCheckbox(HtmlSelectBooleanCheckbox folioCheckbox) {
		this.folioCheckbox = folioCheckbox;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	/**
	 * @return the codigoErr
	 */
	public String getCodigoErr() {
		return codigoErr;
	}

	/**
	 * @param codigoErr the codigoErr to set
	 */
	public void setCodigoErr(String codigoErr) {
		this.codigoErr = codigoErr;
	}

	/**
	 * @return the productoErr
	 */
	public String getProductoErr() {
		return productoErr;
	}

	/**
	 * @param productoErr the productoErr to set
	 */
	public void setProductoErr(String productoErr) {
		this.productoErr = productoErr;
	}

	/**
	 * @return the precioErr
	 */
	public String getPrecioErr() {
		return precioErr;
	}

	/**
	 * @param precioErr the precioErr to set
	 */
	public void setPrecioErr(String precioErr) {
		this.precioErr = precioErr;
	}

	/**
	 * @return the unidadErr
	 */
	public String getUnidadErr() {
		return unidadErr;
	}

	/**
	 * @param unidadErr the unidadErr to set
	 */
	public void setUnidadErr(String unidadErr) {
		this.unidadErr = unidadErr;
	}

	/**
	 * @return the entregaErr
	 */
	public String getEntregaErr() {
		return entregaErr;
	}

	/**
	 * @param entregaErr the entregaErr to set
	 */
	public void setEntregaErr(String entregaErr) {
		this.entregaErr = entregaErr;
	}

	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}

	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}

}
