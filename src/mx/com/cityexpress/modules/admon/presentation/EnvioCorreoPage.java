package mx.com.cityexpress.modules.admon.presentation;


import java.util.List;
import java.util.Set;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyListbox;
import javax.faces.model.SelectItem;

import mx.com.cityexpress.general.common.CatalogoItem;
import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EnvioCorreoPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());

	private HtmlInputText input;
	private HtmlInputText title;
    
    private HtmlSelectManyListbox clientListBox;
    private SelectItem[] clientSelectItem;
    private UISelectItems clientUISelectItems;
    
    private HtmlSelectManyListbox profileListBox;
    private SelectItem[] profileSelectItem;
    private UISelectItems profileUISelectItems;
    
    private HtmlSelectBooleanCheckbox allUserSelected;
    
    private String message;
    
    private IAdministraController administraController; 
    
    
	/**
	 * @return the clientListBox
	 */
	public HtmlSelectManyListbox getClientListBox() {
		if (clientListBox == null) {
			clientListBox = (HtmlSelectManyListbox) 
					findComponentInRoot("clientListBox");
        }
		return clientListBox;
	}
	
	/**
	 * @param clientListBox the clientListBox to set
	 */
	public void setClientListBox(HtmlSelectManyListbox clientListBox) {
		this.clientListBox = clientListBox;
	}
	
	/**
	 * @return the clientSelectItem
	 */
	public SelectItem[] getClientSelectItem() {
		log.info("==> getSelectItemProdLine");
		CatalogoItem item = new CatalogoItem();
		try {
			List<ClientVo> clientList = administraController.getOnlyClients();
			Set<SelectItem[]> clientSet = item.getSelectClientsMenuList(
					clientList);
			
			clientSelectItem = (SelectItem[]) clientSet.iterator().next();
		} catch (ControlException ce) {
			log.error(ce.getMessage());
		} catch (Exception e) {
			log.error("Error al cargar el catalogo de clientes: "
					+ e.getMessage());
		}
		return clientSelectItem;
	}
	
	/**
	 * @param clientSelectItem the clientSelectItem to set
	 */
	public void setClientSelectItem(SelectItem[] clientSelectItem) {
		this.clientSelectItem = clientSelectItem;
	}
	
	/**
	 * @return the clientUISelectItems
	 */
	public UISelectItems getClientUISelectItems() {
		if (clientUISelectItems == null) {
			clientUISelectItems = (UISelectItems) 
					findComponentInRoot("clientUISelectItems");
        }
		return clientUISelectItems;
	}
	
	/**
	 * @param clientUISelectItems the clientUISelectItems to set
	 */
	public void setClientUISelectItems(UISelectItems clientUISelectItems) {
		this.clientUISelectItems = clientUISelectItems;
	}
	
	
	
	/**
	 * @return the profileListBox
	 */
	public HtmlSelectManyListbox getProfileListBox() {
		if (profileListBox == null) {
			profileListBox = (HtmlSelectManyListbox) 
					findComponentInRoot("profileListBox");
        }
		return profileListBox;
	}

	/**
	 * @param profileListBox the profileListBox to set
	 */
	public void setProfileListBox(HtmlSelectManyListbox profileListBox) {
		this.profileListBox = profileListBox;
	}

	/**
	 * @return the profileSelectItem
	 */
	public SelectItem[] getProfileSelectItem() {
		log.info("==> getProfileSelectItem");
		CatalogoItem item = new CatalogoItem();
		try {
			List<ProfileVo> profileList = administraController.
					retrieveProfileByType("E");
			Set<SelectItem[]> profileSet = item.
					getSelectProfilesManyList(profileList);
			
			profileSelectItem = (SelectItem[]) profileSet.iterator().next();
		} catch (ControlException ce) {
			log.error(ce.getMessage());
		} catch (Exception e) {
			log.error("Error al cargar el catalogo de clientes: "
					+ e.getMessage());
		}
		
		return profileSelectItem;
	}

	/**
	 * @param profileSelectItem the profileSelectItem to set
	 */
	public void setProfileSelectItem(SelectItem[] profileSelectItem) {
		this.profileSelectItem = profileSelectItem;
	}

	/**
	 * @return the profileUISelectItems
	 */
	public UISelectItems getProfileUISelectItems() {
		if (profileUISelectItems == null) {
			profileUISelectItems = (UISelectItems) 
					findComponentInRoot("profileUISelectItems");
        }
		return profileUISelectItems;
	}

	/**
	 * @param profileUISelectItems the profileUISelectItems to set
	 */
	public void setProfileUISelectItems(UISelectItems profileUISelectItems) {
		this.profileUISelectItems = profileUISelectItems;
	}

	
	

	public String doAceptarAction() {
		log.info(":::...[PAGE] begin doAceptarAction OK");
		String[] clientIdList = null;
		String[] profileIdList = null;
		String body = getInput().getValue().toString();
		String subject = getTitle().getValue().toString();
		
		Boolean isSelected = (Boolean) getAllUserSelected().getValue();
		if (!isSelected) {
			clientIdList = (String[])clientListBox.getSelectedValues();
			profileIdList = (String[])profileListBox.getSelectedValues();
			
		}
		
		try {
			String msg = administraController.sendMasiveMail(clientIdList, profileIdList, 
					body, subject);
			setMessage(msg);
		} catch (ControlException e) {
			setMessage("Ocurrio un error al enviar el correo.");
			e.printStackTrace();
		}
		
		return "";
		
	}
	
	public String doRegresarAction() {
		return ConstantsCExpress.GO_ENVIO_CORREO_TO_MENU;
	}



	/**
	 * @return the input
	 */
	public HtmlInputText getInput() {
		if (input == null) {
			input = (HtmlInputText) findComponentInRoot("input");
        }
		return input;
	}



	/**
	 * @param input the input to set
	 */
	public void setInput(HtmlInputText input) {
		this.input = input;
	}

	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}

	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}

	/**
	 * @return the allUserSelected
	 */
	public HtmlSelectBooleanCheckbox getAllUserSelected() {
		if (allUserSelected == null) {
			allUserSelected = (HtmlSelectBooleanCheckbox) 
					findComponentInRoot("allUserSelected");
        }
		return allUserSelected;
	}

	/**
	 * @param allUserSelected the allUserSelected to set
	 */
	public void setAllUserSelected(HtmlSelectBooleanCheckbox allUserSelected) {
		this.allUserSelected = allUserSelected;
	}

	/**
	 * @return the title
	 */
	public HtmlInputText getTitle() {
		if (title == null) {
			title = (HtmlInputText) findComponentInRoot("title");
        }
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(HtmlInputText title) {
		this.title = title;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	
	

}
