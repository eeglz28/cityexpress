package mx.com.cityexpress.modules.admon.presentation;

import java.util.List;
import java.util.Set;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.CatalogoItem;
import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.modules.admon.builder.AdministraUsuarioBuilder;
import mx.com.cityexpress.modules.admon.validator.AdministraUsuarioValidator;
import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioValidateVO;

public class CreaUsuarioPage extends PageCode {
		
		private Log log = LogFactory.getLog(getClass());
		
    	final String idCliente = (String) FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().get(ConstantsCExpress.SESSION_ID);
		
		private HtmlInputText nickText;
		private HtmlInputSecret passText;
		private HtmlInputSecret confirmarText;
		private HtmlInputText nameText;
		private HtmlInputText emailText;
		private HtmlSelectBooleanCheckbox estatusCheckbox;
		private HtmlSelectOneMenu perfilSelectOneMenu;
		protected UISelectItems perfilSelectItems;
		protected SelectItem[] selectItemPerfil;
		
		private String nickErr;
		private String passErr;
		private String confirmarErr;
		private String nameErr;
		private String emailErr;
		private String perfilErr;
		
		private String cliente;
		private String message;
		private String initPage;
		
		private IAdministraController administraController; 
		
		
		public void loadPage() {
			log.info(":::...[PAGE] loadPage --> idCliente: " + idCliente);
			
			try {
				
	    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
	    				getCurrentInstance().getExternalContext().getSessionMap().
	    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
				if (userSesion == null) { 
					return;		
				}
				
				String nombre = administraController.
						obtieneNombreClientePorId(idCliente);
				setCliente(nombre);
				
			} catch(Exception e) {
				log.error(":::...[PAGE] loadPage " + e.getMessage());
				return;
			}
		}
		
		
		@SuppressWarnings("unchecked")
		public String doAceptarAction() {
			log.info(":::...[PAGE] begin doAceptarAction OK");
			
			log.info("nick: " + getNickText().getValue());
			log.info("pass: " + getPassText().getValue());
			log.info("conf: " + getConfirmarText().getValue());
			log.info("name: " + getNameText().getValue());
			log.info("email: " + getEmailText().getValue());
			log.info("estatus: " + getEstatusCheckbox().getValue());
			log.info("perfil: " + getPerfilSelectOneMenu().getValue());
			
			try {				
	    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
	    				getCurrentInstance().getExternalContext().getSessionMap().
	    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
				if (userSesion == null) { 
					return ConstantsCExpress.GO_CREA_USUA_TO_NOSESSION;
				}
				
				UsuarioValidateVO validateVO = new UsuarioValidateVO(
						idCliente, getNickText(), getPassText(), 
						getConfirmarText(), getEstatusCheckbox(),
						getEmailText(), getNameText(), getPerfilSelectOneMenu());
				
				if (!AdministraUsuarioValidator.validateCreaUsuario(validateVO)) {
					setNickErr(validateVO.getNickErr());
					setPassErr(validateVO.getPassErr());
					setConfirmarErr(validateVO.getConfErr());
					setNameErr(validateVO.getNameErr());
					setEmailErr(validateVO.getEmailErr());
					setPerfilErr(validateVO.getPerfilErr());
					setMessage(validateVO.getMessage());
					
					return ConstantsCExpress.GO_REFRESH_PAGE;
				}
				
				AdministraUsuarioBuilder builder = new AdministraUsuarioBuilder();
				UsuarioInputVO inputVO = builder.buildCreaUsuario(validateVO);
				
				String msg = administraController.creaUsuario(inputVO);
				if (msg != null) {
					setMessage(msg);
					return ConstantsCExpress.GO_REFRESH_PAGE;
				}
		
				log.info(":::...[PAGE] end doAceptarAction OK");
				return ConstantsCExpress.GO_CREA_USUA_TO_SUCCESS;
	    	} catch(ControlException e) {    		
	    		FacesContext.getCurrentInstance().getExternalContext().
	    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
	    						e.getMessage());
	    		
	    		return ConstantsCExpress.GO_CREA_USUA_TO_FAIL;
	    	} catch(Exception e) {
	    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
	    		
	    		FacesContext.getCurrentInstance().getExternalContext().
	    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
	    						ConstantsCExpress.ERROR_PAGE);
	    		
	    		return ConstantsCExpress.GO_CREA_USUA_TO_FAIL;
	    	}
		}
		
		
		public String doRegresarAction() {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_CREA_USUA_TO_NOSESSION;
			}
			
			log.info(":::...[PAGE] doRegresarAction --> usuarios");
			
			return ConstantsCExpress.GO_CREA_USUA_TO_USUARIOS;
		}


		/**
		 * @return the nickText
		 */
		public HtmlInputText getNickText() {
			if (nickText == null) {
				nickText = (HtmlInputText) findComponentInRoot("nickText");
	        }
			return nickText;
		}


		/**
		 * @param nickText the nickText to set
		 */
		public void setNickText(HtmlInputText nickText) {
			this.nickText = nickText;
		}


		/**
		 * @return the passText
		 */
		public HtmlInputSecret getPassText() {
			if (passText == null) {
				passText = (HtmlInputSecret) findComponentInRoot("passText");
	        }
			return passText;
		}


		/**
		 * @param passText the passText to set
		 */
		public void setPassText(HtmlInputSecret passText) {
			this.passText = passText;
		}


		/**
		 * @return the confirmarText
		 */
		public HtmlInputSecret getConfirmarText() {
			if (confirmarText == null) {
				confirmarText = (HtmlInputSecret) findComponentInRoot("confirmarText");
	        }
			return confirmarText;
		}


		/**
		 * @param confirmarText the confirmarText to set
		 */
		public void setConfirmarText(HtmlInputSecret confirmarText) {
			this.confirmarText = confirmarText;
		}


		/**
		 * @return the emailText
		 */
		public HtmlInputText getEmailText() {
			if (emailText == null) {
				emailText = (HtmlInputText) findComponentInRoot("emailText");
	        }
			return emailText;
		}


		/**
		 * @param emailText the emailText to set
		 */
		public void setEmailText(HtmlInputText emailText) {
			this.emailText = emailText;
		}


		/**
		 * @return the estatusCheckbox
		 */
		public HtmlSelectBooleanCheckbox getEstatusCheckbox() {
			if (estatusCheckbox == null) {
				estatusCheckbox = (HtmlSelectBooleanCheckbox) findComponentInRoot("estatusCheckbox");
	        }
			return estatusCheckbox;
		}


		/**
		 * @param estatusCheckbox the estatusCheckbox to set
		 */
		public void setEstatusCheckbox(HtmlSelectBooleanCheckbox estatusCheckbox) {
			this.estatusCheckbox = estatusCheckbox;
		}


		/**
		 * @return the nickErr
		 */
		public String getNickErr() {
			return nickErr;
		}


		/**
		 * @param nickErr the nickErr to set
		 */
		public void setNickErr(String nickErr) {
			this.nickErr = nickErr;
		}


		/**
		 * @return the passErr
		 */
		public String getPassErr() {
			return passErr;
		}


		/**
		 * @param passErr the passErr to set
		 */
		public void setPassErr(String passErr) {
			this.passErr = passErr;
		}


		/**
		 * @return the confirmarErr
		 */
		public String getConfirmarErr() {
			return confirmarErr;
		}


		/**
		 * @param confirmarErr the confirmarErr to set
		 */
		public void setConfirmarErr(String confirmarErr) {
			this.confirmarErr = confirmarErr;
		}


		/**
		 * @return the emailErr
		 */
		public String getEmailErr() {
			return emailErr;
		}


		/**
		 * @param emailErr the emailErr to set
		 */
		public void setEmailErr(String emailErr) {
			this.emailErr = emailErr;
		}


		/**
		 * @return the cliente
		 */
		public String getCliente() {
			return cliente;
		}


		/**
		 * @param cliente the cliente to set
		 */
		public void setCliente(String cliente) {
			this.cliente = cliente;
		}


		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}


		/**
		 * @param message the message to set
		 */
		public void setMessage(String message) {
			this.message = message;
		}


		/**
		 * @return the initPage
		 */
		public String getInitPage() {
			loadPage();
			return initPage;
		}


		/**
		 * @param initPage the initPage to set
		 */
		public void setInitPage(String initPage) {
			this.initPage = initPage;
		}


		/**
		 * @return the administraController
		 */
		public IAdministraController getAdministraController() {
			return administraController;
		}


		/**
		 * @param administraController the administraController to set
		 */
		public void setAdministraController(IAdministraController administraController) {
			this.administraController = administraController;
		}


		/**
		 * @return the nameText
		 */
		public HtmlInputText getNameText() {
			if (nameText == null) {
				nameText = (HtmlInputText) findComponentInRoot("nameText");
	        }
			return nameText;
		}


		/**
		 * @param nameText the nameText to set
		 */
		public void setNameText(HtmlInputText nameText) {
			this.nameText = nameText;
		}


		/**
		 * @return the nameErr
		 */
		public String getNameErr() {
			return nameErr;
		}


		/**
		 * @param nameErr the nameErr to set
		 */
		public void setNameErr(String nameErr) {
			this.nameErr = nameErr;
		}


		/**
		 * @return the perfilSelectOneMenu
		 */
		public HtmlSelectOneMenu getPerfilSelectOneMenu() {
			if (perfilSelectOneMenu == null) {
				perfilSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("perfilSelectOneMenu");
	        }
			return perfilSelectOneMenu;
		}


		/**
		 * @param perfilSelectOneMenu the perfilSelectOneMenu to set
		 */
		public void setPerfilSelectOneMenu(HtmlSelectOneMenu perfilSelectOneMenu) {
			this.perfilSelectOneMenu = perfilSelectOneMenu;
		}


		/**
		 * @return the perfilSelectItems
		 */
		public UISelectItems getPerfilSelectItems() {
			if (perfilSelectItems == null) {
				perfilSelectItems = (UISelectItems) findComponentInRoot("perfilSelectItems");
	        }
			return perfilSelectItems;
		}


		/**
		 * @param perfilSelectItems the perfilSelectItems to set
		 */
		public void setPerfilSelectItems(UISelectItems perfilSelectItems) {
			this.perfilSelectItems = perfilSelectItems;
		}


		/**
		 * @return the perfilErr
		 */
		public String getPerfilErr() {
			return perfilErr;
		}


		/**
		 * @param perfilErr the perfilErr to set
		 */
		public void setPerfilErr(String perfilErr) {
			this.perfilErr = perfilErr;
		}


		/**
		 * @return the selectItemPerfil
		 */
		@SuppressWarnings("rawtypes")
		public SelectItem[] getSelectItemPerfil() {
			CatalogoItem item = new CatalogoItem();
			try {
				String idCli = (String) FacesContext.getCurrentInstance().
						getExternalContext().getSessionMap().get(
								ConstantsCExpress.SESSION_ID);
				idCli = idCli.equals("9999")?idCli : null;
				List<Profile> perfilesList = administraController.obtienePerfiles(idCli);
				Set perfilSet = item.getSelectPerfiles(perfilesList);
				
				selectItemPerfil = (SelectItem[])perfilSet.iterator().next();
				
			} catch(ControlException e) {
				log.error(":::...[PAGE]: getSelectItemPerfil --> ControlException");
			} catch(Exception e) {
				log.error(":::...[PAGE]: getSelectItemPerfil --> Exception");
			}
			return selectItemPerfil;
		}


		/**
		 * @param selectItemPerfil the selectItemPerfil to set
		 */
		public void setSelectItemPerfil(SelectItem[] selectItemPerfil) {
			this.selectItemPerfil = selectItemPerfil;
		}

}
