package mx.com.cityexpress.modules.admon.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.admon.vo.ProductosVO;

public class ProductosPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	private HtmlDataTable dataTableProductos;
	
	private String initPage;
    private List<ProductosVO> productosList;
    private Long rowCount = new Long(50);
    
    private IAdministraController administraController;
    
    
    private void loadPage() {
    	log.info(":::...[PAGE] loadPage inicia OK");
    	try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return;		
			}
			
			log.info(":::...[PAGE] (loadPage) Usuario session: " + userSesion.getUsuario());
			
			setProductosList( administraController.obtieneTodosProductos() );
			
    	} catch (Exception e) {
    		log.error("Error al obtener los pedidos del catalogo.");
    		return;
    	}
    }
    
    public String doCrearAction() { 
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_PRODUCTOS_TO_NOSESSION;		
		}
		
    	log.info(":::...[PAGE] doCrearAction --> Crea Prod");
    	return ConstantsCExpress.GO_PRODUCTOS_TO_CREA_PROD;
    }
    
    @SuppressWarnings("unchecked")
	public String doActualizarAction() {
    	log.info(":::...[PAGE] begin doActualizarAction OK");
    	try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PRODUCTOS_TO_NOSESSION;
			}
			
			ProductosVO prodVO = (ProductosVO)
					getDataTableProductos().getRowData();
			
			log.info(":::...[PAGE] Id: " + prodVO.getIdProducto() + 
					"  Nombre: " +prodVO.getNombre());
			
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().put(ConstantsCExpress.SESSION_TBL_VO, prodVO);
			
			log.info(":::...[PAGE] end doActualizarAction OK");
			return ConstantsCExpress.GO_PRODUCTOS_TO_ACT_PROD;
			
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doActualizarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PRODUCTOS_TO_FAIL;
    	}
    }
    
    public String doRegresarAction() {
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_PRODUCTOS_TO_NOSESSION;		
		}
		
    	log.info(":::...[PAGE] doRegresarAction --> Menu");
    	
    	return ConstantsCExpress.GO_PRODUCTOS_TO_MENU;
    }
    
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + 
				", pageIndex: " + scrollerEvent.getPageIndex());
		
	}

	/**
	 * @return the dataTableProductos
	 */
	public HtmlDataTable getDataTableProductos() {
		if (dataTableProductos == null) {
			dataTableProductos = (HtmlDataTable) 
					findComponentInRoot("dataTableProductos");
        }
		return dataTableProductos;
	}

	/**
	 * @param dataTableProductos the dataTableProductos to set
	 */
	public void setDataTableProductos(HtmlDataTable dataTableProductos) {
		this.dataTableProductos = dataTableProductos;
	}

	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	/**
	 * @return the productosList
	 */
	public List<ProductosVO> getProductosList() {
		return productosList;
	}

	/**
	 * @param productosList the productosList to set
	 */
	public void setProductosList(List<ProductosVO> productosList) {
		this.productosList = productosList;
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}

	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}

}
