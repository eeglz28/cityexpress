package mx.com.cityexpress.modules.admon.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IAdministraController;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;

public class ClientesPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	private HtmlDataTable dataTableClientes;
	
	private String initPage;
    private List<ClientesTblVO> clientesList;
    private Long rowCount = new Long(30);
    
    private IAdministraController administraController;
    
    
    private void loadPage() {
    	log.info(":::...[PAGE] begin loadPage OK");
    	try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return;	
			}
			
			log.info(":::...[PAGE] Usuario session: " + userSesion.getUsuario());
			
			setClientesList( administraController.obtieneTodosClientes() );
			log.info(":::...[PAGE] end loadPage OK");
    	} catch (Exception e) {
    		log.error(":::...[PAGE] end loadPage " + e.getMessage());
    		return;
    	}
    }
    
    public String doCrearAction() { 
    	log.info(":::...[PAGE] doCrearAction --> Crea Prod");
    	return ConstantsCExpress.GO_CLIENTES_TO_CREA_CLIE;
    }
    
    @SuppressWarnings("unchecked")
	public String doActualizarAction() {
    	log.info(":::...[PAGE] begin doActualizarAction OK");
    	try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_CLIENTES_TO_NOSESSION;	
			}
			
			ClientesTblVO clientesVO = (ClientesTblVO)
					getDataTableClientes().getRowData();
			
			log.info(":::...[PAGE] Id: " + clientesVO.getIdCliente() + 
					"  Nombre: " +clientesVO.getNombre());
			
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().put(
							ConstantsCExpress.SESSION_TBL_VO, clientesVO);
			
			log.info(":::...[PAGE] end doActualizarAction OK");
			return ConstantsCExpress.GO_CLIENTES_TO_ACT_CLIE;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doActualizarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_CLIENTES_TO_FAIL;
    	}
    }
    
    @SuppressWarnings("unchecked")
	public String doUsuariosAction() {
    	log.info(":::...[PAGE] doUsuariosAction --> Usuarios");
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_CLIENTES_TO_NOSESSION;		
		}
    	
    	ClientesTblVO clientesVO = (ClientesTblVO)
				getDataTableClientes().getRowData();
    	
    	log.info(">>> Id Cliente : " + clientesVO.getIdCliente());
    	
    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
    			put(ConstantsCExpress.SESSION_ID, clientesVO.getIdCliente());
    	
    	return ConstantsCExpress.GO_CLIENTES_TO_USUARIOS;
    }
    
    public String doRegresarAction() {
    	log.info(":::...[PAGE] doRegresarAction --> Menu");
    	
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_CLIENTES_TO_NOSESSION;		
		}
    	
    	return ConstantsCExpress.GO_CLIENTES_TO_MENU;
    }
    
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + 
				", pageIndex: " + scrollerEvent.getPageIndex());
		
	}

	/**
	 * @return the dataTableClientes
	 */
	public HtmlDataTable getDataTableClientes() {
		if (dataTableClientes == null) {
			dataTableClientes = (HtmlDataTable) 
					findComponentInRoot("dataTableClientes");
        }
		return dataTableClientes;
	}

	/**
	 * @param dataTableClientes the dataTableClientes to set
	 */
	public void setDataTableClientes(HtmlDataTable dataTableClientes) {
		this.dataTableClientes = dataTableClientes;
	}

	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	/**
	 * @return the clientesList
	 */
	public List<ClientesTblVO> getClientesList() {
		return clientesList;
	}

	/**
	 * @param clientesList the clientesList to set
	 */
	public void setClientesList(List<ClientesTblVO> clientesList) {
		this.clientesList = clientesList;
	}

	/**
	 * @return the administraController
	 */
	public IAdministraController getAdministraController() {
		return administraController;
	}

	/**
	 * @param administraController the administraController to set
	 */
	public void setAdministraController(IAdministraController administraController) {
		this.administraController = administraController;
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

}
