package mx.com.cityexpress.modules.admon.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;
import mx.com.cityexpress.mail.sender.MailerSender;
import mx.com.cityexpress.mail.sender.MailerVo;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.mapping.pojo.Product;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.mapping.pojo.Users;
import mx.com.cityexpress.modules.admon.services.IAdministraService;
import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;
import mx.com.cityexpress.modules.admon.vo.ProductosVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuariosTblVO;
import mx.com.cityexpress.persistence.dao.IClientDao;
import mx.com.cityexpress.persistence.dao.IProductDao;
import mx.com.cityexpress.persistence.dao.IProfileDao;
import mx.com.cityexpress.persistence.dao.IUsersDao;

public class AdministraServiceImpl implements IAdministraService {
	
	private Log log = LogFactory.getLog(getClass());
	
	private IProductDao productDao;
	private IClientDao clientDao;
	private IUsersDao usersDao;
	private IProfileDao profileDao;
	private MailerSender mailer;
	
	
	public List<ProductosVO> obtieneTodosProductos() throws BusinessException {
		log.info(":::...[S] begin obtieneTodosProductos OK");
		List<ProductosVO> prodVOList = new ArrayList<ProductosVO>();
		try {
			List<Product> productList = productDao.selectAllProducts();
			if (productList != null) {
				for (Product product : productList) {
					ProductosVO prodVO = new ProductosVO();
					prodVO.setIdProducto(product.getIdProduct().toString());
					prodVO.setCodigo(String.valueOf(product.getCodeProduct()));
					prodVO.setNombre(product.getName());
					prodVO.setPrecio(product.getPrice().toString());
					prodVO.setEstatus(String.valueOf(product.getStatus()));
					
					if (product.getStatus() == 0) {
						prodVO.setDescEstatus("Activo");	
					} else {
						prodVO.setDescEstatus("Bloqueado");
					}
					
					prodVO.setDescripcion(product.getDescription());
					prodVO.setUnidad(product.getUnidad());
					prodVO.setEntrega(product.getEntrega());
					
					if (product.isFolioStatus()) {
						prodVO.setConFolio("1");	
					} else {
						prodVO.setConFolio("0");
					}
					
					prodVOList.add(prodVO);
					
				}
			}
			
			log.info(":::...[S] end obtieneTodosProductos OK");
			return prodVOList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtieneTodosProductos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtieneTodosProductos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	
	public String creaProducto(ProductoInputVO productoVO) 
			throws BusinessException {
		
		log.info(":::...[S] begin creaProducto OK");
		String message = null;
		
		try {
			
			Product p = productDao.selectProductByCode(
					new Integer(productoVO.getCodigo()));
			if (p != null) {
				message = "El código del producto ya existe, ingrese otro código.";
				return message;
			}
			
			Product product = new Product();
			product.setCodeProduct(new Integer(productoVO.getCodigo()));
			product.setName(productoVO.getNombre());
			product.setPrice(new BigDecimal(productoVO.getPrecio()));
			
			boolean estatusBool = new Boolean(productoVO.getEstatus());
			log.info(":::...[S] prdEstatus: " + productoVO.getEstatus() + " -boolean: " + estatusBool);
			if (estatusBool) {
				product.setStatus(0);
			} else {
				product.setStatus(1);
			}
			
			product.setDescription(productoVO.getDescripcion());
			product.setUnidad(productoVO.getUnidad());
			product.setEntrega(productoVO.getEntrega());
			product.setFolioStatus(new Boolean(productoVO.getFolio()));
			
			productDao.saveProduct(product);
			
			log.info(":::...[S] end creaProducto OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end creaProducto : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end creaProducto : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	
	public String actualizaProducto(ProductoInputVO productoVO) 
			throws BusinessException {
		
		log.info(":::...[S] begin actualizaProducto OK");
		String message = null;
		try {
			
			if (!productoVO.getCodigo().equals(productoVO.getCodigoAnterior())) {
				Product p = productDao.selectProductByCode(
						new Integer(productoVO.getCodigo()));
				if (p != null) {
					message = "El código del producto ya existe, ingrese otro código.";
					return message;
				}
			}
			
			Product product = new Product();
			product.setIdProduct(new Long(productoVO.getIdProducto()));
			product.setCodeProduct(new Integer(productoVO.getCodigo()));
			product.setName(productoVO.getNombre());
			product.setPrice(new BigDecimal(productoVO.getPrecio()));
			
			boolean estatusBool = new Boolean(productoVO.getEstatus());
			log.info(":::...[S] prdEstatus: " + productoVO.getEstatus() + " -boolean: " + estatusBool);
			if (estatusBool) {
				product.setStatus(0);
			} else {
				product.setStatus(1);
			}
			
			product.setDescription(productoVO.getDescripcion());
			product.setUnidad(productoVO.getUnidad());
			product.setEntrega(productoVO.getEntrega());
			product.setFolioStatus(new Boolean(productoVO.getFolio()));
			
			productDao.updateProduct(product);
			
			log.info(":::...[S] end actualizaProducto OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end actualizaProducto : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end actualizaProducto : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	
	public List<ClientesTblVO> obtieneTodosClientes() throws BusinessException {
		log.info(":::...[S] begin obtieneTodosClientes OK");
		List<ClientesTblVO> clieVOList = new ArrayList<ClientesTblVO>();
		try {
			List<Client> clientList = clientDao.selectClients();
			if (clientList != null) {
				for (Client client : clientList) {
					ClientesTblVO clientVO = new ClientesTblVO();
					clientVO.setIdCliente(client.getIdClient().toString());
					clientVO.setNombre(client.getClientName());
					clientVO.setContacto(client.getAgent());
					clientVO.setEstado(client.getAddress());
					clientVO.setTelefono(client.getPhone());
					
					clieVOList.add(clientVO);
				}
			}
			
			log.info(":::...[S] end obtieneTodosClientes OK");
			return clieVOList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtieneTodosClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtieneTodosClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public String creaCliente(ClienteInputVO clienteVO) 
			throws BusinessException {
		log.info(":::...[S] begin creaCliente OK");
		
		String message = null;
		try {
			
			Client c = clientDao.selectClientByName(clienteVO.getNombre());
			if (c != null) {
				message = "El nombre del cliente ya existe, ingrese otro nombre.";
				return message;
			}
			
			Client client = new Client();
			client.setClientName(clienteVO.getNombre());
			client.setAgent(clienteVO.getContacto());
			client.setAddress(clienteVO.getEstado());
			client.setPhone(clienteVO.getTelefono());
			client.setEmail(clienteVO.getEmail());
			
			clientDao.saveClient(client);
			
			log.info(":::...[S] end creaCliente OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end creaCliente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end creaCliente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public String actualizaCliente(ClienteInputVO clienteVO) 
			throws BusinessException {
		log.info(":::...[S] begin actualizaCliente OK");
		
		String message = null;
		try {
			
			if (!clienteVO.getNombre().trim().equals(
					clienteVO.getNombreAnterior().trim())) {
				Client c = clientDao.selectClientByName(clienteVO.getNombre());
				if (c != null) {
					message = "El nombre del cliente ya existe, ingrese otro nombre.";
					return message;
				}
			}
			
			Client client = new Client();
			client.setIdClient(new Long(clienteVO.getIdCliente()));
			client.setClientName(clienteVO.getNombre());
			client.setAgent(clienteVO.getContacto());
			client.setAddress(clienteVO.getEstado());
			client.setPhone(clienteVO.getTelefono());
			client.setEmail(clienteVO.getEmail());
			
			clientDao.updateClient(client);
			
			log.info(":::...[S] end actualizaCliente OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end actualizaCliente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end actualizaCliente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public List<UsuariosTblVO> obtieneTodosUsuarios(Long idClient) 
			throws BusinessException {
		
		log.info(":::...[S] begin obtieneTodosUsuarios OK");
		List<UsuariosTblVO> usuVOList = new ArrayList<UsuariosTblVO>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try {
			List<Users> usuariosList = usersDao.selectUsersByClient(idClient);
			if (usuariosList != null) {
				for (Users user : usuariosList) {
					UsuariosTblVO usuarioVO = new UsuariosTblVO();
					usuarioVO.setIdUsuario(user.getIdUser().toString());
					usuarioVO.setIdCliente(idClient.toString());
					usuarioVO.setNickName(user.getNick());
					usuarioVO.setPassword(user.getPass());
					
					log.info("Otiene todos los usuarios --> Creation Date 1: " + user.getCreationDate());
					usuarioVO.setCreationDate(user.getCreationDate());
					log.info("Otiene todos los usuarios --> Creation Date 2: " + usuarioVO.getCreationDate());
					
					if (user.getUpdateDate() != null) {
						usuarioVO.setUpdateDate(
								sdf.format(user.getUpdateDate()));
					}
					
					usuarioVO.setInvalid(String.valueOf(user.getInvalid()));
					usuarioVO.setEstatus(String.valueOf(user.getStatus()));
					if (user.getStatus() == 1) {
						usuarioVO.setEstatusDesc("Activo");	
					} else {
						usuarioVO.setEstatusDesc("Bloqueado");
					}
					
					usuarioVO.setEmail(user.getEmail());
					usuarioVO.setName(user.getName());
					usuarioVO.setProfile(String.valueOf(
							user.getProfile().getIdProfile()));
					usuarioVO.setProfileDesc(user.getProfile().getDescription());
					
					usuVOList.add(usuarioVO);
				}
			}
			
			log.info(":::...[S] end obtieneTodosUsuarios OK");
			return usuVOList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtieneTodosUsuarios : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtieneTodosUsuarios : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	
	public String creaUsuario(UsuarioInputVO usuarioVO) 
			throws BusinessException {
		log.info(":::...[S] begin creaUsuario OK");
		
		String message = null;
		Date date = new Date();
		try {
			
			Users u = usersDao.getUserbyNick(usuarioVO.getNickName());
			if (u != null) {
				message = "El nombre de usuario ya existe, ingrese otro nombre.";
				return message;
			}
			
			Users user = new Users();
			
			Client cli = clientDao.selectClient(
					new Long(usuarioVO.getIdCliente()));
			user.setClient(cli);
			
			user.setNick(usuarioVO.getNickName());
			user.setPass(usuarioVO.getPassword());
			
			user.setCreationDate(date);
			user.setInvalid(0);
			
			boolean estatusBool = new Boolean(usuarioVO.getEstatus());
			if (estatusBool) {
				user.setStatus(2); //Usuario Bloqueado
			} else {
				user.setStatus(1); //Usuario Activo
			}
			
			user.setName(usuarioVO.getName());
			user.setEmail(usuarioVO.getEmail());
			
			Profile p = profileDao.selectProfileById(usuarioVO.getIdPerfil());
			user.setProfile(p);
			
			usersDao.saveUsuario(user);
			
			log.info(":::...[S] end creaUsuario OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end creaUsuario : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end creaUsuario : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public String actualizaUsuario(UsuarioInputVO usuarioVO) 
			throws BusinessException {
		log.info(":::...[S] begin actualizaUsuario OK");
		
		String message = null;
		Date date = new Date();
		try {
			
			if (!usuarioVO.getNickName().trim().equals(
					usuarioVO.getNickNameAnterior().trim())) {
				Users u = usersDao.getUserbyNick(usuarioVO.getNickName());
				if (u != null) {
					message = "El nombre de usuario ya existe, ingrese otro nombre.";
					return message;
				}
			}
			
			Users user = new Users();
			
			user.setIdUser(new Long(usuarioVO.getIdUsuario()));
			Client cli = clientDao.selectClient(
					new Long(usuarioVO.getIdCliente()));
			user.setClient(cli);
			
			user.setNick(usuarioVO.getNickName());
			user.setPass(usuarioVO.getPassword());
			log.info("Creation Date: " + usuarioVO.getCreationDate());
			user.setCreationDate(usuarioVO.getCreationDate());
			user.setUpdateDate(date);
			user.setInvalid(0);
			
			boolean estatusBool = new Boolean(usuarioVO.getEstatus());
			if (estatusBool) {
				user.setStatus(2);
			} else {
				user.setStatus(1);
			}
			
			user.setName(usuarioVO.getName());
			user.setEmail(usuarioVO.getEmail());
			
			Profile p = profileDao.selectProfileById(usuarioVO.getIdPerfil());
			user.setProfile(p);
			
			usersDao.updateUsuario(user);
			
			log.info(":::...[S] end actualizaUsuario OK");
			return message;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end actualizaUsuario : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end actualizaUsuario : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public String obtieneNombreClientePorId(String idCliente) 
			throws BusinessException {
		log.info(":::...[S] begin obtieneNombreClientePorId OK");
		String nombre = null;
		try {
			Client c = clientDao.selectClient(new Long(idCliente));
			nombre = c.getClientName();
			log.info(":::...[S] end obtieneNombreClientePorId OK");
			return nombre;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtieneNombreClientePorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtieneNombreClientePorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public List<Profile> obtienePerfiles(String val) throws BusinessException {
		log.info(":::...[S] begin obtienePerfiles OK");
		try {
			List<Profile> listaPerfiles = profileDao.selectAllProfiles(val);
			log.info(":::...[S] end obtienePerfiles OK");
			return listaPerfiles;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtienePerfiles : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtienePerfiles : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public List<ProfileVo> retrieveProfileByType(String type) throws BusinessException {
		log.info(":::...[S] begin retrieveProfileByType OK");
		List<ProfileVo> profileVoList = new ArrayList<ProfileVo>();
		try {
			List<Profile> profileList = profileDao.selectProfileByType(type);
			if (profileList != null && profileList.size() > 0) {
				for(Profile profile : profileList ) {
					ProfileVo profileVo = new ProfileVo();
					profileVo.setIdProfile(profile.getIdProfile().toString());
					profileVo.setDescription(profile.getDescription());
					profileVo.setTypeId(profile.getType());
					profileVoList.add(profileVo);
				}
			}
			log.info(":::...[S] end retrieveProfileByType OK");
			return profileVoList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end retrieveProfileByType : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end retrieveProfileByType : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public List<ClientVo> getOnlyClients() throws BusinessException {
		log.info(":::...[S] begin getOnlyClients OK");
		List<ClientVo> clientVoList = new ArrayList<ClientVo>();
		try {
			List<Client> clientList = clientDao.selectOnlyClients();
			if (clientList != null) {
				for (Client client : clientList) {
					ClientVo clientVo = new ClientVo(
							Integer.parseInt(client.getIdClient().toString()), 
							client.getClientName());
					
					clientVoList.add(clientVo);
				}
			}
			
			log.info(":::...[S] end getOnlyClients OK");
			return clientVoList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end getOnlyClients : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end getOnlyClients : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	public String sendMasiveMail(String[] clientIdList, String[] profileIdList, 
			String body, String subject) throws BusinessException {
		String message = "El correo se ha enviando satisfactoriamente";
		log.info(":::...[S] begin sendMasiveMail OK");
		try {
			List<String> emailList =
					usersDao.selectUserEmail(clientIdList, profileIdList);
			Object[] userEmails = emailList.toArray();
			String bccDestination[] = new String[userEmails.length];
			int index = 0;
			for (Object obj : userEmails) {
				bccDestination[index] = obj.toString();
				index++;
			}
			
			if (bccDestination == null || bccDestination.length < 1) {
				message = "No hay destinatarios a quien enviar el correo";
				return message;
			}
			
			String[] destination = new String[] {
					ConstantsCExpress.MAIL_ADMIN, 
					ConstantsCExpress.MAIL_CC_1,
					ConstantsCExpress.MAIL_CC_2,
					ConstantsCExpress.MAIL_CC_3,
					ConstantsCExpress.MAIL_CC_4,
					ConstantsCExpress.MAIL_CC_5,
					ConstantsCExpress.MAIL_CC_6
					};
			
			MailerVo mailerVo = new MailerVo();
			mailerVo.setSubject(subject);
			mailerVo.setFrom(ConstantsCExpress.MAIL_FROM);
			mailerVo.setDestination(destination);
			mailerVo.setBccDestination(bccDestination);
			mailerVo.setBody(body);
			
			mailer.send(mailerVo);
			
		} catch (DatabaseException e) {
    		log.error(":::...[S] end sendMasiveMail : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end sendMasiveMail : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		return message;
	}
	
	

	/**
	 * @return the productDao
	 */
	public IProductDao getProductDao() {
		return productDao;
	}

	/**
	 * @param productDao the productDao to set
	 */
	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}


	/**
	 * @return the clientDao
	 */
	public IClientDao getClientDao() {
		return clientDao;
	}


	/**
	 * @param clientDao the clientDao to set
	 */
	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}


	/**
	 * @return the usersDao
	 */
	public IUsersDao getUsersDao() {
		return usersDao;
	}


	/**
	 * @param usersDao the usersDao to set
	 */
	public void setUsersDao(IUsersDao usersDao) {
		this.usersDao = usersDao;
	}


	/**
	 * @return the profileDao
	 */
	public IProfileDao getProfileDao() {
		return profileDao;
	}


	/**
	 * @param profileDao the profileDao to set
	 */
	public void setProfileDao(IProfileDao profileDao) {
		this.profileDao = profileDao;
	}


	/**
	 * @return the mailer
	 */
	public MailerSender getMailer() {
		return mailer;
	}


	/**
	 * @param mailer the mailer to set
	 */
	public void setMailer(MailerSender mailer) {
		this.mailer = mailer;
	}

}
