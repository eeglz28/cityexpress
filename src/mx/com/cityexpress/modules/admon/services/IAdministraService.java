package mx.com.cityexpress.modules.admon.services;

import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.ClientVo;
import mx.com.cityexpress.general.vo.ProfileVo;
import mx.com.cityexpress.mapping.pojo.Profile;
import mx.com.cityexpress.modules.admon.vo.ClienteInputVO;
import mx.com.cityexpress.modules.admon.vo.ClientesTblVO;
import mx.com.cityexpress.modules.admon.vo.ProductoInputVO;
import mx.com.cityexpress.modules.admon.vo.ProductosVO;
import mx.com.cityexpress.modules.admon.vo.UsuarioInputVO;
import mx.com.cityexpress.modules.admon.vo.UsuariosTblVO;

public interface IAdministraService {
	
	public List<ProductosVO> obtieneTodosProductos() throws BusinessException;
	
	public String creaProducto(ProductoInputVO productoVO) 
			throws BusinessException;
	
	public String actualizaProducto(ProductoInputVO productoVO) 
			throws BusinessException;
	
	
	public List<ClientesTblVO> obtieneTodosClientes() throws BusinessException;
	
	public String creaCliente(ClienteInputVO clienteVO) 
			throws BusinessException;
	
	public String actualizaCliente(ClienteInputVO clienteVO) 
			throws BusinessException;
	
	public List<UsuariosTblVO> obtieneTodosUsuarios(Long idClient) 
			throws BusinessException;
	
	public String creaUsuario(UsuarioInputVO usuarioVO) 
			throws BusinessException;
	
	public String actualizaUsuario(UsuarioInputVO usuarioVO) 
			throws BusinessException;
	
	public String obtieneNombreClientePorId(String idCliente) 
			throws BusinessException;
	
	public List<Profile> obtienePerfiles(String val) throws BusinessException;
	
	public List<ProfileVo> retrieveProfileByType(String type) 
			throws BusinessException;
	
	public List<ClientVo> getOnlyClients() throws BusinessException;
	
	public String sendMasiveMail(String[] clientIdList, String[] profileIdList, 
			String body, String subject) throws BusinessException;
	

}
