package mx.com.cityexpress.modules.operation.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IOperationController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;

public class PedidosEnProcesoPage extends PageCode {
	
	private Log log = LogFactory.getLog(PedidosEnProcesoPage.class);
	
    private HtmlDataTable dataTablePedidos;
	private List<OrdersTableVo> ordersList;
    private Long rowCount = new Long(40);
    
    /*
     * 
     * 
     * 
     * 					                   <t:column id="column7"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Exportar" id="dtPeOt13"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{pedEnPro.doExportarAction}" id="exportar" styleClass="linkNormal" rendered="#{varlistPedidosTableVO.estatusRendered}">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/entregar.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                   </t:column>
     */
    
	private IOperationController operationController;
	
	@SuppressWarnings("unchecked")
	public String doExportarAction() {
		log.info(":::...[PAGE]: Begin doEntregarAction --> OK");
		try {			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
			}
	
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doEntregarAction) Pedido Seleccionado: " + ordTblVo.getNumOrder());
			log.info(":::...[PAGE] (doEntregarAction) Cliente Seleccionado: " + ordTblVo.getClientName());
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					put(ConstantsCExpress.SESSION_TBL_VO, ordTblVo);
						
			log.info(":::...[PAGE]: End doEntregarAction --> OK");
			return ConstantsCExpress.GO_PEDIDOS_TO_CAMBIA_ESTATUS;
			
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doEntregarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	}
	}

	
	@SuppressWarnings("unchecked")
	public String doVerHojaSurtidoAction() {
		log.info(":::...[PAGE]: Begin doDetalleAction --> OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PED_EN_PRO_TO_NOSESSION;
			}
	
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doDetalleAction) Pedido Seleccionado: " + ordTblVo.getIdOrder());
			log.info(":::...[PAGE] (doDetalleAction) Cliente Seleccionado: " + ordTblVo.getIdClient());
			
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
					put(ConstantsCExpress.REQUEST_ID_ORDER, ordTblVo);
			
			
			log.info(":::...[PAGE]: End doDetalleAction --> OK");
			return ConstantsCExpress.GO_PED_EN_PRO_TO_HOJA_SUR;
			
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doDetalleAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PED_EN_PRO_TO_FAIL;
    	}
	}
	

	public HtmlDataTable getDataTablePedidos() {
		if (dataTablePedidos == null) {
			dataTablePedidos = (HtmlDataTable) findComponentInRoot("dataTablePedidos");
        }
		return dataTablePedidos;
	}

	public void setDataTablePedidos(HtmlDataTable dataTablePedidos) {
		this.dataTablePedidos = dataTablePedidos;
	}



	public List<OrdersTableVo> getOrdersList() {
		
		BusquedaPedidoVo busquedaVo = new BusquedaPedidoVo(); 
		busquedaVo.setEstatus(ConstantsCExpress.ESTATUS_EN_PROCESO_NUM);
		log.info(":::...[PAGE] (getOrdersList) estatus: " + busquedaVo.getEstatus());
	
		try {
			ordersList = operationController.obtenerPedidos(busquedaVo);
		} catch(ControlException e) {
			e.printStackTrace();
		}
	
		return ordersList;
	}

	public void setOrdersList(List<OrdersTableVo> ordersList) {
		this.ordersList = ordersList;
	}


	public String doRegresarAction() {
		log.info(":::...[PAGE] doRegresarAction regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_PED_EN_PRO_TO_NOSESSION;
		}
		
		return ConstantsCExpress.GO_PED_EN_PRO_TO_MENU;
	}
	
	
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + ", pageIndex: " + scrollerEvent.getPageIndex());
		
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}


	/**
	 * @return the operationController
	 */
	public IOperationController getOperationController() {
		return operationController;
	}


	/**
	 * @param operationController the operationController to set
	 */
	public void setOperationController(IOperationController operationController) {
		this.operationController = operationController;
	}



}
