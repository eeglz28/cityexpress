package mx.com.cityexpress.modules.operation.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IOperationController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class HojaSurtidoPage  extends PageCode {
	
	private Log log = LogFactory.getLog(HojaSurtidoPage.class);
	
	private HtmlDataTable dataTableDetalle;
	
	private String initPage;
	private String finishPage;
	private int sizeListProd;
	
	private String fechaRecepcion;
	private String fechaEntrega;
	private String confirmacion;
	private String cliente;
	private String usuario;
	private Long rowCount = new Long(30);
	private List<DetailOrderConsultaTableVo> listaDetalle;
	
	private String cantidad1;
	private String unidad1;
	private String producto1;
	private String cantidad2;
	private String unidad2;
	private String producto2;
	private String cantidad3;
	private String unidad3;
	private String producto3;
	private String cantidad4;
	private String unidad4;
	private String producto4;
	private String cantidad5;
	private String unidad5;
	private String producto5;
	private String cantidad6;
	private String unidad6;
	private String producto6;
	private String cantidad7;
	private String unidad7;
	private String producto7;
	private String cantidad8;
	private String unidad8;
	private String producto8;
	private String cantidad9;
	private String unidad9;
	private String producto9;
	private String cantidad10;
	private String unidad10;
	private String producto10;
	
	
	private IOperationController operationController;
	
	
	OrdersTableVo ordersVO = (OrdersTableVo) FacesContext.getCurrentInstance().
			getExternalContext().getRequestMap().get(
					ConstantsCExpress.REQUEST_ID_ORDER);
	
	public void load() {
		
//		OrdersTableVo ordersVO = (OrdersTableVo) FacesContext.getCurrentInstance().
//				getExternalContext().getRequestMap().get(
//						ConstantsCExpress.REQUEST_ID_ORDER);
		
		try {
			listaDetalle = operationController.obtenerDetallePedido(
					ordersVO.getIdOrder());
			sizeListProd = listaDetalle.size();
		} catch (ControlException e) {
			e.printStackTrace();
		}
		

		
		log.info("Inicio");

	}

	/**
	 * @return the intiPage
	 */
	public String getInitPage() {
		load();
		return initPage;
	}

	/**
	 * @param intiPage the intiPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	/**
	 * @return the fechaRecepcion
	 */
	public String getFechaRecepcion() {
		log.info("Fecha Recepcion: " + ordersVO.getCreationDate());
		fechaRecepcion = ordersVO.getCreationDate();
		return fechaRecepcion;
	}

	/**
	 * @param fechaRecepcion the fechaRecepcion to set
	 */
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	/**
	 * @return the fechaEntrega
	 */
	public String getFechaEntrega() {
		log.info("Fecha Entrega: " + ordersVO.getEntrega());
		fechaEntrega = ordersVO.getEntrega();
		return fechaEntrega;
	}

	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	/**
	 * @return the confirmacion
	 */
	public String getConfirmacion() {
		log.info("Num Order: " + ordersVO.getNumOrder());
		confirmacion = "Le confirmamos que ha generado el pedido " + 
				ordersVO.getNumOrder() + " y enviado a Publione.";
		return confirmacion;
	}

	/**
	 * @param confirmacion the confirmacion to set
	 */
	public void setConfirmacion(String confirmacion) {
		this.confirmacion = confirmacion;
	}

	/**
	 * @return the operationController
	 */
	public IOperationController getOperationController() {
		return operationController;
	}

	/**
	 * @param operationController the operationController to set
	 */
	public void setOperationController(IOperationController operationController) {
		this.operationController = operationController;
	}

	/**
	 * @return the dataTableDetalle
	 */
	public HtmlDataTable getDataTableDetalle() {
		if (dataTableDetalle == null) {
			dataTableDetalle = (HtmlDataTable) 
					findComponentInRoot("dataTableDetalle");
        }
		return dataTableDetalle;
	}

	/**
	 * @param dataTableDetalle the dataTableDetalle to set
	 */
	public void setDataTableDetalle(HtmlDataTable dataTableDetalle) {
		this.dataTableDetalle = dataTableDetalle;
	}

	/**
	 * @return the listaDetalle
	 */
	public List<DetailOrderConsultaTableVo> getListaDetalle() {
//		try {
//			listaDetalle = operationController.obtenerDetallePedido(
//					ordersVO.getIdOrder());
//		} catch (ControlException e) {
//			e.printStackTrace();
//		}
//		log.info("Listado detail: " + listaDetalle.size());
		return listaDetalle;
	}

	/**
	 * @param listaDetalle the listaDetalle to set
	 */
	public void setListaDetalle(List<DetailOrderConsultaTableVo> listaDetalle) {
		this.listaDetalle = listaDetalle;
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the cantidad1
	 */
	public String getCantidad1() {
		if (sizeListProd > 0) {
			cantidad1 = listaDetalle.get(0).getCantidad().toString();
		}
		return cantidad1;
	}

	/**
	 * @param cantidad1 the cantidad1 to set
	 */
	public void setCantidad1(String cantidad1) {
		this.cantidad1 = cantidad1;
	}

	/**
	 * @return the unidad1
	 */
	public String getUnidad1() {
		if (sizeListProd > 0) {
			unidad1 = listaDetalle.get(0).getUnidad();
		}
		return unidad1;
	}

	/**
	 * @param unidad1 the unidad1 to set
	 */
	public void setUnidad1(String unidad1) {
		this.unidad1 = unidad1;
	}

	/**
	 * @return the producto1
	 */
	public String getProducto1() {
		if (sizeListProd > 0) {
			producto1 = listaDetalle.get(0).getProducto();
		}
		return producto1;
	}

	/**
	 * @param producto1 the producto1 to set
	 */
	public void setProducto1(String producto1) {
		this.producto1 = producto1;
	}



	/**
	 * @return the cantidad2
	 */
	public String getCantidad2() {
		if (sizeListProd > 1) {
			cantidad2 = listaDetalle.get(1).getCantidad().toString();
		}
		return cantidad2;
	}

	/**
	 * @param cantidad2 the cantidad2 to set
	 */
	public void setCantidad2(String cantidad2) {

		this.cantidad2 = cantidad2;
	}

	/**
	 * @return the unidad2
	 */
	public String getUnidad2() {
		if (sizeListProd > 1) {
			unidad2 = listaDetalle.get(1).getUnidad();
		}
		return unidad2;
	}

	/**
	 * @param unidad2 the unidad2 to set
	 */
	public void setUnidad2(String unidad2) {
		this.unidad2 = unidad2;
	}

	/**
	 * @return the producto2
	 */
	public String getProducto2() {
		if (sizeListProd > 1) {
			producto2 = listaDetalle.get(1).getProducto();
		}
		return producto2;
	}

	/**
	 * @param producto2 the producto2 to set
	 */
	public void setProducto2(String producto2) {
		this.producto2 = producto2;
	}


	/**
	 * @return the cantidad3
	 */
	public String getCantidad3() {
		if (sizeListProd > 2) {
			cantidad3 = listaDetalle.get(2).getCantidad().toString();
		}
		return cantidad3;
	}

	/**
	 * @param cantidad3 the cantidad3 to set
	 */
	public void setCantidad3(String cantidad3) {
		this.cantidad3 = cantidad3;
	}

	/**
	 * @return the unidad3
	 */
	public String getUnidad3() {
		if (sizeListProd > 2) {
			unidad3 = listaDetalle.get(2).getUnidad();
		}
		return unidad3;
	}

	/**
	 * @param unidad3 the unidad3 to set
	 */
	public void setUnidad3(String unidad3) {
		this.unidad3 = unidad3;
	}

	/**
	 * @return the producto3
	 */
	public String getProducto3() {
		if (sizeListProd > 2) {
			producto3 = listaDetalle.get(2).getProducto();
		}
		return producto3;
	}

	/**
	 * @param producto3 the producto3 to set
	 */
	public void setProducto3(String producto3) {
		this.producto3 = producto3;
	}


	/**
	 * @return the cantidad4
	 */
	public String getCantidad4() {
		if (sizeListProd > 3) {
			cantidad4 = listaDetalle.get(3).getCantidad().toString();
		}
		return cantidad4;
	}

	/**
	 * @param cantidad4 the cantidad4 to set
	 */
	public void setCantidad4(String cantidad4) {
		this.cantidad4 = cantidad4;
	}

	/**
	 * @return the unidad4
	 */
	public String getUnidad4() {
		if (sizeListProd > 3) {
			unidad4 = listaDetalle.get(3).getUnidad();
		}
		return unidad4;
	}

	/**
	 * @param unidad4 the unidad4 to set
	 */
	public void setUnidad4(String unidad4) {
		this.unidad4 = unidad4;
	}

	/**
	 * @return the producto4
	 */
	public String getProducto4() {
		if (sizeListProd > 3) {
			producto4 = listaDetalle.get(3).getProducto();
		}
		return producto4;
	}

	/**
	 * @param producto4 the producto4 to set
	 */
	public void setProducto4(String producto4) {
		this.producto4 = producto4;
	}

	

	/**
	 * @return the cantidad5
	 */
	public String getCantidad5() {
		if (sizeListProd > 4) {
			cantidad5 = listaDetalle.get(4).getCantidad().toString();
		}
		return cantidad5;
	}

	/**
	 * @param cantidad5 the cantidad5 to set
	 */
	public void setCantidad5(String cantidad5) {
		this.cantidad5 = cantidad5;
	}

	/**
	 * @return the unidad5
	 */
	public String getUnidad5() {
		if (sizeListProd > 4) {
			unidad5 = listaDetalle.get(4).getUnidad();
		}
		return unidad5;
	}

	/**
	 * @param unidad5 the unidad5 to set
	 */
	public void setUnidad5(String unidad5) {
		this.unidad5 = unidad5;
	}

	/**
	 * @return the producto5
	 */
	public String getProducto5() {
		if (sizeListProd > 4) {
			producto5 = listaDetalle.get(4).getProducto();
		}
		return producto5;
	}

	/**
	 * @param producto5 the producto5 to set
	 */
	public void setProducto5(String producto5) {
		this.producto5 = producto5;
	}


	/**
	 * @return the cantidad6
	 */
	public String getCantidad6() {
		if (sizeListProd > 5) {
			cantidad6 = listaDetalle.get(5).getCantidad().toString();
		}
		return cantidad6;
	}

	/**
	 * @param cantidad6 the cantidad6 to set
	 */
	public void setCantidad6(String cantidad6) {
		this.cantidad6 = cantidad6;
	}

	/**
	 * @return the unidad6
	 */
	public String getUnidad6() {
		if (sizeListProd > 5) {
			unidad6 = listaDetalle.get(5).getUnidad();
		}
		return unidad6;
	}

	/**
	 * @param unidad6 the unidad6 to set
	 */
	public void setUnidad6(String unidad6) {
		this.unidad6 = unidad6;
	}

	/**
	 * @return the producto6
	 */
	public String getProducto6() {
		if (sizeListProd > 5) {
			producto6 = listaDetalle.get(5).getProducto();
		}
		return producto6;
	}

	/**
	 * @param producto6 the producto6 to set
	 */
	public void setProducto6(String producto6) {
		this.producto6 = producto6;
	}


	/**
	 * @return the cantidad7
	 */
	public String getCantidad7() {
		if (sizeListProd > 6) {
			cantidad7 = listaDetalle.get(6).getCantidad().toString();
		}
		return cantidad7;
	}

	/**
	 * @param cantidad7 the cantidad7 to set
	 */
	public void setCantidad7(String cantidad7) {
		this.cantidad7 = cantidad7;
	}

	/**
	 * @return the unidad7
	 */
	public String getUnidad7() {
		if (sizeListProd > 6) {
			unidad7 = listaDetalle.get(6).getUnidad();
		}
		return unidad7;
	}

	/**
	 * @param unidad7 the unidad7 to set
	 */
	public void setUnidad7(String unidad7) {
		this.unidad7 = unidad7;
	}

	/**
	 * @return the producto7
	 */
	public String getProducto7() {
		if (sizeListProd > 6) {
			producto7 = listaDetalle.get(6).getProducto();
		}
		return producto7;
	}

	/**
	 * @param producto7 the producto7 to set
	 */
	public void setProducto7(String producto7) {
		this.producto7 = producto7;
	}


	/**
	 * @return the cantidad8
	 */
	public String getCantidad8() {
		if (sizeListProd > 7) {
			cantidad8 = listaDetalle.get(7).getCantidad().toString();
		}
		return cantidad8;
	}

	/**
	 * @param cantidad8 the cantidad8 to set
	 */
	public void setCantidad8(String cantidad8) {
		this.cantidad8 = cantidad8;
	}

	/**
	 * @return the unidad8
	 */
	public String getUnidad8() {
		if (sizeListProd > 7) {
			unidad8 = listaDetalle.get(7).getUnidad();
		}
		return unidad8;
	}

	/**
	 * @param unidad8 the unidad8 to set
	 */
	public void setUnidad8(String unidad8) {
		this.unidad8 = unidad8;
	}

	/**
	 * @return the producto8
	 */
	public String getProducto8() {
		if (sizeListProd > 7) {
			producto8 = listaDetalle.get(7).getProducto();
		}
		return producto8;
	}

	/**
	 * @param producto8 the producto8 to set
	 */
	public void setProducto8(String producto8) {
		this.producto8 = producto8;
	}


	/**
	 * @return the cantidad9
	 */
	public String getCantidad9() {
		if (sizeListProd > 8) {
			cantidad9 = listaDetalle.get(8).getCantidad().toString();
		}
		return cantidad9;
	}

	/**
	 * @param cantidad9 the cantidad9 to set
	 */
	public void setCantidad9(String cantidad9) {
		this.cantidad9 = cantidad9;
	}

	/**
	 * @return the unidad9
	 */
	public String getUnidad9() {
		if (sizeListProd > 8) {
			unidad9 = listaDetalle.get(8).getUnidad();
		}
		return unidad9;
	}

	/**
	 * @param unidad9 the unidad9 to set
	 */
	public void setUnidad9(String unidad9) {
		this.unidad9 = unidad9;
	}

	/**
	 * @return the producto9
	 */
	public String getProducto9() {
		if (sizeListProd > 8) {
			producto9 = listaDetalle.get(8).getProducto();
		}
		return producto9;
	}

	/**
	 * @param producto9 the producto9 to set
	 */
	public void setProducto9(String producto9) {
		this.producto9 = producto9;
	}

	


	/**
	 * @return the cantidad10
	 */
	public String getCantidad10() {
		if (sizeListProd > 9) {
			cantidad10 = listaDetalle.get(9).getCantidad().toString();
		}
		return cantidad10;
	}

	/**
	 * @param cantidad10 the cantidad10 to set
	 */
	public void setCantidad10(String cantidad10) {
		this.cantidad10 = cantidad10;
	}

	/**
	 * @return the unidad10
	 */
	public String getUnidad10() {
		if (sizeListProd > 9) {
			unidad10 = listaDetalle.get(9).getUnidad();
		}
		return unidad10;
	}

	/**
	 * @param unidad10 the unidad10 to set
	 */
	public void setUnidad10(String unidad10) {
		this.unidad10 = unidad10;
	}

	/**
	 * @return the producto10
	 */
	public String getProducto10() {
		if (sizeListProd > 9) {
			producto10 = listaDetalle.get(9).getProducto();
		}
		return producto10;
	}

	/**
	 * @param producto10 the producto10 to set
	 */
	public void setProducto10(String producto10) {
		this.producto10 = producto10;
	}

	


	/**
	 * @return the finishPage
	 */
	public String getFinishPage() {
		int cont = 1;
		try {
			listaDetalle = operationController.obtenerDetallePedido(
					ordersVO.getIdOrder());
		} catch (ControlException e) {
			e.printStackTrace();
		}
		
		for (DetailOrderConsultaTableVo detalleVo : listaDetalle) {
			if (cont == 1) {
				cantidad1 = detalleVo.getCantidad().toString();
				unidad1 = detalleVo.getUnidad().toString();
				producto1 = detalleVo.getProducto().toString();
			}
			if (cont == 2) {
				cantidad2 = detalleVo.getCantidad().toString();
				unidad2 = detalleVo.getUnidad().toString();
				producto2 = detalleVo.getProducto().toString();
			}
			if (cont == 3) {
				cantidad3 = detalleVo.getCantidad().toString();
				unidad3 = detalleVo.getUnidad().toString();
				producto3 = detalleVo.getProducto().toString();
			}
			if (cont == 4) {
				cantidad4 = detalleVo.getCantidad().toString();
				unidad4 = detalleVo.getUnidad().toString();
				producto4 = detalleVo.getProducto().toString();
			}
			if (cont == 5) {
				cantidad5 = detalleVo.getCantidad().toString();
				unidad5 = detalleVo.getUnidad().toString();
				producto5 = detalleVo.getProducto().toString();
			}
			if (cont == 6) {
				cantidad6 = detalleVo.getCantidad().toString();
				unidad6 = detalleVo.getUnidad().toString();
				producto6 = detalleVo.getProducto().toString();
			}
			if (cont == 7) {
				cantidad7 = detalleVo.getCantidad().toString();
				unidad7 = detalleVo.getUnidad().toString();
				producto7 = detalleVo.getProducto().toString();
			}
			if (cont == 8) {
				cantidad8 = detalleVo.getCantidad().toString();
				unidad8 = detalleVo.getUnidad().toString();
				producto8 = detalleVo.getProducto().toString();
			}
			if (cont == 9) {
				cantidad9 = detalleVo.getCantidad().toString();
				unidad9 = detalleVo.getUnidad().toString();
				producto9 = detalleVo.getProducto().toString();
			}
			if (cont == 10) {
				cantidad10 = detalleVo.getCantidad().toString();
				unidad10 = detalleVo.getUnidad().toString();
				producto10 = detalleVo.getProducto().toString();
			}
			cont++;
		}
		
		
		
		
		
		
		return finishPage;
	}

	/**
	 * @param finishPage the finishPage to set
	 */
	public void setFinishPage(String finishPage) {
		this.finishPage = finishPage;
	}

	/**
	 * @return the cliente
	 */
	public String getCliente() {
		cliente = ordersVO.getClientName();
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		usuario = ordersVO.getNombreUsuario();
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	

}
