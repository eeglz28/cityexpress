package mx.com.cityexpress.modules.reportes.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.transformer.ProductToProductReportTransformer;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.reportes.services.IReporteService;
import mx.com.cityexpress.modules.reportes.vo.ClienteCantidadVo;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;
import mx.com.cityexpress.persistence.dao.IClientDao;
import mx.com.cityexpress.persistence.dao.IProductDao;

public class ReporteServiceImpl implements IReporteService {
	
	private Log log = LogFactory.getLog(ReporteServiceImpl.class);
	
	private IProductDao productDao;
	private IClientDao clientDao;
	
	public List<RepProductoVo> obtenerReporteProductos(BusquedaFiltroVO filtroVo)
			throws BusinessException {
		
		log.info(":::...[S]: begin obtenerReporteProductos OK");
		
		List<RepProductoVo> repProductList = new ArrayList<RepProductoVo>(); 
		try {
			List<Object[]> objList = productDao.selectRepProductos(filtroVo);
			repProductList = ProductToProductReportTransformer.
					transform(objList);
			
			
			for (RepProductoVo prodVo : repProductList) {
				log.info("...................................................");
				log.info("Concepto: " + prodVo.getConcepto());
				
				for (ClienteCantidadVo clienteVo : prodVo.getClientes()) {
					log.info("   Cliente: " + clienteVo.getCliente() + " | " +
							 "   Unidad: " + clienteVo.getUnidad() + " | " +
							 "   Pedido: " + clienteVo.getPedido() + " | " +
							 "   Cantidad: " + clienteVo.getTotal());
				}
				
				log.info("Total Producto: " + prodVo.getTotalProducto());
				
			}
			
			
			log.info(":::...[S]: end obtenerReporteProductos OK");
			return repProductList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerReporteProductos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerReporteProductos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}

	}
	
	
	public List<Client> obtenerClientes() throws BusinessException {
		log.info(":::...[S] begin obtenerClientes OK");
		try {
			List<Client> clientesList = clientDao.selectClients();
			
			log.info(":::...[S] end obtenerClientes OK");
			return clientesList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}

	/**
	 * @return the productDao
	 */
	public IProductDao getProductDao() {
		return productDao;
	}

	/**
	 * @param productDao the productDao to set
	 */
	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}


	/**
	 * @return the clientDao
	 */
	public IClientDao getClientDao() {
		return clientDao;
	}


	/**
	 * @param clientDao the clientDao to set
	 */
	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}

}
