package mx.com.cityexpress.modules.reportes.services;

import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

public interface IReporteService {
	
	public List<RepProductoVo> obtenerReporteProductos(BusquedaFiltroVO filtroVo) 
			throws BusinessException;
	
	public List<Client> obtenerClientes() throws BusinessException;

}
