package mx.com.cityexpress.modules.reportes.vo;

public class HeaderReportVo {
	private String header1;
	private String header2;
	private String header3;
	private String header4;
	private String header5;
	private String header6;
	private String header7;
	private String header8;
	
	/**
	 * @param header1
	 * @param header2
	 * @param header3
	 * @param header4
	 * @param header5
	 */
	public HeaderReportVo(String header1, String header2, String header3,
			String header4, String header5) {
		super();
		this.header1 = header1;
		this.header2 = header2;
		this.header3 = header3;
		this.header4 = header4;
		this.header5 = header5;
	}
	
	/**
	 * @param header1
	 * @param header2
	 * @param header3
	 * @param header4
	 * @param header5
	 */
	public HeaderReportVo(String header1, String header2, String header3,
			String header4, String header5, String header6) {
		super();
		this.header1 = header1;
		this.header2 = header2;
		this.header3 = header3;
		this.header4 = header4;
		this.header5 = header5;
		this.header6 = header6;
	}

	/**
	 * @return the header1
	 */
	public String getHeader1() {
		return header1;
	}

	/**
	 * @param header1 the header1 to set
	 */
	public void setHeader1(String header1) {
		this.header1 = header1;
	}

	/**
	 * @return the header2
	 */
	public String getHeader2() {
		return header2;
	}

	/**
	 * @param header2 the header2 to set
	 */
	public void setHeader2(String header2) {
		this.header2 = header2;
	}

	/**
	 * @return the header3
	 */
	public String getHeader3() {
		return header3;
	}

	/**
	 * @param header3 the header3 to set
	 */
	public void setHeader3(String header3) {
		this.header3 = header3;
	}

	/**
	 * @return the header4
	 */
	public String getHeader4() {
		return header4;
	}

	/**
	 * @param header4 the header4 to set
	 */
	public void setHeader4(String header4) {
		this.header4 = header4;
	}

	/**
	 * @return the header5
	 */
	public String getHeader5() {
		return header5;
	}

	/**
	 * @param header5 the header5 to set
	 */
	public void setHeader5(String header5) {
		this.header5 = header5;
	}

	/**
	 * @return the header6
	 */
	public String getHeader6() {
		return header6;
	}

	/**
	 * @param header6 the header6 to set
	 */
	public void setHeader6(String header6) {
		this.header6 = header6;
	}

	/**
	 * @return the header7
	 */
	public String getHeader7() {
		return header7;
	}

	/**
	 * @param header7 the header7 to set
	 */
	public void setHeader7(String header7) {
		this.header7 = header7;
	}

	/**
	 * @return the header8
	 */
	public String getHeader8() {
		return header8;
	}

	/**
	 * @param header8 the header8 to set
	 */
	public void setHeader8(String header8) {
		this.header8 = header8;
	}
	
	

}
