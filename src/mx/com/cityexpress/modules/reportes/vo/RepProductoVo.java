package mx.com.cityexpress.modules.reportes.vo;

import java.util.List;

public class RepProductoVo {
	
	private String concepto;
	private List<ClienteCantidadVo> clientes;
	private String totalProducto;
	
	
	/**
	 * 
	 */
	public RepProductoVo() {
		super();
	}


	/**
	 * @param concepto
	 * @param clientes
	 * @param totalProducto
	 */
	public RepProductoVo(String concepto, List<ClienteCantidadVo> clientes,
			String totalProducto) {
		super();
		this.concepto = concepto;
		this.clientes = clientes;
		this.totalProducto = totalProducto;
	}


	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}


	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	/**
	 * @return the clientes
	 */
	public List<ClienteCantidadVo> getClientes() {
		return clientes;
	}


	/**
	 * @param clientes the clientes to set
	 */
	public void setClientes(List<ClienteCantidadVo> clientes) {
		this.clientes = clientes;
	}


	/**
	 * @return the totalProducto
	 */
	public String getTotalProducto() {
		return totalProducto;
	}


	/**
	 * @param totalProducto the totalProducto to set
	 */
	public void setTotalProducto(String totalProducto) {
		this.totalProducto = totalProducto;
	}
	
	

}
