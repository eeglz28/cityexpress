package mx.com.cityexpress.modules.reportes.vo;

import javax.faces.component.html.HtmlSelectOneMenu;

import org.apache.myfaces.custom.calendar.HtmlInputCalendar;

public class RepProductoBusValidateVo {
	
	private HtmlInputCalendar initDate;
	private HtmlInputCalendar endDate;
	private HtmlSelectOneMenu estatusSelectOneMenu;
	private HtmlSelectOneMenu clienteSelectOneMenu;
	
	private String fechasErr;
	private String estatusErr;
	/**
	 * @param initDate
	 * @param endDate
	 * @param estatusSelectOneMenu
	 */
	public RepProductoBusValidateVo(HtmlInputCalendar initDate,
			HtmlInputCalendar endDate, HtmlSelectOneMenu estatusSelectOneMenu,
			HtmlSelectOneMenu clienteSelectOneMenu) {
		super();
		this.initDate = initDate;
		this.endDate = endDate;
		this.estatusSelectOneMenu = estatusSelectOneMenu;
		this.clienteSelectOneMenu = clienteSelectOneMenu;
	}
	/**
	 * @param initDate
	 * @param endDate
	 * @param estatusSelectOneMenu
	 * @param fechasErr
	 * @param estatusErr
	 */
	public RepProductoBusValidateVo(HtmlInputCalendar initDate,
			HtmlInputCalendar endDate, HtmlSelectOneMenu estatusSelectOneMenu,
			String fechasErr, String estatusErr, HtmlSelectOneMenu clienteSelectOneMenu) {
		super();
		this.initDate = initDate;
		this.endDate = endDate;
		this.estatusSelectOneMenu = estatusSelectOneMenu;
		this.clienteSelectOneMenu = clienteSelectOneMenu;
		this.fechasErr = fechasErr;
		this.estatusErr = estatusErr;
	}
	/**
	 * @return the initDate
	 */
	public HtmlInputCalendar getInitDate() {
		return initDate;
	}
	/**
	 * @param initDate the initDate to set
	 */
	public void setInitDate(HtmlInputCalendar initDate) {
		this.initDate = initDate;
	}
	/**
	 * @return the endDate
	 */
	public HtmlInputCalendar getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(HtmlInputCalendar endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the estatusSelectOneMenu
	 */
	public HtmlSelectOneMenu getEstatusSelectOneMenu() {
		return estatusSelectOneMenu;
	}
	/**
	 * @param estatusSelectOneMenu the estatusSelectOneMenu to set
	 */
	public void setEstatusSelectOneMenu(HtmlSelectOneMenu estatusSelectOneMenu) {
		this.estatusSelectOneMenu = estatusSelectOneMenu;
	}
	/**
	 * @return the fechasErr
	 */
	public String getFechasErr() {
		return fechasErr;
	}
	/**
	 * @param fechasErr the fechasErr to set
	 */
	public void setFechasErr(String fechasErr) {
		this.fechasErr = fechasErr;
	}
	/**
	 * @return the estatusErr
	 */
	public String getEstatusErr() {
		return estatusErr;
	}
	/**
	 * @param estatusErr the estatusErr to set
	 */
	public void setEstatusErr(String estatusErr) {
		this.estatusErr = estatusErr;
	}
	/**
	 * @return the clienteSelectOneMenu
	 */
	public HtmlSelectOneMenu getClienteSelectOneMenu() {
		return clienteSelectOneMenu;
	}
	/**
	 * @param clienteSelectOneMenu the clienteSelectOneMenu to set
	 */
	public void setClienteSelectOneMenu(HtmlSelectOneMenu clienteSelectOneMenu) {
		this.clienteSelectOneMenu = clienteSelectOneMenu;
	}
	
	

}
