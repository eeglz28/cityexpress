package mx.com.cityexpress.modules.reportes.vo;

public class ClienteCantidadVo {
	
	private String cliente;
	private String unidad;
	private String total;
	private String pedido;
	
	
	/**
	 * @param cliente
	 * @param unidad
	 * @param total
	 */
	public ClienteCantidadVo(String cliente, String unidad, String total) {
		super();
		this.cliente = cliente;
		this.unidad = unidad;
		this.total = total;
	}
	
	/**
	 * @param cliente
	 * @param unidad
	 * @param total
	 */
	public ClienteCantidadVo(String cliente, String unidad, String total, String pedido) {
		super();
		this.cliente = cliente;
		this.unidad = unidad;
		this.total = total;
		this.pedido = pedido;
	}


	/**
	 * @return the cliente
	 */
	public String getCliente() {
		return cliente;
	}


	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	/**
	 * @return the unidad
	 */
	public String getUnidad() {
		return unidad;
	}


	/**
	 * @param unidad the unidad to set
	 */
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}


	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}


	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	
	
	public String toString() {
		return  "[Cliente: "+ getCliente() +"], " +
				"[Unidad: "+ getUnidad() +"], " +
				"[Total: "+ getTotal() +"], ";
	}

	/**
	 * @return the pedido
	 */
	public String getPedido() {
		return pedido;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}
	

}
