package mx.com.cityexpress.modules.reportes.builder;

import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.modules.reportes.vo.RepProductoBusValidateVo;

public class ReporteBuilder {
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public BusquedaFiltroVO buildRepProductoBus(RepProductoBusValidateVo validateVO) {
		BusquedaFiltroVO inputVO = new BusquedaFiltroVO ();
		
		if (validateVO.getInitDate().getValue() != null && validateVO.getInitDate().getValue().toString().length() > 0) {
			Date fi = (Date)validateVO.getInitDate().getValue();
			String sfi = sdf.format(fi);
			inputVO.setFechaInicial(sfi);	
		}
		
		if (validateVO.getEndDate().getValue() != null && validateVO.getEndDate().getValue().toString().length() > 0) {
			Date ff = (Date)validateVO.getEndDate().getValue();
			String sff = sdf.format(ff);
			inputVO.setFechaFinal(sff);
		}
		
		inputVO.setEstatus(
				validateVO.getEstatusSelectOneMenu().getValue().toString());
		
		inputVO.setIdCliente(
				validateVO.getClienteSelectOneMenu().getValue().toString());
		
		return inputVO;
	}

}
