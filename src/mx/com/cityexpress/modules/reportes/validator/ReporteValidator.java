package mx.com.cityexpress.modules.reportes.validator;

import mx.com.cityexpress.general.common.DataValidator;
import mx.com.cityexpress.modules.reportes.vo.RepProductoBusValidateVo;

public class ReporteValidator {
	
	public static boolean validateReporteProducto(RepProductoBusValidateVo validateVO) {
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
//		if ( validator.isEmptyText(validateVO.getInitDate()) || 
//				validator.isEmptyText(validateVO.getEndDate())) {
//			validateVO.setFechasErr("Es obligatorio elegir un rango de fechas.");
//		        valid = false;
//		}
		
		if ( !validator.isValidOption(validateVO.getEstatusSelectOneMenu())) {
			validateVO.setEstatusErr("Es obligatorio elegir un estatus.");
		        valid = false;
		}
        
        return valid;
        
	}

}
