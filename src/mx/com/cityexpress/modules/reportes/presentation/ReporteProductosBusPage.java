package mx.com.cityexpress.modules.reportes.presentation;


import java.util.List;
import java.util.Set;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.calendar.HtmlInputCalendar;

import mx.com.cityexpress.general.common.CatalogoItem;
import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IReporteController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.reportes.builder.ReporteBuilder;
import mx.com.cityexpress.modules.reportes.validator.ReporteValidator;
import mx.com.cityexpress.modules.reportes.vo.RepProductoBusValidateVo;

public class ReporteProductosBusPage extends PageCode {
	
	private Log log = LogFactory.getLog(ReporteProductosBusPage.class);
	
	private IReporteController reporteController;
	
	private HtmlInputCalendar initDate;
	private HtmlInputCalendar endDate;
	private HtmlSelectOneMenu estatusSelectOneMenu;
	private HtmlSelectOneMenu clienteSelectOneMenu;
	protected UISelectItems clienteSelectItems;
	protected SelectItem[] selectItemCliente;
	
	private String fechasErr;
	private String estatusErr;
	
	private String initPage;
	
	public void loadPage() {
		log.info(":::...[PAGE] (loadPage) --> OK");
	}
	
	public HtmlInputCalendar getInitDate() {
		if (initDate == null) {
			initDate = (HtmlInputCalendar) findComponentInRoot("initDate");
        }
		return initDate;
	}

	public void setInitDate(HtmlInputCalendar initDate) {
		this.initDate = initDate;
	}

	public HtmlInputCalendar getEndDate() {
		if (endDate == null) {
			endDate = (HtmlInputCalendar) findComponentInRoot("endDate");
        }
		return endDate;
	}

	public void setEndDate(HtmlInputCalendar endDate) {
		this.endDate = endDate;
	}

	public HtmlSelectOneMenu getEstatusSelectOneMenu() {
		if (estatusSelectOneMenu == null) {
			estatusSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("estatusSelectOneMenu");
        }
		return estatusSelectOneMenu;
	}

	public void setEstatusSelectOneMenu(HtmlSelectOneMenu estatusSelectOneMenu) {
		this.estatusSelectOneMenu = estatusSelectOneMenu;
	}

	public String getInitPage() {
		loadPage();
		return initPage;
	}

	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info(":::...[PAGE]: Begin doAceptarAction --> OK");
		
		
		try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_REP_PRO_BUS_TO_NOSESSION;	
			}
			
			RepProductoBusValidateVo validateVO = new RepProductoBusValidateVo (
					getInitDate(), getEndDate(), getEstatusSelectOneMenu(), 
					getClienteSelectOneMenu() );
			
			if (!ReporteValidator.validateReporteProducto(validateVO)) {
				//setFechasErr(validateVO.getFechasErr());
				setEstatusErr(validateVO.getEstatusErr());
				
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			ReporteBuilder builder = new ReporteBuilder();
			BusquedaFiltroVO filtroVo = builder.buildRepProductoBus(validateVO);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					put(ConstantsCExpress.SESSION_VO, filtroVo);
			
			return ConstantsCExpress.GO_REP_PRO_BUS_TO_REP_PRO_RES;
		} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_REP_PRO_BUS_TO_FAIL;
    	}
		
	}
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] (doRegresarAction) regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_REP_PRO_BUS_TO_NOSESSION;		
		}
		
		return ConstantsCExpress.GO_REP_PRO_BUS_TO_MENU;
	}

	/**
	 * @return the fechasErr
	 */
	public String getFechasErr() {
		return fechasErr;
	}

	/**
	 * @param fechasErr the fechasErr to set
	 */
	public void setFechasErr(String fechasErr) {
		this.fechasErr = fechasErr;
	}

	/**
	 * @return the estatusErr
	 */
	public String getEstatusErr() {
		return estatusErr;
	}

	/**
	 * @param estatusErr the estatusErr to set
	 */
	public void setEstatusErr(String estatusErr) {
		this.estatusErr = estatusErr;
	}
	

	/**
	 * @return the clienteSelectOneMenu
	 */
	public HtmlSelectOneMenu getClienteSelectOneMenu() {
		if (clienteSelectOneMenu == null) {
			clienteSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("clienteSelectOneMenu");
        }
		return clienteSelectOneMenu;
	}

	/**
	 * @param clienteSelectOneMenu the clienteSelectOneMenu to set
	 */
	public void setClienteSelectOneMenu(HtmlSelectOneMenu clienteSelectOneMenu) {
		this.clienteSelectOneMenu = clienteSelectOneMenu;
	}

	/**
	 * @return the clienteSelectItems
	 */
	public UISelectItems getClienteSelectItems() {
		if (clienteSelectItems == null) {
			clienteSelectItems = (UISelectItems) findComponentInRoot("clienteSelectItems");
        }
		return clienteSelectItems;
	}

	/**
	 * @param clienteSelectItems the clienteSelectItems to set
	 */
	public void setClienteSelectItems(UISelectItems clienteSelectItems) {
		this.clienteSelectItems = clienteSelectItems;
	}

	/**
	 * @return the selectItemCliente
	 */
	@SuppressWarnings("rawtypes")
	public SelectItem[] getSelectItemCliente() {
		CatalogoItem item = new CatalogoItem();
		try {
			List<Client> clientList = reporteController.obtenerClientes();
			Set clientSet = item.getSelectClientesReporte(clientList);
			
			selectItemCliente = (SelectItem[])clientSet.iterator().next();
			
		} catch(ControlException e) {
			log.error(":::...[PAGE]: getSelectItemCliente --> ControlException");
		} catch(Exception e) {
			log.error(":::...[PAGE]: getSelectItemCliente --> Exception");
		}

		return selectItemCliente;
	}

	/**
	 * @param selectItemCliente the selectItemCliente to set
	 */
	public void setSelectItemCliente(SelectItem[] selectItemCliente) {
		this.selectItemCliente = selectItemCliente;
	}

	/**
	 * @return the reporteController
	 */
	public IReporteController getReporteController() {
		return reporteController;
	}

	/**
	 * @param reporteController the reporteController to set
	 */
	public void setReporteController(IReporteController reporteController) {
		this.reporteController = reporteController;
	}
	
	

}
