package mx.com.cityexpress.modules.reportes.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.common.XlsBuilder;
import mx.com.cityexpress.general.controllers.IReporteController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaFiltroVO;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.modules.reportes.vo.HeaderReportVo;
import mx.com.cityexpress.modules.reportes.vo.RepProductoVo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ReporteProductosResPage extends PageCode {
	
	private Log log = LogFactory.getLog(ReporteProductosResPage.class);
	
	private IReporteController reporteController;
	
	private List<RepProductoVo> reporteList;
	private List<HeaderReportVo> headerList;
	private String totalGeneral;
	
	
	private List<HeaderReportVo> obtieneTitulosEncabezado() {
		List<HeaderReportVo> headList = new ArrayList<HeaderReportVo>();
		HeaderReportVo headerVo = new HeaderReportVo(
				"CONCEPTO", "CLIENTE", "UNIDAD", "CANTIDAD", "PEDIDO", "TOTAL");
		
		headList.add(headerVo);
		return headList;
	}
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] doRegresarAction regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_REP_PRO_RES_TO_NOSESSION;
		}

		FacesContext.getCurrentInstance().getExternalContext().
				getSessionMap().remove(ConstantsCExpress.SESSION_VO);
		
		return ConstantsCExpress.GO_REP_PRO_RES_TO_REP_PRO_BUS;
	}

	/**
	 * @return the reporteController
	 */
	public IReporteController getReporteController() {
		return reporteController;
	}

	/**
	 * @param reporteController the reporteController to set
	 */
	public void setReporteController(IReporteController reporteController) {
		this.reporteController = reporteController;
	}


	/**
	 * @return the reporteList
	 */
	public List<RepProductoVo> getReporteList() {
		BusquedaFiltroVO filtroVo = (BusquedaFiltroVO) 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					get(ConstantsCExpress.SESSION_VO);
	
		log.info(":::...[PAGE] (getReporteList) fechaIn: " + filtroVo.getFechaInicial());
		log.info(":::...[PAGE] (getReporteList) fechaFi: " + filtroVo.getFechaFinal());
		log.info(":::...[PAGE] (getReporteList) estatus: " + filtroVo.getEstatus());
		log.info(":::...[PAGE] (getReporteList) idClient: " + filtroVo.getIdCliente());
	
		try {
			reporteList = reporteController.obtenerReporteProductos(filtroVo);
		} catch(ControlException e) {
			e.printStackTrace();
		}
		
		return reporteList;
	}

	
	public void doExportAction() {
		
		BusquedaFiltroVO filtroVo = (BusquedaFiltroVO) 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					get(ConstantsCExpress.SESSION_VO);

		//byte[] csvData = sb.toString().getBytes();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		response.setHeader("Content-disposition",
				"attachment; filename= ReporteProducto.xls");
		response.addHeader("Cache-Control", "no-cache");
		//response.setContentLength(sb.length());
		response.setContentType("application/ms-excel");
		try {
			
			ServletOutputStream out = response.getOutputStream();
			XlsBuilder xlsBuilder = new XlsBuilder(); 
			List<RepProductoVo> listPed = reporteController.
					obtenerReporteProductos(filtroVo);
			xlsBuilder.crearXlsReportePedidos(listPed).write(out);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			context.responseComplete();
			//response.getOutputStream().write(csvData);
			
			log.info("Export File");
		} catch (Exception e) {
			e.printStackTrace();
		}
			 
	}

	/**
	 * @param reporteList the reporteList to set
	 */
	public void setReporteList(List<RepProductoVo> reporteList) {
		this.reporteList = reporteList;
	}

	/**
	 * @return the headerList
	 */
	public List<HeaderReportVo> getHeaderList() {
		headerList = obtieneTitulosEncabezado();
		return headerList;
	}

	/**
	 * @param headerList the headerList to set
	 */
	public void setHeaderList(List<HeaderReportVo> headerList) {
		this.headerList = headerList;
	}

	/**
	 * @return the totalGeneral
	 */
	public String getTotalGeneral() {
		return totalGeneral;
	}

	/**
	 * @param totalGeneral the totalGeneral to set
	 */
	public void setTotalGeneral(String totalGeneral) {
		this.totalGeneral = totalGeneral;
	}
}
