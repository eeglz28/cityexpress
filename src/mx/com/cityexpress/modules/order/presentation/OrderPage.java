package mx.com.cityexpress.modules.order.presentation;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;


import javax.faces.application.ViewHandler;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IOrderController;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;


public class OrderPage extends PageCode {
	
	private Log log = LogFactory.getLog(OrderPage.class);

	private String initPage;
	
	private HtmlSelectBooleanCheckbox selectBooleanCheck;
	private HtmlInputText inputTextQuantity;
    private HtmlDataTable dataTableProduct;
    
    private List<ProductTableVo> productList;
    
    private Long rowCount = new Long(25);
    
    private IOrderController orderController;
    
    /**
     * Inicializa la pagina de Order
     */
    @SuppressWarnings("unchecked")
	private void loadPage() {
		log.info("[P] loadPage --> OK");
	    List<ProductTableVo> list = (List<ProductTableVo>)
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
				get(ConstantsCExpress.SESSION_PRODUCTS);
	    setProductList(list);
    }

	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		this.loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	/**
	 * @return the dataTableProduct
	 */
	public HtmlDataTable getDataTableProduct() {
		if (dataTableProduct == null) {
			dataTableProduct = (HtmlDataTable) findComponentInRoot("dataTableProduct");
        }
		return dataTableProduct;
	}

	/**
	 * @param dataTableProduct the dataTableProduct to set
	 */
	public void setDataTableProduct(HtmlDataTable dataTableProduct) {
		this.dataTableProduct = dataTableProduct;
	}


	/**
	 * @return the productList
	 */
	public List<ProductTableVo> getProductList() {
		return productList;
	}

	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<ProductTableVo> productList) {
		this.productList = productList;
	}

	/**
	 * @return the orderController
	 */
	public IOrderController getOrderController() {
		return orderController;
	}

	/**
	 * @param orderController the orderController to set
	 */
	public void setOrderController(IOrderController orderController) {
		this.orderController = orderController;
	}
	
	
	/**
	 * Método que actualiza el valor de la cantidad y la agrega a la lista
	 * que despliega el datatable.
	 * 
	 * @param valueChangedEvent
	 */
	public void handleTextValueChange(ValueChangeEvent valueChangedEvent) {
		log.info(":::...[VIEW]: Event Old - "+ valueChangedEvent.getOldValue() +
				"; New - " + valueChangedEvent.getNewValue());

		ProductTableVo prodVo = (ProductTableVo)getDataTableProduct().
				getRowData();
		prodVo.setQuantity(valueChangedEvent.getNewValue().toString());
		
		if(prodVo.getQuantity() != null && 
				prodVo.getQuantity().trim().length() > 0 ) {
			prodVo.setSubtotal(
				new BigDecimal(prodVo.getQuantity()).multiply(
						prodVo.getProduct().getPrice()) );
		}
	}
	
	
	
	public void handleTextCommentChange(ValueChangeEvent valueChangedEvent) {
		log.debug(":::...[VIEW]: Event Old - "+ valueChangedEvent.getOldValue() +
				"; New - " + valueChangedEvent.getNewValue());

		ProductTableVo prodVo = (ProductTableVo)getDataTableProduct().
				getRowData();
		prodVo.setComment(valueChangedEvent.getNewValue().toString());
	}

	/**
	 * @return the selectBooleanCheck
	 */
	public HtmlSelectBooleanCheckbox getSelectBooleanCheck() {
		if (selectBooleanCheck == null) {
			selectBooleanCheck = (HtmlSelectBooleanCheckbox) findComponentInRoot("selectBooleanCheck");
		}
		return selectBooleanCheck;
	}

	/**
	 * @param selectBooleanCheck the selectBooleanCheck to set
	 */
	public void setSelectBooleanCheck(HtmlSelectBooleanCheckbox selectBooleanCheck) {
		this.selectBooleanCheck = selectBooleanCheck;
	}

	/**
	 * @return the inputTextQuantity
	 */
	public HtmlInputText getInputTextQuantity() {
		if (inputTextQuantity == null) {
			inputTextQuantity = (HtmlInputText) findComponentInRoot("inputTextQuantity");
		}
		return inputTextQuantity;
	}

	/**
	 * @param inputTextQuantity the inputTextQuantity to set
	 */
	public void setInputTextQuantity(HtmlInputText inputTextQuantity) {
		this.inputTextQuantity = inputTextQuantity;
	}
	
	public String doCancelarAction() {
		log.info(":::...[PAGE]: doCancelarAction OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_ORDER_TO_NOSESSION;
		}
		
		return ConstantsCExpress.GO_ORDER_TO_MENU;
	}
	
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		try {	
			log.info(":::...[PAGE]: begin doAceptarAction OK");
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_ORDER_TO_NOSESSION;
			}
			
			List<ProductTableVo> productListSelected = new ArrayList<ProductTableVo>();
			
			for (ProductTableVo table : productList) {
				log.debug("Producto: " + table.getProduct().getName() + " Cantidad: " + table.getQuantity());
				if (table.getQuantity() != null && 
						table.getQuantity().trim().length() > 0 && 
						!table.getQuantity().trim().equals("0")) {
					BigDecimal a = new BigDecimal(table.getQuantity().trim());
					
					if (a.compareTo(new BigDecimal("0")) > 0) {
						productListSelected.add(table);	
					}
					
				}
    		}
			
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().put(ConstantsCExpress.SESSION_TBL_VO, 
								productListSelected);
			
			//enviar tambien productList para el regreso
			
			log.info(":::...[PAGE]: end doAceptarAction OK");
			return ConstantsCExpress.GO_ORDER_TO_DETAILORDER;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_ORDER_TO_FAIL;
    	}
	}
	
	
	@SuppressWarnings("unchecked")
	public String doFotoAction() {
		log.info(":::...[PAGE]: begin doFotoAction OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_ORDER_TO_NOSESSION;
			}
			
			ProductTableVo prodTblVo = 
					(ProductTableVo)getDataTableProduct().getRowData();
			
			log.info(":::...[PAGE] (doFotoAction) Pedido Seleccionado: " + 
						prodTblVo.getProduct().getIdProduct());
			
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
					put(ConstantsCExpress.REQUEST_ID, prodTblVo.getProduct().getIdProduct());
			
			log.info(":::...[PAGE]: end doFotoAction OK");
			
			
			return ConstantsCExpress.GO_ORDER_TO_FOTO;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doFotoAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_ORDER_TO_FAIL;
    	}
		
	}
	

	
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + ", pageIndex: " + scrollerEvent.getPageIndex());
		
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}
	
	
	@SuppressWarnings("unchecked")
	public void openPopupClicked(ActionEvent event) {
		
	    log.info(":::...[PAGE]: begin openPopupClicked OK");
	    // View's id in the same form as used in the navigation rules in faces-config.xml
	    // This value could be passed as parameter (using <f:param>)
	    final String viewId = "/production/order/foto.jsp"; 
	    
	    ProductTableVo prodTblVo = 
				(ProductTableVo)getDataTableProduct().getRowData();
		
		log.info(":::...[PAGE] (openPopupClicked) Pedido Seleccionado: " + 
					prodTblVo.getProduct().getIdProduct());
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
				put(ConstantsCExpress.REQUEST_ID, prodTblVo.getProduct().getIdProduct());
	    
	    FacesContext facesContext = FacesContext.getCurrentInstance();

	    // This is the proper way to get the view's url
	    ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	    String actionUrl = viewHandler.getActionURL(facesContext, viewId);
	    
	    log.info(":::...[PAGE]: openPopupClicked : actionUrl - " + actionUrl);
	    
	    String javaScriptText = "window.open('"+actionUrl+"', 'popupWindow', 'dependent=yes, menubar=no, toolbar=no, height=800, width=820, top=20, left=50, scrollbars=no, location=no, resizable=no, directories=no');";
	    
	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);  
	    
	    log.info(":::...[PAGE]: end openPopupClicked OK");
	}
	
	
}
