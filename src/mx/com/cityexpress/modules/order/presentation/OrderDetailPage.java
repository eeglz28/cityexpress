package mx.com.cityexpress.modules.order.presentation;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIColumn;
import javax.faces.component.UIData;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.HtmlDataScroller;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IOrderController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.FechasVo;
import mx.com.cityexpress.general.vo.MailBodyOrderVo;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mail.sender.MailSendHelper;
import mx.com.cityexpress.mail.sender.MailerVo;

public class OrderDetailPage extends PageCode {
	
	private Log log = LogFactory.getLog(OrderDetailPage.class);
	
	private boolean sendValid;
	private String initPage;
	private String userName;
	private String userMail;
	private String total;
	private String solicitante;
    private UIData uiDataTable;
    private HtmlDataTable dataTableDetail;
    private HtmlDataScroller scroller;
	private UIColumn column1;
    private UIColumn column2;
    private UIColumn column3;
    private UIColumn column4;
    
    private IOrderController orderController;
    
    private UsuarioSesionVO loginUser = (UsuarioSesionVO)
    		FacesContext.getCurrentInstance().getExternalContext().
    				getSessionMap().get(ConstantsCExpress.SESSION_USERS_LOGIN);
    
    @SuppressWarnings("unchecked")
    private List<ProductTableVo> productList = (List<ProductTableVo>) 
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
				get(ConstantsCExpress.SESSION_TBL_VO);
    
    
    private List<DetailOrderTableVo> detailOrderList = 
    		new ArrayList<DetailOrderTableVo>();

    
    private void loadPage() {
    	log.info(":::...[PAGE] loadPage OK");
    }
    
    
	/**
	 * @return the uiDataTable
	 */
	public UIData getUiDataTable() {
		return uiDataTable;
	}

	/**
	 * @param uiDataTable the uiDataTable to set
	 */
	public void setUiDataTable(UIData uiDataTable) {
		this.uiDataTable = uiDataTable;
	}

	/**
	 * @return the dataTableProduct
	 */
	public HtmlDataTable getDataTableDetail() {
		if (dataTableDetail == null) {
			dataTableDetail = (HtmlDataTable) findComponentInRoot("dataTableDetail");
        }
		return dataTableDetail;
	}

	/**
	 * @param dataTableProduct the dataTableProduct to set
	 */
	public void setDataTableDetail(HtmlDataTable dataTableDetail) {
		this.dataTableDetail = dataTableDetail;
	}

	/**
	 * @return the scroller
	 */
	public HtmlDataScroller getScroller() {
		if (scroller == null) {
			scroller = (HtmlDataScroller) findComponentInRoot("scroller");
        }
		return scroller;
	}

	/**
	 * @param scroller the scroller to set
	 */
	public void setScroller(HtmlDataScroller scroller) {
		this.scroller = scroller;
	}

	/**
	 * @return the column1
	 */
	public UIColumn getColumn1() {
		if (column1 == null) {
			column1 = (UIColumn) findComponentInRoot("column1");
        }
		return column1;
	}

	/**
	 * @param column1 the column1 to set
	 */
	public void setColumn1(UIColumn column1) {
		this.column1 = column1;
	}

	/**
	 * @return the column2
	 */
	public UIColumn getColumn2() {
		if (column2 == null) {
			column2 = (UIColumn) findComponentInRoot("column2");
        }
		return column2;
	}

	/**
	 * @param column2 the column2 to set
	 */
	public void setColumn2(UIColumn column2) {
		this.column2 = column2;
	}

	/**
	 * @return the column3
	 */
	public UIColumn getColumn3() {
		if (column3 == null) {
			column3 = (UIColumn) findComponentInRoot("column3");
        }
		return column3;
	}

	/**
	 * @param column3 the column3 to set
	 */
	public void setColumn3(UIColumn column3) {
		this.column3 = column3;
	}

	/**
	 * @return the column4
	 */
	public UIColumn getColumn4() {
		if (column4 == null) {
			column4 = (UIColumn) findComponentInRoot("column4");
        }
		return column4;
	}

	/**
	 * @param column4 the column4 to set
	 */
	public void setColumn4(UIColumn column4) {
		this.column4 = column4;
	}

	/**
	 * @return the orderController
	 */
	public IOrderController getOrderController() {
		return orderController;
	}

	/**
	 * @param orderController the orderController to set
	 */
	public void setOrderController(IOrderController orderController) {
		this.orderController = orderController;
	}


	/**
	 * @return the detailOrderList
	 */
	public List<DetailOrderTableVo> getDetailOrderList() {
		return detailOrderList;
	}


	/**
	 * @param detailOrderList the detailOrderList to set
	 */
	public void setDetailOrderList(List<DetailOrderTableVo> detailOrderList) {
		this.detailOrderList = detailOrderList;
	}


	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}


	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	/**
	 * @return the total
	 */
	public String getTotal() {
		DecimalFormat df = new DecimalFormat("##,###,###.00");
		BigDecimal sumaTotal = new BigDecimal("0");
		for (ProductTableVo table : productList) {
			log.debug("Producto: " + table.getProduct().getName() + " Cantidad: " + table.getQuantity());
			sumaTotal = sumaTotal.add(table.getSubtotal());
		}
		total = df.format(Double.parseDouble(sumaTotal.toString()));
		return total;
	}


	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		userName = loginUser.getName();
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the userMail
	 */
	public String getUserMail() {
		userMail = loginUser.getEmail();
		return userMail;
	}


	/**
	 * @param userMail the userMail to set
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info(":::...[PAGE]: begin doAceptarAction OK");
		
		String mensajeConfirmacion = ConstantsCExpress.MSG_ORDER_OK;
		
		try {
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_DETAILORDER_TO_NOSESSION;
			}
			
			String numOrder = orderController.insertOrder(productList, loginUser);
			List<DetailOrderTableVo> 
					detailOrderList = orderController.getOrderDetail(numOrder);
			
			List<String> destination = new ArrayList<String>();
			String[] bccDestionation = new String[]{
					ConstantsCExpress.MAIL_ADMIN, //
					ConstantsCExpress.MAIL_CC_1, //
					ConstantsCExpress.MAIL_CC_2, //
					ConstantsCExpress.MAIL_CC_3, //
					ConstantsCExpress.MAIL_CC_4, //
					ConstantsCExpress.MAIL_CC_5, //
					ConstantsCExpress.MAIL_CC_6};
			destination.add(loginUser.getEmail());
			// Get the manager email
			if (!userSesion.getPerfil().equals(ConstantsCExpress.PERFIL_GERENTE)) {
				String emailGerente = orderController.obtenerEmailGerente(userSesion.getIdCliente(),
						ConstantsCExpress.PERFIL_GERENTE);
				if (emailGerente != null && emailGerente.length() > 0) {
					destination.add(emailGerente);
				}
			}
			
			
			MailBodyOrderVo bodyVo = new MailBodyOrderVo();
			bodyVo.setListDetail(detailOrderList);
			bodyVo.setOrder(numOrder);
			
			String nombreCliente = 
					orderController.getClientName(loginUser.getIdCliente());
			
			if (nombreCliente == null) {
				throw new ControlException("No existe cliente asignado al usuario.");
			}
			
			bodyVo.setSolicitante(nombreCliente);
			bodyVo.setUser(loginUser.getName());
			bodyVo.setAtencion(loginUser.getName());
			
			try {
				FechasVo fechasVo = orderController.
						obtenerFechasPedidoPorNumPed(numOrder);
				bodyVo.setFecha_alta(fechasVo.getCreacion());
				bodyVo.setFecha_entrega(fechasVo.getEntrega());
				
				MailSendHelper mailHelper = new MailSendHelper();
				String body = mailHelper.createBodyNewOrder(bodyVo);
				
				MailerVo mailerVo = new MailerVo();
				mailerVo.setFrom(ConstantsCExpress.MAIL_FROM);
				mailerVo.setAttach(null);
				mailerVo.setDestination(destination.toArray(new String[destination.size()]));
				mailerVo.setBccDestination(bccDestionation);
				mailerVo.setSubject(ConstantsCExpress.MAIL_SUBJECT.concat(numOrder));
				mailerVo.setBody(body);
				
				sendValid = orderController.sendMail(mailerVo);
			} catch (Exception ex) {
				mensajeConfirmacion = ConstantsCExpress.MSG_ORDER_MAIL_FAILED;
			}
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					put(ConstantsCExpress.SESSION_ID, numOrder);
			
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
					put(ConstantsCExpress.REQUEST_MESSAGE, mensajeConfirmacion);
			
			log.info(":::...[PAGE] end doAceptarAction OK");
			return ConstantsCExpress.GO_DETAILORDER_TO_CONFIRM;
    	} catch(ControlException e) {    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_DETAILORDER_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_DETAILORDER_TO_FAIL;
    	}

	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE]: begin doRegresarAction OK ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_DETAILORDER_TO_NOSESSION;
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
				remove(ConstantsCExpress.SESSION_TBL_VO);
		
		log.info(":::...[PAGE]: end doRegresarAction GO -> order ");
		
		return ConstantsCExpress.GO_DETAILORDER_TO_ORDER;
	}


	/**
	 * @return the sendValid
	 */
	public boolean isSendValid() {
		return sendValid;
	}


	/**
	 * @param sendValid the sendValid to set
	 */
	public void setSendValid(boolean sendValid) {
		this.sendValid = sendValid;
	}


	/**
	 * @return the solicitante
	 */
	public String getSolicitante() {
		
		String nombre = "";
		try {
			nombre = orderController.getClientName(loginUser.getIdCliente());
		} catch(ControlException e){
			
		}
		solicitante = nombre;
		return solicitante;
	}


	/**
	 * @param solicitante the solicitante to set
	 */
	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}


	/**
	 * @return the productList
	 */
	public List<ProductTableVo> getProductList() {
		return productList;
	}


	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<ProductTableVo> productList) {
		this.productList = productList;
	}
    


}
