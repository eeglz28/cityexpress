package mx.com.cityexpress.modules.order.presentation;

import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.presentation.PageCode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FotoPage extends PageCode {
	
	private Log log = LogFactory.getLog(FotoPage.class);

	private String url;

	/**
	 * @return the url
	 */
	public String getUrl() {
		Long idProd = (Long)
	    		FacesContext.getCurrentInstance().getExternalContext().
	    				getSessionMap().get(ConstantsCExpress.REQUEST_ID);
		log.info("Id Producto: " + idProd);
		if (idProd != null) {
			url = "..\\..\\resources\\img\\products\\" + idProd +".jpg";	
		} else {
			url = "..\\..\\resources\\img\\products\\imagen_no_disponible.gif";
		}
		 
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
