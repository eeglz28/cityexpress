package mx.com.cityexpress.modules.order.presentation;

//import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IOrderController;
//import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
//import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;


public class OrderConfirmPage extends PageCode {
	
	private Log log = LogFactory.getLog(OrderConfirmPage.class);
	
	private String initPage;
	private String numPedido;
	private String mensajeConfirmacion;
	
	
	private String numOrder = (String)
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().get(ConstantsCExpress.SESSION_ID);
	
	private String mensaje  = (String)
			FacesContext.getCurrentInstance().getExternalContext().
				getRequestMap().get(ConstantsCExpress.REQUEST_MESSAGE);
	
	private IOrderController orderController;

	
	private void loadPage() {
		log.info(":::...[PAGE] loadPage --> OK");
	}
	
	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}

	/**
	 * @return the numPedido
	 */
	public String getNumPedido() {
		numPedido = numOrder;
		return numPedido;
	}

	/**
	 * @param numPedido the numPedido to set
	 */
	public void setNumPedido(String numPedido) {
		this.numPedido = numPedido;
	}
	
	
	@SuppressWarnings("unchecked")
	public String doTerminarAction() {
		
		log.info(":::...[PAGE]: begin doTerminarAction OK");
		try {
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_CONFIRM_TO_NOSESSION;
			}
			
//			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
//					remove(ConstantsCExpress.SESSION_TBL_VO);
			
			//List<ProductTableVo> listTblVo = orderController.getProductsToCatalog();
	
//			FacesContext.getCurrentInstance().getExternalContext().
//					getSessionMap().put(ConstantsCExpress.SESSION_TBL_VO, 
//							listTblVo);
//	
//			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
//					remove(ConstantsCExpress.SESSION_TBL_VO_2);
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					remove(ConstantsCExpress.SESSION_ID);
			
			log.info(":::...[PAGE] end doTerminarAction OK");
			return ConstantsCExpress.GO_CONFIRM_TO_MENU;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doTerminarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_CONFIRM_TO_FAIL;
    	}
	}

	/**
	 * @return the orderController
	 */
	public IOrderController getOrderController() {
		return orderController;
	}

	/**
	 * @param orderController the orderController to set
	 */
	public void setOrderController(IOrderController orderController) {
		this.orderController = orderController;
	}

	/**
	 * @return the mensajeConfirmacion
	 */
	public String getMensajeConfirmacion() {
		mensajeConfirmacion = mensaje;
		return mensajeConfirmacion;
	}

	/**
	 * @param mensajeConfirmacion the mensajeConfirmacion to set
	 */
	public void setMensajeConfirmacion(String mensajeConfirmacion) {
		this.mensajeConfirmacion = mensajeConfirmacion;
	}

	
}
