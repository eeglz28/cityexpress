package mx.com.cityexpress.modules.order.services;

import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.FechasVo;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

public interface IOrderService {
	
	public List<ProductTableVo> getProductsToCatalog() throws BusinessException;
	
	public String insertOrder(List<ProductTableVo> productList, 
			UsuarioSesionVO usuarioSesion) throws BusinessException;
	
	public List<DetailOrderTableVo> getOrderDetail(String norder) throws 
			BusinessException;
	
	public String getClientName(String idClient) throws BusinessException;
	
	public FechasVo obtenerFechasPedidoPorNumPed(String numOrder)
			throws BusinessException;
	
	public String obtenerEmailGerente(String idClient, String idProfile)
			throws BusinessException;

}
