package mx.com.cityexpress.modules.order.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.transformer.DetOrdToDetailTableTransformer;
import mx.com.cityexpress.general.transformer.ProductToProductTableTransformer;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.FechasVo;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.mapping.pojo.Orders;
import mx.com.cityexpress.mapping.pojo.OrdersDetail;
import mx.com.cityexpress.mapping.pojo.Product;
import mx.com.cityexpress.mapping.pojo.Users;
import mx.com.cityexpress.modules.order.services.IOrderService;
import mx.com.cityexpress.persistence.dao.IClientDao;
import mx.com.cityexpress.persistence.dao.IOrderDao;
import mx.com.cityexpress.persistence.dao.IProductDao;
import mx.com.cityexpress.persistence.dao.IUsersDao;

public class OrderServiceImpl implements IOrderService {
	
	private Log log = LogFactory.getLog(OrderServiceImpl.class);
	
	private IProductDao productDao;
	private IOrderDao orderDao;
	private IUsersDao usersDao;
	private IClientDao clientDao;

	public List<ProductTableVo> getProductsToCatalog() 
			throws BusinessException {
		
		log.info(":::...[S]: begin getProductsToCatalog OK");
		
		List<ProductTableVo> prodTableList = new ArrayList<ProductTableVo>(); 
		try {
			List<Product> prodList = productDao.selectActiveProducts();
			prodTableList = ProductToProductTableTransformer.
					transform(prodList);
			
			log.info(":::...[S]: end getProductsToCatalog OK");
			return prodTableList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end getProductsToCatalog : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end getProductsToCatalog : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	
	public String insertOrder(List<ProductTableVo> productList, 
			UsuarioSesionVO usuarioSesion) throws BusinessException {
		
		log.info(":::...[S]: begin insertOrder OK");
			
		Orders orders = new Orders();
		int numOrder = 1;
		BigDecimal total = new BigDecimal(0);
		String order = "PE-1";
		try {
			
			Long max = orderDao.getMaxOrder();
			if(max != null) {
				numOrder = max.intValue() + 1;
			}
			order = "PE-".concat(String.valueOf(numOrder));
		
			Set<OrdersDetail> ordDetailSet = new HashSet<OrdersDetail>();
			
			for(ProductTableVo prodTblVo : productList) {
				log.info(":::...[S] Validando producto " + prodTblVo.
						getProduct().getName() + "; Cantidad: "+ 
						prodTblVo.getQuantity());
				BigDecimal quant = new BigDecimal(prodTblVo.getQuantity());
				log.info(" --> Cantidad : " + quant);
				if(quant.compareTo(new BigDecimal("0")) > 0) {
					OrdersDetail oDetail = new OrdersDetail();
					oDetail.setProduct(prodTblVo.getProduct());
					oDetail.setQuantity(quant);
					oDetail.setComments(prodTblVo.getComment());
					
					total = total.add(
							prodTblVo.getProduct().getPrice().multiply(
									new BigDecimal(prodTblVo.getQuantity())));  
					
					oDetail.setOrders(orders);
					ordDetailSet.add(oDetail);	
				}
			}
			
			orders.setNumOrder(order);
			orders.setCreationDate(new Date());
			
			Users user = 
					usersDao.selectUserById(new Long(usuarioSesion.getIdUsuario()));
			
			orders.setUsers(user);
			orders.setTotal(total);
			orders.setStatus(ConstantsCExpress.ESTATUS_EN_PROCESO_NUM);
			orders.setOrdersDetails(ordDetailSet);
			
			orderDao.insertOrder(orders);
			
			log.info(":::...[S]: end insertOrder OK");
			return order;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end insertOrder : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end insertOrder : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	
	@SuppressWarnings("rawtypes")
	public List<DetailOrderTableVo> getOrderDetail(String norder) throws 
			BusinessException {
		
		log.info(":::...[S]: begin getOrderDetail OK");
		
		List<DetailOrderTableVo> detailList = 
				new ArrayList<DetailOrderTableVo>();
		try {
			List resList = orderDao.getOrderDetail(norder);
			detailList = DetOrdToDetailTableTransformer.transform(resList);
			log.info(":::...[S]: end getOrderDetail OK");
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end getOrderDetail : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end getOrderDetail : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		return detailList;	
	}
	
	
	public String getClientName(String idClient) throws BusinessException {
		log.info(":::...[S]: begin getClientName OK");
		String name = null;
		try {
			Client client = clientDao.selectClient(new Long(idClient));
			if (client != null) {
				name = client.getClientName();
			}
			
			log.info(":::...[S]: end getClientName OK");
			return name;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end getClientName : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end getClientName : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public FechasVo obtenerFechasPedidoPorNumPed(String numOrder)
			throws BusinessException {
		log.info(":::...[S] begin obtenerFechasPedidoPorNumPed OK");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		GregorianCalendar gc = new GregorianCalendar();
		
		
		
		FechasVo fechasVo = new FechasVo();
		try {
			
			List<Object[]> fechasList = orderDao.
					selectFechaCreacionPedidoPorNumPed(numOrder);
			if (fechasList != null && fechasList.size() > 0) {
				for (Object[] obj : fechasList) {

					Date fecha = (Date)obj[0];
					fechasVo.setCreacion(sdf.format(fecha));
					
					gc.setTime(fecha);
					gc.add(GregorianCalendar.DATE, 14);

					fechasVo.setEntrega(sdf.format(gc.getTime()));
				}
			}
			
			log.info(":::...[S] end obtenerFechasPedidoPorNumPed OK");
			return fechasVo;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerFechasPedidoPorNumPed : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerFechasPedidoPorNumPed : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public String obtenerEmailGerente(String idClient, String idProfile)
			throws BusinessException {
		log.info(":::...[S]: begin obtenerEmailGerente OK");
		try {
			String email = usersDao.selectUserEmail(idClient, idProfile);
			
			log.info(":::...[S]: end obtenerEmailGerente OK");
			return email;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerEmailGerente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerEmailGerente : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	

	/**
	 * @return the productDao 
	 */
	public IProductDao getProductDao() {
		return productDao;
	}

	/**
	 * @param productDao the productDao to set
	 */
	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}


	/**
	 * @return the orderDao
	 */
	public IOrderDao getOrderDao() {
		return orderDao;
	}


	/**
	 * @param orderDao the orderDao to set
	 */
	public void setOrderDao(IOrderDao orderDao) {
		this.orderDao = orderDao;
	}



	/**
	 * @return the usersDao
	 */
	public IUsersDao getUsersDao() {
		return usersDao;
	}



	/**
	 * @param usersDao the usersDao to set
	 */
	public void setUsersDao(IUsersDao usersDao) {
		this.usersDao = usersDao;
	}



	/**
	 * @return the clientDao
	 */
	public IClientDao getClientDao() {
		return clientDao;
	}



	/**
	 * @param clientDao the clientDao to set
	 */
	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}

	

}
