package mx.com.cityexpress.modules.consultas.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DetPedidosUsuarioPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
    private HtmlDataTable dataTableDetPedido;
	private String initPage;
	
	private IConsultaController consultaController;
	
    private Long idOrder = (Long) FacesContext.getCurrentInstance().
    		getExternalContext().getRequestMap().get(ConstantsCExpress.REQUEST_ID_ORDER);
    

    private List<DetailOrderConsultaTableVo> detPedidoList;
    
	private void loadPage() {
		log.info(":::...[PAGE] (loadPage) --> OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return;
			}
			
			List<DetailOrderConsultaTableVo> res = 
					consultaController.obtenerDetallePedido(idOrder);
			setDetPedidoList(res);
		} catch(ControlException e) {
			e.printStackTrace();
		}
	}
    

	public IConsultaController getConsultaController() {
		return consultaController;
	}

	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}



	public HtmlDataTable getDataTableDetPedido() {
		if (dataTableDetPedido == null) {
			dataTableDetPedido = (HtmlDataTable) findComponentInRoot("dataTableDetPedido");
        }
		return dataTableDetPedido;
	}


	public void setDataTableDetPedido(HtmlDataTable dataTableDetPedido) {
		this.dataTableDetPedido = dataTableDetPedido;
	}


	public String getInitPage() {
		loadPage();
		return initPage;
	}


	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	public List<DetailOrderConsultaTableVo> getDetPedidoList() {
		return detPedidoList;
	}


	public void setDetPedidoList(List<DetailOrderConsultaTableVo> detPedidoList) {
		this.detPedidoList = detPedidoList;
	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] begin doRegresarAction OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_DET_PEDIDOS_USU_TO_NOSESSION;
		}
		
		log.info(":::...[PAGE] end doRegresarAction OK");
		return ConstantsCExpress.GO_DET_PEDIDOS_USU_TO_PEDIDOS_USU;
	}
	
}