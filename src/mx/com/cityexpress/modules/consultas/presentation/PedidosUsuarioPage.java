package mx.com.cityexpress.modules.consultas.presentation;

import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;

public class PedidosUsuarioPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
    private HtmlDataTable dataTablePedidos;
	
	private String initPage;
	
	private IConsultaController consultaController;
	
    private List<OrdersTableVo> ordersList;
    
    private boolean clientTblRender = false;
    
    private Long rowCount = new Long(40);
	

	private void loadPage() {
		final BusquedaPedidoVo filtroVo = new BusquedaPedidoVo();
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return;
			}
			
			log.info(":::...[PAGE] (loadPage) Usuario session: " + userSesion.getUsuario());
			
			filtroVo.setIdUsuario(userSesion.getIdUsuario().toString());
			filtroVo.setIdPerfil(userSesion.getPerfil());
			
			List<OrdersTableVo> res = 
					consultaController.obtenerPedidosUsuarios(filtroVo);
			setOrdersList(res);
		} catch(ControlException e) {
			e.printStackTrace();
		}
	}


	public String getInitPage() {
		loadPage();
		return initPage;
	}

	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}
	
	@SuppressWarnings("unchecked")
	public String doDetalleAction() {
		log.info(":::...[PAGE]: begin doDetalleAction OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_USU_TO_NOSESSION;
			}
			
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doDetalleAction) Pedido Seleccionado: " + ordTblVo.getIdOrder());
			log.info(":::...[PAGE] (doDetalleAction) Cliente Seleccionado: " + ordTblVo.getIdClient());
			
			
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
					put(ConstantsCExpress.REQUEST_ID_ORDER, ordTblVo.getIdOrder());
			
			log.info(":::...[PAGE]: end doDetalleAction OK");
			setClientTblRender(false);
			
			return ConstantsCExpress.GO_PEDIDOS_USU_TO_DET_PEDIDOS_USU;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doDetalleAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_USU_TO_FAIL;
    	}
	}
		
	
	
	public IConsultaController getConsultaController() {
		return consultaController;
	}

	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}


	public HtmlDataTable getDataTablePedidos() {
		if (dataTablePedidos == null) {
			dataTablePedidos = (HtmlDataTable) findComponentInRoot("dataTablePedidos");
        }
		return dataTablePedidos;
	}

	public void setDataTablePedidos(HtmlDataTable dataTablePedidos) {
		this.dataTablePedidos = dataTablePedidos;
	}


	public List<OrdersTableVo> getOrdersList() {
		return ordersList;
	}

	public void setOrdersList(List<OrdersTableVo> ordersList) {
		this.ordersList = ordersList;
	}

	public boolean isClientTblRender() {
		return clientTblRender;
	}

	public void setClientTblRender(boolean clientTblRender) {
		this.clientTblRender = clientTblRender;
	}

	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] begin doRegresarAction OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_PEDIDOS_USU_TO_NOSESSION;
		}

		log.info(":::...[PAGE] end doRegresarAction OK");
		return ConstantsCExpress.GO_PEDIDOS_USU_TO_MENU;
	}


	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}


	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

	
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + ", pageIndex: " + scrollerEvent.getPageIndex());
		
	}
}
