package mx.com.cityexpress.modules.consultas.presentation;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.MailBodyOrderVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mail.sender.MailSendHelper;
import mx.com.cityexpress.mail.sender.MailerVo;
import mx.com.cityexpress.modules.consultas.builder.ConsultaPedidosBuilder;
import mx.com.cityexpress.modules.consultas.validator.ConsultaPedidosValidator;
import mx.com.cityexpress.modules.consultas.vo.ConsultaPedidoValidateVO;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;

public class CambiaEstatusPage  extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	OrdersTableVo ordTblVo = 
			(OrdersTableVo) FacesContext.getCurrentInstance().getExternalContext().
			getSessionMap().get(ConstantsCExpress.SESSION_TBL_VO);
	
	private HtmlSelectOneMenu estatusSelectOneMenu;
	private HtmlInputText guiaText;
	private HtmlInputTextarea commentsTextArea;
	private HtmlOutputText otext2;
	private HtmlSelectBooleanCheckbox informarCheck;
	
	
	private String cliente;
	private String pedido;
	private String fecha;
	private String usuario;
	private String guia;
	private String estatus;
	private String comments;
	
	private String estatusErr;
	private String message;
	
	
	
	private String initPage;
	
	private IConsultaController consultaController;
	
	public void loadPage() {
		log.info(":::...[PAGE] (loadPage) --> OK");
		
		setGuia(ordTblVo.getGuia());
		setEstatus(ordTblVo.getEstatus());
		setComments(ordTblVo.getComments());
	}

	
	public String doRegresarAction() {
		log.info(":::...[PAGE] (doRegresarAction) --> OK");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_NOSESSION;		
		}
		
		FacesContext.getCurrentInstance().getExternalContext().
				getSessionMap().remove(ConstantsCExpress.SESSION_TBL_VO);
		
		return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_PEDIDOS;
	}
	
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info(":::...[PAGE] (doAceptarAction) --> OK");
		
		log.info(":::...[PAGE] " +
				"estatus seleccionado: " + getEstatusSelectOneMenu().getValue() + 
				" | guia: " + getGuiaText().getValue() + 
				" | id_pedido: " +ordTblVo.getIdOrder());
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_NOSESSION;
			}
			
			ConsultaPedidoValidateVO validateVO = new ConsultaPedidoValidateVO(
					ordTblVo.getIdOrder().toString(),
					getEstatusSelectOneMenu(),
					getGuiaText(),
					getCommentsTextArea());
			
			if (!ConsultaPedidosValidator.validateCambiaEstatus(validateVO)) {
				setEstatusErr(validateVO.getEstatusErr());
				setMessage(validateVO.getMsgErr());
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			ConsultaPedidosBuilder builder = new ConsultaPedidosBuilder();
			InputPedidoVO inputVO = builder.buildActualizaPedido(validateVO);
		
			consultaController.actualizarEstatusPedido(inputVO);
			
			log.info("Valor del checkBox: " +getInformarCheck().getValue());
			Boolean isCheckGuia = (Boolean)getInformarCheck().getValue();
			if (isCheckGuia.booleanValue() && 
					getGuiaText().getValue().toString().trim().length() > 0) {
				log.info("Se enviará al usuario el número de guía");
				
				UsuarioEnvioCorreoVO usEnvioVO = consultaController.
						obtenerUsuarioPorNick(ordTblVo.getNick());
				
				if (usEnvioVO != null) {
					String[] destination = new String[] {
							usEnvioVO.getEmail()
							};
					String[] bccDestination = new String[]{
							ConstantsCExpress.MAIL_ADMIN, 
							ConstantsCExpress.MAIL_CC_1,
							ConstantsCExpress.MAIL_CC_2,
							ConstantsCExpress.MAIL_CC_3,
							ConstantsCExpress.MAIL_CC_4,
							ConstantsCExpress.MAIL_CC_5,
							ConstantsCExpress.MAIL_CC_6
					};
					
					MailBodyOrderVo bodyVo = new MailBodyOrderVo();
					bodyVo.setOrder(ordTblVo.getNumOrder());
					
					bodyVo.setSolicitante(ordTblVo.getClientName());
					bodyVo.setUser(usEnvioVO.getNombre());
					bodyVo.setAtencion(usEnvioVO.getNombre());
					bodyVo.setInfo1(inputVO.getGuia());
					bodyVo.setComentario(inputVO.getComments());
					MailSendHelper mailHelper = new MailSendHelper();
					String body = mailHelper.createBodyInfoGuia(bodyVo);
					
					MailerVo mailerVo = new MailerVo();
					mailerVo.setFrom(ConstantsCExpress.MAIL_FROM);
					mailerVo.setAttach(null);
					mailerVo.setDestination(destination);
					mailerVo.setBccDestination(bccDestination);
					mailerVo.setSubject(ConstantsCExpress.MAIL_SUBJECT_GUIA.
							concat(ordTblVo.getNumOrder()));
					mailerVo.setBody(body);
					consultaController.sendMail(mailerVo);	
				}
				
			}
				
			return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_SUCCESS;
    	} catch(ControlException e) {    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_CAMBIA_ESTATUS_TO_FAIL;
    	}
	}
	
	
	/**
	 * @return the estatusSelectOneMenu
	 */
	public HtmlSelectOneMenu getEstatusSelectOneMenu() {
		if (estatusSelectOneMenu == null) {
			estatusSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("estatusSelectOneMenu");
        }
		return estatusSelectOneMenu;
	}

	/**
	 * @param estatusSelectOneMenu the estatusSelectOneMenu to set
	 */
	public void setEstatusSelectOneMenu(HtmlSelectOneMenu estatusSelectOneMenu) {
		this.estatusSelectOneMenu = estatusSelectOneMenu;
	}

	/**
	 * @return the guiaText
	 */
	public HtmlInputText getGuiaText() {
		if (guiaText == null) {
			guiaText = (HtmlInputText) findComponentInRoot("guiaText");
        }
		return guiaText;
	}

	/**
	 * @param guiaText the guiaText to set
	 */
	public void setGuiaText(HtmlInputText guiaText) {
		this.guiaText = guiaText;
	}
	
	/**
	 * @return the commentsTextArea
	 */
	public HtmlInputTextarea getCommentsTextArea() {
		if (commentsTextArea == null) {
			commentsTextArea = (HtmlInputTextarea) findComponentInRoot("commentsTextArea");
        }
		return commentsTextArea;
	}


	/**
	 * @param commentsTextArea the commentsTextArea to set
	 */
	public void setCommentsTextArea(HtmlInputTextarea commentsTextArea) {
		this.commentsTextArea = commentsTextArea;
	}
	
	
	/**
	 * @return the informarCheck
	 */
	public HtmlSelectBooleanCheckbox getInformarCheck() {
		if (informarCheck == null) {
			informarCheck = (HtmlSelectBooleanCheckbox) findComponentInRoot("informarCheck");
        }
		return informarCheck;
	}


	/**
	 * @param informarCheck the informarCheck to set
	 */
	public void setInformarCheck(HtmlSelectBooleanCheckbox informarCheck) {
		this.informarCheck = informarCheck;
	}

	/**
	 * @return the cliente
	 */
	public String getCliente() {
		cliente = ordTblVo.getClientName();
		return cliente;
	}

	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * @return the pedido
	 */
	public String getPedido() {
		pedido = ordTblVo.getNumOrder();
		return pedido;
	}

	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		fecha = ordTblVo.getCreationDate();
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		usuario = ordTblVo.getNick();
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the consultaController
	 */
	public IConsultaController getConsultaController() {
		return consultaController;
	}

	/**
	 * @param consultaController the consultaController to set
	 */
	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}

	/**
	 * @return the initPage
	 */
	public String getInitPage() {
		loadPage();
		return initPage;
	}

	/**
	 * @param initPage the initPage to set
	 */
	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	/**
	 * @return the otext2
	 */
	public HtmlOutputText getOtext2() {
		if (otext2 == null) {
			otext2 = (HtmlOutputText) findComponentInRoot("otext2");
        }
		return otext2;
	}


	/**
	 * @param otext2 the otext2 to set
	 */
	public void setOtext2(HtmlOutputText otext2) {
		this.otext2 = otext2;
	}


	/**
	 * @return the guia
	 */
	public String getGuia() {
		return guia;
	}


	/**
	 * @param guia the guia to set
	 */
	public void setGuia(String guia) {
		this.guia = guia;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the estatusErr
	 */
	public String getEstatusErr() {
		return estatusErr;
	}


	/**
	 * @param estatusErr the estatusErr to set
	 */
	public void setEstatusErr(String estatusErr) {
		this.estatusErr = estatusErr;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	
}
