package mx.com.cityexpress.modules.consultas.presentation;

import java.util.List;

import javax.faces.component.UIData;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DetallePedidoPage extends PageCode {
	
	private Log log = LogFactory.getLog(DetallePedidoPage.class);
	
    private UIData uiDataTable;
    private HtmlDataTable dataTableDetPedido;
	private String initPage;
	
	private String cliente;
	private String pedido;
	private String fecha;
	private String entrega;
	private String estatus;
	private String usuario;
	private String comentarios;
	
	private IConsultaController consultaController;
	
    private Long idOrder = (Long) FacesContext.getCurrentInstance().
    		getExternalContext().getRequestMap().get(ConstantsCExpress.REQUEST_ID_ORDER);
    

    private List<DetailOrderConsultaTableVo> detPedidoList;
    private OrdersTableVo ordersTableVo;
    
	private void loadPage() {
		log.info(":::...[PAGE] (loadPage) --> OK");
		try {
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return;		
			}
			
			List<DetailOrderConsultaTableVo> res = 
					consultaController.obtenerDetallePedido(idOrder);
			
			
			setDetPedidoList(res);
			setOrdersTableVo(consultaController.obtenerPedidoPorId(idOrder));
		} catch(ControlException e) {
			e.printStackTrace();
		}
	}
    

	public IConsultaController getConsultaController() {
		return consultaController;
	}

	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}


	public UIData getUiDataTable() {
		return uiDataTable;
	}


	public void setUiDataTable(UIData uiDataTable) {
		this.uiDataTable = uiDataTable;
	}


	public HtmlDataTable getDataTableDetPedido() {
		if (dataTableDetPedido == null) {
			dataTableDetPedido = (HtmlDataTable) findComponentInRoot("dataTableDetPedido");
        }
		return dataTableDetPedido;
	}


	public void setDataTableDetPedido(HtmlDataTable dataTableDetPedido) {
		this.dataTableDetPedido = dataTableDetPedido;
	}


	public String getInitPage() {
		loadPage();
		return initPage;
	}


	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}


	public List<DetailOrderConsultaTableVo> getDetPedidoList() {
		return detPedidoList;
	}


	public void setDetPedidoList(List<DetailOrderConsultaTableVo> detPedidoList) {
		this.detPedidoList = detPedidoList;
	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] doRegresarAction regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_DETALLE_PEDIDO_TO_NOSESSION;
		}
		
		FacesContext.getCurrentInstance().getExternalContext().
				getSessionMap().remove(ConstantsCExpress.REQUEST_ID_ORDER);
		
		return ConstantsCExpress.GO_DETALLE_PEDIDO_TO_PEDIDOS;
	}


	/**
	 * @return the cliente
	 */
	public String getCliente() {
		cliente = ordersTableVo.getClientName();
		return cliente;
	}


	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	/**
	 * @return the pedido
	 */
	public String getPedido() {
		pedido = ordersTableVo.getNumOrder();
		return pedido;
	}


	/**
	 * @param pedido the pedido to set
	 */
	public void setPedido(String pedido) {
		this.pedido = pedido;
	}


	/**
	 * @return the fecha
	 */
	public String getFecha() {
		fecha = ordersTableVo.getCreationDate();
		return fecha;
	}


	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	/**
	 * @return the entrega
	 */
	public String getEntrega() {
		entrega = ordersTableVo.getEntrega();
		return entrega;
	}


	/**
	 * @param entrega the entrega to set
	 */
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		estatus = ordersTableVo.getDescEstatus();
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		usuario = ordersTableVo.getNick().concat(" (").
				concat(ordersTableVo.getNombreUsuario()).concat(")");
		return usuario;
	}


	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		comentarios = ordersTableVo.getComments();
		return comentarios;
	}


	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}


	/**
	 * @return the ordersTableVo
	 */
	public OrdersTableVo getOrdersTableVo() {
		return ordersTableVo;
	}


	/**
	 * @param ordersTableVo the ordersTableVo to set
	 */
	public void setOrdersTableVo(OrdersTableVo ordersTableVo) {
		this.ordersTableVo = ordersTableVo;
	}



}
