package mx.com.cityexpress.modules.consultas.presentation;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.apache.myfaces.custom.datascroller.HtmlDataScroller;
import org.apache.myfaces.custom.datascroller.ScrollerActionEvent;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.ClientTableVo;
import mx.com.cityexpress.general.vo.DetailOrderTableVo;
import mx.com.cityexpress.general.vo.MailBodyOrderVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mail.sender.MailSendHelper;
import mx.com.cityexpress.mail.sender.MailerVo;

public class PedidosPage extends PageCode {
	
	private Log log = LogFactory.getLog(PedidosPage.class);
	
    private HtmlDataTable dataTablePedidos;
//    private HtmlDataScroller scroller;
    
    private HtmlDataTable dataTableCliente;
    
	
	//private String initPage;
    private Long rowCount = new Long(40);
	
	private IConsultaController consultaController;
	
    private List<OrdersTableVo> ordersList;
    private List<ClientTableVo> clienteList;
    
    private boolean clientTblRender = false;
    
    
	@SuppressWarnings("unchecked")
	public String doEntregarAction() {
		log.info(":::...[PAGE]: Begin doEntregarAction --> OK");
		try {			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
			}
	
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doEntregarAction) Pedido Seleccionado: " + ordTblVo.getNumOrder());
			log.info(":::...[PAGE] (doEntregarAction) Cliente Seleccionado: " + ordTblVo.getClientName());
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					put(ConstantsCExpress.SESSION_TBL_VO, ordTblVo);
						
			log.info(":::...[PAGE]: End doEntregarAction --> OK");
			setClientTblRender(false);
			return ConstantsCExpress.GO_PEDIDOS_TO_CAMBIA_ESTATUS;
			
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doEntregarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	}
	}

	
	@SuppressWarnings("unchecked")
	public String doDetalleAction() {
		log.info(":::...[PAGE]: Begin doDetalleAction --> OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
			}
	
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doDetalleAction) Pedido Seleccionado: " + ordTblVo.getIdOrder());
			log.info(":::...[PAGE] (doDetalleAction) Cliente Seleccionado: " + ordTblVo.getIdClient());
			
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
					put(ConstantsCExpress.REQUEST_ID_ORDER, ordTblVo.getIdOrder());
			
			
			log.info(":::...[PAGE]: End doDetalleAction --> OK");
			setClientTblRender(false);
			return ConstantsCExpress.GO_PEDIDOS_TO_DETALLE_PEDIDO;
			
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doDetalleAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	}
	}
	
	@SuppressWarnings("unchecked")
	public String doSendAction() {
		log.info(":::...[PAGE]: Begin doSendAction --> OK");
		
		String mensajeConfirmacion = ConstantsCExpress.MSG_RESEND_MAIL_OK;
		
		try {
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
			}
			
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doSendAction) Pedido Seleccionado: " + ordTblVo.getIdOrder());
			String numOrder = "PE-".concat(ordTblVo.getIdOrder().toString());
			
			List<DetailOrderTableVo> 
					detailOrderList = consultaController.getOrderDetail(numOrder);
			
			MailBodyOrderVo bodyVo = new MailBodyOrderVo();
			bodyVo.setListDetail(detailOrderList);
			bodyVo.setOrder(numOrder);
			
			String nombreCliente = ordTblVo.getClientName(); 
					//consultaController.getClientName(userSesion.getIdCliente());
			
			if (nombreCliente == null) {
				throw new ControlException("No existe cliente asignado al usuario.");
			}
			
			UsuarioEnvioCorreoVO usEnvioVO = consultaController.
					obtenerUsuarioPorNick(ordTblVo.getNick());
			
			
			List<String> destination = new ArrayList<String>();
			String[] bccDestionation = new String[]{
					ConstantsCExpress.MAIL_ADMIN, //
					ConstantsCExpress.MAIL_CC_1, //
					ConstantsCExpress.MAIL_CC_2, //
					ConstantsCExpress.MAIL_CC_3, //
					ConstantsCExpress.MAIL_CC_4, //
					ConstantsCExpress.MAIL_CC_5, //
					ConstantsCExpress.MAIL_CC_6};
			destination.add(usEnvioVO.getEmail());
			// Get the manager email
			if (!userSesion.getPerfil().equals(ConstantsCExpress.PERFIL_GERENTE)) {
				String emailGerente = consultaController.obtenerEmailGerente(userSesion.getIdCliente(),
						ConstantsCExpress.PERFIL_GERENTE);
				if (emailGerente != null && emailGerente.length() > 0) {
					destination.add(emailGerente);
				}
			}
			
			bodyVo.setSolicitante(nombreCliente);
			bodyVo.setUser(usEnvioVO.getNombre());
			bodyVo.setAtencion(usEnvioVO.getNombre());
			bodyVo.setFecha_alta(ordTblVo.getCreationDate());
			bodyVo.setFecha_entrega(ordTblVo.getEntrega());
			
			
			MailSendHelper mailHelper = new MailSendHelper();
			String body = mailHelper.createBodyNewOrder(bodyVo);
			
			MailerVo mailerVo = new MailerVo();
			mailerVo.setFrom(ConstantsCExpress.MAIL_FROM);
			mailerVo.setAttach(null);
			mailerVo.setDestination(destination.toArray(new String[destination.size()]));
			mailerVo.setBccDestination(bccDestionation);
			mailerVo.setSubject(ConstantsCExpress.MAIL_SUBJECT.concat(numOrder));
			mailerVo.setBody(body);
			
			try {
				
				consultaController.sendMail(mailerVo);
			} catch (Exception ex) {
				mensajeConfirmacion = ConstantsCExpress.MSG_RESEND_MAIL_OK;
				
				FacesContext.getCurrentInstance().getExternalContext().getRequestMap().
						put(ConstantsCExpress.REQUEST_MESSAGE, mensajeConfirmacion);
				
				log.info(":::...[PAGE] end doSendAction FAIL");
				return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
			}
			
			log.info(":::...[PAGE] end doSendAction OK");
			return ConstantsCExpress.GO_PEDIDOS_TO_SUCCESS;

    	} catch(ControlException e) {   
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doSendAction : " + e.getMessage());
    		e.printStackTrace();
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	}

	}
	
	@SuppressWarnings("unchecked")
	public String doLigaClienteAction() {
		log.info(":::...[PAGE]: Begin doLigaClienteAction --> OK");
		try {
			
			UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
					getCurrentInstance().getExternalContext().getSessionMap().
							get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
			}
			
			OrdersTableVo ordTblVo = 
					(OrdersTableVo)getDataTablePedidos().getRowData();
			
			log.info(":::...[PAGE] (doLigaClienteAction) Pedido Seleccionado: " + ordTblVo.getIdOrder());
			log.info(":::...[PAGE] (doLigaClienteAction) Cliente Seleccionado: " + ordTblVo.getIdClient());
			
			List<ClientTableVo> res = consultaController.
					obtenerClientePorId(ordTblVo.getIdClient());
			setClienteList(res);
			setClientTblRender(true);
			
			return ConstantsCExpress.GO_REFRESH_PAGE;
    	} catch(ControlException e) {    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						e.getMessage());
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	} catch(Exception e) {
    		log.error(":::...[PAGE] end doLigaClienteAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_PEDIDOS_TO_FAIL;
    	}
	}
	
	
	public IConsultaController getConsultaController() {
		return consultaController;
	}

	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}


	public HtmlDataTable getDataTablePedidos() {
		if (dataTablePedidos == null) {
			dataTablePedidos = (HtmlDataTable) findComponentInRoot("dataTablePedidos");
        }
		return dataTablePedidos;
	}

	public void setDataTablePedidos(HtmlDataTable dataTablePedidos) {
		this.dataTablePedidos = dataTablePedidos;
	}



	public List<OrdersTableVo> getOrdersList() {
		BusquedaPedidoVo busquedaVo = (BusquedaPedidoVo) 
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					get(ConstantsCExpress.SESSION_VO);
	
		log.info(":::...[PAGE] (getOrdersList) estatus: " + busquedaVo.getEstatus());
	
		try {
			ordersList = consultaController.obtenerPedidos(busquedaVo);
		} catch(ControlException e) {
			e.printStackTrace();
		}
	
		return ordersList;
	}

	public void setOrdersList(List<OrdersTableVo> ordersList) {
		this.ordersList = ordersList;
	}

	public boolean isClientTblRender() {
		return clientTblRender;
	}

	public void setClientTblRender(boolean clientTblRender) {
		this.clientTblRender = clientTblRender;
	}


	public HtmlDataTable getDataTableCliente() {
		if (dataTableCliente == null) {
			dataTableCliente = (HtmlDataTable) findComponentInRoot("dataTableCliente");
        }
		return dataTableCliente;
	}

	public void setDataTableCliente(HtmlDataTable dataTableCliente) {
		this.dataTableCliente = dataTableCliente;
	}

	public List<ClientTableVo> getClienteList() {
		return clienteList;
	}

	public void setClienteList(List<ClientTableVo> clienteList) {
		this.clienteList = clienteList;
	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] doRegresarAction regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_PEDIDOS_TO_NOSESSION;
		}

		FacesContext.getCurrentInstance().getExternalContext().
				getSessionMap().remove(ConstantsCExpress.SESSION_VO);
		
		setClienteList(new ArrayList<ClientTableVo>());
		setClientTblRender(false);
		
		return ConstantsCExpress.GO_PEDIDOS_TO_BUSQUEDA_PED;
	}
	
	
	public void scrollerAction(ActionEvent event) {
		ScrollerActionEvent scrollerEvent = (ScrollerActionEvent)event;
		FacesContext.getCurrentInstance().getExternalContext().log(
				"scrollerAction: facet: " + scrollerEvent.getScrollerfacet() + ", pageIndex: " + scrollerEvent.getPageIndex());
		
	}

	/**
	 * @return the rowCount
	 */
	public Long getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(Long rowCount) {
		this.rowCount = rowCount;
	}

}
