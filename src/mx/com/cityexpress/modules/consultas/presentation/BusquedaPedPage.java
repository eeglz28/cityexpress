package mx.com.cityexpress.modules.consultas.presentation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import mx.com.cityexpress.general.common.CatalogoItem;
import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.IConsultaController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mapping.pojo.Client;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.myfaces.custom.calendar.HtmlInputCalendar;

public class BusquedaPedPage  extends PageCode {
	
	private Log log = LogFactory.getLog(BusquedaPedPage.class);
	
	private HtmlSelectOneMenu clienteSelectOneMenu;
	protected UISelectItems clienteSelectItems;
	protected SelectItem[] selectItemCliente;
	private HtmlInputText pedidoText;
	private HtmlInputCalendar initDate;
	private HtmlInputCalendar endDate;
	private HtmlSelectOneMenu estatusSelectOneMenu;
	
	private String initPage;
	
	private IConsultaController consultaController;
	

	public void loadPage() {
		log.info(":::...[PAGE] (loadPage) --> OK");
	}
	
	
	public HtmlSelectOneMenu getClienteSelectOneMenu() {
		if (clienteSelectOneMenu == null) {
			clienteSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("clienteSelectOneMenu");
        }
		return clienteSelectOneMenu;
	}

	public void setClienteSelectOneMenu(HtmlSelectOneMenu clienteSelectOneMenu) {
		this.clienteSelectOneMenu = clienteSelectOneMenu;
	}

	public UISelectItems getClienteSelectItems() {
		if (clienteSelectItems == null) {
			clienteSelectItems = (UISelectItems) findComponentInRoot("clienteSelectItems");
        }
		return clienteSelectItems;
	}

	public void setClienteSelectItems(UISelectItems clienteSelectItems) {
		this.clienteSelectItems = clienteSelectItems;
	}

	
	@SuppressWarnings("rawtypes")
	public SelectItem[] getSelectItemCliente() {
		CatalogoItem item = new CatalogoItem();
		try {
			
			List<Client> clientesList = consultaController.obtenerClientes();
			Set clienteSet = item.getSelectClientes(clientesList);
			
			selectItemCliente = (SelectItem[])clienteSet.iterator().next();
			
		} catch(ControlException e) {
			log.error(":::...[PAGE]: getSelectItemCliente --> ControlException");
		} catch(Exception e) {
			log.error(":::...[PAGE]: getSelectItemCliente --> Exception");
		}
		return selectItemCliente;
	}

	public void setSelectItemCliente(SelectItem[] selectItemCliente) {
		this.selectItemCliente = selectItemCliente;
	}

	public HtmlInputText getPedidoText() {
		if (pedidoText == null) {
			pedidoText = (HtmlInputText) findComponentInRoot("pedidoText");
        }
		return pedidoText;
	}

	public void setPedidoText(HtmlInputText pedidoText) {
		this.pedidoText = pedidoText;
	}

	public HtmlInputCalendar getInitDate() {
		if (initDate == null) {
			initDate = (HtmlInputCalendar) findComponentInRoot("initDate");
        }
		return initDate;
	}

	public void setInitDate(HtmlInputCalendar initDate) {
		this.initDate = initDate;
	}

	public HtmlInputCalendar getEndDate() {
		if (endDate == null) {
			endDate = (HtmlInputCalendar) findComponentInRoot("endDate");
        }
		return endDate;
	}

	public void setEndDate(HtmlInputCalendar endDate) {
		this.endDate = endDate;
	}

	public HtmlSelectOneMenu getEstatusSelectOneMenu() {
		if (estatusSelectOneMenu == null) {
			estatusSelectOneMenu = (HtmlSelectOneMenu) findComponentInRoot("estatusSelectOneMenu");
        }
		return estatusSelectOneMenu;
	}

	public void setEstatusSelectOneMenu(HtmlSelectOneMenu estatusSelectOneMenu) {
		this.estatusSelectOneMenu = estatusSelectOneMenu;
	}

	public String getInitPage() {
		loadPage();
		return initPage;
	}

	public void setInitPage(String initPage) {
		this.initPage = initPage;
	}
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info(":::...[PAGE]: Begin doAceptarAction --> OK");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
    		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
    				getCurrentInstance().getExternalContext().getSessionMap().
    						get(ConstantsCExpress.SESSION_USERS_LOGIN);
			if (userSesion == null) { 
				return ConstantsCExpress.GO_BUSQUEDA_PED_TO_NOSESSION;	
			}
	
			log.info(":::...[PAGE]: Cliente : " + getClienteSelectOneMenu().getValue());
			log.info(":::...[PAGE]: Num Pedidio: " + getPedidoText().getValue());
			log.info(":::...[PAGE]: Fecha Incial: " + getInitDate().getValue());
			log.info(":::...[PAGE]: Fecha Final: " + getEndDate().getValue());
			log.info(":::...[PAGE]: Estatus: " + getEstatusSelectOneMenu().getValue());
			
			BusquedaPedidoVo pedidoVo = new BusquedaPedidoVo(); 
			if (getClienteSelectOneMenu().getValue() != null) {
				pedidoVo.setCliente(getClienteSelectOneMenu().getValue().toString());	
			}
			if (getPedidoText().getValue() != null) {
				pedidoVo.setNumPedido(getPedidoText().getValue().toString());	
			}
			if (getInitDate().getValue() != null) {
				Date fi = (Date)getInitDate().getValue();
				String sfi = sdf.format(fi);
				pedidoVo.setFechaInicial(sfi);
				log.info(":::...[PAGE]: Fecha Incial String: " + pedidoVo.getFechaInicial());
			}
			if (getEndDate().getValue() != null) {
				Date fe = (Date)getEndDate().getValue();
				String sfe = sdf.format(fe);
				pedidoVo.setFechaFinal(sfe);
				log.info(":::...[PAGE]: Fecha Final String: " + pedidoVo.getFechaFinal());
			}
			if (getEstatusSelectOneMenu().getValue() != null) {
				pedidoVo.setEstatus(getEstatusSelectOneMenu().getValue().toString());	
			}
			
			
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().
					put(ConstantsCExpress.SESSION_VO, pedidoVo);
			
			return ConstantsCExpress.GO_BUSQUEDA_PED_TO_PEDIDOS;
		} catch(Exception e) {
    		log.error(":::...[PAGE] end doAceptarAction : " + e.getMessage());
    		
    		FacesContext.getCurrentInstance().getExternalContext().
    				getRequestMap().put(ConstantsCExpress.REQUEST_EXCEPTION, 
    						ConstantsCExpress.ERROR_PAGE);
    		
    		return ConstantsCExpress.GO_BUSQUEDA_PED_TO_FAIL;
    	}
		
	}

	public IConsultaController getConsultaController() {
		return consultaController;
	}

	public void setConsultaController(IConsultaController consultaController) {
		this.consultaController = consultaController;
	}
	
	
	public String doRegresarAction() {
		log.info(":::...[PAGE] (doRegresarAction) regresando .... ");
		
		UsuarioSesionVO userSesion = (UsuarioSesionVO) FacesContext.
				getCurrentInstance().getExternalContext().getSessionMap().
						get(ConstantsCExpress.SESSION_USERS_LOGIN);
		if (userSesion == null) { 
			return ConstantsCExpress.GO_BUSQUEDA_PED_TO_NOSESSION;		
		}
		
		return ConstantsCExpress.GO_BUSQUEDA_PED_TO_MENU;
	}

}
