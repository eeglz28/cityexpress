package mx.com.cityexpress.modules.consultas.validator;

import mx.com.cityexpress.general.common.DataValidator;
import mx.com.cityexpress.modules.consultas.vo.ConsultaPedidoValidateVO;

public class ConsultaPedidosValidator {
	
	public static boolean validateCambiaEstatus(
			ConsultaPedidoValidateVO validateVO) {
		
		DataValidator validator = new DataValidator();
        boolean valid = true;
        
        if ( validator.isEmptyNull(validateVO.getIdPedido()) ) {
        	validateVO.setMsgErr("El número de pedido no esta en sesión.");
            valid = false;
        }

        if ( !validator.isValidOption(validateVO.getEstatusSelectOneMenu()) ) {
        	validateVO.setEstatusErr("El estatus del pedido es obligatorio");
        	valid = false;
        }

        return valid;

	}

}
