package mx.com.cityexpress.modules.consultas.builder;

import mx.com.cityexpress.modules.consultas.vo.ConsultaPedidoValidateVO;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;

public class ConsultaPedidosBuilder {
	
	public InputPedidoVO buildActualizaPedido(ConsultaPedidoValidateVO validVO) {
		InputPedidoVO inputVO = new InputPedidoVO(
				new Long(validVO.getIdPedido()),
				validVO.getEstatusSelectOneMenu().getValue().toString().trim(),
				validVO.getGuiaText().getValue().toString().trim(),
				validVO.getCommentsTextArea().getValue().toString().trim()
				);
		
		return inputVO;
	}

}
