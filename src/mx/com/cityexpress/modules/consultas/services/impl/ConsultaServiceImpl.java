package mx.com.cityexpress.modules.consultas.services.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.ClientTableVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.mapping.pojo.Users;
import mx.com.cityexpress.modules.consultas.services.IConsultaService;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;
import mx.com.cityexpress.persistence.dao.IClientDao;
import mx.com.cityexpress.persistence.dao.IOrderDao;
import mx.com.cityexpress.persistence.dao.IOrdersDetailDao;
import mx.com.cityexpress.persistence.dao.IUsersDao;

public class ConsultaServiceImpl implements IConsultaService{
	
	private Log log = LogFactory.getLog(ConsultaServiceImpl.class);
	
	private IClientDao clientDao;
	private IOrderDao orderDao;
	private IOrdersDetailDao detailDao;
	private IUsersDao usersDao;
	
	
	public List<Client> obtenerClientes() throws BusinessException {
		log.info(":::...[S] begin obtenerClientes OK");
		try {
			List<Client> clientesList = clientDao.selectClients();
			
			log.info(":::...[S] end obtenerClientes OK");
			return clientesList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerClientes : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}


	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo)
			throws BusinessException {
		log.info(":::...[S] begin obtenerPedidos OK");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		GregorianCalendar gc = new GregorianCalendar();
		
		List<OrdersTableVo> ordersList = new ArrayList<OrdersTableVo>();
		try {
			
			List<Object[]> clientesList = orderDao.selectPedidos(busquedaVo);
			if (clientesList != null && clientesList.size() > 0) {
				for (Object[] obj : clientesList) {
					OrdersTableVo orderVo = new OrdersTableVo();
					orderVo.setIdOrder( (Long)obj[0] );
					orderVo.setNumOrder( (String)obj[1] );
					Date fecha = (Date)obj[2];
					orderVo.setCreationDate(sdf.format(fecha));
					
					gc.setTime(fecha);
					gc.add(GregorianCalendar.DATE, ConstantsCExpress.C_DIAS_ENTREGA_PEDIDO);

					orderVo.setEntrega(sdf.format(gc.getTime()));
					orderVo.setTotal( (BigDecimal)obj[3] );
					orderVo.setNick( (String)obj[4] );
					orderVo.setClientName( (String)obj[5] );
					orderVo.setIdClient( (Long)obj[6] );
					orderVo.setEstatus( (String)obj[7] );
					orderVo.setGuia( (String)obj[8] );
					orderVo.setComments( (String)obj[9] );
					orderVo.setNombreUsuario( (String)obj[10] );
					
					if (orderVo.getEstatus().equals("1")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_EN_PROCESO);
						orderVo.setEstatusRendered(true);
					} else if (orderVo.getEstatus().equals("2")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_ENTREGADO);
						orderVo.setEstatusRendered(true);
					} else if (orderVo.getEstatus().equals("3")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_CERRADO);
						orderVo.setEstatusRendered(false);
					} else if (orderVo.getEstatus().equals("4")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_PARCIAL);
						orderVo.setEstatusRendered(true);
					}
					
					ordersList.add(orderVo);
				}
			}
			
			log.info(":::...[S] end obtenerPedidos OK");
			return ordersList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerPedidos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerPedidos : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	
	public List<OrdersTableVo> obtenerPedidosUsuarios(BusquedaPedidoVo busquedaVo)
			throws BusinessException {
		log.info(":::...[S] begin obtenerPedidosUsuarios OK");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		GregorianCalendar gc = new GregorianCalendar();
		
		List<OrdersTableVo> ordersList = new ArrayList<OrdersTableVo>();
		try {
			List<Object[]> clientesList;
			
			// If the user is manager then get all orders by client.
			if (busquedaVo.getIdPerfil().equals(ConstantsCExpress.PERFIL_GERENTE)) {
				String idClient = usersDao.selectIdClientbyUser(busquedaVo.getIdUsuario());
				if (idClient != null && idClient.length() > 0) {
					busquedaVo.setCliente(idClient);
					clientesList = orderDao.selectPedidosCliente(busquedaVo);
				} else {
					clientesList = orderDao.selectPedidosUsuario(busquedaVo);
				}
			} else { // If the user is not manager, only get orders by user.
				clientesList = orderDao.selectPedidosUsuario(busquedaVo);	
			}
			
			if (clientesList != null && clientesList.size() > 0) {
				for (Object[] obj : clientesList) {
					OrdersTableVo orderVo = new OrdersTableVo();
					orderVo.setIdOrder( (Long)obj[0] );
					orderVo.setNumOrder( (String)obj[1] );
					Date fecha = (Date)obj[2];
					orderVo.setCreationDate(sdf.format(fecha));
					
					gc.setTime(fecha);
					gc.add(GregorianCalendar.DATE, ConstantsCExpress.C_DIAS_ENTREGA_PEDIDO);

					orderVo.setEntrega(sdf.format(gc.getTime()));
					orderVo.setTotal( (BigDecimal)obj[3] );
					orderVo.setEstatus( (String)obj[4] );
					orderVo.setGuia( (String)obj[5] );
					
					if (orderVo.getEstatus().equals("1")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_EN_PROCESO);
						orderVo.setEstatusRendered(true);
					} else if (orderVo.getEstatus().equals("2")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_ENTREGADO);
						orderVo.setEstatusRendered(false);
					} else if (orderVo.getEstatus().equals("3")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_CERRADO);
						orderVo.setEstatusRendered(false);
					} else if (orderVo.getEstatus().equals("4")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_PARCIAL);
						orderVo.setEstatusRendered(false);
					}
					ordersList.add(orderVo);
				}
			}
			
			log.info(":::...[S] end obtenerPedidosUsuarios OK");
			return ordersList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerPedidosUsuarios : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerPedidosUsuarios : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public List<ClientTableVo> obtenerClientePorId(long idCliente) 
			throws BusinessException {
		log.info(":::...[S] begin obtenerClientePorId OK");
		List<ClientTableVo> clienteList = new ArrayList<ClientTableVo>();
		try {
			Client cliente = clientDao.selectClient(idCliente);
			ClientTableVo cliTblVo = new ClientTableVo(); 
			if (cliente != null) {
				cliTblVo.setClientName(cliente.getClientName());
				cliTblVo.setEmail(cliente.getEmail());
				cliTblVo.setTelephone(cliente.getPhone());
			}
			clienteList.add(cliTblVo);
			
			log.info(":::...[S] end obtenerClientePorId OK");
			return clienteList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerClientePorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerClientePorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}

	public void actualizarEstatusPedido(InputPedidoVO inputVO) 
			throws BusinessException {
		log.info(":::...[S] begin actualizarEstatusPedido OK");

		try {
			orderDao.updateEstatusPedido(inputVO);
			
			log.info(":::...[S] end actualizarEstatusPedido OK");
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end actualizarEstatusPedido : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end actualizarEstatusPedido : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}



	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder)
			throws BusinessException {
		log.info(":::...[S] begin obtenerDetallePedido OK");
		
		List<DetailOrderConsultaTableVo> ordDetList = new ArrayList<DetailOrderConsultaTableVo>();
		try {
			
			List<Object[]> detList = detailDao.selectOrderDetail(idOrder);
			if (detList != null && detList.size() > 0) {
				for (Object[] obj : detList) {
					DetailOrderConsultaTableVo detailVo = new DetailOrderConsultaTableVo();
					detailVo.setIdProduct((Long)obj[0]);
					detailVo.setProducto( (String)obj[1] );
					detailVo.setCantidad((BigDecimal)obj[2]);
					detailVo.setUnidad( (String)obj[3] );
					detailVo.setPrecio((BigDecimal)obj[4]);
					detailVo.setSubtotal(
							detailVo.getCantidad().multiply(detailVo.getPrecio()));
					detailVo.setFolio( (String)obj[5] );
					
					ordDetList.add(detailVo);
				}
			}
			
			log.info(":::...[S] end obtenerDetallePedido OK");
			return ordDetList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerDetallePedido : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerDetallePedido : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
	public UsuarioEnvioCorreoVO obtenerUsuarioPorNick(String nick) 
			throws BusinessException {
		log.info(":::...[S] begin obtenerUsuarioPorNick OK");
		UsuarioEnvioCorreoVO usCorreoVO = null;
		try {
			Users user = usersDao.getUserbyNick(nick);
			if (user != null) {
				usCorreoVO = new UsuarioEnvioCorreoVO();
				usCorreoVO.setNombre(user.getName());
				usCorreoVO.setEmail(user.getEmail());
				usCorreoVO.setIdCliente(user.getClient().getIdClient().toString());
				usCorreoVO.setIdPerfil(user.getProfile().getIdProfile().toString());
			}
			
			log.info(":::...[S] end obtenerUsuarioPorNick OK");
			return usCorreoVO;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerUsuarioPorNick : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerUsuarioPorNick : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
		
	}
	
	
	
	public OrdersTableVo obtenerPedidoPorId(Long idOrder)
			throws BusinessException {
		log.info(":::...[S] begin obtenerPedidoPorId OK");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		GregorianCalendar gc = new GregorianCalendar();
		
		
		
		OrdersTableVo orderVo = new OrdersTableVo();
		try {
			
			List<Object[]> clientesList = orderDao.selectPedidoPorId(idOrder);
			if (clientesList != null && clientesList.size() > 0) {
				for (Object[] obj : clientesList) {
					orderVo.setIdOrder( (Long)obj[0] );
					orderVo.setNumOrder( (String)obj[1] );
					Date fecha = (Date)obj[2];
					orderVo.setCreationDate(sdf.format(fecha));
					
					gc.setTime(fecha);
					gc.add(GregorianCalendar.DATE, ConstantsCExpress.C_DIAS_ENTREGA_PEDIDO);

					orderVo.setEntrega(sdf.format(gc.getTime()));
					
					orderVo.setTotal( (BigDecimal)obj[3] );
					orderVo.setNick( (String)obj[4] );
					orderVo.setClientName( (String)obj[5] );
					orderVo.setIdClient( (Long)obj[6] );
					orderVo.setEstatus( (String)obj[7] );
					orderVo.setGuia( (String)obj[8] );
					orderVo.setComments( (String)obj[9] );
					orderVo.setNombreUsuario( (String)obj[10] );
					
					if (orderVo.getEstatus().equals("1")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_EN_PROCESO);
						orderVo.setEstatusRendered(true);
					} else if (orderVo.getEstatus().equals("2")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_ENTREGADO);
						orderVo.setEstatusRendered(true);
					} else if (orderVo.getEstatus().equals("3")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_CERRADO);
						orderVo.setEstatusRendered(false);
					} else if (orderVo.getEstatus().equals("4")) {
						orderVo.setDescEstatus(ConstantsCExpress.ESTATUS_PARCIAL);
						orderVo.setEstatusRendered(true);
					}
				}
			}
			
			log.info(":::...[S] end obtenerPedidoPorId OK");
			return orderVo;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end obtenerPedidoPorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end obtenerPedidoPorId : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}


	public IOrdersDetailDao getDetailDao() {
		return detailDao;
	}

	public void setDetailDao(IOrdersDetailDao detailDao) {
		this.detailDao = detailDao;
	}
	
	public IClientDao getClientDao() {
		return clientDao;
	}

	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}
	
	public IOrderDao getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(IOrderDao orderDao) {
		this.orderDao = orderDao;
	}


	/**
	 * @return the usersDao
	 */
	public IUsersDao getUsersDao() {
		return usersDao;
	}


	/**
	 * @param usersDao the usersDao to set
	 */
	public void setUsersDao(IUsersDao usersDao) {
		this.usersDao = usersDao;
	}
	
}
