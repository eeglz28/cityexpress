package mx.com.cityexpress.modules.consultas.services;

import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.BusquedaPedidoVo;
import mx.com.cityexpress.general.vo.ClientTableVo;
import mx.com.cityexpress.general.vo.DetailOrderConsultaTableVo;
import mx.com.cityexpress.general.vo.OrdersTableVo;
import mx.com.cityexpress.general.vo.UsuarioEnvioCorreoVO;
import mx.com.cityexpress.mapping.pojo.Client;
import mx.com.cityexpress.modules.consultas.vo.InputPedidoVO;

public interface IConsultaService {
	
	public List<Client> obtenerClientes() throws BusinessException;
	
	
	public List<OrdersTableVo> obtenerPedidos(BusquedaPedidoVo busquedaVo) 
			throws BusinessException;
	
	public List<OrdersTableVo> obtenerPedidosUsuarios(BusquedaPedidoVo busquedaVo)
			throws BusinessException;
	
	
	public List<ClientTableVo> obtenerClientePorId(long idCliente) 
			throws BusinessException;
	
	
	public void actualizarEstatusPedido(InputPedidoVO inputVO) 
			throws BusinessException;
	
	
	public List<DetailOrderConsultaTableVo> obtenerDetallePedido(Long idOrder) 
			throws BusinessException;
	
	public UsuarioEnvioCorreoVO obtenerUsuarioPorNick(String nick) 
			throws BusinessException;
	
	
	public OrdersTableVo obtenerPedidoPorId(Long idOrder)
			throws BusinessException;

}
