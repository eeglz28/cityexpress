package mx.com.cityexpress.modules.consultas.vo;

import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneMenu;

public class ConsultaPedidoValidateVO {
	
	private String idPedido;
	private HtmlSelectOneMenu estatusSelectOneMenu;
    private HtmlInputText guiaText;
    private HtmlInputTextarea commentsTextArea;
    
    private String msgErr;
    private String estatusErr;
    private String guiaErr;
    
    
	/**
	 * @param estatusSelectOneMenu
	 * @param guiaText
	 */
	public ConsultaPedidoValidateVO(String idPedido, 
			HtmlSelectOneMenu estatusSelectOneMenu, HtmlInputText guiaText,
			HtmlInputTextarea commentsTextArea) {
		super();
		this.idPedido = idPedido;
		this.estatusSelectOneMenu = estatusSelectOneMenu;
		this.guiaText = guiaText;
		this.commentsTextArea = commentsTextArea;
	}


	/**
	 * @return the estatusSelectOneMenu
	 */
	public HtmlSelectOneMenu getEstatusSelectOneMenu() {
		return estatusSelectOneMenu;
	}


	/**
	 * @param estatusSelectOneMenu the estatusSelectOneMenu to set
	 */
	public void setEstatusSelectOneMenu(HtmlSelectOneMenu estatusSelectOneMenu) {
		this.estatusSelectOneMenu = estatusSelectOneMenu;
	}


	/**
	 * @return the guiaText
	 */
	public HtmlInputText getGuiaText() {
		return guiaText;
	}


	/**
	 * @param guiaText the guiaText to set
	 */
	public void setGuiaText(HtmlInputText guiaText) {
		this.guiaText = guiaText;
	}


	/**
	 * @return the estatusErr
	 */
	public String getEstatusErr() {
		return estatusErr;
	}


	/**
	 * @param estatusErr the estatusErr to set
	 */
	public void setEstatusErr(String estatusErr) {
		this.estatusErr = estatusErr;
	}


	/**
	 * @return the guiaErr
	 */
	public String getGuiaErr() {
		return guiaErr;
	}


	/**
	 * @param guiaErr the guiaErr to set
	 */
	public void setGuiaErr(String guiaErr) {
		this.guiaErr = guiaErr;
	}


	/**
	 * @return the idPedido
	 */
	public String getIdPedido() {
		return idPedido;
	}


	/**
	 * @param idPedido the idPedido to set
	 */
	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}


	/**
	 * @return the msgErr
	 */
	public String getMsgErr() {
		return msgErr;
	}


	/**
	 * @param msgErr the msgErr to set
	 */
	public void setMsgErr(String msgErr) {
		this.msgErr = msgErr;
	}

	/**
	 * @return the commentsTextArea
	 */
	public HtmlInputTextarea getCommentsTextArea() {
		return commentsTextArea;
	}


	/**
	 * @param commentsTextArea the commentsTextArea to set
	 */
	public void setCommentsTextArea(HtmlInputTextarea commentsTextArea) {
		this.commentsTextArea = commentsTextArea;
	}
    

}
