package mx.com.cityexpress.modules.consultas.vo;

public class InputPedidoVO {
	
	private Long idPedido;
	private String estatus;
	private String guia;
	private String comments;
	
	
	/**
	 * @param idPedido
	 * @param estatus
	 * @param guia
	 */
	public InputPedidoVO(Long idPedido, String estatus, String guia, 
			String comments) {
		super();
		this.idPedido = idPedido;
		this.estatus = estatus;
		this.guia = guia;
		this.comments = comments;
	}


	/**
	 * @return the idPedido
	 */
	public Long getIdPedido() {
		return idPedido;
	}


	/**
	 * @param idPedido the idPedido to set
	 */
	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}


	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}


	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	/**
	 * @return the guia
	 */
	public String getGuia() {
		return guia;
	}


	/**
	 * @param guia the guia to set
	 */
	public void setGuia(String guia) {
		this.guia = guia;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
