package mx.com.cityexpress.modules.login.presentation;

import java.util.List;

import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.controllers.ILoginController;
import mx.com.cityexpress.general.exception.ControlException;
import mx.com.cityexpress.general.presentation.PageCode;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;


public class LoginPage extends PageCode {

	private HtmlInputText userText;
    private HtmlInputSecret passSecret;
    
    private String message = "";
    
    private ILoginController loginController;
    
    private Log log = LogFactory.getLog(LoginPage.class);
    

	/**
	 * @return the userText
	 */
	public HtmlInputText getUserText() {
		if (userText == null) {
			userText = (HtmlInputText) findComponentInRoot("userText");
        }
		return userText;
	}
	
	/**
	 * @return the passSecret
	 */
	public HtmlInputSecret getPassSecret() {
		if (passSecret == null) {
			passSecret = (HtmlInputSecret) findComponentInRoot("passSecret");
        }
		return passSecret;
	}

	/**
	 * @param passSecret the passSecret to set
	 */
	public void setPassSecret(HtmlInputSecret passSecret) {
		this.passSecret = passSecret;
	}
    
	/**
	 * @param userText the userText to set
	 */
	public void setUserText(HtmlInputText userText) {
		this.userText = userText;
	}
	
	@SuppressWarnings("unchecked")
	public String doAceptarAction() {
		log.info(":::...[PAGE]: Login - Usuario: " + getUserText().getValue().
				toString() + "  Pass: " + getPassSecret().getValue().
				toString());
		try {
			UsuarioSesionVO usuarioSesion = 
					loginController.validateLogin(getUserText().getValue().
							toString(), getPassSecret().getValue().toString());
			
			if(usuarioSesion == null) {
				setMessage("Ocurrió un problema al ingresar, contacte al administrador.");
				return ConstantsCExpress.GO_REFRESH_PAGE;
			}
			
			log.info(":::...[PAGE] usurioSesion - idCliente: " + usuarioSesion.getIdCliente());
			setMessage("");
			
			if (!usuarioSesion.getIdCliente().equals("9999")) {
				List<ProductTableVo> listTblVo = loginController.getProductsToCatalog();

				FacesContext.getCurrentInstance().getExternalContext().
						getSessionMap().put(
								ConstantsCExpress.SESSION_PRODUCTS, listTblVo);
			}
								
			FacesContext.getCurrentInstance().getExternalContext().
					getSessionMap().put(ConstantsCExpress.SESSION_USERS_LOGIN, 
							usuarioSesion);
			
			log.info(":::...[PAGE] end doAceptarAction OK");	
			return ConstantsCExpress.GO_LOGIN_TO_INICIO;
		} catch(ControlException e) {
			log.error(e.getMessage());
			setMessage(e.getMessage());
			return ConstantsCExpress.GO_REFRESH_PAGE;
		} catch (Exception e) {
			log.error(e.getMessage());
			setMessage(ConstantsCExpress.ERROR_PAGE);
			return ConstantsCExpress.GO_REFRESH_PAGE;
		}
	}

	/**
	 * @return the loginController
	 */
	public ILoginController getLoginController() {
		return loginController;
	}

	/**
	 * @param loginController the loginController to set
	 */
	public void setLoginController(ILoginController loginController) {
		this.loginController = loginController;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


    
    
}
