package mx.com.cityexpress.modules.login.services;

import java.util.List;

import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;

public interface ILoginService {
	
	public UsuarioSesionVO validateLogin(String user, String pass) 
			throws BusinessException;
	
    public List<ProductTableVo> getProductsToCatalog() 
    		throws BusinessException;
	
}
