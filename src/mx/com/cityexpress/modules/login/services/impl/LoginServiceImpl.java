package mx.com.cityexpress.modules.login.services.impl;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.exception.BusinessException;
import mx.com.cityexpress.general.exception.DatabaseException;
import mx.com.cityexpress.general.transformer.ProductToProductTableTransformer;
import mx.com.cityexpress.general.vo.ProductTableVo;
import mx.com.cityexpress.general.vo.UsuarioSesionVO;
import mx.com.cityexpress.mapping.pojo.Product;
import mx.com.cityexpress.mapping.pojo.Users;
import mx.com.cityexpress.modules.login.services.ILoginService;
import mx.com.cityexpress.persistence.dao.IProductDao;
import mx.com.cityexpress.persistence.dao.IUsersDao;

public class LoginServiceImpl implements ILoginService {
	
	private Log log = LogFactory.getLog(LoginServiceImpl.class);
	
	private IUsersDao usersDao;
	private IProductDao productDao;

	public UsuarioSesionVO validateLogin(String user, String pass)
			throws BusinessException {
		Users users = null;
		try {
			log.info(":::...[S] begin validateLogin OK");
			if (user == null || user.equals("") ) {
				throw new BusinessException("El usuario no puede ser vacio.");
			}
			
			if (pass == null || pass.equals("") ) {
				throw new BusinessException("La contraseña no puede ser vacia.");
			}
			
			users = usersDao.getUserbyNick(user);
			if(users == null) {
				throw new BusinessException("El usuario no esta registrado.");
			}
			
			if(!users.getPass().equals(pass)) {
				throw new BusinessException("La constraseña proporcionada es inválida.");
			}
			
			if (users.getStatus() == 2) {
				throw new BusinessException("El usuario esta bloqueado.");
			}
			
			UsuarioSesionVO usuarioSesion = new UsuarioSesionVO( 
					users.getIdUser().toString(),
					users.getClient().getIdClient().toString(),
					users.getNick(), String.valueOf(users.getStatus()), 
					users.getEmail(), users.getName(), 
					users.getProfile().getIdProfile().toString());
			
			log.info(":::...[S] end validateLogin OK");
			return usuarioSesion;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end validateLogin : " + e.getMessage());
    		throw new BusinessException(e.getMessage());
    	} catch(BusinessException e) {
    		log.error(":::...[S] end validateLogin : " + e.getMessage());
    		throw new BusinessException(e.getMessage());
    	} catch(Exception e) {
    		log.error(":::...[S] end validateLogin : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
	}
	
	
    public List<ProductTableVo> getProductsToCatalog() 
    		throws BusinessException {
    	log.info(":::...[S] begin getProductsToCatalog OK");
    	
	    List<ProductTableVo> prodTableList = new ArrayList<ProductTableVo>();
	    try {
	    	List<Product> prodList = productDao.selectActiveProducts();
	    	prodTableList = ProductToProductTableTransformer.transform(prodList);
	    	
	    	log.info(":::...[S] end getProductsToCatalog OK");
	    	return prodTableList;
    	} catch (DatabaseException e) {
    		log.error(":::...[S] end getProductsToCatalog : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS);
    	} catch(Exception e) {
    		log.error(":::...[S] end getProductsToCatalog : " + e.getMessage());
    		throw new BusinessException(ConstantsCExpress.ERROR_BUSINESS_GENERAL);
    	}
    }
	

	/**
	 * @return the usersDao
	 */
	public IUsersDao getUsersDao() {
		return usersDao;
	}

	/**
	 * @param usersDao the usersDao to set
	 */
	public void setUsersDao(IUsersDao usersDao) {
		this.usersDao = usersDao;
	}


	/**
	 * @return the productDao
	 */
	public IProductDao getProductDao() {
		return productDao;
	}


	/**
	 * @param productDao the productDao to set
	 */
	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}
    

}
