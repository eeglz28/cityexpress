package mx.com.cityexpress.modules.basics.presentation;

import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.presentation.PageCode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MenuPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	public String doSalirAction() {
		log.info("Saliendo de la aplicacion");
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_ID);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO_2);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_VO);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_USERS_LOGIN);

		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_PRODUCTS);
		
		
		
		return ConstantsCExpress.GO_MENU_TO_LOGIN;
	}
	
}
