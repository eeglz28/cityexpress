package mx.com.cityexpress.modules.basics.presentation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.presentation.PageCode;

public class NosessionPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	public String doLinkAction() {
		log.info("Session Expired, return to login page.");
		
		return ConstantsCExpress.GO_NOSESSION_TO_LOGIN;
	}

}
