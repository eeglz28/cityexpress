package mx.com.cityexpress.modules.basics.presentation;

import javax.faces.context.FacesContext;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.presentation.PageCode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FailPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	private String error;
	
	public String doAceptarAction() {
		log.info("Regresa al menu principal");
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_ID);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO_2);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_VO);
		
		
		return ConstantsCExpress.GO_FAIL_TO_MENU;
	}

	/**
	 * @return the error
	 */
	public String getError() {
		String exc = (String) FacesContext.getCurrentInstance().
				getExternalContext().getRequestMap().get(
						ConstantsCExpress.REQUEST_EXCEPTION);
		error = exc;
		return error;
	}

	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}

}
