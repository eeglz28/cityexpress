package mx.com.cityexpress.modules.basics.presentation;

import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import mx.com.cityexpress.general.common.ConstantsCExpress;
import mx.com.cityexpress.general.presentation.PageCode;

public class SuccessPage extends PageCode {
	
	private Log log = LogFactory.getLog(getClass());
	
	public String doAceptarAction() {
		log.info("Regresa al menu principal");
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_ID);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_TBL_VO_2);
		
		FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().remove(
						ConstantsCExpress.SESSION_VO);
		
		
		return ConstantsCExpress.GO_SUCCESS_TO_MENU;
	}

}
