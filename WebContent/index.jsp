<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
		<LINK href="resources/css/cityExpress-login.css" rel="stylesheet" type="text/css">
		<title>Inicio de Aplicaci&oacute;n Web</title>
      
      
      <script language="JavaScript">
          function redirect() {
        	  setTimeout("location.href='faces/production/login/login.jsp'", 2000);
              //var destination = "faces/production/login/login.jsp";
              //var w = screen.availWidth;
              //var h = screen.availHeight;
              //var popW = w, popH = h;
              
              //var leftPos = (w-popW)/2, topPos = (h-popH)/2;
              //window.open(destination, 'mainpage','width=' + popW + ',height=' + popH + ',top=' + topPos + ',left=' + leftPos + ',status=1,scrollbars=1');
          }
      </script>
      
      
   </head>
   
   
	<body onload="redirect()">
	
		<p class="labelDark2">La aplicación esta cargando...<br></p>
		
		<br>
		
		<table width="400" cellpadding="0" cellspacing="0" border="0" align="left">
			<tr>
				<td>
					<img alt="publione" src="resources/img/publione_logo_vertical.png" height="25%" width="25%">
				</td>
			</tr>
			<tr>
				<td class="labelDark12">
					<b>Impulsora Publione Tecnolog&iacute;a e Informaci&oacute;n</b>
				</td>
			</tr>
			<tr>
				<td class="label10">
					lázaro cárdenas # 05 Int. 102 sn lucas tepetlacalco
				</td>
			</tr>
			<tr>
				<td class="label10">
					tlalnepantla, estado de méxico C.P. 54055
				</td>
			</tr>
			<tr>
				<td class="labelDark12">
                 Tel. 01 (55) 5236 4002
				</td>
			</tr>
			
		</table>
		
	</body>

</html>
