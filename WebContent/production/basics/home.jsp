<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<title>Success</title>
	</head>
	
	 <f:view>
	
		<body>
		
			<h:form id="homeForm">
			
				
			
				<table cellpadding="0" cellspacing="0" border="0" width="210" align="center">
					<tr height="20"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="left">Bienvenidos al Sistema de Administración de Pedidos City Express</td>
					</tr>
					<tr height="20"><td></td></tr>
				</table>
			
				<table cellpadding="0" cellspacing="0" border="0" width="400" height="90" align="center">
					<tr bgcolor="#FFFFFF" height="90">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="400" height="90" class="tableDefault1">
								<tr height="20">
									<td colspan="5"></td>
								</tr>
								<tr height="50">
									<td width="20"></td>
									<td width="50">
										<h:graphicImage alt="ok" height="48" width="48" url="../../resources/img/ok.jpg" ></h:graphicImage>
									</td>
									<td width="20"></td>
									<td width="290" class="labelTitle2">El proceso se realizó exitosamente.</td>
									<td width="20"></td>
								</tr>
								<tr height="20">
									<td colspan="5"></td>
								</tr>            
							</table>
						</td>
						
					</tr>
					
					<tr><td height="25" align="center"></td></tr>
					
					<tr bgcolor="#FFFFFF">
						<td align="center">
							<h:commandButton type="submit" action="#{success.doAceptarAction}"
								id="aceptar" value="OK" styleClass="botonGris">
							</h:commandButton>
						</td>
					</tr>
					
				</table>
				
				
				
			
				
			</h:form>
	
		</body>
	
	</f:view>
</html>