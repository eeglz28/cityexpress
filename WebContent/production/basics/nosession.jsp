<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-basics.css" rel="stylesheet" type="text/css">
		<title>No Sesión</title>
	</head>
	
	 <f:view>
	
		<body>
		
			<h:form id="nosessionForm">
			
				<table cellpadding="0" cellspacing="0" border="0" width="210" align="center">
					<tr height="20"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Sesión Inactiva</td>
					</tr>
					<tr height="20"><td></td></tr>
				</table>
			
				<table cellpadding="0" cellspacing="0" border="0" width="400" height="90" align="center">
					<tr bgcolor="#FFFFFF" height="90">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" width="400" height="90" class="tableBasics">
								<tr height="20">
									<td colspan="5"></td>
								</tr>
								<tr height="50">
									<td width="20"></td>
									<td width="50">
										<h:graphicImage alt="warning" height="30" width="30" url="../../resources/img/warning.jpg" ></h:graphicImage>
									</td>
									<td width="20"></td>
									<td width="290" class="labelTitle2">Sin sesión</td>
									<td width="20"></td>
								</tr>
								<tr height="10">
									<td colspan="5"></td>
								</tr>    
								
								
								<tr>
									<td width="20"></td>
									<td width="360" colspan="3" align="left" class="labelTitle2">
										<h:outputText id="otext1" styleClass="labelTitle2" value="Inactividad en la aplicación."></h:outputText>:
									</td>
									<td width="20"></td>
								</tr>
								
								<tr>
									<td width="20"></td>
									<td width="360" colspan="3" align="left">
										<h:outputText id="otext2" styleClass="labelTitle2" value="La sesión ha expirado, por favor vuelva a firmarse."></h:outputText>
									</td>
									<td width="20"></td>
								</tr>
								
								<tr height="30">
									<td colspan="5"></td>
								</tr>    
								        
							</table>
						</td>
					</tr>
					
					<tr height="1">
						<td></td>
					</tr>		
					
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" width="210" align="center">
					<tr height="50"><td></td></tr>
					<tr>
						<td class="labelTitle2" align="center">
								<h:commandLink target="_parent" id="login" styleClass="linkDefault" value="Página de Inicio" action="#{noSession.doLinkAction}"></h:commandLink>
						</td>
					</tr>
					<tr height="20"><td></td></tr>
				</table>
			
				
			</h:form>
	
		</body>
	
	</f:view>
</html>