<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
    <head>
	    <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <META name="GENERATOR" content="IBM WebSphere Studio">
	    <meta name="description" content="Business">
	    <meta name="keywords" content="Business">
	    <meta name="robots" content="index,follow">
	    <link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
	    <META http-equiv="Content-Style-Type" content="text/css">
	    <LINK href="../../resources/css/cityexpress-menu.css" rel="stylesheet" type="text/css">


		<%
		UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
		if(usuarioSesion == null) {
			response.sendRedirect("../../index.jsp");
		}
		%>
		
		
		<script type="text/javascript">
			function limpiar() {
				
			}
		</script>
	    <title></title>
		
		
		
	</head>


    
	<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
		<f:view>
            
                <h:form id="headerForm">
                	
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

		                <tr>
		                    <td bgcolor="white" valign="top">
		                

                                <!-- Tabla que contiene encabezado, menu y frame -->
			                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
			                    
									<tr height="4">
			                        	<td width="100%">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="4">
			                                    <tr>
			                                        <td>
			                                            <img src="../../resources/img/subm.jpg" width="100%" height="4"/>
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>

			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr bgcolor="#E3EEFB">
													<td width="15"></td>
													<td class="labelG11N1" align="left" width="48%">
														Bienvenido Usuario, <b><%=usuarioSesion.getName()%></b> &nbsp; &nbsp;  |  &nbsp; &nbsp; 
														<h:commandLink action="#{menu.doSalirAction}" id="salir" styleClass="labelG11N1" target="_parent">
															<h:outputText styleClass="labelG11N1" id="ot1" value="Salir"></h:outputText>
														</h:commandLink>
														 &nbsp; &nbsp; | &nbsp; &nbsp;  
														<a href="#" title="Cambiar Contrase�a" target="content">Cambiar Contrase�a</a>
													</td>
													<td class="labelG11N1" align="right" width="48%">
														Ayuda &nbsp; &nbsp;  | &nbsp; &nbsp;  Acerca
													</td>
													<td width="15"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        
			                        <tr height="1" bgcolor="#BE342D">
			                            <td width="100%"></td>
			                        </tr>
			                        
			                        
			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr background="../../resources/img/header_off_1.jpg">
													<td width="15"></td>
													<td class="labelG11N1" align="left" width="60">
														<img src="../../resources/img/publione_logo_vertical.png" width="120" height="60"/>
													</td>
													<td class="labelTitleHeader" align="left">
														Sistema de Gesti�n de Pedidos - CITY EXPRESS
													</td>
													<td></td>
													<td class="labelG11N1" align="right" valign="bottom">
														<table border="0" cellpadding="0" cellspacing="0" width="80" align="right">
															<tr>
																<td>
																	<a href="http://www.gmail.com" target="_blank">
																		<img src="../../resources/img/gmail_icon.png" width="20" height="20" border="0"/>  
																	</a> 
																</td>
																<td>
																	<a href="http://www.facebook.com" target="_blank">
																		<img src="../../resources/img/facebook_icon.gif" width="20" height="20" border="0"/>  
																	</a> 
																</td>
															</tr>
														</table> 
													</td>
													<td width="15"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        <tr>
			                            <td width="100%">
			                               <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr background="../../resources/img/subm.jpg">
													<td class="labelB10N" align="right">
														 Impulsora Publione Tecnolog&iacute;a e Informaci&oacute;n, Tel. 01 (55) 5236 4002.
													</td>
													<td width="15"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
    
			                    </table>
			                    <!-- Fin tabla de encabezado, menu y frame -->
			                    
			                         
			                </td>
	                    </tr>
	                </table>
	                
	                
	                    
                </h:form>

            
        
    	</f:view>
    </body>
</html>