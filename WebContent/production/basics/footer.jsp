<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.mapping.pojo.Users"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
    <head>
	    <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <META name="GENERATOR" content="IBM WebSphere Studio">
	    <meta name="description" content="Business">
	    <meta name="keywords" content="Business">
	    <meta name="robots" content="index,follow">
	    <link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
	    <META http-equiv="Content-Style-Type" content="text/css">
	    <LINK href="../../resources/css/cityexpress-menu.css" rel="stylesheet" type="text/css">

	    <title>CityExpress</title>
		
	</head>


    
	<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
		<f:view>
            
                <h:form id="footerForm">
                	
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
                    
                    	<tr height="1" bgcolor="#BE342D">
                            <td width="100%"></td>
                        </tr>

		                <tr>
		                    <td bgcolor="white" valign="top">
		                

                                <!-- Tabla que contiene encabezado, menu y frame -->
			                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
			                        
			                        <tr height="15">
			                            <td width="100%" align="right">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="15">
			                                    <tr>
			                                        <td>
			                                            <img src="../../resources/img/subm.jpg" width="100%" height="15"/>
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                        
			                        <tr height="15">
			                            <td width="100%" align="center">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="15">
			                                    <tr>
			                                        <td class="fechaStyle" align="center">
			                            				ESTE FORMATO ESTA DESARROLLADO POR CONSULTORIA DE SOLUCIONES INFORMATICAS S.A. PARA IMPULSORA PUBLIONE TECNOLOG&Iacute;A E INFORMACI&Oacute;N PARA USO EXCLUSIVO DE HOTELES CITY EXPRESS Y/O SUS MARCAS RELACIONADAS, NOS RESERVAMOS LOS DERECHOS DE USO Y SU REPRODUCCIÓN TOTAL O PARCIAL, QUEDA PROHIBIDO SUS USO PARA OTROS FINES SIN PERMISO EXPRESO Y POR ESCRITO DEL TITULAR, DUDAS AL TELEFONO + 52 (55) 5236 4002.
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                        
			                    </table>
			                         
			                </td>
	                    </tr>
	                </table>
	                
	                    
                </h:form>
        
    	</f:view>
    </body>
</html>