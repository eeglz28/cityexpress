<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.mapping.pojo.Users"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
    <head>
	    <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <META name="GENERATOR" content="IBM WebSphere Studio">
	    <meta name="description" content="Business">
	    <meta name="keywords" content="Business">
	    <meta name="robots" content="index,follow">
	    <link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
	    <META http-equiv="Content-Style-Type" content="text/css">
	    <LINK href="../../resources/css/cityexpress-menu.css" rel="stylesheet" type="text/css">


		<%
		
		Users user = (Users) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
		
		if(user == null) {
			response.sendRedirect("../../index.jsp");
		}
		%>
		
		
		<script type="text/javascript">
			function limpiar() {
				
			}
		</script>
	    <title>CityExpress</title>
		
		
		
	</head>


    <f:view>
        <body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
            
                <h:form id="menuInicioForm">
                	
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

		                <tr>
		                    <td bgcolor="white" valign="top">
		                

                                <!-- Tabla que contiene encabezado, menu y frame -->
			                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
			                    
									<tr height="4">
			                        	<td width="100%">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="4">
			                                    <tr>
			                                        <td>
			                                            <img src="../../resources/img/subm.jpg" width="100%" height="4"/>
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>

			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr bgcolor="#E3EEFB">
													<td width="15"></td>
													<td class="labelG11N1" align="left" width="48%">
														Bienvenido Usuario, <b><%=user.getNick()%></b> &nbsp; &nbsp;  |  &nbsp; &nbsp; 
														<a href="../login/login.jsp" title="salir" onclick="limpiar();">Salir</a>  &nbsp; &nbsp; | &nbsp; &nbsp;  
														<a href="#" title="Change Password" target="_parent">Change Password</a>
													</td>
													<td class="labelG11N1" align="right" width="48%">
														Ayuda &nbsp; &nbsp;  | &nbsp; &nbsp;  Acerca
													</td>
													<td width="15"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr bgcolor="#FFFFFF">
													<td width="15"></td>
													<td class="labelG11N1" align="left" width="48%">
														<img src="../../resources/img/publione_logo_horizontal_pedidos.png" width="221" height="55"/>
													</td>
													<td class="labelG11N1" align="right" width="48%" valign="bottom">
														<table border="0" cellpadding="0" cellspacing="0" width="300" align="right">
														<tr>
															<td>
																<a href="http://mail.yahoo.com" target="_blank">
																	<img src="../../resources/img/yahoo_icon.png" width="20" height="20" border="0" />  
																</a>
															</td>
															<td>
																<a href="http://www.gmail.com" target="_blank">
																	<img src="../../resources/img/gmail_icon.png" width="20" height="20" border="0"/>  
																</a> 
															</td>
															<td>
																<a href="http://www.facebook.com" target="_blank">
																	<img src="../../resources/img/facebook_icon.gif" width="20" height="20" border="0"/>  
																</a> 
															</td>
															<td>
																<a href="http://www.twitter.com" target="_blank">
																	<img src="../../resources/img/twitter-icono.png" width="20" height="20" border="0"/> 
																</a> 
															</td>
															<td>
																<a href="http://www.hotmail.com" target="_blank">
																	<img src="../../resources/img/hotmail-icono.gif" width="20" height="20" border="0"/> 
																</a> 
															</td>
															<td>
																<a href="http://www.wordpress.com" target="_blank">
																	<img src="../../resources/img/wordpress_icono.jpg" width="20" height="20" border="0"/> 
																</a> 
															</td>
															<td>
																<a href="http://www.wikipedia.com" target="_blank">
																	<img src="../../resources/img/wikipedia-icono.png" width="20" height="20" border="0"/>
																</a> 
															</td>
														</tr>
														</table> 
													</td>
													<td width="15"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
												<tr>
													<td width="100%" class="labelB10N" align="left" background="../../resources/img/subm.jpg" colspan="4">
														 Impulsora Publione Tecnolog&iacute;a e Informaci&oacute;n , Tel. 01 (55) 5236 4002.
													</td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        
			                        <tr>
			                            <td width="100%">
			                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
			                                	<tr bgcolor="#164364" height="5">
													<td width="15" colspan="4"></td>
												</tr>
												
												<%
												if(user.getNick().equals("AdminCE")) {
												%>
												<tr bgcolor="#164364">
													<td width="15"></td>
													<td class="labelOpcion" align="left" width="48%">
														<a href="../admon/clientes.jsp" title="clientes" target="content">Administrar Clientes</a>  &nbsp; &nbsp; | &nbsp; &nbsp;  
														<a href="../admon/productos.jsp" title="productos" target="content">Administrar Productos</a>  &nbsp; &nbsp; | &nbsp; &nbsp;
														<a href="../consultas/busquedaPed.jsp" title="ConsultaPedidos" target="content">Consultar Pedidos</a>
													</td>
													<td class="labelOpcion" align="right" width="48%"></td>
													<td width="15"></td>
												</tr>
												<%
												} else {
												%>
												<tr bgcolor="#164364">
													<td width="15"></td>
													<td class="labelOpcion" align="left" width="48%">
														<a href="../order/order.jsp" title="NuevoPedido" target="content">Nuevo Pedido</a>  &nbsp; &nbsp; | &nbsp; &nbsp;  
														<a href="../consultas/pedidosUsuario.jsp" title="ConsultaPedido" target="content">Consultar Pedidos</a>
														
													</td>
													<td class="labelOpcion" align="right" width="48%"></td>
													<td width="15"></td>
												</tr>
												<%	
												}
												%>
	
												<tr bgcolor="#164364" height="5">
													<td width="15" colspan="4"></td>
												</tr>
											</table>
			                            </td>
			                        </tr>
			                        
			                        
			                        
			                        
			                        
			           
			                        
			                        <tr height="3" bgcolor="#BE342D">
			                            <td width="100%"></td>
			                        </tr>
			                        <tr height="5">
			                            <td width="100%" align="right">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="5">
			                                    <tr>
			                                        <td align="right">
			                                            <font></font>
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                       
			                        <tr>
			                            <td width="100%">
			                                <IFRAME src="../basics/blank.jsp" name="content" frameborder="0" align="top" width="100%" height="720" scrolling="auto"></IFRAME>
			                            </td>
			                        </tr>
			                        
			                        <tr height="15">
			                            <td width="100%" align="right">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="15">
			                                    <tr>
			                                        <td>
			                                            <img src="../../resources/img/subm.jpg" width="100%" height="15"/>
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                        
			                        <tr height="15">
			                            <td width="100%" align="center">
			                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="15">
			                                    <tr>
			                                        <td class="fechaStyle" align="center">
			                            				ESTE FORMATO ESTA DESARROLLADO POR CONSULTORIA EN SOLUCIONES INFORMATICAS S.A. PARA IMPULSORA PUBLIONE TECNOLOG&Iacute;A E INFORMACI&Oacute;N PARA USO EXCLUSIVO DE HOTELES CITY EXPRESS Y/O SUS MARCAS RELACIONADAS, NOS RESERVAMOS LOS DERECHOS DE USO Y SU REPRODUCCIÓN TOTAL O PARCIAL, QUEDA PROHIBIDO SUS USO PARA OTROS FINES SIN PERMISO EXPRESO Y POR ESCRITO DEL TITULAR, DUDAS AL TELEFONO + 52 (55) 5236 4002.
			                                        </td>
			                                    </tr>
			                                </table>
			                            </td>
			                        </tr>
			                        
			                    </table>
			                    <!-- Fin tabla de encabezado, menu y frame -->
			                    
			                         
			                </td>
			                
			               
			                
	                    </tr>
	                    
	                    
	                   
	                                       
	                </table>
	                
	                
	                    
                </h:form>

            
        </body>
    </f:view>
</html>

