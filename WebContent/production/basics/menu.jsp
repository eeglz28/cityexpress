<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
    <head>
	    <META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <META name="GENERATOR" content="IBM WebSphere Studio">
	    <meta name="description" content="Business">
	    <meta name="keywords" content="Business">
	    <meta name="robots" content="index,follow">
	    <META http-equiv="Content-Style-Type" content="text/css">
	    <LINK href="../../resources/css/cityexpress-menu.css" rel="stylesheet" type="text/css">
	    
		<%
		UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
		if(usuarioSesion == null) {
			response.sendRedirect("../../index.jsp");
		}
		%>
		
		
		<script type="text/javascript">
			function limpiar() {
				
			}
		</script>


		
	    <title></title>
		<script type="text/javascript" src="../../resources/js/jquery-menu.js"></script>
		<script type="text/javascript"> 
			$(document).ready(function(){
			
				$("ul.subnav").parent().append("<span></span>"); //Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav
				
				$("ul.topnav li a").mouseover(function() { //When trigger is clicked...
					
					//Following events are applied to the subnav itself (moving subnav up and down)
					$(this).parent().find("ul.subnav").slideDown('fast').show(); //Drop down the subnav on click
			
					$(this).parent().hover(function() {
					}, function(){	
						$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
					});
			
					//Following events are applied to the trigger (Hover events for the trigger)
					}).hover(function() { 
						$(this).addClass("subhover"); //On hover over, add class "subhover"
					}, function(){	//On Hover Out
						$(this).removeClass("subhover"); //On hover out, remove class "subhover"
				});
			
			});
		</script>
		
		
	</head>


    <f:view>
        <body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
            
                <h:form id="menuInicioForm">
                	
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">

		                <tr>
		                    <td bgcolor="white" valign="top">
		                

                                <!-- Tabla que contiene encabezado, menu y frame -->
			                    <table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
			                    
									

			                        
			                        <tr height="6">
			                            <td width="100%" align="left">
				                            <div class="container">
												<div>		
											        <ul class="topnav">
											        
											            <li><a href="#" title="Home">Home</a></li>
														<%
														if(usuarioSesion.getPerfil().equals("1")) {
														%>
											            	            
	            							            <li>
											            	<a href="#">Administración</a>
											            	<ul class="subnav">
											                    <li><a href="../admon/clientes.jsp" title="Administrar Clientes">Administrar Clientes</a></li>
											                    <li><a href="../admon/productos.jsp" title="Administrar Productos">Administrar Productos</a></li>
											                    <li><a href="../admon/envioCorreo.jsp" title="Correo Masivo">Correo Masivo</a></li>
											                </ul>
											            </li>
											            
											            <li>
											            	<a href="#">Operación</a>
											            	<ul class="subnav">
											                    <li><a href="../consultas/busquedaPed.jsp" title="Pedidos">Consulta de Pedidos</a></li>
											                    <li><a href="../operation/pedidosEnProceso.jsp" title="HS">Hoja de Surtido</a></li>
											                </ul>
											            </li>
											            
											            <li>
											                <a href="#">Reportes</a>
											                <ul class="subnav">
																<li><a href="../reportes/reporteProductosBus.jsp" title="reporteSutir">Reporte por Productos</a></li>
											                </ul>
											            </li>
											            
											            <%
														} else if(usuarioSesion.getPerfil().equals("2")) {
														%>												
											            
											            <li>
											            	<a href="#">Operación</a>
											            	<ul class="subnav">
											                    <li><a href="../consultas/busquedaPed.jsp" title="Pedidos">Consulta de Pedidos</a></li>
											                    <li><a href="../operation/pedidosEnProceso.jsp" title="HS">Hoja de Surtido</a></li>
											                </ul>
											            </li>
											            
											            <li>
											                <a href="#">Reportes</a>
											                <ul class="subnav">
																<li><a href="../reportes/reporteProductosBus.jsp" title="reporteSutir">Reporte por Productos</a></li>
											                </ul>
											            </li>
														
														<%
														} else {
														%>
														
														<li>
											            	<a href="#">Pedidos</a>
											            	<ul class="subnav">
											                    <li><a href="../order/order.jsp" title="Pedidos">Alta de Pedidos</a></li>
											                </ul>
											            </li>
											            
											            <li>
											            	<a href="#">Consultas</a>
											            	<ul class="subnav">
											                    <li><a href="../consultas/pedidosUsuario.jsp" title="Pedidos">Consulta de Pedidos</a></li>
											                </ul>
											            </li>
														
														<%	
														}
														%>
											            
											        </ul>
											    </div>
											</div>
			                            </td>
			                        </tr> 
			                        
			                        
			                        
			                        <tr height="3" bgcolor="#BE342D">
			                            <td width="100%"></td>
			                        </tr>
			                        
			                        
			                    </table>
			                    <!-- Fin tabla de encabezado, menu y frame -->
			                    
			                         
			                </td>
			                
			               
			                
	                    </tr>
	                    
	                    
	                   
	                                       
	                </table>
	                
	                
	                    
                </h:form>

            
        </body>
    </f:view>
</html>