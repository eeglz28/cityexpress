<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-pedus.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Consulta de pedidos</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
			
			<h:form id="pedusuForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{pedusu.initPage}"></h:outputText>
			
				
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Mis Pedidos</td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				<table border="0" cellpadding="0" cellspacing="0" width="710" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="710" rowIndexVar="row"
					                   columnClasses="columnDTPedus1,columnDTPedus2,columnDTPedus3,columnDTPedus4,columnDTPedus5,columnDTPedus6" 
					                   headerClass="headerDataTable1" 
					                   footerClass="footerDataTable1" 
					                   rowClasses="rowDTPedus1,rowDTPedus2" 
					                   styleClass="dataTable" 
					                   id="dataTablePed" 
					                   var="varlistPedidosTableVO"
					                   binding="#{pedusu.dataTablePedidos}" 
					                   value="#{pedusu.ordersList}"
					                   preserveDataModel="false"
					                   rendered="true"
					                   rows="#{pedusu.rowCount}" >
					                  
					                   <f:facet name="header">
					                           <h:outputText styleClass="header1" id="otexth" value="Pedidos"></h:outputText> 
					                   </f:facet>
					                   
					                   <t:column id="column1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Pedido" id="otext1"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext2" value="#{varlistPedidosTableVO.numOrder}"></h:outputText>
					                       <f:attribute value="10" name="width" />
					                   </t:column>
					                    
					                   <t:column id="column2">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Fecha" id="otext3"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext4" value="#{varlistPedidosTableVO.creationDate}"></h:outputText>
					                   </t:column>
					                   
					                   <t:column id="column3"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Total" id="otext5"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext6" value="#{varlistPedidosTableVO.total}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column4"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Estatus" id="otext7"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext8" value="#{varlistPedidosTableVO.descEstatus}"/>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
									   <t:column id="column5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Guia" id="otext9"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext10" value="#{varlistPedidosTableVO.guia}"/>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column6"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Detalle" id="otext11"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{pedusu.doDetalleAction}" id="detalle" styleClass="linkNormal">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/detalle.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                   </t:column>

					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="otext12" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					               
					               <h:panelGrid  columns="1" styleClass="dataTable" columnClasses="scrollerStyle" >
					                    <t:dataScroller id="scroll" 
					                    	for="dataTablePed" 
					                    	fastStep="10"
					                        pageCountVar="pageCount"
					                        pageIndexVar="pageIndex"
					                        styleClass="scrollerStyle"
					                        paginator="true"
					                        paginatorMaxPages="9" 
					                        paginatorTableClass="scrollerStyle"  
					                        paginatorActiveColumnStyle="font-weight:bold;"
					                        immediate="true" 
											actionListener="#{pedusu.scrollerAction}" />
					                    
					               </h:panelGrid>
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>
							
								
				<br>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
	
					<tr height="15">
						<td colspan="6"></td>
					</tr>
					<tr height="20">
						<td colspan="6" align="center">
							 <h:commandButton type="submit" action="#{pedusu.doRegresarAction}"
                                   		id="Regresar" value="Regresar" styleClass="botonGris">
                                   </h:commandButton>
						</td>
                    </tr>
					<tr height="15">
						<td colspan="6"></td>
					</tr>
				</table>
				
				
			</h:form>

		</body>
	 
	</f:view>
	
</html>