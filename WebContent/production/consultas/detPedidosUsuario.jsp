<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-consultas.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Detalle del Pedido</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="usudetpedForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{usudetped.initPage}"></h:outputText>
			

				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Detalle del Pedido </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
	
				<table border="0" cellpadding="0" cellspacing="0" width="960" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="800" rowIndexVar="row"
					                   columnClasses="columnDataTableDetPed1,columnDataTableDetPed2,columnDataTableDetPed3,columnDataTableDetPed4,columnDataTableDetPed5,columnDataTableDetPed6,columnDataTableDetPed7" 
					                   headerClass="headerDataTable1" footerClass="footerDataTable1" 
					                   rowClasses="rowClass1,rowClass2" styleClass="dataTable" id="dataTableDetPed" var="varlistDetPedidoTableVO"
					                   binding="#{usudetped.dataTableDetPedido}" value="#{usudetped.detPedidoList}" rendered="true"
					                   rows="50" >
					                  
					                   <f:facet name="header">
					                           <h:outputText styleClass="header1" id="otext1" value="Detalle del Pedido"></h:outputText> 
					                   </f:facet>
					                   
					                   <t:column id="column1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="ID" id="otext2"></h:outputText>
					                       </f:facet>
					                       		<h:outputText styleClass="outputTextDt" id="otext3" value="#{varlistDetPedidoTableVO.idProduct}" />
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column2"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Producto" id="otext4"></h:outputText>
					                       </f:facet>
					                       		<h:outputText styleClass="outputTextDt" id="otext5" value="#{varlistDetPedidoTableVO.producto}" />
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                  	
					                   <t:column id="column3"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Cantidad" id="otext6"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext7" value="#{varlistDetPedidoTableVO.cantidad}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="10" name="width" />
					                   </t:column>
					                    
					                   <t:column id="column4">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Unidad" id="otext8"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext9" value="#{varlistDetPedidoTableVO.unidad}"></h:outputText>
					                   </t:column>
					                   
					                   <t:column id="column5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Precio" id="otext10"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext11" value="#{varlistDetPedidoTableVO.precio}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column6"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Subtotal" id="otext12"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext13" value="#{varlistDetPedidoTableVO.subtotal}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column7"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Folio" id="otext14"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext15" value="#{varlistDetPedidoTableVO.folio}"></h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>

					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="otext16" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>
				
				
				
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
	
					<tr height="15">
						<td colspan="6"></td>
					</tr>
					<tr height="20">
						<td colspan="6" align="center">
							 <h:commandButton type="submit" action="#{usudetped.doRegresarAction}"
                                   		id="regresar" value="Regresar" styleClass="botonGris">
                             </h:commandButton>
						</td>
                    </tr>
					<tr height="15">
						<td colspan="6"></td>
					</tr>
				</table>

			
			</h:form>

		</body>
	 
	</f:view>
	
</html>