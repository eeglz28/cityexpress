<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-consultas.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Consulta de Pedidos</title>
		
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="respedForm" >
			
				
			

				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Resultado de la Consulta </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
	
				<table border="0" cellpadding="0" cellspacing="0" width="1020" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="1000" rowIndexVar="row"
					                   columnClasses="columnDataTable1Z,columnDataTable2Z,columnDataTable25Z,columnDataTable3Z,columnDataTable4Z,columnDataTable5Z,columnDataTable6Z,columnDataTable7Z,columnDataTable8Z" 
					                   headerClass="headerDataTable1" footerClass="footerDataTable1" 
					                   rowClasses="rowClass1,rowClass2" styleClass="dataTable" id="dataTablePed" var="varlistPedidosTableVO"
					                   binding="#{resped.dataTablePedidos}" value="#{resped.ordersList}" rendered="true" preserveDataModel="false" first="0" 
					                   rows="#{resped.rowCount}" >
					                  
					                   <f:facet name="header">
					                           <h:outputText styleClass="header1" id="otext" value="Pedidos"></h:outputText> 
					                   </f:facet>
					                   
					                   <t:column id="column1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Cliente" id="otext3"></h:outputText>
					                       </f:facet>
					                       <h:commandLink id="clink1" style="text-decoration: none;" action="#{resped.doLigaClienteAction}">
					                       		<h:outputText styleClass="outputTextDt" id="otext4" value="#{varlistPedidosTableVO.clientName}" />
					                       </h:commandLink>
					                       <f:attribute value="270" name="width" />
					                   </t:column>
					                   
					                  	
					                   <t:column id="column2"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Pedido" id="otext5"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext6" value="#{varlistPedidosTableVO.numOrder}" title="#{varlistPedidosTableVO.nombreUsuario}"></h:outputText>
					                       <f:attribute value="70" name="width" />
					                   </t:column>
					                   
					                   
					                   <t:column id="column2_5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Envío" id="otextFEL"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otextFEV" value="#{varlistPedidosTableVO.entrega}"></h:outputText>
					                       <f:attribute value="80" name="width" />
					                   </t:column>
					                   
					                   
					                   <t:column id="column3"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Total" id="otext9"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext10" value="#{varlistPedidosTableVO.total}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="70" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column4"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Estatus" id="otext_anex5_1"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext_anex_5_2" value="#{varlistPedidosTableVO.descEstatus}"/>
					                       <f:attribute value="90" name="width" />
					                   </t:column>
					                   
					               	   <t:column id="column5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Comentario" id="otext11"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext12" value="#{varlistPedidosTableVO.comments}"></h:outputText>
					                       <f:attribute value="300" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column6"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Actualizar" id="otext16"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{resped.doEntregarAction}" id="entregar" styleClass="linkNormal" rendered="#{varlistPedidosTableVO.estatusRendered}">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/entregar.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                       <f:attribute value="40" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column7"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Detalle" id="otext17"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{resped.doDetalleAction}" id="detalle" styleClass="linkNormal">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/detalle.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                       <f:attribute value="40" name="width" />
					                   </t:column>
					                   
										<t:column id="column8"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Enviar" id="otext18"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{resped.doSendAction}" id="send" styleClass="linkNormal" onclick="if (!confirm('Estas seguro de enviar el pedido por correo electrónico?')) return false">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/mail_ico.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                       <f:attribute value="40" name="width" />
										</t:column>

					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="otext13" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					               
					               <h:panelGrid  columns="1" styleClass="dataTable" columnClasses="scrollerStyle" >
					                    <t:dataScroller id="scroll_1" for="dataTablePed" fastStep="4"
					                        pageCountVar="pageCount"
					                        pageIndexVar="pageIndex"
					                        styleClass="scrollerStyle"
					                        paginator="true"
					                        paginatorMaxPages="9" 
					                        paginatorTableClass="scrollerStyle"  
					                        paginatorActiveColumnStyle="font-weight:bold;"
					                        immediate="true"
					                        actionListener="#{resped.scrollerAction}">
					
					                    </t:dataScroller>
					               </h:panelGrid>
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>
				
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="30"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">
							<h:outputText styleClass="labelTitle1" id="title2" value="Datos Personales del Cliente" rendered="#{resped.clientTblRender}"/> 
						</td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table border="0" cellpadding="0" cellspacing="0" width="960" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body2">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="600" rowIndexVar="row"
					                   columnClasses="columnDTCliente1,columnDTCliente2,columnDTCliente3,columnDTCliente4" 
					                   headerClass="headerDataTable1" footerClass="footerDataTable1" 
					                   rowClasses="rowClass2" styleClass="dataTable" id="dataTableCli" var="varlistClienteTableVO"
					                   binding="#{resped.dataTableCliente}" value="#{resped.clienteList}" rendered="#{resped.clientTblRender}"
					                   rows="5" >
					                   
					                   <t:column id="column2_1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Id" id="otext15"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext16" value="#{row + 1}"/>  
					                       
					                   </t:column>
					                   
					                   <t:column id="column2_2"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Cliente" id="otext17"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext18" value="#{varlistClienteTableVO.clientName}" />
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                  	
					                   <t:column id="column2_3"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Correo" id="otext19"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext20" value="#{varlistClienteTableVO.email}"></h:outputText>
					                       <f:attribute value="10" name="width" />
					                   </t:column>
					                    
					                   <t:column id="column2_4">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Telefono" id="otext21"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otext22" value="#{varlistClienteTableVO.telephone}"></h:outputText>
					                   </t:column>
					                   
					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="otext23" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>
	

	
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
	
					<tr height="15">
						<td colspan="6"></td>
					</tr>
					<tr height="20">
						<td colspan="6" align="center">
							 <h:commandButton type="submit" action="#{resped.doRegresarAction}"
                                   		id="Regresar" value="Regresar" styleClass="botonGris">
                                   </h:commandButton>
						</td>
                          </tr>
					<tr height="15">
						<td colspan="6"></td>
					</tr>
				</table>
				
				
			
			</h:form>

		</body>
	 
	</f:view>
	
</html>