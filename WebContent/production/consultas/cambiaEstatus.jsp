<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-consultas.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Cambia Estatus</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#F0F8FF" onload="onLoadForm();">
		
			<h:form id="camEstForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{cambiaEstatus.initPage}"></h:outputText>
				

					
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Actualización del Pedido </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr bgcolor="#FFFFFF">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" class="tableConsulta">
								<tr height="10">
									<td colspan="6"></td>
								</tr>


								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Cliente:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="boldLabel1" align="left">
	                            		<h:outputText id="otext1" value="#{cambiaEstatus.cliente}" />
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>							
							
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Pedido:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="boldLabel1" align="left">
										<h:outputText id="otext2" value="#{cambiaEstatus.pedido}" />
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Fecha:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="boldLabel1" align="left">
										<h:outputText id="otext3" value="#{cambiaEstatus.fecha}" />
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">ID Usuario:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="boldLabel1" align="left">
										<h:outputText id="otext4" value="#{cambiaEstatus.usuario}" />
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
	
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Estatus</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
	                            		<h:selectOneMenu binding="#{cambiaEstatus.estatusSelectOneMenu}" id="selectOneMenuEstatus" styleClass="input1" value="#{cambiaEstatus.estatus}">
											<f:selectItem itemValue="-1" itemLabel="Seleccione un Estatus" />
											<f:selectItem itemValue="1" itemLabel="En Proceso" />
											<f:selectItem itemValue="2" itemLabel="Entregado" />
											<f:selectItem itemValue="3" itemLabel="Cerrado" />
											<f:selectItem itemValue="4" itemLabel="Parcial" />
										</h:selectOneMenu>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Seleccione en que estatus se encuentra el pedido.
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                           	<tr height="10">
									<td width="10"></td>
	                            	<td class="labelError1" align="left" width="400" colspan="4">
	                            		<h:outputText id="otEsErr" value="#{cambiaEstatus.estatusErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                            
	                            
	                            
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">No. Guía:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
										<h:inputText immediate="true" size="15" maxlength="15" styleClass="input1" value="#{cambiaEstatus.guia}" binding="#{cambiaEstatus.guiaText}"/>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                           	<tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Introduzca el número de guía.
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left"></td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
	                            		<h:selectBooleanCheckbox immediate="true" styleClass="label1" binding="#{cambiaEstatus.informarCheck}"></h:selectBooleanCheckbox> <font class="label1">Informar al Usuario</font>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								
								<tr height="10">
									<td colspan="6"></td>
								</tr>
								
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Comentarios:</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
										<h:inputTextarea immediate="true" styleClass="input1" cols="20" rows="5" value="#{cambiaEstatus.comments}" binding="#{cambiaEstatus.commentsTextArea}"></h:inputTextarea>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                           	<tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Introduzca sus comentarios u observaciones del pedido.
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								
								<tr height="15">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td colspan="6">
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td></td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{cambiaEstatus.doAceptarAction}"
	                                     				id="aceptar" value="Aceptar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{cambiaEstatus.doRegresarAction}"
	                                     				id="regresar" value="Regresar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td></td>
											</tr>
										</table>
									</td>
	                            </tr>
								<tr height="15">
									<td colspan="6"></td>
								</tr>
								                      
							</table>
						</td>
						
					</tr>
				</table>
				
				
								<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
					<tr>
						<td align="center">
							<h:outputText id="otMsgErr" value="#{cambiaEstatus.message}" styleClass="labelError1"></h:outputText>
						</td>
					</tr>
				</table>
				

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>