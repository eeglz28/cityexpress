<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>



<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-consultas.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Consulta de Pedidos</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#F0F8FF" onload="onLoadForm();">
		
			<h:form id="conpedForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{conped.initPage}"></h:outputText>
			

					
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="200">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Consulta de Pedidos </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr bgcolor="#FFFFFF">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" class="tableConsulta">
								<tr height="10">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									<td colspan="5" class="labelTitle2">Introduce los parámetros de búsqueda </td>
								</tr>
								<tr height="10">
									<td colspan="6"></td>
								</tr>
							
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left" width="80">Cliente</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
										<h:selectOneMenu binding="#{conped.clienteSelectOneMenu}" id="selectOneMenuCliente" styleClass="input1">
											<f:selectItems binding="#{conped.clienteSelectItems}"
												id="combo1SelectItems" value="#{conped.selectItemCliente}" />
										</h:selectOneMenu>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Escoge un cliente que haya levantado el pedido
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
			                                    
			                                    
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Num. Pedido</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
										<h:inputText immediate="true" id="pedidoText" size="10" maxlength="10" styleClass="input1"/>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                           	<tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Escribe un número de pedido, ej. PE-1
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
	
	
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1">Fechas Entre</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
	                            		<t:inputCalendar id="initDate" renderPopupButtonAsImage="true" popupButtonImageUrl="../../resources/img/calendar.gif"
	                            				popupDateFormat="dd-MM-yyyy" renderAsPopup="true" popupTodayString="today" popupWeekString="week"
	                            				immediate="true" size="10" styleClass="input1" maxlength="10" /> &nbsp; y &nbsp;
	                            		
	                            		<t:inputCalendar id="endDate" renderPopupButtonAsImage="true" popupButtonImageUrl="../../resources/img/calendar.gif"
	                            				popupDateFormat="dd-MM-yyyy" renderAsPopup="true" popupTodayString="today" popupWeekString="week"
	                            				immediate="true" size="10" styleClass="input1" maxlength="10" /> &nbsp;
	                            		 
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                           	<tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Rango de fechas en las que se registro el pedido
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
	
	
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
	                            	<td class="label1" align="left">Estatus</td>
	                            	<td width="10"></td>
	                            	
	                            	<td width="10"></td>
	                            	<td class="label1" align="left">
	                            		<h:selectOneMenu binding="#{conped.estatusSelectOneMenu}" id="selectOneMenuEstatus" styleClass="input1">
											<f:selectItem itemValue="-1" itemLabel="Selecciona una Opción" />
											<f:selectItem itemValue="1" itemLabel="En Proceso" />
											<f:selectItem itemValue="2" itemLabel="Entregado" />
											<f:selectItem itemValue="3" itemLabel="Cerrado" />
											<f:selectItem itemValue="4" itemLabel="Parcial" />
										</h:selectOneMenu>
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
	                            	<td width="80"></td>
	                            	<td width="10"></td>
	                            	<td width="10"></td>
	                            	<td class="smallLabel1" align="left" width="300">
	                            		Estatus del pedido (En Proceso, Entregado)
	                            	</td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="6"></td>
								</tr>
								
								
								<tr height="15">
									<td colspan="6"></td>
								</tr>
								<tr height="20">
									<td colspan="6">
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td></td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{conped.doAceptarAction}"
	                                     				id="aceptar" value="Aceptar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{conped.doRegresarAction}"
	                                     				id="regresar" value="Regresar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td></td>
											</tr>
										</table>
									</td>
	                            </tr>
								<tr height="15">
									<td colspan="6"></td>
								</tr>
								                      
							</table>
						</td>
						
					</tr>
				</table>
				

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>