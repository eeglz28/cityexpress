<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-order.css" rel="stylesheet" type="text/css">
		<title>Detalle del Pedido</title>
		
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<script language="Javascript">
		
			<!--
						
			function confirma() {
				confirmar = confirm("¿Esta seguro de enviar el pedido?");
				if (confirmar) {
					return;
				} else {
					
				}
			}  

			function solamenteNumero(e) {
				if (window.event) {
					keynum = e.keyCode;
				} else {
					keynum = e.which;
				}
					
				if((keynum>45 && keynum<58) || keynum == 8){
					return true;
				} else {
					return false;
				}
			}

			function validar(e){
				tecla_codigo = (document.all) ? e.keyCode : e.which;
				alert(tecla_codigo);
				if (tecla_codigo==8) return true;
				patron =/[0-9.]/;
				tecla_valor = String.fromCharCode(tecla_codigo);
				return patron.test(tecla_valor);
			}

			
			-->
		</script>
		
	</head>
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="onLoadForm();">
			
			<h:form id="orderDetailForm">
			
				<h:outputText styleClass="outputText" id="init" value="#{detailPage.initPage}"></h:outputText>
				
				
				
				<table border="0" cellpadding="0" cellspacing="0" width="960" align="center">
					<tr>
						<td class="label4" align="right">
							Hoja de Pedido
						</td>
					</tr>
					<tr>
						<td class="label10" align="right">
							Exclusivo City Express
						</td>
					</tr>
					<tr>
						<td class="label4" align="right" height="10">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="label6" align="left">
							El detalle de su pedido se enviará a la dirección de correo de inocruz@publione.com.mx, también se enviará una copia a su correo registrado. 
							Le confirmaremos su pedido por esa vía. Le agradecemos de antemano.
						</td>
					</tr>
					<tr>
						<td class="label6" align="right" height="10">
							&nbsp;
						</td>
					</tr>
						
				</table>
				
				

                <table border="0" cellpadding="0" cellspacing="0" width="960" align="center">
	                <tr>
	                	<td align="left">
	                	
		                	<table border="0" cellpadding="0" cellspacing="0" width="960" align="left">
								<tr>
									<td class="labelDark12" width="140" align="left">
										<b>Usuario:</b> 
									</td>
									
									<td class="label11" width="350">
										<h:outputText styleClass="label11" value="#{detailPage.userName}" id="username"></h:outputText>
									</td>
									<td width="470">&nbsp;</td>
								</tr>
								<tr>
									<td class="labelDark12" width="140"  align="left">
										<b>Solicitante:</b> 
									</td>
									
									<td class="label11" width="350">
										<h:outputText styleClass="label11" value="#{detailPage.solicitante}" id="solicitante"></h:outputText>
									</td>
									<td width="470">&nbsp;</td>
								</tr>
								<tr>
									<td class="labelDark12" width="140" align="left">
										E-mail: 
									</td>
									<td class="label11" width="350">
										<h:outputText styleClass="label11" value="#{detailPage.userMail}" id="usermail"></h:outputText>
									</td>
									<td width="470">&nbsp;</td>
								</tr>
								
								<tr>
									<td class="labelDark12" width="140" align="left">
										Monto Total: 
									</td>
									
									<td class="label11" width="350">
										<h:outputText styleClass="labelDark12" value="#{detailPage.total}" id="usertotal"></h:outputText>
									</td>
									<td width="470">&nbsp;</td>
								</tr>
								
							</table>
	                	
	                	</td>
	                </tr>
	                
	                <tr height="10"><td>&nbsp;</td></tr>
	                
	                <tr>
	                	<td align="center">
	                		<table border="0" cellpadding="0" cellspacing="0" width="960" align="center">
			                    <tr>
			                        <td align="center">
							
										<h:panelGroup id="body">   
						
								               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="800" rowIndexVar="row"
								                   columnClasses="columnDataTable1,columnDataTable2,columnDataTable3,columnDataTable4,columnDataTable5,columnDataTable61,columnDataTable71" 
								                   headerClass="headerDataTable1" footerClass="footerDataTable1" 
								                   rowClasses="rowClass1,rowClass2" styleClass="dataTable" id="dataTableDetail" var="varlistProductTableVO"
								                   binding="#{detailPage.uiDataTable}" value="#{detailPage.productList}" 
								                   rows="30" >
								                  
								                   <f:facet name="header">
								                           <h:outputText styleClass="header1" id="otext" value="Hoja de Pedido"></h:outputText> 
								                   </f:facet>
								                   
								                   
								                   <t:column id="column1"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Id" id="otext1"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext2" value="#{row + 1}"/>  
								                       
								                   </t:column>
								                   
								                   <t:column id="column2"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Cantidad" id="otext3"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext15" value="#{varlistProductTableVO.quantity}" onkeypress="javascript:return validar(event);"></h:outputText>
								                       <f:attribute value="30" name="width" />
								                   </t:column>
								                   
								                  	
								                   <t:column id="column3"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Unidad" id="otext4"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext5" value="#{varlistProductTableVO.product.unidad}"></h:outputText>
								                       <f:attribute value="10" name="width" />
								                   </t:column>
								                    
								                   <t:column id="column4">
								                       <f:facet name="header">
								                           <h:outputText styleClass="outputText" value="Artículo" id="otext6"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext7" value="#{varlistProductTableVO.product.name}"></h:outputText>
								                   </t:column>
								                   
								                   <t:column id="column5"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="outputText" value="Envío" id="otext8"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext9" value="#{varlistProductTableVO.product.entrega}"></h:outputText>
								                       <f:attribute value="30" name="width" />
								                   </t:column>
								                   
								                   <t:column id="column6"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="outputText" value="Costo" id="otext10"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext11" value="#{varlistProductTableVO.product.price}">
								                       		<f:convertNumber pattern="###,###.##"/>
								                       </h:outputText>
								                       <f:attribute value="30" name="width" />
								                   </t:column>
								                   
								                   <t:column id="column7"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="outputText" value="Total" id="otext11"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="otext12" value="#{varlistProductTableVO.subtotal}">
								                       		<f:convertNumber pattern="###,###.##"/>
								                       </h:outputText>
								                       <f:attribute value="30" name="width" />
								                   </t:column>
								                                                   
								                   
								                   <f:facet name="footer">
														<h:outputText styleClass="header1" id="otext12" value=""></h:outputText>
								                   </f:facet>
								
								
								               </t:dataTable> 
								               
								               <h:panelGrid  columns="1" styleClass="dataTable" columnClasses="scrollerStyle" >
								                    <t:dataScroller id="scroll" binding="#{detailPage.scroller}" for="dataTableDetail" fastStep="30"
								                        pageCountVar="pageCount"
								                        pageIndexVar="pageIndex"
								                        styleClass="scrollerStyle"
								                        paginator="true"
								                        paginatorMaxPages="9" 
								                        paginatorTableClass="scrollerStyle"  
								                        paginatorActiveColumnStyle="font-weight:bold;">
								
								                    </t:dataScroller>
								               </h:panelGrid>
								              
										</h:panelGroup>
										
									</td>
								</tr>
								
							</table>
	                	</td>
	                </tr>
	                
	                <tr height="30"><td>&nbsp;</td></tr>
	                
	                <tr class="label13">
	                	<td>CONDICIONES COMERCIALES:</td>
	                </tr>
	                
	                <tr height="15"><td>&nbsp;</td></tr>
	                
	                <tr>
	                	<td>
	                
							<table border="0" cellpadding="0" cellspacing="1" width="900" align="center">
					        	<tr height="6">
		                    		<td><img src="../../resources/img/corner_1.gif" width="6" height="6"></td>
		                    		<td align="center" background="../../resources/img/middle_1.gif"><img width="1" height="6"></td>
	                        		<td><img src="../../resources/img/corner_2.gif" width="6" height="6"></td>
		                		</tr>
		                		
		                		<tr>
		                    		<td background="../../resources/img/middle_4.gif"><img width="1" height="6"> </td>
		                    		<td bgcolor="#FFFFFF">
		                    		
			                    		<table border="0" cellpadding="0" cellspacing="1" width="894">
								            <tr>
								                <td width="4" align="center"></td>
								                <td width="880">
								                
								                	
								                    <table border="0" cellpadding="0" cellspacing="1" width="880">
								                        <tr>
								                            <td colspan="3" align="right" width="880" height="8"></td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8" width="150">Tiempo de entrega:</td>
								                            <td align="right" width="5"></td>
								                            <td align="left"  width="725" class="label9">
								                            	10 DÍAS HÁBILES
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8"  width="150">Forma de Pago:</td>
								                            <td align="right"  width="5"></td>
								                            <td align="left"   width="725" class="label9">
								                            	15 DÍAS NATURALES, DEPÓSITO A BBVA BANCOMER NO.  011042250-9, CLABE 012 180 00110422509 6 A NOMBRE DE IMPULSORA PUBLIONE TECNOLOGIA E INFORMACIÓN
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8"  width="150">Lugar de entrega:</td>
								                            <td align="right"  width="5"></td>
								                            <td align="left"   width="725" class="label9">
								                            	VÍA MENSAJERÍA CON CARGO ADICIONAL DE ACUERDO A PESO Y VOLÚMEN U OFICINAS CORPORATIVAS MÉXICO.
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8"  width="150">Vigencia de precios:</td>
								                            <td align="right" width="5"></td>
								                            <td align="left" class="label9"  width="725">
								                            	SUJETA A CAMBIOS SIN PREVIO AVISO
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8"  width="150">I.V.A.:</td>
								                            <td align="right"  width="5"></td>
								                            <td align="left" class="label9"  width="725">
								                            	PRECIOS MAS IVA DE ACUERDO A TASA VIGENTE
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="right" class="label8"  width="150">Observaciones:</td>
								                            <td align="right"  width="5"></td>
								                            <td align="left"   width="725"  class="label9">
								                            	ENTREGAS URGENTES Y/O ESPECIALES POR FAVOR CONSÚLTELO, APLICA <FONT color="red">COSTO EXTRA</FONT> POR SERVICIO DE ENVÍO DE <FONT color="red">MESNAJERÍA EXPRESS</FONT>.
								                            </td>
								                        </tr>
								                        <tr>
								                            <td colspan="3" align="right" width="880" height="8"></td>
								                        </tr>
								                    </table>
											                    
								                </td>
								                <td width="4" align="center"></td>
								            </tr>
								        </table>
		                    		
		                    		
		                    		</td>
									<td background="../../resources/img/middle_2.gif"><img width="1" height="6"></td>
								</tr>
								<tr>
									<td><img src="../../resources/img/corner_3.gif" width="6" height="6"></td>
									<td align="center" background="../../resources/img/middle_3.gif"><img width="1" height="6"></td>
									<td><img src="../../resources/img/corner_4.gif" width="6" height="6"></td>
								</tr>
					        </table>
				        
						</td>
					</tr>
					
					<tr height="15"><td>&nbsp;</td></tr>
					
					<tr class="label12" align="center">
	                	<!-- <td>FAVOR DE REALIZAR SUS PAGOS EN BANCOMER CTA. 045126208-4 A NOMBRE DE IMPULSORA PUBLIONE TECNOLOGIA E INFORMACIÓN, FAVOR DE ENVIAR COPIA POR CORREO </td> -->
	                </tr>
                
                </table>

				
				
				<br />
				<br />
				
				
				
				<table border="0" cellpadding="0" cellspacing="0" width="200" align="center">
					<tr>
						<td align="center" width="100">
							<h:commandButton type="submit" action="#{detailPage.doRegresarAction}" 
	                                         id="regresar" value="Regresar" immediate="true" styleClass="botonGris">
	                        </h:commandButton>	
						</td>
						<td align="center" width="100">
							<h:commandButton type="submit" action="#{detailPage.doAceptarAction}" onclick="if (!confirm('Estas seguro de enviar el pedido?')) return false"
	                                         id="aceptar" value="Aceptar" immediate="true" styleClass="botonGris">
	                        </h:commandButton>	
						</td>
					</tr>
				</table>
							
						
			
			</h:form>
		
		</body>
		
	</f:view>
	
	
</html>