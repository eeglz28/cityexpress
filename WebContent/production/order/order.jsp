<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<f:loadBundle var="msgs" basename="mx.com.cityexpress.general.messages.Messages"/>


<html>

	

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-order.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		
		<script language="text/javascript">	
		
			var browserName = navigator.appName;
			
			function validate(evt) {
				var nav4a = window.Event ? true : false;
				if (nav4a) {
					var key1 = evt.which;
					if (key1 == 13) {
						return false;
					} else {
						return true;
					}
					return (key1 == 8 || key1 == 46 || (key1 >= 48 && key1 <= 57));
				} else {
					var key2 = evt.keyCode;
					if (key2 == 13) {
						return false;
					} else {
						return true;
					}
				}
			}

			function validateCant(evt) {
				var nav4a = window.Event ? true : false;
				if (nav4a) {
					var key1 = evt.which;
					if (key1 == 8 || key1 == 46 || (key1 >= 48 && key1 <= 57)) {
						return true;
					} else {
						return false;
					}
					return (key1 == 8 || key1 == 46 || (key1 >= 48 && key1 <= 57));
				} else {
					var key2 = evt.keyCode;
					if (key2 == 46 || (key2 >= 48 && key2 <= 57)) {
						return true;
					} else {
						return false;
					}
				}
			}

          function fotoPopUp() {
			  alert('1');
              var destination = "faces/production/order/foto.jsp";
              var w = screen.availWidth;
              var h = screen.availHeight;
              var popW = w, popH = h;
              
              var leftPos = (w-popW)/2, topPos = (h-popH)/2;
              window.open(destination, 'foto','width=' + popW + ',height=' + popH + ',top=' + topPos + ',left=' + leftPos + ',status=1,scrollbars=0,location=0,menubar=0,resizable=0');
          }
      </script>
			
		
		
		<title>Pedidos</title>
	</head>
	
	
	
 	<f:view>
 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="onLoadForm();">
		
		
		
			<h:form id="orderForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{orderPage.initPage}"></h:outputText>
			
				<h:panelGrid columns="1" align="center" id="instrucciones">
					<h:panelGrid columns="1" align="center" id="insTitle">
						<h:outputText styleClass="label4" id="info1" value="#{msgs.order_info1}"></h:outputText>
						<h:outputText styleClass="label10" id="info2" value="#{msgs.order_info2}"></h:outputText>
					</h:panelGrid>
					<h:outputText styleClass="label6" id="info3" value="#{msgs.order_info3}"></h:outputText>
					<h:outputText styleClass="label6" id="info4" value=""></h:outputText>
					<h:outputText styleClass="label7" id="info5" value="#{msgs.order_info4}"></h:outputText>
					<h:outputText styleClass="label6" id="info6" value="#{msgs.order_info5}"></h:outputText>
					<h:outputText styleClass="label6" id="info7" value="#{msgs.order_info6}"></h:outputText>					
				</h:panelGrid>



				<h:panelGroup id="body">
		
					<t:dataTable border="0" cellpadding="5" cellspacing="1" width="840"
						rowIndexVar="row" align="center"
						columnClasses="columnDataTable1,columnDataTable15,columnDataTable2,columnDataTable3,columnDataTable4,columnDataTable5,columnDataTable6"
						headerClass="headerDataTable1" footerClass="footerDataTable1"
						rowClasses="rowClass1,rowClass2" styleClass="dataTable"
						id="dataTableProd" var="varlistProductTableVO"
						binding="#{orderPage.dataTableProduct}" value="#{orderPage.productList}"
						preserveDataModel="false"
						rows="#{orderPage.rowCount}">
		
						<f:facet name="header">
							<h:outputText styleClass="header1" id="otext"
								value="Cátalogo de Productos"></h:outputText>
						</f:facet>
		
		
						<t:column id="column1">
							<f:facet name="header">
								<h:outputText styleClass="label3" value="Clave" id="otext1"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputTextDt" id="otext2"
								value="#{varlistProductTableVO.product.codeProduct}"></h:outputText>
						</t:column>
						
						<t:column id="column15"> 
	                       <f:facet name="header">
	                           <h:outputText styleClass="label15" value="Foto" id="otextF"></h:outputText>
	                       </f:facet>
	                       <h:commandLink id="foto" styleClass="linkNormal" actionListener="#{orderPage.openPopupClicked}">
	                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/camara_icon.jpg" ></h:graphicImage>
	                       </h:commandLink>
						</t:column>
		
						<t:column id="column2">
							<f:facet name="header">
								<h:outputText styleClass="label3" value="Cantidad" id="otext3"></h:outputText>
							</f:facet>
							<h:inputText styleClass="inputNum" id="inputTextQuantity"
								value="#{varlistProductTableVO.quantity}" size="3"
								valueChangeListener="#{orderPage.handleTextValueChange}"
								immediate="true" onkeypress="return validateCant(event);"
								style="text-align: right;"></h:inputText>
							<f:attribute value="30" name="width" />
						</t:column>
		
		
						<t:column id="column3">
							<f:facet name="header">
								<h:outputText styleClass="label3" value="Unidad" id="otext4"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputTextDt" id="otext5"
								value="#{varlistProductTableVO.product.unidad}"></h:outputText>
							<f:attribute value="10" name="width" />
						</t:column>
		
						<t:column id="column4">
							<f:facet name="header">
								<h:outputText styleClass="outputText" value="Artículo" id="otext6"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputTextDt" id="otext7"
								value="#{varlistProductTableVO.product.name}"></h:outputText>
						</t:column>
		
						<t:column id="column5">
							<f:facet name="header">
								<h:outputText styleClass="outputText" value="Costo" id="otext8"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputTextDt" id="otext9"
								value="#{varlistProductTableVO.product.price}">
								<f:convertNumber pattern="###,###.##" />
							</h:outputText>
							<f:attribute value="30" name="width" />
						</t:column>
		
						<t:column id="column6">
							<f:facet name="header">
								<h:outputText styleClass="outputText" value="Folio Inicial"
									id="otext10"></h:outputText>
							</f:facet>
							<h:inputText styleClass="input1" id="inputTextComments"
								value="#{varlistProductTableVO.comment}" size="10"
								valueChangeListener="#{orderPage.handleTextCommentChange}"
								immediate="true"
								rendered="#{varlistProductTableVO.product.folioStatus}"
								onkeypress="return validate(event);"></h:inputText>
							<f:attribute value="120" name="width" />
						</t:column>
		
						<f:facet name="footer">
							<h:outputText styleClass="header1" id="otext12" value=""></h:outputText>
						</f:facet>
		
					</t:dataTable>
		
		
					<h:panelGrid columns="1" styleClass="dataTable" columnClasses="scrollerStyle" align="center">
						<t:dataScroller id="scroll" 
							for="dataTableProd" 
							fastStep="10" 
							pageCountVar="pageCount"
							pageIndexVar="pageIndex" 
							styleClass="scrollerStyle"
							paginator="true" 
							paginatorMaxPages="9"
							paginatorTableClass="paginator"
							paginatorActiveColumnStyle="font-weight:bold;"
							immediate="true"
							actionListener="#{orderPage.scrollerAction}"/>
					</h:panelGrid>
		
				</h:panelGroup>
				
				
				<h:panelGrid columns="1" align="center">
						<h:outputText styleClass="label4" value="" />	
				</h:panelGrid>
				

				<h:panelGrid columns="3" align="center" id="comandos">
					<h:commandButton type="submit" action="#{orderPage.doCancelarAction}"
							id="cancelar" value="Cancelar" immediate="true" styleClass="botonGris"/>
					<h:outputText styleClass="label4" id="comm2" value="    " ></h:outputText>
				    <h:commandButton type="submit" action="#{orderPage.doAceptarAction}"
							id="continuar" value="Continuar" immediate="true" styleClass="botonGris"/>
				</h:panelGrid>


				<h:panelGrid columns="1" align="center">
						<h:outputText styleClass="label4" value="" />	
				</h:panelGrid>
			
					
			</h:form>

		
		</body>
	
	</f:view>
	
</html>