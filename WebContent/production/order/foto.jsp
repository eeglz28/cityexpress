<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<LINK href="../../resources/css/cityexpress-login.css" rel="stylesheet" type="text/css">
		<title>Foto</title>
	</head>
	
	<f:view>
		<body>
			<h:form id="fotos" >
				<center>
					<table cellspacing="0" cellpadding="0" align="center" width="780" height="780">
						<tr>
							<td align="center">
								<h:graphicImage height="75%" width="75%" style="border: none;" url="#{fotoPage.url}" id="imageFoto"></h:graphicImage>
							</td>
						</tr>
						<tr>
							<td align="center" class="linkCerrar">
								<h:outputLink onclick="window.close(); return false;" value="#" styleClass="linkCerrar">
								        <h:outputText value="Cerrar" styleClass="linkCerrar"/>
								</h:outputLink>
							</td>
						</tr>
					</table>
				</center>
			</h:form>
		</body>
	</f:view>
</html>