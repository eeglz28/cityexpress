<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>



<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-order.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Confirmación</title>
		
		
	</head>
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="onLoadForm();">
		
			<h:form id="orderConfirmForm">
			
				<h:outputText styleClass="outputText" id="init" value="#{confirmPage.initPage}"></h:outputText>
			
				
				
							
				<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
					<tr>
						<td class="label4" align="right">
							Hoja de Pedido
						</td>
					</tr>
					<tr>
						<td class="label10" align="right">
							Exclusivo City Express
						</td>
					</tr>
					<tr>
						<td class="label4" align="right" height="20">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="label4" align="center" height="20">
							<h:graphicImage url="../../resources/img/OK.gif" height="32" width="32"></h:graphicImage>
						</td>
					</tr>
					<tr>
						<td class="label6" align="center">
							Se generó el pedido número 
						</td>
					</tr>
					<tr>
						<td class="label6" align="center">
							<b>
								<h:outputText styleClass="label6" id="otext1" value="#{confirmPage.numPedido}"></h:outputText>
							</b> 
						</td>
					</tr>
					<tr>
						<td class="label6" align="right" height="10">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="label6" align="center">
							<h:outputText styleClass="label6" id="otext2" value="#{confirmPage.mensajeConfirmacion}"></h:outputText>
						</td>
					</tr>
					
					
				</table>
				
				
				
				
				<br />
				<br />
				
				
				
				<table border="0" cellpadding="0" cellspacing="0" width="100" align="center">
					<tr>
						<td align="center" width="100">
							<h:commandButton type="submit" action="#{confirmPage.doTerminarAction}" 
	                                         id="nuevo" value="Nuevo Pedido" immediate="true" styleClass="botonGris">
	                        </h:commandButton>	
						</td>
						
					</tr>
				</table>
							
							
							
				
			
	
				
				
			</h:form>
		
		</body>
		
	</f:view>
	
	
</html>