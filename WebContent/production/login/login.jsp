<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-login.css" rel="stylesheet" type="text/css">
		<title>Login Portal</title>
	</head>
	
	 <f:view>
	
		<body>
		
			<h:form id="loginForm">
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="200" align="center">
					<tr>
						<td align="center" class="label1b">
							Ingresa a SGP - CE!	
						</td>
					</tr>
					<tr height="2">
						<td></td>
					</tr>
				</table>
		
				<table cellpadding="0" cellspacing="0" border="0" align="center" class="tablelogin">
					<tr bgcolor="#FFFFFF">
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
							    <tr height="5">
                                    <td colspan="4"></td>
                                </tr> 
                                
							    <tr height="10">
							    	<td width="10"></td>
                                    <td colspan="2" align="right">
                                    	<h:commandLink id="cerrar" styleClass="linkDefault" value="CERRAR" onclick="window.close()"></h:commandLink>
                                    </td>
                                    <td width="10"></td>
                                </tr>
                                
                                <tr height="5">
                                    <td colspan="4"></td>
                                </tr>  
							         
							         
							    
							    <tr>
                                    <td class="label1b" align="center" rowspan="4">
                                         <img alt="login" src="../../resources/img/keys_login.jpg" height="112" width="150">		
                                    </td>
                                    <td width="10"></td>
                                    <td class="label1" align="left">
                                         Usuario:
                                    </td>
                                    <td width="10"></td>
                                </tr>
                                
                             	<tr>
                                    <td width="10"></td>
                                    <td class="label1" align="left">
                                         <h:inputText immediate="true" id="userText" size="16" maxlength="8" styleClass="input1" />
                                    </td>
                                    <td width="10"></td>
                                </tr>
                                
                                
                              	<tr>
                                    <td width="10"></td>
                                    <td class="label1" align="left">
                                         Contraseña:
                                    </td>
                                    <td width="10"></td>
                                </tr>
                                
                                
                                <tr>
                                    <td width="10"></td>
                                    <td class="label1" align="left">
                                         <h:inputSecret immediate="true" id="passSecret" size="16" maxlength="16" styleClass="input1"></h:inputSecret>
                                    </td>
                                    <td width="10"></td>
                                </tr>
                                
                                
                                
                                
                              
                                
                               
                                
                                <tr height="10">
                                	<td colspan="4"></td>
                                </tr>

                                <tr height="40">
                                    <td colspan="4" align="center">
										<h:commandButton type="submit" action="#{login.doAceptarAction}"
                                                             id="aceptar" value="Aceptar" immediate="true" styleClass="botonGris">
										</h:commandButton>
                                    </td>
                                </tr>
                                
                                <tr height="10">
                                	<td colspan="4"></td>
                                </tr>
                                
                                
			                                    
							</table>
						</td>
					</tr>
				</table>
				
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
					<tr>
						<td align="center">
							<h:outputText id="otext1" value="#{login.message}" styleClass="labelError1"></h:outputText>
						</td>
					</tr>
				</table>
				
				
			</h:form>
	
		</body>
	
	</f:view>
</html>