<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-admon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Crea Cliente</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#F0F8FF" onload="onLoadForm();">
		
			<h:form id="creaClieForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{creaClie.initPage}"></h:outputText>
				

					
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Creación de Nuevo Cliente </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr bgcolor="#FFFFFF">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" class="tableDefault2">
								<tr height="10">
									<td colspan="9"></td>
								</tr>


								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Nombre:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="60" maxlength="65" styleClass="input1" binding="#{creaClie.nombreText}" id="nombreText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el nombre del cliente.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otNomErr" value="#{creaClie.nombreErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Contacto:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="60" maxlength="65" styleClass="input1" binding="#{creaClie.contactoText}" id="contactoText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el nombre del contacto.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otConErr" value="#{creaClie.contactoErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								

								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" width="100">Estado:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		<h:inputText immediate="true" size="16" maxlength="16" styleClass="input1" binding="#{creaClie.estadoText}" id="estadoText"/>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="label1" align="left" width="100">Telefono:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
										<h:inputText immediate="true" size="16" maxlength="16" styleClass="input1" binding="#{creaClie.telefonoText}" id="telefonoText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left">
	                            		El estado es obligatorio.
	                            	</td>
	                            	
	                            	<td colspan="3"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Ingrese el teléfono del contacto.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otEstErr" value="#{creaClie.estadoErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otTelErr" value="#{creaClie.telefonoErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								
															
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								
								<tr height="20">
									<td colspan="9">
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td></td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{creaClie.doAceptarAction}"
	                                     				id="aceptar" value="Aceptar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{creaClie.doRegresarAction}"
	                                     				id="regresar" value="Regresar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td></td>
											</tr>
										</table>
									</td>
	                            </tr>
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								                      
							</table>
						</td>
						
					</tr>
				</table>
				
				
								<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
					<tr>
						<td align="center">
							<h:outputText id="otMsgErr" value="#{creaClie.message}" styleClass="labelError1"></h:outputText>
						</td>
					</tr>
				</table>
				

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>