<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-admon.css" rel="stylesheet" type="text/css">
		
		
		<link type="text/css" rel="stylesheet" href="../../resources/css/jquery-te-1.4.0.css">
		<script type="text/javascript" src="../../resources/js/jquery.min.js" charset="utf-8"></script>
		<script type="text/javascript" src="../../resources/js/jquery-te-1.4.0.min.js" charset="utf-8"></script>

		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}

		</script>
		<title></title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="envioMailForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{prod.initPage}"></h:outputText>
			

				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Envío de Correo Masivos</td>
					</tr>
					<tr height="25"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" width="870" height="250" align="center">
					<!-- Renglon 1 -->
                    <tr height="20">
						<td width="10"></td>
                        <td colspan="4" class="label1" align="left" width="430">
                        	Enviar a todos los usuarios registrados
                        </td>
                        
                        <td colspan="4" class="label1" align="left" width="430">
                        	<h:selectBooleanCheckbox id="allUsersSelect" binding="#{envioMail.allUserSelected}"></h:selectBooleanCheckbox>
                        </td>
                    </tr>
				
					<!-- Renglon 2 -->
					<tr height="20">
						<td width="10"></td>
                        <td class="label1" align="left" width="70">Cliente</td>
                        <td width="20"></td>
                        <td class="label1" align="left" width="330">
                        	<h:selectManyListbox binding="#{envioMail.clientListBox}" id="selectOneMenuClient" styleClass="input1" size="8" >
                          			<f:selectItems binding="#{envioMail.clientUISelectItems}"
									id="combo1SelectItems" value="#{envioMail.clientSelectItem}" />
                          	</h:selectManyListbox>
                        </td>
                          	
                        <td width="10"></td>
                        
                        <td class="label1" align="left" width="70">Perfil</td>
                        <td width="20"></td>
                        <td class="label1" align="left" width="330">
                        	<h:selectManyListbox binding="#{envioMail.profileListBox}" id="selectOneMenuPerfil" styleClass="input1" size="8" >
                          			<f:selectItems binding="#{envioMail.profileUISelectItems}"
									id="combo2SelectItems" value="#{envioMail.profileSelectItem}" />
                          	</h:selectManyListbox>
                        </td>
                        <td width="10"></td>
                    </tr>
                    <tr height="10">
                        <td colspan="3" width="100"></td>
                        <td class="smallLabel1" align="left" width="330">
                          		Indique los clientes a los que se le enviará el correo
                        </td>
                        
                        <td colspan="3" width="100"></td>
                        <td class="smallLabel1" align="left" width="330">
                          		Indique el perfil de los usuarios a los que se le enviará el correo
                        </td>
                        <td width="10"></td>
                    </tr>
                    
                    <!-- Renglon 3 -->
                    <tr height="20">
						<td width="10"></td>
                        <td class="label1" align="left" width="70">Titulo</td>
                        <td width="20"></td>
                        <td class="label1" align="left" width="330">
                        	<h:inputText immediate="true" styleClass="label1" id="subject" size="20" binding="#{envioMail.title}"/>
                        </td>
                          	
                        <td width="10"></td>
                        
                        <td class="label1" align="left" width="70"></td>
                        <td width="20"></td>
                        <td class="label1" align="left" width="330">
                        	
                        </td>
                        <td width="10"></td>
                    </tr>
                    <tr height="10">
                        <td colspan="3" width="100"></td>
                        <td colspan="6" class="smallLabel1" align="left" width="770">
                          		Indique un titulo para el correo
                        </td>
                    </tr>
					
				</table>
				
				
				
				<table cellpadding="0" cellspacing="0" border="0" width="800" height="400" align="center">
					<tr class="label1">
						<td>
							Ingrese la informacion que desea enviar a los usuarios: &nbsp;
							<h:inputText immediate="true" styleClass="jqte-test" id="input" size="600" binding="#{envioMail.input}"/>
							<script>
								$('.jqte-test').jqte();
								
								// settings of status
								var jqteStatus = true;
								$(".status").click(function()
								{
									jqteStatus = jqteStatus ? false : true;
									$('.jqte-test').jqte({"status" : jqteStatus})
								});
							</script>
						</td>
					</tr>
				</table>
				
                
                
	                            	
				
				
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="250" align="center">
					<tr>
						<td align="center">
							
				 			<h:commandButton type="submit" action="#{envioMail.doAceptarAction}"
	                               				id="aceptar" value="Enviar" styleClass="botonGris">
	                        </h:commandButton>
	                    </td>
	                    <td align="center">
	                        <h:commandButton type="submit" action="#{envioMail.doRegresarAction}"
	                               				id="regresar" value="Regresar" styleClass="botonGris">
	                        </h:commandButton>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
				 			<h:outputText styleClass="label1" id="msg" value="#{envioMail.message}"></h:outputText>
						</td>
					</tr>
					
				</table>
				
				<br>
				<br>

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>