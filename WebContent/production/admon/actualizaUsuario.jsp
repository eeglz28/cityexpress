<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-admon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Actualiza Usuario</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#F0F8FF" onload="onLoadForm();">
		
			<h:form id="actUsuaForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{actUsua.initPage}"></h:outputText>
				

					
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Actualización de Usuarios </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr bgcolor="#FFFFFF">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" class="tableDefault2">
								<tr height="10">
									<td colspan="9"></td>
								</tr>


								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Cliente:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:outputText id="ot1" value="#{actUsua.cliente}" styleClass="boldLabel1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" width="100">Nick:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		<h:inputText immediate="true" size="12" maxlength="8" styleClass="input1" 
	                            				value="#{actUsua.nick}" binding="#{actUsua.nickText}" id="nickText"/>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="label1" align="left" width="100">Contraseña:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		<h:inputSecret immediate="true" size="12" maxlength="16" styleClass="input1" 
	                            				value="#{actUsua.pass}" binding="#{actUsua.passText}" id="passText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Nombre de usuario.
	                            	</td>
	                            	
	                            	<td colspan="3"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Contraseña del usuario.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otNicErr" value="#{actUsua.nickErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otPasErr" value="#{actUsua.passErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" width="100"></td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="label1" align="left" width="100">Confirmar:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		<h:inputSecret immediate="true" size="12" maxlength="16" styleClass="input1" 
	                            				value="#{actUsua.confirmar}" binding="#{actUsua.confirmarText}" id="confirmarText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left">
	                            		
	                            	</td>
	                            	
	                            	<td colspan="3"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Confirmar la contraseña del usuario.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="3">
	                            		
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otConErr" value="#{actUsua.confirmarErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Perfil:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:selectOneMenu binding="#{actUsua.perfilSelectOneMenu}" id="selectOneMenuPerfil" styleClass="input1" value="#{actUsua.perfil}">
											<f:selectItems binding="#{actUsua.perfilSelectItems}"
												id="combo1SelectItems" value="#{actUsua.selectItemPerfil}" />
										</h:selectOneMenu>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el perfil para el Usuario
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otPerfErr" value="#{actUsua.perfilErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Nombre:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="55" maxlength="60" styleClass="input1" value="#{actUsua.name}" binding="#{actUsua.nameText}" id="nameText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el nombre y apellidos del usuario. 
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otNamErr" value="#{actUsua.nameErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Email:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="55" maxlength="65" styleClass="input1" 
	                            				value="#{actUsua.email}" binding="#{actUsua.emailText}" id="emailText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el correo electrónico del usuario, para el envío de notificaciones. 
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otEmaErr" value="#{actUsua.emailErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								

								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" colspan="3">
	                            		<h:selectBooleanCheckbox value="#{actUsua.estatus}" binding="#{actUsua.estatusCheckbox}" id="estatusCheckbox" title=""/> Bloquear Usuario
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	<td class="label1" align="left" colspan="3"></td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="smallLabel2" align="left" colspan="3">
	                            		Esta opción es para bloquear al usuario.
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	<td class="smallLabel2" align="left" colspan="3"></td>
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
	
	
															
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								
								<tr height="20">
									<td colspan="9">
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td></td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{actUsua.doAceptarAction}"
	                                     				id="aceptar" value="Aceptar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{actUsua.doRegresarAction}"
	                                     				id="regresar" value="Regresar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td></td>
											</tr>
										</table>
									</td>
	                            </tr>
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								                      
							</table>
						</td>
						
					</tr>
				</table>
				
				
								<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
					<tr>
						<td align="center">
							<h:outputText id="otMsgErr" value="#{actUsua.message}" styleClass="labelError1"></h:outputText>
						</td>
					</tr>
				</table>
				

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>