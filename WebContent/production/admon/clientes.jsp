<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-admon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Clientes</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="clientesForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{clie.initPage}"></h:outputText>
			

				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Clientes</td>
					</tr>
					<tr height="25"><td></td></tr>
				</table>
				
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="960">
					<tr height="15"><td></td></tr>
					<tr>
						<td align="right">
							<h:commandLink action="#{clie.doCrearAction}" id="crear" styleClass="linkAdmon1">
								<h:outputText styleClass="linkAdmon1" id="ot1" value="Crear Nuevo Cliente"></h:outputText>
							</h:commandLink>
						</td>
					</tr>
					<tr height="5"><td></td></tr>
				</table> 
	
				<table border="0" cellpadding="0" cellspacing="0" width="980" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="960" rowIndexVar="row"
					                   columnClasses="columnDTClie1,columnDTClie2,columnDTClie3,columnDTClie4,columnDTClie5,columnDTClie6"
					                   headerClass="headerDT1" footerClass="footerDT1" 
					                   rowClasses="rowDT1,rowDT2" styleClass="dataTable" id="dataTableClie" var="varlistCliTableVO" 
					                   binding="#{clie.dataTableClientes}" value="#{clie.clientesList}" rendered="true" 
					                   preserveDataModel="false" first="0" rows="#{clie.rowCount}" >
					                  
					                   <f:facet name="header">
					                           <h:outputText styleClass="header1" id="otDT1" value="Listado"></h:outputText> 
					                   </f:facet>
					                   
					                   <t:column id="column1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Nombre" id="otDT2"></h:outputText>
					                       </f:facet>
					                       	<h:outputText styleClass="outputTextDt" id="otDT3" value="#{varlistCliTableVO.nombre}" />
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                  	
					                   <t:column id="column2"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Contacto" id="otDT4"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otDT5" value="#{varlistCliTableVO.contacto}"></h:outputText>
					                       <f:attribute value="10" name="width" />
					                   </t:column>
					                    
					                   <t:column id="column3">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Estado" id="otDT6"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otDT7" value="#{varlistCliTableVO.estado}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                   </t:column>
					                   
					                   <t:column id="column4"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Telefono" id="otDT8"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="otDT9" value="#{varlistCliTableVO.telefono}"></h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Actualizar" id="otDT10"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{clie.doActualizarAction}" id="actualizar" styleClass="linkDefault2">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/entregar.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                   </t:column>
					                   
										<t:column id="column6"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Usuarios" id="otDT11"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{clie.doUsuariosAction}" id="usuarios" styleClass="linkDefault2">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/usuario.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                   </t:column>
					                   
					                 

					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="otext13" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					               
					               <h:panelGrid  columns="1" styleClass="dataTable" columnClasses="scrollerStyle" >
					                    <t:dataScroller id="scroll_1" for="dataTableClie" fastStep="4"
					                        pageCountVar="pageCount"
					                        pageIndexVar="pageIndex"
					                        styleClass="scrollerStyle"
					                        paginator="true"
					                        paginatorMaxPages="9" 
					                        paginatorTableClass="scrollerStyle"  
					                        paginatorActiveColumnStyle="font-weight:bold;"
					                        immediate="true"
					                        actionListener="#{clie.scrollerAction}">
					
					                    </t:dataScroller>
					               </h:panelGrid>
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>
				
				
			
	

	
				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
	
					<tr height="15">
						<td colspan="6"></td>
					</tr>
					<tr height="20">
						<td colspan="6" align="center">
							 <h:commandButton type="submit" action="#{clie.doRegresarAction}"
									id="Regresar" value="Regresar" styleClass="botonGris">
							</h:commandButton>
						</td>
                          </tr>
					<tr height="15">
						<td colspan="6"></td>
					</tr>
				</table>
				
				
			
			</h:form>

		</body>
	 
	</f:view>
	
</html>