<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-admon.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
		</script>
		<title>Actualiza Poroducto</title>
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="#F0F8FF" onload="onLoadForm();">
		
			<h:form id="actProdForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{actProd.initPage}"></h:outputText>
				

					
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Actualización de Productos </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
				
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr bgcolor="#FFFFFF">
						
						<td>
							<table border="0" cellpadding="0" cellspacing="0" class="tableDefault2">
								<tr height="10">
									<td colspan="9"></td>
								</tr>


								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Código:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="3" maxlength="3" styleClass="input1" 
	                            				value="#{actProd.codigo}" binding="#{actProd.codigoText}" id="codigoText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el código numérico que identifica el producto.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otCodErr" value="#{actProd.codigoErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Producto:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="60" maxlength="65" styleClass="input1" 
	                            				value="#{actProd.producto}" binding="#{actProd.productoText}" id="productoText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el nombre del producto, ejemplo: HOJAS MEMBRETADAS.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otProErr" value="#{actProd.productoErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	


								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Descripción:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="60" maxlength="65" styleClass="input1" 
	                            				value="#{actProd.descripcion}" binding="#{actProd.descripcionText}" id="descripcionText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese la descripciòn del producto. Esta descripción no se muestra al usuario.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>														
							
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left">Precio:</td>
	                            	<td width="10"></td>
	                            	<td align="left" colspan="5">
	                            		<h:inputText immediate="true" size="9" maxlength="10" styleClass="input1" 
	                            				value="#{actProd.precio}" binding="#{actProd.precioText}" id="precioText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left" colspan="5">
	                            		Ingrese el precio del producto, no se aceptan comas, ni espacios. Ejemplo: 1234.23 , 0.25, etc.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="7">
	                            		<h:outputText id="otPreErr" value="#{actProd.precioErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								

								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" width="100">Unidad:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
	                            		<h:inputText immediate="true" size="12" maxlength="12" styleClass="input1" 
	                            				value="#{actProd.unidad}" binding="#{actProd.unidadText}" id="unidadText"/>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="label1" align="left" width="100">Entrega:</td>
	                            	<td width="10"></td>
	                            	<td align="left" width="200">
										<h:inputText immediate="true" size="12" maxlength="12" styleClass="input1" 
												value="#{actProd.entrega}" binding="#{actProd.entregaText}" id="entregaText"/>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td colspan="2"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Unidad en que se vende el producto.
	                            	</td>
	                            	
	                            	<td colspan="3"></td>
	                            	<td class="smallLabel2" align="left">
	                            		Tiempo de entrega del producto.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
	                            <tr height="10">
									<td width="10"></td>
									
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otUniErr" value="#{actProd.unidadErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="labelError1" align="left" colspan="3">
	                            		<h:outputText id="otEntErr" value="#{actProd.entregaErr}" styleClass="labelError1"></h:outputText>
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
								
								
								<tr height="5">
									<td colspan="9"></td>
								</tr>
								<tr height="20">
									<td width="10"></td>
									
	                            	<td class="label1" align="left" colspan="3">
	                            		<h:selectBooleanCheckbox value="#{actProd.estatus}" binding="#{actProd.estatusCheckbox}" id="estatusCheckbox" title=""/> Activar Producto
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="label1" align="left" colspan="3">
	                            		<h:selectBooleanCheckbox value="#{actProd.folio}" binding="#{actProd.folioCheckbox}" id="folioCheckbox"/> Activar Campo Folio
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="10">
									<td width="10"></td>
									
	                            	
	                            	<td class="smallLabel2" align="left" colspan="3">
	                            		Esta opción es para que el producto sea visible.
	                            	</td>
	                            	
	                            	<td width="20"></td>
	                            	
	                            	<td class="smallLabel2" align="left" colspan="3">
	                            		Esta opción es para deshabilitar el campo de folio.
	                            	</td>
	                            	
	                            	<td width="10"></td>
	                            </tr>
								<tr height="5">
									<td colspan="9"></td>
								</tr>	
	
															
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								
								<tr height="20">
									<td colspan="9">
										<table border="0" cellpadding="0" cellspacing="0" align="center">
											<tr>
												<td></td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{actProd.doAceptarAction}"
	                                     				id="aceptar" value="Aceptar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td align="center" width="100">
										 			<h:commandButton type="submit" action="#{actProd.doRegresarAction}"
	                                     				id="regresar" value="Regresar" styleClass="botonGris">
	                                     			</h:commandButton>
												</td>
												<td></td>
											</tr>
										</table>
									</td>
	                            </tr>
								<tr height="15">
									<td colspan="9"></td>
								</tr>
								                      
							</table>
						</td>
						
					</tr>
				</table>
				
				
								<br>
				
				<table cellpadding="0" cellspacing="0" border="0" width="500" align="center">
					<tr>
						<td align="center">
							<h:outputText id="otMsgErr" value="#{actProd.message}" styleClass="labelError1"></h:outputText>
						</td>
					</tr>
				</table>
				

				
			</h:form>

		</body>
	 
	</f:view>
	
</html>