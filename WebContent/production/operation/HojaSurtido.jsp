<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-oper.css" rel="stylesheet" type="text/css">
		
		<title>Hoja de Surtido</title>
		
		
		<script type="text/javascript">
			function doPrint(){
				document.getElementById("imprimir").style.display='none'
				//se imprime la pagina
				window.print()
				//reaparece el boton
				document.getElementById("imprimir").style.display='inline'
			}
		</script>

	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="hojaSurtidoForm" >
			
				<h:outputText styleClass="outputText" id="init" value="#{hojaSur.initPage}"></h:outputText>
			
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" height="900">

										
					<tr height="900">
						<td class="labelTitle1" align="center">
						
							<table cellpadding="0" cellspacing="0" border="0" align="center" width="650">
								<!-- encabezado -->
								<tr height="100">
									<td>
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="650">
											<tr height="100" valign="top">
												<td width="300" align="center">
													<img src="../../resources/img/publione_logo_horizontal_transparente.png" height="100" width="200"/>
												</td>
												<td width="150"></td>
												<td width="300">
													<table cellpadding="0" cellspacing="0" border="0" align="center" width="300">
														<tr><td align="right">
															<span class="label1"> Impulsora Publione Tecnologia e Información </span>
														</td></tr>
														<tr><td align="right"><span class="label4">www.aldakar.com.mx</span></td></tr>
														<tr><td align="center" class="columnGral1"><span class="label5">PEDIDO CITY</span></td></tr>
														<tr><td align="center" class="columnGral2"><span class="label6">HOJA DE SURTIDO</span></td></tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<!-- fechas -->
								<tr height="50">
									<td>
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="650">
											<tr height="25" valign="top">
												<td width="200" align="center"><span class="label7">PRODUCCIÓN | OPERACIONES</span></td>
												<td width="150" align="right" class="columnGral3"><span class="label8">recepción:</span></td>
												<td width="300" align="center">
													<h:outputText styleClass="label9" id="recepcion" value="#{hojaSur.fechaRecepcion}"></h:outputText>
												</td>
											</tr>
											<tr height="25" valign="top">
												<td width="200" align="center"></td>
												<td width="150" align="right" class="columnGral3"><span class="label8">promesa de envío:</span></td>
												<td width="300" align="center">
													<h:outputText styleClass="label9" id="entrega" value="#{hojaSur.fechaEntrega}"></h:outputText>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<!-- mensaje confirmacion -->
								<tr height="50">
									<td align="center">
										<h:outputText styleClass="texto1" id="conf" value="#{hojaSur.confirmacion}"></h:outputText>
									</td>
								</tr>

								<!-- atencion -->
								<tr height="60">
									<td align="center">
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="650">
											<tr height="20">
												<td width="20" align="left"></td>
												<td width="250" align="left"><h:outputText styleClass="label9" id="client" value="#{hojaSur.cliente}"></h:outputText></td>
												<td width="380" align="left"></td>
											</tr>
											<tr height="20">
												<td width="20" align="left"></td>
												<td width="250" align="left" class="columnGral4"><h:outputText styleClass="label10" id="usern" value="#{hojaSur.usuario}"></h:outputText></td>
												<td width="380" align="left"></td>
											</tr>
											<tr height="20">
												<td width="20" align="left"></td>
												<td width="250" align="left"><span class="label3">ATENCIÓN</span></td>
												<td width="380" align="left"></td>
											</tr>
										</table>
									</td>
								</tr>

								<!-- mensaje a continuacion -->
								<tr height="40">
									<td>
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="650">
											<tr>
												<td width="20" align="left"></td>
												<td width="630" align="left">
													<span class="texto2">A continuación le mostramos el detalle del pedido.</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								
								
								<!-- *********************************************************************************** -->
								
	
			                    <tr>
			                        <td align="center" valign="top">
							
										<h:panelGroup id="body">   
						
								               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="650" rowIndexVar="row"
								                   columnClasses="colDTHJ1,colDTHJ2,colDTHJ3,colDTHJ4,colDTHJ5,colDTHJ6" 
								                   headerClass="headerDTHJ" styleClass="dtborder" preserveDataModel="false" rules="all" border="15"
								                   rowClasses="rowDTHJ" id="dataTableDet" var="varlistDetailOrderVO" rendered="true" 
								                   binding="#{hojaSur.dataTableDetalle}" value="#{hojaSur.listaDetalle}"  first="0">
								                  
								                   
								                   <t:column id="column1"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Cantidad" id="dtPeOt1"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="dtPeOt2" value="#{varlistDetailOrderVO.cantidad}"></h:outputText>
								                       <f:attribute value="80" name="width" />
								                   </t:column>
								                   
								                   <t:column id="column2">
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Unidad" id="dtPeOt3"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="dtPeOt4" value="#{varlistDetailOrderVO.unidad}"></h:outputText>
								                       <f:attribute value="90" name="width" />
								                   </t:column>
								                   
												   <t:column id="column3">
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Producto" id="dtPeOt5"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="dtPeOt6" value="#{varlistDetailOrderVO.producto}"></h:outputText>
								                       <f:attribute value="90" name="width" />
								                   </t:column>
			
								                   <t:column id="column4"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Estatus de surtido" id="dtPeOt7"></h:outputText>
								                       </f:facet>
								                       	<h:outputText styleClass="outputTextDt" id="dtPeOt8" value="" />
								                       	<f:attribute value="90" name="width" />
								                   </t:column>
								                   
								                   <t:column id="column5"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="OP | generada" id="dtPeOt9"></h:outputText>
								                       </f:facet>
								                       	<h:outputText styleClass="outputTextDt" id="dtPeOt10" value="" />
								                       	<f:attribute value="150" name="width" />
								                   </t:column>
				
								                   <t:column id="column6"> 
								                       <f:facet name="header">
								                           <h:outputText styleClass="label3" value="Surtido | firma de control" id="dtPeOt11"></h:outputText>
								                       </f:facet>
								                       <h:outputText styleClass="outputTextDt" id="dtPeOt12" value=""></h:outputText>
								                       <f:attribute value="150" name="width" />
								                   </t:column>
													
								               </t:dataTable> 
								              
										</h:panelGroup>
										
									</td>
								</tr>
								
								<!-- *********************************************************************************** -->


								<tr height="20">
									<td valign="top">
									</td>
								</tr>


								<!-- control -->
								<tr height="125">
									<td valign="top">
										<table cellpadding="0" cellspacing="0" border="0" align="center" width="650" class="tablaProductos">
											<tr height="25">
												<td width="170" align="center" class="header"><span class="label3">producción | supervisión</span></td>
												<td width="180" align="center" class="header"><span class="label3">guía(s) de envío</span></td>
												<td width="150" align="center" class="header" colspan="2"><span class="label3">facturación</span></td>
												<td width="150" align="center" class="header"><span class="label3">notas | observaciones</span></td>
											</tr>
											<tr height="50" valign="top">
												<td width="170" align="left" class="tablaProductos"><span class="label7">nombre:</span></td>
												<td width="180" align="left" class="tablaProductos"><span class="label7">1ra:</span></td>
												<td width="90" align="left" class="tablaProductos"><span class="label7">1ra:</span></td>
												<td width="80" align="left" class="tablaProductos"><span class="label7">2da:</span></td>
												<td width="150" align="left" class="tablaProductos"></td>
											</tr>
											<tr height="50" valign="top">
												<td width="170" align="left" class="tablaProductos"><span class="label7">nombre:</span></td>
												<td width="180" align="left" class="tablaProductos"><span class="label7">1ra:</span></td>
												<td width="90" align="left" class="tablaProductos"><span class="label7">1ra:</span></td>
												<td width="80" align="left" class="tablaProductos"><span class="label7">2da:</span></td>
												<td width="150" align="left" class="tablaProductos"></td>
											</tr>
											
										</table>
									</td>
								</tr>

								
								<tr>
									<td>
										<table align="center">
											<tr>
												<td id="noprint" align="center">
													<input name="imprimir" id="imprimir" type="button" onClick="doPrint();" value="Imprimir">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								

							</table>
						
						</td>
					</tr>
					
				</table>
			
			
			
			
			
				<h:outputText styleClass="outputText" id="end" value="#{hojaSur.finishPage}"></h:outputText>
			
			
			
		
				

			</h:form>

		</body>
	 
	</f:view>
	
</html>