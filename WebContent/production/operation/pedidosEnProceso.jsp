<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>

<%@ page language="java" %> 
<%@page import="mx.com.cityexpress.general.vo.UsuarioSesionVO"  %>
<%@page import="mx.com.cityexpress.general.common.ConstantsCExpress"  %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-oper.css" rel="stylesheet" type="text/css">
		<LINK href="../../resources/css/cityexpress-consultas.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			<%
			UsuarioSesionVO usuarioSesion = (UsuarioSesionVO) session.getAttribute(ConstantsCExpress.SESSION_USERS_LOGIN);
			%>
			function onLoadForm() {
				var usuario = <%= usuarioSesion %> ;
				var url = '<%= ConstantsCExpress.URL_REDIRECT_NOSESSION %>' ;
				if (usuario == null) {
					window.location.href=url;
				}
			}
			

		</script>
		<title>Pedidos en Proceso</title>
		
	</head>
	
	
	
	<f:view>
	 
		<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" bgcolor="white" onload="onLoadForm();">
		
			<h:form id="pedidosEnProForm" >
			
				
			

				
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Pedidos en Proceso </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
	
				<table border="0" cellpadding="0" cellspacing="0" width="980" align="center">
                    <tr>
                        <td align="center">
				
							<h:panelGroup id="body">   
			
					               <t:dataTable border="0" cellpadding="5" cellspacing="1" width="960" rowIndexVar="row"
					                   columnClasses="colDtPedProc1,colDtPedProc2,colDtPedProc3,colDtPedProc4,colDtPedProc5,colDtPedProc6,colDtPedProc8" 
					                   headerClass="headerDT1" footerClass="footerDT1" 
					                   rowClasses="rowDtPedProc1,rowDtPedProc2" styleClass="dataTable" id="dataTablePed" var="varlistPedidosTableVO"
					                   binding="#{pedEnPro.dataTablePedidos}" value="#{pedEnPro.ordersList}" rendered="true" preserveDataModel="false" first="0" 
					                   rows="#{pedEnPro.rowCount}" >
					                  
					                   <f:facet name="header">
					                           <h:outputText styleClass="header1" id="otext" value="Pedidos"></h:outputText> 
					                   </f:facet>
					                   
					                   <t:column id="column1"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Pedido" id="dtPeOt1"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="dtPeOt2" value="#{varlistPedidosTableVO.numOrder}"></h:outputText>
					                   </t:column>
					                   
					                   <t:column id="column2">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Recepción" id="dtPeOt3"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="dtPeOt4" value="#{varlistPedidosTableVO.creationDate}"></h:outputText>
					                   </t:column>
					                   
									   <t:column id="column3">
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Envío" id="dtPeOt5"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="dtPeOt6" value="#{varlistPedidosTableVO.entrega}"></h:outputText>
					                   </t:column>

					                   <t:column id="column4"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Cliente" id="dtPeOt7"></h:outputText>
					                       </f:facet>
					                       	<h:outputText styleClass="outputTextDt" id="dtPeOt8" value="#{varlistPedidosTableVO.clientName}" />
					                   </t:column>
					                   
					                   <t:column id="column5"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Atención" id="dtPeOt9"></h:outputText>
					                       </f:facet>
					                       	<h:outputText styleClass="outputTextDt" id="dtPeOt10" value="#{varlistPedidosTableVO.nombreUsuario}" />
					                   </t:column>
	
					                   <t:column id="column6"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Total" id="dtPeOt11"></h:outputText>
					                       </f:facet>
					                       <h:outputText styleClass="outputTextDt" id="dtPeOt12" value="#{varlistPedidosTableVO.total}">
					                       		<f:convertNumber pattern="###,###.##"/>
					                       </h:outputText>
					                       <f:attribute value="30" name="width" />
					                   </t:column>
					                   
					                   <t:column id="column8"> 
					                       <f:facet name="header">
					                           <h:outputText styleClass="label2" value="Ver" id="dtPeOt14"></h:outputText>
					                       </f:facet>
					                       <h:commandLink action="#{pedEnPro.doVerHojaSurtidoAction}" id="visualizar" styleClass="linkNormal" target="container">
					                       		<h:graphicImage height="20" width="20" style="border: none;" url="../../resources/img/detalle.jpg" ></h:graphicImage>
					                       </h:commandLink>
					                   </t:column>
					                   
										
					                   <f:facet name="footer">
											<h:outputText styleClass="header1" id="dtPeOt15" value=""></h:outputText>
					                   </f:facet>

					               </t:dataTable> 
					               
					               <h:panelGrid  columns="1" styleClass="dataTable" columnClasses="scrollerStyle" >
					                    <t:dataScroller id="scroll_1" for="dataTablePed" fastStep="3"
					                        pageCountVar="pageCount"
					                        pageIndexVar="pageIndex"
					                        styleClass="scrollerStyle"
					                        paginator="true"
					                        paginatorMaxPages="9" 
					                        paginatorTableClass="scrollerStyle"  
					                        paginatorActiveColumnStyle="font-weight:bold;"
					                        immediate="true"
					                        actionListener="#{pedEnPro.scrollerAction}">
					
					                    </t:dataScroller>
					               </h:panelGrid>
					              
							</h:panelGroup>
							
						</td>
					</tr>
					
				</table>

				<br>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
	
					<tr height="15">
						<td colspan="6"></td>
					</tr>
					<tr height="20">
						<td colspan="6" align="center">
							 <h:commandButton type="submit" action="#{pedEnPro.doRegresarAction}"
                                   		id="Regresar" value="Regresar" styleClass="botonGris">
                                   </h:commandButton>
						</td>
                          </tr>
					<tr height="15">
						<td colspan="6"></td>
					</tr>
				</table>
				
				
			
			</h:form>

		</body>
	 
	</f:view>
	
</html>