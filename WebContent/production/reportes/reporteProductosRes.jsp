<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@taglib prefix="t" uri="http://myfaces.apache.org/tomahawk" %>


<%@ page language="java" %> 


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="../../resources/img/favicon.ico" type="image/x-icon" />
		<LINK href="../../resources/css/cityexpress-reportes.css" rel="stylesheet" type="text/css">
		
		<title>Reporte por Producto</title>
	</head>
	
	<f:view>
	
		<body>
		
			<h:form>
			
				<table cellpadding="0" cellspacing="0" border="0" align="center" width="400">
					<tr height="15"><td></td></tr>
					<tr>
						<td class="labelTitle1" align="center">Pizarra de Pedidos por Producto </td>
					</tr>
					<tr height="15"><td></td></tr>
				</table>
			
				
				<t:dataTable columnClasses="colDTHdRp1,colDTHdRp2,colDTHdRp3,colDTHdRp4,colDTHdRp5,colDTHdRp6" border="0" 
					rowClasses="rowDTHdRp1" value="#{repProdRes.headerList}" var="hName" align="center" width="950">
					
					<t:column> 
						<h:outputText value="#{hName.header1}" id="oTDt1"></h:outputText>
					</t:column>
					<t:column> 
                           <h:outputText value="#{hName.header2}" id="oTDt2"></h:outputText>
                   </t:column>
                   	<t:column> 
                           <h:outputText value="#{hName.header3}" id="oTDt3"></h:outputText>
                   </t:column>
                   <t:column> 
                           <h:outputText value="#{hName.header4}" id="oTDt4"></h:outputText>
                   </t:column>
                   <t:column> 
                           <h:outputText value="#{hName.header5}" id="oTDt5"></h:outputText>
                   </t:column>
                   <t:column> 
                           <h:outputText value="#{hName.header6}" id="oTDt6"></h:outputText>
                   </t:column>
                   
				</t:dataTable>
				
				
				<t:dataTable columnClasses="columnDTRepPed1,columnDTRepPed2,columnDTRepPed3" border="0" 
					rowClasses="rowClass1,rowClass2" value="#{repProdRes.reporteList}" var="reporte" align="center" width="950">
				
				 	<t:column> 
                      	<h:outputText styleClass="outputDataTable1" id="otext1" value="#{reporte.concepto}" />  
                  	</t:column>
                  	
					<t:column>
						<h:panelGroup>
						<h:panelGrid>
							  
							<t:dataTable columnClasses="columnDTRepPedCli1,columnDTRepPedCli2,columnDTRepPedCli3,columnDTRepPedCli4" 
									value="#{reporte.clientes}" var="clienteCant">
							
								<t:column>
									<h:panelGroup>
										<h:outputText styleClass="outputTextDt" id="otext2" value="#{clienteCant.cliente}" />
									</h:panelGroup>
								</t:column>
								
								<t:column>
									<h:panelGroup>
										<h:outputText styleClass="outputTextDt" id="otext3" value="#{clienteCant.unidad}" />
									</h:panelGroup>
								</t:column>
								
								<t:column>
									<h:panelGroup>
										<h:outputText styleClass="outputTextDt" id="otext4" value="#{clienteCant.total}" />
									</h:panelGroup>
								</t:column>
								
								<t:column>
									<h:panelGroup>
										<h:outputText styleClass="outputTextDt" id="otext5" value="#{clienteCant.pedido}" />
									</h:panelGroup>
								</t:column>
								
							</t:dataTable>
						</h:panelGrid>
						</h:panelGroup>
					</t:column>	
					
					<t:column> 
                      	<h:outputText styleClass="outputDataTable2" id="otext1" value="#{reporte.totalProducto}" />  
                  	</t:column>	
					
				</t:dataTable>
				
				
				<h:panelGrid columns="2" align="center" id="control">
				
					<h:commandButton type="submit" action="#{repProdRes.doRegresarAction}"
							id="regresar" value="Regresar" immediate="true" styleClass="botonGris"/>
							
					<h:commandButton type="submit" action="#{repProdRes.doExportAction}"
							id="exportar" value="Exportar" immediate="true" styleClass="botonGris"/>
				</h:panelGrid>	
				
			
			
			</h:form>
		
		</body>
	
	
	</f:view>
	
</html>